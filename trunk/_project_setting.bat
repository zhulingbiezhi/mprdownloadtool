@echo off  

::请按照项目要求配置好一下参数

::设置项目名称
set ProName=MPRDownloadTool
set PackageName=MPRDownloadTool

::设置项目所在源代码的主目录，精确到Client目录
set ProTrunkDir=%~dp0

::设置项目的client目录位置
set ProClientDir=%ProTrunkDir%client

::设置项目的package目录位置
set ProPackageDir=%ProTrunkDir%\client_package\

::设置32位编译使用的Qt版本
set QT_Version_X86=%qt5.6.0_x86_vs2013%
set QT_CREATOR_BIN_X86="%QT_Version_X86%\..\..\Tools\QtCreator\BIN"
set ReleaseDir_X86=%ProClientDir%\bin\release_x86

::设置64位编译使用的Qt版本
set QT_Version_X64=%qt5.6.0_x64_vs2013%
set QT_CREATOR_BIN_X64="%QT_Version_X64%\..\..\Tools\QtCreator\BIN"
set ReleaseDir_X64=%ProClientDir%\bin\release_x64

::设置开发使用的VS版本
set VS_Version=%VS2013_VCPath%

