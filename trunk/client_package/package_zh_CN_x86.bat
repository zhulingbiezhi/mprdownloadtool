﻿@echo. off

call ..\_project_setting.bat

set PackageDir=%cd%
set ProgramName=%ProName%
set ExeFileName=%ProName%.exe
set PackageScript=package_cn.iss
set PackageSrcDir=program
set PackageOutDir=@output
set InstallPkgName=%PackageName%_Setup_zh_CN.exe

echo.[Package for win32 release (ZH_CN)] ......
cd /d %PackageDir%
echo.Package dir: %PackageDir%
echo.

echo.[Check File] ......
IF NOT EXIST "%ReleaseDir_X86%\%ExeFileName%" (
	echo.Error: "%ReleaseDir_X86%\%ExeFileName%" is not exist.
	exit 1
)

echo.[Clear old data] ......
del /f /a /q %PackageOutDir%\*.*
rmdir /s/q "%PackageSrcDir%"
md "%PackageSrcDir%"
md "%PackageSrcDir%\resource"
echo.

echo.[Copy main_files] ......
copy /y "%ReleaseDir_X86%\%ExeFileName%" "%PackageSrcDir%"
xcopy /s/q "%ReleaseDir_X86%\resource" "%PackageSrcDir%\resource"
copy /y "%ReleaseDir_X86%\config.ini" "%PackageSrcDir%"
copy /y "%ReleaseDir_X86%\qt_zh_CN.qm" "%PackageSrcDir%"
copy /y "%ReleaseDir_X86%\qt_en.qm" "%PackageSrcDir%"
copy /y "%ReleaseDir_X86%\update.exe" "%PackageSrcDir%"
copy /y "%ReleaseDir_X86%\%ProgramName%_zh_CN.qm" "%PackageSrcDir%"
copy /y "%ReleaseDir_X86%\%ProgramName%_en_EN.qm" "%PackageSrcDir%"

copy /y "%ReleaseDir_X86%\%ProgramName%.pdb" "%PackageOutDir%"
echo.

echo.[Copy extend libs] ......
windeployqt -release "%PackageSrcDir%\%ExeFileName%"
copy /y "%VS_Version%\redist\x86\Microsoft.VC120.CRT\msvcr120.dll" "%PackageSrcDir%"
copy /y "%VS_Version%\redist\x86\Microsoft.VC120.CRT\msvcp120.dll" "%PackageSrcDir%"
copy /y "%VS_Version%\redist\x86\Microsoft.VC120.CRT\vccorlib120.dll" "%PackageSrcDir%"
copy /y "%QT_CREATOR_BIN_X86%\libeay32.dll" "%PackageSrcDir%"
copy /y "%QT_CREATOR_BIN_X86%\ssleay32.dll" "%PackageSrcDir%"

echo.

echo.[Package] ......
InnoSetup\ISCC.exe %PackageScript%
echo. 

echo. #############################################################################
echo.
echo.     生成中文版安装包成功!!!!!!
echo.
echo.     安装包路径：package\%PackageOutDir%\%InstallPkgName%
echo.
echo. #############################################################################
echo.
