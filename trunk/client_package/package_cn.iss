#include "package_version.iss"
#define MyAppName "MPRDownloadTool"
#define MyAppPublisher "MPRTimes Company, Inc."
#define MyAppExeName "MPRDownloadTool.exe"
#define PackageSrcDir "program"

[Setup]
; 注: AppId的值为单独标识该应用程序。
; 不要为其他安装程序使用相同的AppId值。
AppId={{EB286E11-EAAD-4611-A37B-16C926F8FA48}
AppName={#MyAppName}
AppVersion={#CurrentVersion}
;AppVerName={#MyAppName} {#CurrentVersion}
AppPublisher={#MyAppPublisher}
;AppPublisherURL={#MyAppURL}
;AppSupportURL={#MyAppURL}
;AppUpdatesURL={#MyAppURL}
DefaultDirName={pf}\MPRDownloadTool
DefaultGroupName={#MyAppName}
AllowNoIcons=yes
OutputDir=@output
OutputBaseFilename=MPRDownloadTool_Setup_zh_CN
LicenseFile=@outputDep\License.txt
;InfoBeforeFile=@outputDep\Declaration.txt
;InfoAfterFile=@outputDep\Readme.txt
UninstallDisplayIcon={app}\{#MyAppName}
SetupIconFile=@outputDep\setup.ico
;WizardImageFile=@outputDep\WizardImage.bmp
;WizardSmallImageFile=@outputDep\WizardThumbImage.bmp
;AppCopyright=Copyright (C) 2001-2014 MPRTimes Company, Inc.
Compression=lzma
SolidCompression=yes
;VersionInfoVersion=1.0.0.3
;VersionInfoCompany=MPRTimes Software
;VersionInfoDescription=trial
;VersionInfoTextVersion=1, 0, 0, 3

[Languages]
;Name: "english"; MessagesFile: "compiler:Default.isl"
;Name: "chinesetrad"; MessagesFile: "compiler:Languages\ChineseTrad.isl"
Name: "chinesesimp"; MessagesFile: "compiler:Languages\chinesesimp.isl"

[Files]
Source: "{#PackageSrcDir}\*"; DestDir: "{app}"; Flags: recursesubdirs
;Source: compiler:ISSkin.dll; DestDir: {app}; Flags: dontcopy
;Source: compiler:IsSkins\No\*.cjstyles; DestDir: {tmp}; Flags: dontcopy

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"
Name: "quicklaunchicon"; Description: "{cm:CreateQuickLaunchIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked

[Icons]
Name: "{group}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"
;Name: "{group}\{cm:ProgramOnTheWeb,{#MyAppName}}"; Filename: "{#MyAppURL}"
Name: "{group}\{cm:UninstallProgram,{#MyAppName}}"; Filename: "{uninstallexe}"
Name: "{commondesktop}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; Tasks: desktopicon
Name: "{userappdata}\Microsoft\Internet Explorer\Quick Launch\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; Tasks: quicklaunchicon

[Run]
Filename: "{app}\{#MyAppExeName}"; Description: "{cm:LaunchProgram,{#StringChange(MyAppName, '&', '&&')}}"; Flags: nowait postinstall skipifsilent

[CustomMessages]
AdditionalIcons=附加快捷方式:
CreateDesktopIcon=创建桌面快捷方式
CreateQuickLaunchIcon=创建快速运行栏快捷方式
CreateUserTaskIcon=创建任务栏快捷方式
UninstallProgram=卸载 %1
LaunchProgram=运行 %1

[UninstallDelete]
Type: filesandordirs; Name: "{app}"

[Registry]
Root: HKCU; Subkey: "Software\MPRTimes"
Root: HKCU; Subkey: "Software\MPRTimes\MPRDownloadTool"; Flags: uninsdeletekeyifempty
Root: HKCU; SubKey: "SOFTWARE\MPRTimes\MPRDownloadTool"; ValueType: dword; ValueName: IsPresent; ValueData: 10 ; Flags: uninsdeletevalue
Root: HKCU; SubKey: "SOFTWARE\MPRTimes\MPRDownloadTool"; ValueType: string; ValueName: UninsExePath; ValueData: "{uninstallexe}" ; Flags: uninsdeletevalue
	
[Code]
function InitializeSetup(): Boolean;
var
  No: Integer;
  isbl: boolean;
  StylePath: AnsiString;
  UninsExePath: String;
  ResultCode: Integer;
  hProcess: Integer;
  nProcessId: longword;
  hWnd: Longint;
begin
  isbl := True;
  if RegValueExists(HKEY_CURRENT_USER, 'SOFTWARE\MPRTimes\MPRDownloadTool', 'IsPresent') then
  begin
    isbl := False;
    if MsgBox('安装程序检测到您已经安装了本客户端，继续安装前需要先卸载，是否继续?', mbConfirmation, MB_YESNO) = IDYES then
    begin
      if RegQueryStringValue(HKEY_CURRENT_USER, 'SOFTWARE\MPRTimes\MPRDownloadTool', 'UninsExePath', UninsExePath) then 
         hWnd := FindWindowByWindowName('MPRDownloadTool');
         while hWnd <> 0 do
         begin
           MsgBox('安装程序检测到MPRDownloadTool进程正在运行，请先手动关闭之!', mbConfirmation, MB_OK);           
           hWnd := FindWindowByWindowName('MPRDownloadTool');
           exit;
         end;
         if Exec(UninsExePath, '', '', SW_SHOW, ewWaitUntilTerminated, ResultCode) then
            if ResultCode = 0 then
               isbl := True;
    end;
  end;
  
  Result := isbl;
end;

procedure InitializeWizard();
var
  F: AnsiString;
begin
  WizardForm.WelcomeLabel1.Font.Color := clNavy;
  WizardForm.WelcomeLabel2.Font.Color := clTeal;
  WizardForm.PageNameLabel.Font.Color := clNavy;
  WizardForm.PageDescriptionLabel.Font.Color := clTeal;
  
  F:= ExpandConstant('{tmp}\WizardImage.bmp');
  WizardForm.WizardBitmapImage.Bitmap.SaveToFile(F);

  DeleteFile(F);
end;

var
  ErrorCode: Integer;

procedure CurStepChanged(CurStep: TSetupStep);
begin
  if CurStep=ssDone then
    ShellExec('taskbarpin', ExpandConstant('{userappdata}\Microsoft\Internet Explorer\Quick Launch\{#MyAppName}.lnk'), '','', SW_SHOWNORMAL, ewNoWait,ErrorCode);
end;

procedure DeinitializeUninstall();
begin
  ShellExec('taskbarunpin', ExpandConstant('{userappdata}\Microsoft\Internet Explorer\Quick Launch\{#MyAppName}.lnk'), '', '', SW_SHOWNORMAL, ewNoWait,ErrorCode);
end;
