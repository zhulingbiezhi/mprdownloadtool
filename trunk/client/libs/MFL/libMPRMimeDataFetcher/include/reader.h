#ifndef _READER_H_
#define _READER_H_

#include <stdio.h>
#include <wchar.h>

/*
*���ܷ���1�� ʧ�ܷ���0
*/
int   readFileData(void* fp, unsigned int offset, unsigned int len, unsigned char* pBuffer);
void* openFile(const char * filename, const char * mode);
void* openFileW(const wchar_t * filename, const wchar_t * mode);
void  closeFile(void* fp);
#endif