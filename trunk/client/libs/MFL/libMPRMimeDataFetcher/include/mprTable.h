#ifndef _MPRTABLE_H_
#define _MPRTABLE_H_

//获取成员相对结构的偏移量
#define TOM(TYPE, MEMBER) ((unsigned int)&((TYPE*)0)->MEMBER)

//获取结构中成员的大小
#define TOMS(TYPE, MEMBER) ((unsigned int)sizeof(((TYPE*)0)->MEMBER))
#pragma pack(push)

#pragma pack(1)
typedef struct
{
    char                    tag[4];
    unsigned char           version;
    unsigned long long      prefixNo;
    unsigned char           mediaFormat;
    unsigned char           indexType;
    unsigned short          title[256];
    unsigned short          publishing[256];
    unsigned char           GUID[16];
    unsigned int            indexOffset;
    unsigned int            timeSoundOffset;
    unsigned int            commonGameOffset;
    unsigned char           encryptType;
    unsigned int            extendHeadLen;
}MPRFileHead;

typedef enum
{
    MPRFILE_AES_ECB = 0x0001,     //AES ECB算法
    MPRFILE_AES_CBC = 0x0002,     //AES CBC算法
    MPRFILE_AES_CTR = 0x0004,     //AES CTR算法
    MPRFILE_SM4_ECB = 0x0100,     //国密4 ECB算法
    MPRFILE_SM4_CBC = 0x0200,     //国密4 CBC算法
    MPRFILE_SM4_CTR = 0x0400      //国密4 CTR算法
}MPRFileEncryptType_E;

#define ExtendInfoHeadMaxSize 1312

typedef struct
{
    MPR_DataRef             coverImgRef;
    MPR_DataRef             prevFlashRef;
    unsigned short          mprVer;
    unsigned char           integrityFlag;
    unsigned char           linkerProcessedFlag;
    unsigned short          mediaEncryptType;
    unsigned char           licenseData[1024];
    unsigned short          codeCount;
    unsigned  int           pageImgAndHotlinkHeadOffset;
}ExtendInfoHead;

#pragma pack(pop)

#endif