#ifndef _MPR_MIMEDATA_FETCHER_H_
#define _MPR_MIMEDATA_FETCHER_H_

#include <wchar.h>
#include "MPRDeviceEnum.h"
#include "MFLErrorCode.h"

#ifdef __cplusplus
extern "C"
{
#endif

/*
*点结构，左上角为坐标原点，向右为x轴正方向，向下为y轴正方向
*/
typedef struct MPR_Point
{
    double x;  
    double y;
}MPR_Point;

/*
*描述封闭曲线路径的点结构，参考贝塞尔曲线
*/
typedef struct MPR_PathPointListNod
{
    MPR_Point leftDirPoint;             //左控制点
    MPR_Point rightDirPoint;            //右控制点
    MPR_Point anchorPoint;              //定位点
    struct MPR_PathPointListNod* pNext;
}MPR_PathPointListNod;

/*
*路径节点结构
*/
typedef struct MPR_PathListNod
{
    MPR_PathPointListNod* pPointList;
    struct MPR_PathListNod* pNext;
}MPR_PathListNod;

/*
*热区节点结构
*/
typedef struct MPR_HotlinkListNod
{
    unsigned int suffix;               //热区所对应的MPR码后置码
    unsigned short pageNo;              //所在页面号
    MPR_PathListNod* pPathList;         //热区路径
    struct MPR_HotlinkListNod* pNext;
}MPR_HotlinkListNod;

/*
*数据引用结构
*/
typedef struct MPR_DataRef
{
    unsigned int offset;               //数据在MPR文件中的偏移量
    unsigned int dataLen;              //数据长度
}MPR_DataRef;

/*
*MPR头信息结构
*/
typedef struct _MPR_StdHeadInfo
{
    unsigned short  bookName[256];
    unsigned short  publishing[256];
    unsigned long long  prefixNo;
    unsigned short  langVersion;
    unsigned char   guid[16];
    MPR_DataRef     coverImgRef;
    int             linkProcFlag;
}MPR_StdHeadInfo;


/*
* 获取MPR文件标准头信息
* [IN] mprFilePath：MPR文件路径
* [IN][OUT] info  ：MPR文件标准头信息
* [OUT] 成功返回 >= 0
*/


int getMPRStdHeadInfoW(const wchar_t* mprFilePath, MPR_StdHeadInfo* info);
int getMPRStdHeadInfoA(const char *mprFilePath, MPR_StdHeadInfo* info);

#define getMPRStdHeadInfo getMPRStdHeadInfoW
/*
* 获取MPR前置码
* [IN] mprFilePath：MPR文件路径
* [OUT] 成功返回 0
*/
unsigned long long getMPRPrefixNo(const char* mprFilePath);
unsigned long long getMPRPrefixNoW(const wchar_t* mprFilePath);

/*
* 获取MPR语言版本
* [IN] mprFilePath：                     MPR文件路径
* [IN][OUT] pMprVer ：                   MPR版本号（1-65535），普通版本为1-35，DIY版本为101以上
* [OUT] 成功返回 0
*/
int getMPRLanguageVersion(const char* mprFilePath, unsigned short* pMprVer);
int getMPRLanguageVersionW(const wchar_t* mprFilePath, unsigned short* pMprVer);

/*
* 获取MPR版本名称
* [IN] mprFilePath：                     MPR文件路径
* [IN][OUT] VersionName ：               MPR版本名称
* [OUT] 成功返回 0
*/
int getMPRLanguageVersionName(const char* mprFilePath, unsigned short VersionName[256]);
int getMPRLanguageVersionNameW(const wchar_t* mprFilePath, unsigned short VersionName[256]);

/*
* 获取MPR版本名称
* [IN] mprFilePath：                     MPR文件路径
* [IN][OUT] Description ：               MPR版本描述
* [OUT] 成功返回 0
*/
int getMPRLanguageVersionDescription(const char* mprFilePath, unsigned short VersionDescription[256]);
int getMPRLanguageVersionDescriptionW(const wchar_t* mprFilePath, unsigned short VersionDescription[256]);

/*
* 获取MPR书名
* [IN] mprFilePath：                     MPR文件路径
* [IN][OUT] bookName ：                  书名，255个字符，unicode 4.0字符集
* [OUT] 成功返回 0
*/
int getMPRBookName(const char* mprFilePath, unsigned short bookName[256]);
int getMPRBookNameW(const wchar_t* mprFilePath, unsigned short bookName[256]);

/*
* 获取MPR出版社
* [IN] mprFilePath：                     MPR文件路径
* [IN][OUT] publishing ：                出版社，255个字符，unicode 4.0字符集
* [OUT] 成功返回 0
*/
int getMPRPublishing(const char* mprFilePath, unsigned short publishing[256]);
int getMPRPublishingW(const wchar_t* mprFilePath, unsigned short publishing[256]);

/*
* 获取MPR封面图片数据引用
* [IN] mprFilePath：                     MPR文件路径
* [IN][OUT] pImgRef ：                   MPR_DataRef结构指针
* [OUT] 成功返回 0
*/
int getMPRCoverImgRef(const char* mprFilePath, MPR_DataRef* pImgRef);
int getMPRCoverImgRefW(const wchar_t* mprFilePath, MPR_DataRef* pImgRef);

/*
* 获取MPR预览动画数据引用  
* [IN] mprFilePath：                     MPR文件路径
* [IN][OUT] pImgRef ：                   MPR_DataRef结构指针
* [OUT] 成功返回 0
*/
int getMPRPrevFlashRef(const char* mprFilePath, MPR_DataRef* pPrevFlashRef);
int getMPRPrevFlashRefW(const wchar_t* mprFilePath, MPR_DataRef* pPrevFlashRef);

/*
* 获取MPR License数据引用
* [IN] mprFilePath：                     MPR文件路径
* [IN][OUT] pLicenseData：               license数据，实际长度不超过1024字节
* [OUT] 成功返回license数据字节数，失败返回负值
*/
int getMPRLicenseData(const char* mprFilePath, unsigned char pLicenseData[1024]);
int getMPRLicenseDataW(const wchar_t* mprFilePath, unsigned char pLicenseData[1024]);

/*
* 获取MPR 页面图片数量
* [IN] mprFilePath：                     MPR文件路径
* [OUT] 返回页面图片数量，失败返回负值
*/
int  getMPRPageImgCount(const char* mprFilePath);
int  getMPRPageImgCountW(const wchar_t* mprFilePath);

/*
* 获取MPR 指定页号的页面图片数据引用
* [IN] mprFilePath：                     MPR文件路径
* [IN] pageNo：                          页号
* [IN][OUT] pImgRef：                    
* [OUT] 成功返回 0
*/
int getMPRNthPageImgRef(const char* mprFilePath, int pageNo, MPR_DataRef* pImgRef);
int getMPRNthPageImgRefW(const wchar_t* mprFilePath, int pageNo, MPR_DataRef* pImgRef);

/*
* 获取MPR 所有页面图片数据引用
* [IN] mprFilePath：                     MPR文件路径
* [IN][OUT] imgRefs：      
* [OUT] 成功返回取得的图片引用数量，失败返回负值
*/
int getMPRAllPageImgRef(const char* mprFilePath, MPR_DataRef imgRefs[1000]);
int getMPRAllPageImgRefW(const wchar_t* mprFilePath, MPR_DataRef imgRefs[1000]);

/*
* 获取MPR信息, 函数内为hotlinkListHead及后面节点分配内存，使用完后需要在外面释放
* [IN] mprfile：                         MPR文件路径
* [IN][OUT] hotlinkListHead：            热区列表头节点二级指针，若无则头节点中各数据成员值为0
* [OUT] 成功返回热区数量,失败负值
*/
int getMPRHotlinks(const char* mprFilePath, MPR_HotlinkListNod** hotlinkListHead);
int getMPRHotlinksW(const wchar_t* mprFilePath, MPR_HotlinkListNod** hotlinkListHead);

/*
* 获取媒体加密类型
* [IN] mprfile：                         MPR文件路径
* [IN][OUT] encryptType
* 成功返回0
*/
int getMediaEncryptType(const char* mprFilePath, EncryptAlgorithm* encryptType);
int getMediaEncryptTypeW(const wchar_t* mprFilePath, EncryptAlgorithm* encryptType);

/*
* 获取关联工具制作标识
* [IN] mprfile：                         MPR文件路径
* [OUT]                                  被关联工具制作过，返回1； 否则返回0
*/
int mprlinkerProcessedFlag(const char* mprFilePath);
int mprlinkerProcessedFlagW(const wchar_t* mprFilePath);

/*
* 获取MPR文件用码总数
* [IN] mprfile：                         MPR文件路径
* [OUT]                                  返回用码总数
*/
int usedCodeCount(const char* mprFilePath);
int usedCodeCountW(const wchar_t* mprFilePath);

/*
* 获取GUID
* [IN] mprFilePath：                     MPR文件路径
* [IN][OUT] guid ：                      
* [OUT] 成功返回 0
*/
int getMPRGUID(const char* mprFilePath, unsigned char guid[16]);
int getMPRGUIDW(const wchar_t* mprFilePath, unsigned char guid[16]);

#ifdef __cplusplus
}
#endif


#endif