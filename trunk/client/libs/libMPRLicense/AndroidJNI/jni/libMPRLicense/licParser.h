#ifndef __LICPARSER_H__
#define __LICPARSER_H__
#include "../Common/include/MPRDeviceEnum.h"

#ifdef __cplusplus
extern "C" {
#endif

/*
说明:license结构定义
*/
typedef struct
{
    unsigned char           Version;           //License版本，本版本为0x01,第一版可以通过检测本字节为0x00识别
    PKI_Algorithm           AsymType;          //非对称加密类型:
                                               //0x01: RSA
                                               //0x02: SM2
    EncryptAlgorithm        SymType;           //非对称加密类型
                                               //0x0001: AES_ECB
                                               //0x0002: AES_CBC
                                               //0x0004: AES_CTR
                                               //0x0100: SM4_ECB
                                               //0x0200: SM4_CBC
                                               //0x0400: SM4_CTR
    unsigned char           GUID[16];          //license GUID, 与MPR文件头中的GUID匹配
    unsigned short          KeyParaLength;     //KeyPara长度
    unsigned char           KeyPara[256];      //KeyPara字节流。正确解密后得到32个字节的原文。原文前16个字节为对称加密算法密钥（Key），后16个字节是IV（初始向量，仅在CBC模式下有效）
    unsigned char           ExpireDate[8];     //失效日期，格式YYYYMMDD
    unsigned char           IssuedType;        //授权类型, 下面几种授权类型一种或几种或运算结果
                                               // 0x01: 授权给全体设备
                                               // 0x02: 授权给单个设备
                                               // 0x04: 授权给设备品牌
                                               // 0x08: 授权给用户
                                               // 0x10: 授权给网络平台
    unsigned char           IssuedUser[128];   //授权用户, '\0'结尾字符串
    unsigned char           IssuedFrom[20];    //License发布者，可以是网络平台或MPR设备
    unsigned char           IssuedTo[20];      //License被授权设备，可以是网络平台或MPR设备
    unsigned short          SignatureLength;   //签名数据长度
    unsigned char           SignatureData[256];//签名数据
}MPRLicense_s;

/*
功能：打包license文件
返回值：
        0：               成功
        BAD_PARAM:        错误的参数
*/
int MPRLicense_Pack(const MPRLicense_s*  p_license_s,  //[IN]  传入license结构
                    unsigned char*       p_license,    //[OUT] 打包后的license首地址, 建议buf长度512
                    int*                 p_len         //[IN/OUT] 传入license buff长度, 传出实际打包后license长度            
                    );                  

/*
功能：打包license文件
返回值：
        0：               成功
        BAD_PARAM:        错误的参数
*/
int MPRLicense_UnPack(const unsigned char* p_license,    //[IN] 打包的license首地址
                      const int            len,          //[IN] 打包的license长度           
                      MPRLicense_s*        p_license_s   //[OUT]license结构
                      );                  

#ifdef __cplusplus
}
#endif

#endif
