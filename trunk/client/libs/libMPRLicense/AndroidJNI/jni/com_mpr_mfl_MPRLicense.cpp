#include <jni.h>
#include "com_mpr_mfl_MPRLicense.h"
#include "libMPRLicense/licParser.h"
#include <stdlib.h>

#define MAX_LICENSE_LENGTH 1024

/*
 * Class:     com_mpr_mfl_MPRLicense
 * Method:    nativePack
 * Signature: (III[BI[B[BILjava/lang/String;[B[BI[B)[B
 */
JNIEXPORT jbyteArray JNICALL Java_com_mpr_mfl_MPRLicense_nativePack
  (JNIEnv *env, jobject obj, jint version, jint asymType, jint symType, jbyteArray guid, jint keyParaLength, jbyteArray keyPara, jbyteArray expireDate, jint issuedType, jbyteArray issuedUser, jbyteArray issuedFrom, jbyteArray issuedTo, jint signatureLength, jbyteArray signatureData) {

	unsigned char *guid_ = (unsigned char *) env->GetByteArrayElements(guid, 0);
	unsigned char *keyPara_ = (unsigned char *) env->GetByteArrayElements(keyPara, 0);
	unsigned char *expireDate_ = (unsigned char *) env->GetByteArrayElements(expireDate, 0);
	unsigned char *issuedUser_ = (unsigned char *) env->GetByteArrayElements(issuedUser, 0);
	unsigned char *issuedFrom_ = (unsigned char *) env->GetByteArrayElements(issuedFrom, 0);
	unsigned char *issuedTo_ = (unsigned char *) env->GetByteArrayElements(issuedTo, 0);
	unsigned char *signatureData_ = (unsigned char *) env->GetByteArrayElements(signatureData, 0);

	MPRLicense_s lic = {
		(unsigned char) version,
		(PKI_Algorithm) asymType,
		(EncryptAlgorithm) symType,
		{0},
		(unsigned short) keyParaLength,
		{0},
		{0},
		(unsigned char) issuedType,
		{0},
		{0},
		{0},
		(unsigned short) signatureLength,
		{0}
	};

	memset(lic.IssuedUser, 0x00, sizeof(lic.IssuedUser));
	memcpy(lic.GUID, guid_, sizeof(lic.GUID));
	memcpy(lic.KeyPara, keyPara_, sizeof(lic.KeyPara));
	memcpy(lic.ExpireDate, expireDate_, sizeof(lic.ExpireDate));
	memcpy(lic.IssuedUser, issuedUser_, sizeof(lic.IssuedUser));
	memcpy(lic.IssuedFrom, issuedFrom_, sizeof(lic.IssuedFrom));
	memcpy(lic.IssuedTo, issuedTo_, sizeof(lic.IssuedTo));
	memcpy(lic.SignatureData, signatureData_, sizeof(lic.SignatureData));

	unsigned char licbuf[MAX_LICENSE_LENGTH];
	int liclen = MAX_LICENSE_LENGTH;

	int ret = MPRLicense_Pack(&lic, licbuf, &liclen);

	if (guid_) env->ReleaseByteArrayElements(guid, (jbyte*) guid_, 0);
	if (keyPara_) env->ReleaseByteArrayElements(keyPara, (jbyte*) keyPara_, 0);
	if (expireDate_) env->ReleaseByteArrayElements(expireDate, (jbyte*) expireDate_, 0);
	if (issuedUser_) env->ReleaseByteArrayElements(issuedUser, (jbyte*) issuedUser_, 0);
	if (issuedFrom_) env->ReleaseByteArrayElements(issuedFrom, (jbyte*) issuedFrom_, 0);
	if (issuedTo_) env->ReleaseByteArrayElements(issuedTo, (jbyte*) issuedTo_, 0);
	if (signatureData_) env->ReleaseByteArrayElements(signatureData, (jbyte*) signatureData_, 0);

	jbyteArray ret_ba;
	if (ret == 0) {
		ret_ba = env->NewByteArray(liclen);
		env->SetByteArrayRegion(ret_ba, 0, liclen, (jbyte*) licbuf);
		return ret_ba;
	} else {
		ret_ba = env->NewByteArray(0);
	}

	return ret_ba;
}

/*
 * Class:     com_mpr_mfl_MPRLicense
 * Method:    nativeUnPack
 * Signature: ([B)I
 */
JNIEXPORT jint JNICALL Java_com_mpr_mfl_MPRLicense_nativeUnPack
  (JNIEnv *env, jobject obj, jbyteArray buf) {

	MPRLicense_s lic;
	unsigned char *buf_ = (unsigned char*) env->GetByteArrayElements(buf, 0);
	int size = env->GetArrayLength(buf);

	int ret = MPRLicense_UnPack(buf_, size, &lic);

	if (ret == 0) {
		jclass cls = env->GetObjectClass(obj);
		jfieldID version = env->GetFieldID(cls, "version", "I");
		jfieldID asymType = env->GetFieldID(cls, "asymType", "I");
		jfieldID symType = env->GetFieldID(cls, "symType", "I");
		jfieldID guid = env->GetFieldID(cls, "guid", "[B");
		jfieldID keyParaLength = env->GetFieldID(cls, "keyParaLength", "I");
		jfieldID keyPara = env->GetFieldID(cls, "keyPara", "[B");
		jfieldID expireDate = env->GetFieldID(cls, "expireDate", "[B");
		jfieldID issuedType = env->GetFieldID(cls, "issuedType", "I");
		jfieldID issuedUser = env->GetFieldID(cls, "issuedUser", "[B");
		jfieldID issuedFrom = env->GetFieldID(cls, "issuedFrom", "[B");
		jfieldID issuedTo = env->GetFieldID(cls, "issuedTo", "[B");
		jfieldID signatureLength = env->GetFieldID(cls, "signatureLength", "I");
		jfieldID signatureData = env->GetFieldID(cls, "signatureData", "[B");

		jbyteArray guid_ = env->NewByteArray(16); env->SetByteArrayRegion(guid_, 0, 16, (jbyte*) lic.GUID);
		jbyteArray keyPara_ = env->NewByteArray(lic.KeyParaLength); env->SetByteArrayRegion(keyPara_, 0, lic.KeyParaLength, (jbyte*) lic.KeyPara);
		jbyteArray expireDate_ = env->NewByteArray(8); env->SetByteArrayRegion(expireDate_, 0, 8, (jbyte*) lic.ExpireDate);
		jbyteArray issuedUser_ = env->NewByteArray(128); env->SetByteArrayRegion(issuedUser_, 0, 128, (jbyte*) lic.IssuedUser);
		jbyteArray issuedFrom_ = env->NewByteArray(20); env->SetByteArrayRegion(issuedFrom_, 0, 20, (jbyte*) lic.IssuedFrom);
		jbyteArray issuedTo_ = env->NewByteArray(20); env->SetByteArrayRegion(issuedTo_, 0, 20, (jbyte*) lic.IssuedTo);
		jbyteArray signatureData_ = env->NewByteArray(lic.SignatureLength); env->SetByteArrayRegion(signatureData_, 0, lic.SignatureLength, (jbyte*) lic.SignatureData);

		env->SetIntField(obj, version, lic.Version);
		env->SetIntField(obj, asymType, lic.AsymType);
		env->SetIntField(obj, symType, lic.SymType);
		env->SetIntField(obj, keyParaLength, lic.KeyParaLength);
		env->SetIntField(obj, issuedType, lic.IssuedType);
		env->SetIntField(obj, signatureLength, lic.SignatureLength);
		env->SetObjectField(obj, guid, guid_);
		env->SetObjectField(obj, keyPara, keyPara_);
		env->SetObjectField(obj, expireDate, expireDate_);
		env->SetObjectField(obj, issuedUser, issuedUser_);
		env->SetObjectField(obj, issuedFrom, issuedFrom_);
		env->SetObjectField(obj, issuedTo, issuedTo_);
		env->SetObjectField(obj, signatureData, signatureData_);
	}

	if (buf_) env->ReleaseByteArrayElements(buf, (jbyte*) buf_, 0);
	return ret;
}

