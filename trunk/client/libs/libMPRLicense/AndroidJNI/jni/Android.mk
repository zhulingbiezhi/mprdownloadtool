LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE := prebuilt_lib
LOCAL_SRC_FILES := libMPRLicense/libMPRLicense.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE    := libMPRLicenseJNI
LOCAL_CFLAGS    := -Werror
LOCAL_SRC_FILES := com_mpr_mfl_MPRLicense.cpp
LOCAL_LDLIBS    := -llog 
LOCAL_STATIC_LIBRARIES := prebuilt_lib
include $(BUILD_SHARED_LIBRARY)