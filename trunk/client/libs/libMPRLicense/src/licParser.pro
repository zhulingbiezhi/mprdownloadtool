QT       -= core gui

TARGET = licParser
TEMPLATE = lib
CONFIG  += static
CONFIG  += debug_and_release

DEFINES += _CRT_SECURE_NO_WARNINGS

win32 {
QMAKE_LFLAGS += -MACHINE:X86
DESTDIR = ../lib/win32
}

linux-g++-32 {
QMAKE_CFLAGS += -std=c99
DESTDIR = ../lib/x86-linux
}

linux-g++-64 {
QMAKE_CFLAGS += -std=c99
DESTDIR = ../lib/x86_64-linux
}


CONFIG(debug, debug|release) {
DEFINES += DEBUG
win32{
}
!win32{
LIBS    +=
}
}

CONFIG(release, debug|release) {
win32{
}
!win32{
LIBS    += 
}
}


INCLUDEPATH += ./

RC_FILE = 

SOURCES += licParser.c

HEADERS += ../include/licParser.h
           
					 

