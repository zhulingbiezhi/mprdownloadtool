<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>QObject</name>
    <message>
        <location filename="src/model/dumpcore/TDumpCore.cpp" line="51"/>
        <source>Program an exception occurs, exception information has been saved to the desktop, FileName:</source>
        <translation>Software exception, the exception information has been saved on the desktop!</translation>
    </message>
    <message>
        <location filename="src/model/dumpcore/TDumpCore.cpp" line="52"/>
        <source>Progress Error</source>
        <translation>Software exception</translation>
    </message>
    <message>
        <location filename="src/device/TDeviceEject.cpp" line="157"/>
        <source>Media in Drive %c has been ejected safely.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/device/TDeviceEject.cpp" line="163"/>
        <source>Media in Drive %c can be safely removed.
</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TBaseMessageBox</name>
    <message>
        <location filename="src/view/main/TBaseMessageBox.cpp" line="42"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="src/view/main/TBaseMessageBox.cpp" line="43"/>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
</context>
<context>
    <name>TDeviceInfoWidget</name>
    <message>
        <location filename="src/view/main/TDeviceInfoWidget.cpp" line="20"/>
        <source>Device Type:</source>
        <translation>Model of device:</translation>
    </message>
    <message>
        <location filename="src/view/main/TDeviceInfoWidget.cpp" line="24"/>
        <source>Device Id:</source>
        <translation>Device ID:</translation>
    </message>
    <message>
        <location filename="src/view/main/TDeviceInfoWidget.cpp" line="29"/>
        <source>Capacity:</source>
        <translation>Capacity:</translation>
    </message>
    <message>
        <location filename="src/view/main/TDeviceInfoWidget.cpp" line="39"/>
        <location filename="src/view/main/TDeviceInfoWidget.cpp" line="161"/>
        <source>Used:%1,Total:%2</source>
        <translation>Used: %1, tatal: %2</translation>
    </message>
    <message>
        <location filename="src/view/main/TDeviceInfoWidget.cpp" line="88"/>
        <source>Used:%1%2,Total:%3%4</source>
        <translation>Used: %1%2, tatal: %3%4</translation>
    </message>
</context>
<context>
    <name>TDeviceTabWidget</name>
    <message>
        <location filename="src/view/main/TDeviceTabWidget.cpp" line="73"/>
        <source>  </source>
        <translation> </translation>
    </message>
    <message>
        <location filename="src/view/main/TDeviceTabWidget.cpp" line="73"/>
        <source>Book Name</source>
        <translation>Book name</translation>
    </message>
    <message>
        <location filename="src/view/main/TDeviceTabWidget.cpp" line="73"/>
        <source>Publisher Name</source>
        <translation>Publisher</translation>
    </message>
    <message>
        <location filename="src/view/main/TDeviceTabWidget.cpp" line="73"/>
        <source>Format</source>
        <translation>Format</translation>
    </message>
    <message>
        <location filename="src/view/main/TDeviceTabWidget.cpp" line="73"/>
        <source>Download Time</source>
        <translation>Download time</translation>
    </message>
    <message>
        <location filename="src/view/main/TDeviceTabWidget.cpp" line="73"/>
        <source>File Size</source>
        <translation>File size</translation>
    </message>
    <message>
        <location filename="src/view/main/TDeviceTabWidget.cpp" line="73"/>
        <source>Operate</source>
        <translation>Operate</translation>
    </message>
    <message>
        <location filename="src/view/main/TDeviceTabWidget.cpp" line="187"/>
        <source>delete mpr files</source>
        <translation>Delete MPR file(s)</translation>
    </message>
    <message>
        <location filename="src/view/main/TDeviceTabWidget.cpp" line="187"/>
        <source>Are you sure to delete this MPF file?</source>
        <translation>Are you sure to delete the MPR file(s)?</translation>
    </message>
</context>
<context>
    <name>THeadWidget</name>
    <message>
        <location filename="src/view/main/THeadWidget.cpp" line="23"/>
        <source>Setting</source>
        <translation>Setting</translation>
    </message>
    <message>
        <location filename="src/view/main/THeadWidget.cpp" line="27"/>
        <source>Version Info</source>
        <translation>Version</translation>
    </message>
    <message>
        <location filename="src/view/main/THeadWidget.cpp" line="32"/>
        <source>minimumSize</source>
        <translation>Minimize</translation>
    </message>
    <message>
        <location filename="src/view/main/THeadWidget.cpp" line="36"/>
        <source>close</source>
        <translation>Close</translation>
    </message>
</context>
<context>
    <name>TLeftTabWidget</name>
    <message>
        <location filename="src/view/main/TLeftTabWidget.cpp" line="20"/>
        <source>My Device</source>
        <translation>My device</translation>
    </message>
    <message>
        <location filename="src/view/main/TLeftTabWidget.cpp" line="23"/>
        <source>My Task</source>
        <translation>Download list</translation>
    </message>
</context>
<context>
    <name>TNetMPRSearcher</name>
    <message>
        <location filename="src/model/network/TNetMPRSearcher.cpp" line="186"/>
        <location filename="src/model/network/TNetMPRSearcher.cpp" line="193"/>
        <source>download data error!</source>
        <translation>Download failed!</translation>
    </message>
</context>
<context>
    <name>TPageNaviBar</name>
    <message>
        <location filename="src/view/main/TPageNaviBar.cpp" line="28"/>
        <source>Previous Page</source>
        <translation>Previous</translation>
    </message>
    <message>
        <location filename="src/view/main/TPageNaviBar.cpp" line="74"/>
        <source>Next Page</source>
        <translation>Next</translation>
    </message>
    <message>
        <location filename="src/view/main/TPageNaviBar.cpp" line="94"/>
        <source>Page</source>
        <translation>Page</translation>
    </message>
    <message>
        <location filename="src/view/main/TPageNaviBar.cpp" line="334"/>
        <source>Total %1 pages / Total %2 records, To</source>
        <translation>Total %1 page / Total %2 records, jump to</translation>
    </message>
</context>
<context>
    <name>TPopupDialog</name>
    <message>
        <location filename="src/view/main/TPopupDialog.cpp" line="43"/>
        <source>Close</source>
        <translation>Close</translation>
    </message>
    <message>
        <location filename="src/view/main/TPopupDialog.cpp" line="97"/>
        <source>Ok</source>
        <translation>OK!</translation>
    </message>
    <message>
        <location filename="src/view/main/TPopupDialog.cpp" line="104"/>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="src/view/main/TPopupDialog.cpp" line="110"/>
        <source>Yes</source>
        <translation>Yes</translation>
    </message>
    <message>
        <location filename="src/view/main/TPopupDialog.cpp" line="117"/>
        <source>No</source>
        <translation>No</translation>
    </message>
</context>
<context>
    <name>TSystemSettingDlg</name>
    <message>
        <location filename="src/view/main/TSystemSettingDlg.cpp" line="53"/>
        <source>select save directory</source>
        <oldsource>select import directory</oldsource>
        <translation>Cache path</translation>
    </message>
    <message>
        <location filename="src/view/main/TSystemSettingDlg.cpp" line="72"/>
        <source>setting</source>
        <translation>Setting</translation>
    </message>
    <message>
        <location filename="src/view/main/TSystemSettingDlg.cpp" line="80"/>
        <source>Close</source>
        <translation>Close</translation>
    </message>
    <message>
        <location filename="src/view/main/TSystemSettingDlg.cpp" line="99"/>
        <source>browse</source>
        <translation>Browse</translation>
    </message>
    <message>
        <location filename="src/view/main/TSystemSettingDlg.cpp" line="100"/>
        <source>save</source>
        <translation>Save</translation>
    </message>
    <message>
        <location filename="src/view/main/TSystemSettingDlg.cpp" line="101"/>
        <source>cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="src/view/main/TSystemSettingDlg.cpp" line="118"/>
        <source>save path:</source>
        <translation>Local storage path:</translation>
    </message>
    <message>
        <location filename="src/view/main/TSystemSettingDlg.cpp" line="128"/>
        <source>It does effective when next start software!</source>
        <translation>The setting will be active after restarting!</translation>
    </message>
</context>
<context>
    <name>TTaskTabWidget</name>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="33"/>
        <source>Warm prompt: Using USB data lin to connet device with computer first, and make computer staying connected to the Internet.
 Then your can download mpr files by touch mpr book using touch and talk pen!</source>
        <translation>Tips: Please connect your talking pen to the computer with USB cable and keep your computer connected to the Internet.
 Start the audio files downloading by touch reading the book.</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="38"/>
        <source>There is no download task, please touch reading the MPR book</source>
        <translation>No download,please touch read the book to download audio files</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="78"/>
        <source>  </source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="78"/>
        <source>Book Name</source>
        <translation>Book name</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="78"/>
        <source>Publisher Name</source>
        <translation>Publisher</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="78"/>
        <source>Format</source>
        <translation>Format</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="78"/>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="78"/>
        <source>Operate</source>
        <translation>Operate</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="254"/>
        <source>start</source>
        <translation>Start</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="255"/>
        <source>pause</source>
        <translation>Pause</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="256"/>
        <source>delete</source>
        <translation>Delete</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="342"/>
        <source>Prepare</source>
        <translation>Prepare to download</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="348"/>
        <source>Download</source>
        <translation>Downloading...</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="354"/>
        <source>Pause</source>
        <translation>Paused</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="360"/>
        <source>Copyfile</source>
        <translation>Copying</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="369"/>
        <source>Finished</source>
        <translation>Finished</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="377"/>
        <source>Not Enough Space</source>
        <translation>No enough space</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="381"/>
        <source>Copy File Failed</source>
        <translation>Copying failed</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="385"/>
        <source>Download File Failed</source>
        <translation>Download failed</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="471"/>
        <source>file download error</source>
        <translation>Download failed</translation>
    </message>
</context>
<context>
    <name>TUpdateVersionManager</name>
    <message>
        <location filename="src/control/TUpdateVersionManager.cpp" line="204"/>
        <source>Prompt</source>
        <translatorcomment>Update</translatorcomment>
        <translation>Update</translation>
    </message>
    <message>
        <location filename="src/control/TUpdateVersionManager.cpp" line="204"/>
        <source>Firmware update success,please pull out the device and wait to reboot</source>
        <translation>Firmware update succeccfully,please pull out the talking pen and wait for rebooting.</translation>
    </message>
    <message>
        <location filename="src/control/TUpdateVersionManager.cpp" line="184"/>
        <location filename="src/control/TUpdateVersionManager.cpp" line="214"/>
        <source>Firmware upgrade failure</source>
        <translation>Firmware update failed!</translation>
    </message>
    <message>
        <location filename="src/control/TUpdateVersionManager.cpp" line="214"/>
        <source>Upgrade package download failed</source>
        <translatorcomment>Downloading the firmware upgrade package failed!</translatorcomment>
        <translation>Downloading of the upgrade package failed!</translation>
    </message>
    <message>
        <location filename="src/control/TUpdateVersionManager.cpp" line="185"/>
        <location filename="src/control/TUpdateVersionManager.cpp" line="215"/>
        <source>Retry</source>
        <translation>Retry</translation>
    </message>
    <message>
        <location filename="src/control/TUpdateVersionManager.cpp" line="184"/>
        <source>Upgrade package copy failed</source>
        <translation>Copying of the upgrade package failed!</translation>
    </message>
    <message>
        <location filename="src/control/TUpdateVersionManager.cpp" line="238"/>
        <source>file is downloading, do not pull out the device</source>
        <translation>Firmware is downloding, do not pull out the talking pen</translation>
    </message>
    <message>
        <location filename="src/control/TUpdateVersionManager.cpp" line="346"/>
        <location filename="src/control/TUpdateVersionManager.cpp" line="355"/>
        <source>Firmware upgrade tips</source>
        <translation>Firmware update</translation>
    </message>
    <message>
        <location filename="src/control/TUpdateVersionManager.cpp" line="346"/>
        <source>Reading pen firmware exist new version, whether to download upgrade?</source>
        <translation>There is  update for firmware,do you want to try?</translation>
    </message>
    <message>
        <location filename="src/control/TUpdateVersionManager.cpp" line="355"/>
        <source>current firmwarevision is incompaticatable, you need to download upgrade!</source>
        <translation>The firmware is not compatible with MPR World, please update!</translation>
    </message>
</context>
<context>
    <name>TWaittingDlg</name>
    <message>
        <location filename="src/view/main/TWaittingDlg.cpp" line="92"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
</context>
<context>
    <name>TWebServiceInterface</name>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="669"/>
        <source>Success</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="672"/>
        <source>Parameter error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="675"/>
        <source>Equipment loss</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="678"/>
        <source>Not identify user</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="681"/>
        <source>Failed to get ticket</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="684"/>
        <source>Please connect equipment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="687"/>
        <source>Resource was not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="690"/>
        <source>Account inactive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="693"/>
        <source>Account exception</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="696"/>
        <source>Account does not exist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="699"/>
        <source>Account or password error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="702"/>
        <source>Equipment account binding relationship does not exist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="705"/>
        <source>Equipment abnormal binding</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="708"/>
        <source>Equipment has bound</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="711"/>
        <source>Equipment wasn&apos;t register</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="714"/>
        <source>Inconsistent binding account login account and equipment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="717"/>
        <source>The device did not buy the goods</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="720"/>
        <source>No purchase record fee content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="723"/>
        <source>Equipment validation failure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="726"/>
        <source>A system exception</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="1248"/>
        <source>Failure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="1248"/>
        <source>The search timeout, please try again</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
