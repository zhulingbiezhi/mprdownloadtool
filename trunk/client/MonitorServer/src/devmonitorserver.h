﻿#ifndef DEVMONITORSERVER_H
#define DEVMONITORSERVER_H

#include <QObject>
#include <windows.h>
#include <Wtsapi32.h>
#include <TlHelp32.h>

class DevMonitorServer : public QObject
{
    Q_OBJECT
public:
    explicit DevMonitorServer(QObject *parent = 0);

	static DevMonitorServer* Instance();
	bool JudgeIsRunMprWorld();
    void deviceInOut(const QString& sn, bool in);

private:
	static const QString MprWorldExePath;

	bool GetTokenByName(HANDLE   &hToken, const char*  lpName);
	bool RunProcess(LPCWSTR lpImage);

    QString getExePath();

    QString exePath;
};

//#define PTR_DEVICE_MONITOR_SERVER Singleton<DevMonitorServer>::getInstance()

#endif // DEVMONITORSERVER_H
