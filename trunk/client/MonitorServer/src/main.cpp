﻿#include <QApplication>
#include "devmonitorserver.h"
#include "TDeviceCommMgr.h"

QString sname="MprMonitorServer";  //服务名称
QString sview="MprServer";         //服务显示时的名称
void CmdInstallService();        //安装函数
void CmdRemoveService();         //卸载函数
void CmdStopService();           //停止一个服务
void CmdStartService();          //起动一个服务

SERVICE_STATUS_HANDLE ssh;
SERVICE_STATUS ss;        //服务选行状态

int Argc =0;
char** Argv = NULL;

void  WINAPI servier_ctrl(DWORD Opcode)  //服务控制程序
{
	switch(Opcode)
	{
	case SERVICE_CONTROL_STOP:  //停止Service
		Sleep(2000);
		ss.dwWin32ExitCode = 0;
		ss.dwCurrentState  =SERVICE_STOPPED;
		ss.dwCheckPoint    = 0;
		ss.dwWaitHint      = 0;
		SetServiceStatus (ssh,&ss);
		break;
	case SERVICE_CONTROL_INTERROGATE:
		SetServiceStatus (ssh,&ss);
		break;
	}
}

void  WINAPI ServiceMain(DWORD dwArgc,LPTSTR *lpszArgv)
{
    QApplication a(Argc, Argv);
	ssh=RegisterServiceCtrlHandler(sname.toStdWString().c_str(),servier_ctrl);
	ss.dwServiceType=SERVICE_WIN32_OWN_PROCESS|SERVICE_INTERACTIVE_PROCESS;
	ss.dwCurrentState=SERVICE_START_PENDING;
	ss.dwControlsAccepted=SERVICE_ACCEPT_STOP;//表明Service目前能接受的命令是停止命令。
	ss.dwWin32ExitCode=NO_ERROR;
	ss.dwCheckPoint=0;
	ss.dwWaitHint=0;
	SetServiceStatus(ssh, &ss);
	//必须随时更新数据库中Service的状态。
	ss.dwServiceType=SERVICE_WIN32_OWN_PROCESS|SERVICE_INTERACTIVE_PROCESS;
	ss.dwCurrentState=SERVICE_RUNNING;
	ss.dwControlsAccepted=SERVICE_ACCEPT_STOP;
	ss.dwWin32ExitCode=NO_ERROR;
	ss.dwCheckPoint=0;
	ss.dwWaitHint=0;
	SetServiceStatus(ssh,&ss);

	DWORD prevDevicesMask = GetLogicalDrives();
	while (true)
	{
		DWORD currDevicesMask = GetLogicalDrives();
		DWORD mutableDevicesMask = (prevDevicesMask ^ currDevicesMask) & currDevicesMask;
		TDeviceCommMgr::Instance()->UpdateDevices(mutableDevicesMask);
		prevDevicesMask = currDevicesMask;
		Sleep(500);
	}
}

int main(int argc, char *argv[])
{
    Argc = argc;
    Argv = argv;
	QApplication a(argc, argv);

	if ( (argc>1) && ((*argv[1]=='-')||(*argv[1]=='/'))) //分析参数
	{
		if (_stricmp( "install", argv[1]+1 ) == 0 ) {CmdInstallService();}
		else if ( _stricmp( "remove", argv[1]+1 ) == 0 ){CmdRemoveService();}
		else if (_stricmp("contrl",argv[1]+1)==0 && argc==4)
		{
			sname=argv[2];
			if (_stricmp("stop",argv[3])==0) 
			{
				CmdStopService();
			}
			if (_stricmp("start",argv[3])==0) 
			{
				CmdStartService();
			}
		}
		else
		{//没有参数正确
			printf("%s -install  to install the service\n", argv[0] );
			printf("%s -remove   to remove the service\n", argv[0] );
			printf("%s -contrl [server name] [stop|start] contrl server\n",argv[0] );
			printf("这里的server name 注册表中的名称 \n");
		}
		return 0;
	}
	//没有参数打印参数表并起动服务
	printf( "%s -install  to install the service\n", argv[0] );
	printf( "%s -remove   to remove the service\n", argv[0] );
	printf( "%s -contrl [server name] [stop|start] contrl server\n",argv[0] );
	printf("这里的server name 注册表中的名称 \n");
	printf( "\nStartServiceCtrlDispatcher being called.\n" );
	printf( "This may take several seconds.  Please wait.\n" );

	//起动服务
	SERVICE_TABLE_ENTRY ste[2]; //一个Service进程可以有多个线程，这是每个线程的入口表
	ste[0].lpServiceName=(LPWSTR)sname.toStdWString().data();  //线程名字,实际上如果是SERVICE_WIN32_OWN_PROCESS 这个会忽略的
	ste[0].lpServiceProc=ServiceMain; //线程入口地址
	ste[1].lpServiceName=NULL;      //最后一个必须为NULL
	ste[1].lpServiceProc=NULL;
	StartServiceCtrlDispatcher(ste);

	return a.exec();
}

void CmdInstallService()    //安装函数
{
	SC_HANDLE   schService;
	SC_HANDLE   schSCManager;
	TCHAR szPath[512];
	//得到程序磁盘文件的路径
	if ( GetModuleFileName( NULL, szPath, 512 ) == 0 )
	{
		printf("Unable to install %s-%s\n",sview,sname);
		return;
	}
	//打开服务管理数据库
	schSCManager = OpenSCManager(
		NULL,                   // machine (NULL == local)
		NULL,                   // database (NULL == default)
		SC_MANAGER_ALL_ACCESS   // access required
		);
	if ( schSCManager )
	{   //安装
		schService = CreateService(
			schSCManager,               // SCManager database
			sname.toStdWString().c_str(),                      // 服务名称
			sview.toStdWString().c_str(),                      // 显示的名称
			SERVICE_ALL_ACCESS,         // desired access
			SERVICE_WIN32_OWN_PROCESS,  // service type
			SERVICE_AUTO_START,         //以自动方式开始start type
			SERVICE_ERROR_NORMAL,       // error control type指定启动失败的严重程度
			szPath,                     // Service本体程序路径，必须与具体位置相符
			NULL,                       // 指定顺序装入的服务组名
			NULL,                       // 填NULL
			NULL,                       // 启动本服务必须先启动的服务
			NULL,                       // 指定服务账号如: Domina\username .\username 如果为NULL,说明用LocalSystem
			NULL);                      // 账号密码,如果是LocalSystem 为NULL

		if ( schService )
		{
			printf("%s installed.\n",sname );
			CloseServiceHandle(schService);
		}
		else
		{
			printf("CreateService failed - %s\n",sname);
		}

		CloseServiceHandle(schSCManager);
	}
    else
        printf("OpenSCManager failed - %s\n");
};

void CmdRemoveService()
{
	SC_HANDLE   schService;
	SC_HANDLE   schSCManager;
	SERVICE_STATUS ssStatus;
	schSCManager = OpenSCManager(
		NULL,                   // machine (NULL == local)
		NULL,                   // database (NULL == default)
		SC_MANAGER_ALL_ACCESS   // access required
		);
	if ( schSCManager )
	{
		schService = OpenService(schSCManager,sname.toStdWString().c_str(),SERVICE_ALL_ACCESS);
		if (schService)
		{
			if ( ControlService( schService, SERVICE_CONTROL_STOP, &ssStatus ) )
			{
				printf("Stopping %s.", sview);
				Sleep( 1000 );
				while( QueryServiceStatus( schService, &ssStatus ) )
				{
					if ( ssStatus.dwCurrentState == SERVICE_STOP_PENDING )
					{
						printf(".");
						Sleep( 1000 );
					}
					else
						break;
				}
				if ( ssStatus.dwCurrentState == SERVICE_STOPPED )
					printf("\n%s stopped.\n",sview);
				else
					printf("\n%s failed to stop.\n",sview);
			}

			if( DeleteService(schService) )
				printf("%s removed\n",sview);
			else
				printf("DeleteService failed \n");
			CloseServiceHandle(schService);
		}
		else
			printf("OpenService failed - %s\n");
		CloseServiceHandle(schSCManager);
	}
	else
		printf("OpenSCManager failed \n");
}
void CmdStartService()         //起动一个服务
{
	SC_HANDLE schService;
	SC_HANDLE schSCManager;
	SERVICE_STATUS ssStatus;
	schSCManager = OpenSCManager(
		NULL,                   // machine (NULL == local)
		NULL,                   // database (NULL == default)
		SC_MANAGER_ALL_ACCESS   // access required
		);
	if ( schSCManager )
	{
		schService = OpenService(schSCManager,sname.toStdWString().c_str(),SERVICE_ALL_ACCESS);
		if (schService)
		{
			// 起动服务
			if ( StartService(schService,NULL,NULL))
			{
				printf("Starting %s.", sname);
				Sleep( 1000 );
				while( QueryServiceStatus( schService, &ssStatus ) )
				{
					if ( ssStatus.dwCurrentState == SERVICE_START_PENDING )
					{
						printf(".");
						Sleep( 500 );
					}
					else
						break;
				}
				if ( ssStatus.dwCurrentState == SERVICE_RUNNING )
				{
					printf("\n%s running.\n",sname);
				}
				else
					printf("\n%s failed to run.\n",sname);
			}
			else
			{
				QueryServiceStatus( schService, &ssStatus );
				if ( ssStatus.dwCurrentState == SERVICE_RUNNING )
				{
					printf("\n%s running.\n",sname);
				}
				else
					printf("\n%s failed to run.\n",sname);
			}
			CloseServiceHandle(schService);
		}
		else
			printf("OpenService failed - %s\n");
		CloseServiceHandle(schSCManager);
	}
	else
		printf("OpenSCManager failed \n");
}

void CmdStopService()         //停止一个服务
{
	SC_HANDLE schService;
	SC_HANDLE schSCManager;
	SERVICE_STATUS ssStatus;
	schSCManager = OpenSCManager(
		NULL,                   // machine (NULL == local)
		NULL,                   // database (NULL == default)
		SC_MANAGER_ALL_ACCESS   // access required
		);
	if ( schSCManager )
	{
		schService = OpenService(schSCManager,sname.toStdWString().c_str(),SERVICE_ALL_ACCESS);
		if (schService)
		{
			// 关闭服务
			if ( ControlService(schService,SERVICE_CONTROL_STOP,&ssStatus) )
			{
				printf("Stopping %s.",sname);
				Sleep( 1000 );
				while( QueryServiceStatus(schService,&ssStatus ) )
				{
					if ( ssStatus.dwCurrentState == SERVICE_STOP_PENDING )
					{
						printf(".");
						Sleep( 1000 );
					}
					else
						break;
				}
				if ( ssStatus.dwCurrentState == SERVICE_STOPPED )
					printf("\n%s stopped.\n",sname);
				else
					printf("\n%s failed to stop.\n",sname);
			}

			CloseServiceHandle(schService);
		}
		else
			printf("OpenService failed - %s\n");
		CloseServiceHandle(schSCManager);
	}
	else
		printf("OpenSCManager failed \n");
}
