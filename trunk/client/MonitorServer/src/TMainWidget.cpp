#include "TMainWidget.h"
#include <Windows.h>
#include "TDeviceCommMgr.h"

TMainWindow::TMainWindow()
{
	mpTimer  = new QTimer(this);
	connect(mpTimer, SIGNAL(timeout()), this, SLOT(OnTimerOut()));
	mpTimer->start(500);
}

TMainWindow::~TMainWindow(){

}

TMainWindow* TMainWindow::Instance()
{
	static TMainWindow instance;
	return &instance;
}

void TMainWindow::OnTimerOut()
{
	TDeviceCommMgr::Instance()->EnumDevices();
}