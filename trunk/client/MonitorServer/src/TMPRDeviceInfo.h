#ifndef __TMPRDEVICEINFO_H__
#define __TMPRDEVICEINFO_H__

#include <string>
using namespace std;

enum DongleStat{
    NO_DONGLE,
    DONGLE_CONNECT
};

//设备支持的对称加密算法类型
typedef enum {
	AES_ECB = 0x0001,     //AES ECB算法
	AES_CBC = 0x0002,     //AES CBC算法
	AES_CTR = 0x0004,     //AES CTR算法
	SM4_ECB = 0x0100,     //国密4 ECB算法
	SM4_CBC = 0x0200,     //国密4 CBC算法
	SM4_CTR = 0x0400      //国密4 CTR算法
} EncryptAlgorithm;

//设备支持的非对称加密算法类型
typedef enum {
	RSA = 0x01,                   //RSA算法
	SM2 = 0x02                    //国密2算法
} PKI_Algorithm;

//接口函数返回的错误代码
typedef enum {
	DEVICE_OK = 0,  //接口函数调用成功
	BAD_PARAM = -1, //传入的接口函数参数错误
	BAD_CALL = -2, //接口函数调用顺序错误
	DEVICE_NOT_EXIST = -3, //设备不存在
	COMM_FAIL = -4, //下位机通讯错误
	LOW_PRIORITY = -5, //其他程序正在通讯且优先级高
	ONE_EXIST = -6, //只有一个程序存在
	SYS_ERROR = -7, //其它错误
	VERIFY_SIGNATURE_FAIL = -8, //验证license签名失败
	NO_THIS_LINE = -9, //downloadlist中没有该行
	DECRYPT_FAIL = -10 //下位机解密失败
} MPRDeviceErrorCode;

//识读器支持的音频格式
typedef enum {
	WAV = 0x00000001L,     //非压缩wave格式
	MP3 = 0x00000002L,     //MP3压缩格式
	OGG = 0x00000004L      //OGG压缩格式
} MPRAudioFormat;

//MPR设备类型枚举
typedef enum {
	MPRDevice_Invalid,     //无效的MPR设备
	MPRDevice_PenReader,   //MPR识读器（点读笔）
	MPRDevice_BookDisk,    //MRR书盘（UKey）
	MPRDevice_Dongle,      //USB dongle,无媒体存储能力
	MPRDevice_VideoPlayer  //接转器
} MPRDeviceType;

typedef enum{
    SHORT_PRESS = 1,
    LONG_PRESS = 2,
    DOUBLE_PRESS = 3
}KeyPressType;

//MPR设备描述结构
typedef struct {
    MPRDeviceType  dev_type;                    //设备类型
    char           dev_sn[32];                  //设备序列号，生产管理用
    char           dev_name[32];                //设备名称，例如"MPR-1026"
    int            dev_brand_id;                //设备品牌id，对应的字符串描述须向服务器查询
    unsigned char  dev_uuid[40];                //设备UUID，设备的全局唯一id
    char           dev_firmware_version[512];    //设备固件版本字符串
    char           dev_media_path[128];         //设备媒体存储磁盘路径，最后一个非零字符为'/'
    unsigned long  dev_audio_format;            //设备支持的媒体格式，MPRAudioFormat按位或
    unsigned long  dev_encrypt_algorithm;       //设备支持的加密算法类型，EncryptAlgorithm按位或
    PKI_Algorithm  dev_pki_algorithm;           //设备使用的非对称加密算法，RSA或SM2
} MPRDeviceDesc;

class TMPRDeviceInfo
{
public:
    static void     ParserDeviceDsc(const unsigned char * inBuf, MPRDeviceDesc& rDevDesc);
    static bool     HexToAscii(string hexData, char* asciiData);
};
#endif