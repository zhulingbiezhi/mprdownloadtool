#ifndef _TMAINWINDOW_H_
#define _TMAINWINDOW_H_

#include <QObject>
#include <QTimer>
#include <QFile>

class TMainWindow : public QObject
{
	Q_OBJECT
public:
	TMainWindow();
	~TMainWindow();

	static TMainWindow* Instance();
public slots:
    void OnTimerOut();
private:
	QTimer* mpTimer;
	QFile   mFile;
};

#endif