﻿#include "devmonitorserver.h"
#include <iomanip>
#include <fstream>
#include <QApplication>
#include <QDir>
#include <QSettings>
#include <Userenv.h>

//const QString DevMonitorServer::MprWorldExePath= "G:\\zwm\\MPRSP\\3code\\MprApp5.0\\bin\\MprWorld\\MprWorldd.exe";

DevMonitorServer::DevMonitorServer(QObject *parent) :
    QObject(parent)
{

}

void DevMonitorServer::deviceInOut(const QString& sn, bool in)
{
    if(in)
    {
        if(RunProcess((LPCWSTR)getExePath().toStdWString().data()))
        {

        }
    }
    return ;
}

bool DevMonitorServer::JudgeIsRunMprWorld()
{
    return false;
}

DevMonitorServer* DevMonitorServer::Instance()
{
	static DevMonitorServer ins;
	return &ins;
}

bool DevMonitorServer::GetTokenByName(HANDLE   &hToken,const char*  lpName)
{
	DWORD activeSessionId = WTSGetActiveConsoleSessionId();
	if (WTSQueryUserToken(activeSessionId, &hToken)) {
		return true;
	}

	if(!lpName)
	{
		return false;
	}
	HANDLE hProcessSnap = NULL;
	BOOL bRet = false;
	PROCESSENTRY32 pe32 = {0};

	hProcessSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS,   0);
	if (hProcessSnap == INVALID_HANDLE_VALUE)
		return(false);

	pe32.dwSize=sizeof(PROCESSENTRY32);

	if(Process32First(hProcessSnap,   &pe32))
	{
		do
		{
			char sExeFile[260] = {0};
			wcstombs(sExeFile,pe32.szExeFile,260);
			if(!stricmp(sExeFile, lpName))
			{
				HANDLE hProcess = OpenProcess(PROCESS_QUERY_INFORMATION,FALSE,pe32.th32ProcessID);
				bRet = OpenProcessToken(hProcess,TOKEN_ALL_ACCESS,&hToken);
				CloseHandle(hProcessSnap);
				return (bRet);
			}
		}
		while(Process32Next(hProcessSnap,&pe32));
		bRet = true;
	}
	else
		bRet = false;

	CloseHandle(hProcessSnap);
	return (bRet);
}

bool DevMonitorServer::RunProcess(LPCWSTR lpImage)
{
	if(!lpImage)
	{
		return false;
	}
	HANDLE hToken;
	if(!GetTokenByName(hToken,"EXPLORER.EXE"))
	{
		return false;
	}

    char appData[512];
    ExpandEnvironmentStringsForUserA(hToken, "%APPDATA%", appData, 512);
    QString settingsPath = QString(appData) + "/Mpr World/settings.ini";
    QSettings settings(settingsPath, QSettings::IniFormat);
    if (!settings.value("AutoRunWhileDeviceConnected", false).toBool()) {
        return false;
    }

	STARTUPINFO si;
    PROCESS_INFORMATION pi = { 0 };
	ZeroMemory(&si, sizeof(STARTUPINFO));
	si.cb = sizeof(STARTUPINFO);
	si.lpDesktop = (LPWSTR)(TEXT("winsta0\\default"));

    LPVOID lpEnvironment = NULL;
    BOOL rtn = ::CreateEnvironmentBlock(&lpEnvironment, hToken, false);
    if (rtn)
    {
        rtn = ::CreateProcessAsUser(hToken, lpImage, NULL, NULL, NULL, FALSE,
                                    NORMAL_PRIORITY_CLASS|CREATE_UNICODE_ENVIRONMENT,
                                    lpEnvironment, NULL, &si, &pi);
        ::DestroyEnvironmentBlock(lpEnvironment);
        if (rtn)
        {
            if (pi.hThread)
            {
                CloseHandle(pi.hThread);
            }
            if (pi.hProcess)
            {
                CloseHandle(pi.hProcess);
            }
        }
    }

	CloseHandle(hToken);
    return   rtn;
}

QString DevMonitorServer::getExePath()
{
        exePath = QApplication::applicationDirPath() + QDir::separator() + "MPR_World.exe";
//        FILE *fp;
//        fp=fopen("d:\\data.txt","at+");
//        fwrite(exePath.toLatin1(),exePath.size(),1,fp);
//        fclose(fp);
    return exePath;
}
