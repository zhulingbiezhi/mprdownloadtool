#-------------------------------------------------
#
# Project created by QtCreator 2013-08-05T15:18:15
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG   += console
CONFIG   -= app_bundle
TEMPLATE = app
QMAKE_LFLAGS_RELEASE += /SAFESEH:NO /DEBUG
QMAKE_LFLAGS_DEBUG += /SAFESEH:NO

QMAKE_TARGET_OS = xp

SOURCES += main.cpp \
    TDeviceComm.cpp \
    TDeviceCommMgr.cpp \
    TMPRDeviceInfo.cpp \
    devmonitorserver.cpp

HEADERS += \
    TDeviceComm.h \
    TDeviceCommMgr.h \
    TMPRDeviceInfo.h \
    devmonitorserver.h

LIBS += -L$$PWD/../lib/libAsymmCrypt/
win32:CONFIG(release, debug|release) {
    LIBS += -lAdvapi32 -lUser32 -lole32 -lUserenv -lWtsapi32
            DESTDIR = bin/debug_x86
            OBJECTS_DIR = temp/debug_x86
}
else:win32:CONFIG(debug, debug|release) {
    LIBS += -lAdvapi32 -lUser32 -lole32 -lUserenv -lWtsapi32
            DESTDIR = bin/release_x86
            OBJECTS_DIR = temp/release_x86
}

UI_DIR  = temp/ui
MOC_DIR = temp/moc
RCC_DIR = temp/rcc
