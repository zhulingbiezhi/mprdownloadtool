#include "TDeviceCommMgr.h"
#include "devmonitorserver.h"

TDeviceCommMgr::TDeviceCommMgr()
: QObject(nullptr)
{

}

TDeviceCommMgr::~TDeviceCommMgr()
{

}

TDeviceCommMgr* TDeviceCommMgr::Instance()
{
    static TDeviceCommMgr inst;
    return &inst;
}

int  TDeviceCommMgr::EnumDevices()
{
    return UpdateDevices(GetLogicalDrives());
}

int  TDeviceCommMgr::UpdateDevices(DWORD devicesMask)
{

    for (int i = 2; i < 8 * sizeof(DWORD); i++)
    {
        if (devicesMask & (1 << i))
        {
            char drive = 'A' + i;
            TDeviceComm* pComm = new TDeviceComm(drive);
            if (pComm)
            {
                if (pComm->IsValid())
                {
                    if (pComm->IsReadingPenDevice())
                    {
						MPRDeviceDesc hehe;
						pComm->GetDeviceDesc(hehe);
						DevMonitorServer::Instance()->deviceInOut(hehe.dev_sn, true);
                    }
                }

				delete pComm;
				pComm = nullptr;
            }
            
        }
    }

    return 0;
}

