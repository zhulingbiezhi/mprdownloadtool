#ifndef __TDEVICECOMM_H__
#define __TDEVICECOMM_H__

#include "TMPRDeviceInfo.h"
#include <qglobal.h>
#include <QString>
#include <QThread>

#ifdef _DEBUG
# define VIRTUAL virtual
#else
# define VIRTUAL
#endif

#define CMD_INTERVAL         50
#define RDWR_INTERVAL        50

#define DEV_RET_SUCCESS      0
#define DEV_RET_BUSYING     -100
#define DEV_ERR_INVALID     -101
#define DEV_ERR_PARAMS      -102
#define DEV_ERR_IOCTRL      -103

#define DEV_OP_SUCCEED(rtn) ((rtn) >= 0)
#define DEV_OP_FAILURE(rtn) ((rtn) <  0)
#define DEV_OP_BUSYING(rtn) ((rtn) == -100)

#define DEV_CMD_DELAY(ms) QThread::currentThread()->msleep((ms))

/*
** 设备通信实体类
*/
class TDeviceComm
{
public:
    TDeviceComm(char driver);
    VIRTUAL ~TDeviceComm();

    VIRTUAL bool IsValid() const;

    bool IsDongleDevice();
    bool IsReadingPenDevice();

    char GetDriver() const;
    bool GetDeviceDesc(MPRDeviceDesc& desc);

    int  GetDevBufferLimit() const;

    bool Eject();

    VIRTUAL int  Read(quint8 buffer[], int maxSize);
    VIRTUAL int  Write(const quint8 buffer[], int size);

private:
    void Close();
    bool Open(char driver);
    void ReadDeviceDesc();

protected:
    char            mDriver;
    void*           mhDriver;
    MPRDeviceDesc   mDevDesc;
};

#endif //__TDEVICECOMM_H__


