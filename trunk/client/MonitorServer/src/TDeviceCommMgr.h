#ifndef __TDEVICECOMMMGR_H__
#define __TDEVICECOMMMGR_H__

#include <windows.h>
#include <qwindowdefs.h>
#include "TDeviceComm.h"
#include <QObject>


enum EMPRDEVICETYPE {
    DEV_DONGLE      = 1,
    DEV_READINGPEN  = 2,
};

/*
** 设备通信实体管理类
*/
class TDeviceCommMgr : public QObject
{
    Q_OBJECT

public:
    static TDeviceCommMgr* Instance();

    int  EnumDevices();
    int  UpdateDevices(DWORD deviceMask);

private:
    TDeviceCommMgr();
    TDeviceCommMgr(const TDeviceCommMgr& rhs);
    TDeviceCommMgr& operator = (const TDeviceCommMgr& rhs);
    ~TDeviceCommMgr();

private:
};


#endif //__TDEVICECOMMMGR_H__


