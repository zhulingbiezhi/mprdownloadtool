#include <stddef.h>
#include <windows.h>
#include <ntddscsi.h>
#include "TDeviceComm.h"

#define SPT_SENSE_LENGTH    32
#define CDB6GENERIC_LENGTH   1

struct T_SCSI_PASS_THROUGH_WITH_BUFFERS
{
    SCSI_PASS_THROUGH spt;
    ULONG             Filler; 
    UCHAR             ucSenseBuf[SPT_SENSE_LENGTH];
    UCHAR             ucDataBuf[4096];
};

typedef  T_SCSI_PASS_THROUGH_WITH_BUFFERS SCSI_PASS_THROUGH_WITH_BUFFERS, *PSCSI_PASS_THROUGH_WITH_BUFFERS;


TDeviceComm::TDeviceComm(char driver)
: mhDriver(nullptr)
, mDriver(driver)
{
    

    Open(mDriver);
}

TDeviceComm::~TDeviceComm()
{
    

    Close();
}

bool TDeviceComm::IsValid() const
{
    

    return mhDriver != nullptr && mhDriver != INVALID_HANDLE_VALUE;
}

void TDeviceComm::Close()
{
    

    if (IsValid())
    {
        CloseHandle(mhDriver);
        mhDriver = nullptr;
    }
}

bool TDeviceComm::Open(char driver)
{
    

    Close();

    QString driveName;
    driveName.sprintf("%c:\\", driver);

    if (GetDriveType((LPCWSTR)driveName.utf16()) == DRIVE_REMOVABLE)
    {
        QString driverPath;
        driverPath.sprintf("\\\\?\\%c:", driver);

        mhDriver = CreateFile((LPCWSTR)driverPath.utf16(), GENERIC_READ | GENERIC_WRITE,
                                FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, NULL, NULL);

        ReadDeviceDesc();
    }

    return mhDriver != nullptr && mhDriver != INVALID_HANDLE_VALUE;
}

void TDeviceComm::ReadDeviceDesc()
{
    

    memset(&mDevDesc, 0x0, sizeof(mDevDesc));
    mDevDesc.dev_type = MPRDevice_Invalid;

    if (IsValid())
    {
        quint8 DEVICED_ESC_CMD[] = {0x04,0x00,0x06,0x00};
        int rtn = Write(DEVICED_ESC_CMD, 0x04);
        if (DEV_OP_SUCCEED(rtn))
        {
            quint8 revBuff[4*1024] = {0};
            rtn = Read(revBuff, sizeof(revBuff));
            if (DEV_OP_SUCCEED(rtn))
            {
                TMPRDeviceInfo::ParserDeviceDsc(revBuff, mDevDesc);
            }
        }
    }
}

bool TDeviceComm::IsDongleDevice()
{
    

    return IsValid() && mDevDesc.dev_type == MPRDevice_Dongle;
}

bool TDeviceComm::IsReadingPenDevice()
{
    

    return IsValid() && mDevDesc.dev_type == MPRDevice_PenReader;
}

char TDeviceComm::GetDriver() const
{
    

    return mDriver;
}

bool TDeviceComm::GetDeviceDesc(MPRDeviceDesc& desc)
{
    

    if (IsValid())
    {
        if (mDevDesc.dev_type == MPRDevice_Dongle ||
            mDevDesc.dev_type == MPRDevice_PenReader ) {
                desc = mDevDesc;
                return true;
        }
    }
    return false;
}

int  TDeviceComm::GetDevBufferLimit() const
{
    

    if (mDevDesc.dev_type == MPRDevice_Dongle) {
        return 255;
    }
    if (mDevDesc.dev_type == MPRDevice_PenReader) {
        return 4096;
    }
    return 255;
}

bool TDeviceComm::Eject()
{
    
    return false;
}

int  TDeviceComm::Read(quint8 buffer[], int maxSize)
{
    

    if (!IsValid()) {
        return DEV_ERR_INVALID;
    }

    if (!buffer || maxSize <= 0) {
        return DEV_ERR_PARAMS;
    }

    BOOL status = 0;
    ULONG returned = 0;
    ULONG length = 0;
    ULONG transLength = GetDevBufferLimit() > maxSize ? maxSize : GetDevBufferLimit();
    SCSI_PASS_THROUGH_WITH_BUFFERS sptwb;
    ZeroMemory(&sptwb,sizeof(SCSI_PASS_THROUGH_WITH_BUFFERS));

    sptwb.spt.Length = sizeof(SCSI_PASS_THROUGH);
    sptwb.spt.PathId = 0;
    sptwb.spt.TargetId = 1;
    sptwb.spt.Lun = 0;
    sptwb.spt.CdbLength = CDB6GENERIC_LENGTH;
    sptwb.spt.SenseInfoLength = SPT_SENSE_LENGTH;
    sptwb.spt.DataIn = SCSI_IOCTL_DATA_IN;
    sptwb.spt.DataTransferLength = transLength;
    sptwb.spt.TimeOutValue = 5;
    sptwb.spt.DataBufferOffset = offsetof(SCSI_PASS_THROUGH_WITH_BUFFERS,ucDataBuf);
    sptwb.spt.SenseInfoOffset = offsetof(SCSI_PASS_THROUGH_WITH_BUFFERS,ucSenseBuf);
    sptwb.spt.Cdb[0] = 0xCF;

    length = offsetof(SCSI_PASS_THROUGH_WITH_BUFFERS,ucDataBuf) + sptwb.spt.DataTransferLength;

    status = DeviceIoControl(mhDriver,
             IOCTL_SCSI_PASS_THROUGH,
             &sptwb,
             sizeof(SCSI_PASS_THROUGH_WITH_BUFFERS),
             &sptwb,
             length,
             &returned,
             NULL);

    memcpy(buffer, sptwb.ucDataBuf, (int)returned > maxSize ? maxSize : returned);

    return status == TRUE ? returned : DEV_ERR_IOCTRL;
}

int  TDeviceComm::Write(const quint8 buffer[], int size)
{
    

    if (!IsValid()) {
        return DEV_ERR_INVALID;
    }

    if (!buffer || size <= 0 || size > 4096) {
        return DEV_ERR_PARAMS;
    }

    BOOL status = 0;
    ULONG length = 0;
    ULONG returned = 0;
    SCSI_PASS_THROUGH_WITH_BUFFERS sptwb;
    ZeroMemory(&sptwb,sizeof(SCSI_PASS_THROUGH_WITH_BUFFERS));

    sptwb.spt.Length = sizeof(SCSI_PASS_THROUGH);
    sptwb.spt.PathId = 0;
    sptwb.spt.TargetId = 1;
    sptwb.spt.Lun = 0;
    sptwb.spt.CdbLength = CDB6GENERIC_LENGTH;
    sptwb.spt.SenseInfoLength = SPT_SENSE_LENGTH;
    sptwb.spt.DataIn = SCSI_IOCTL_DATA_OUT;
    sptwb.spt.DataTransferLength = size;
    sptwb.spt.TimeOutValue = 5;
    sptwb.spt.DataBufferOffset = offsetof(SCSI_PASS_THROUGH_WITH_BUFFERS,ucDataBuf);
    sptwb.spt.SenseInfoOffset = offsetof(SCSI_PASS_THROUGH_WITH_BUFFERS,ucSenseBuf);
    sptwb.spt.Cdb[0] = 0xCE;
    memcpy(sptwb.ucDataBuf, buffer, size);

    length = offsetof(SCSI_PASS_THROUGH_WITH_BUFFERS,ucDataBuf) + sptwb.spt.DataTransferLength;

    status = DeviceIoControl(mhDriver,
             IOCTL_SCSI_PASS_THROUGH,
             &sptwb,
             sizeof(SCSI_PASS_THROUGH_WITH_BUFFERS),
             &sptwb,
             length,
             &returned,
             NULL);

    return status == TRUE ? DEV_RET_SUCCESS : DEV_ERR_IOCTRL;
}

