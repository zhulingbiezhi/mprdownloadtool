#ifndef SERVERSOCKET_H
#define SERVERSOCKET_H

#include <QObject>
#include <QStringList>
class QLocalServer;
class QLocalSocket;

class serverSocket : public QObject
{
    Q_OBJECT
public:
    explicit serverSocket(const QString & servername,QObject *parent = 0);
    ~serverSocket();
    bool isServerRun(const QString & servername);
    void serverSocket::send(int site);
signals:
    
public slots:
    void sendFortune();
    void clientDisconnectedSlot();
private:

    bool initServer(const QString & servername);


    QLocalServer *server;
    QStringList fortunes;
    QLocalSocket *clientConnection;
    QString _serverName;
};


#endif // SERVERSOCKET_H
