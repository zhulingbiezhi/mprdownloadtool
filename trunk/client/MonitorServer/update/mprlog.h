﻿#ifndef MPRLOG_H
#define MPRLOG_H
#include <QFile>
#include <QTextStream>
#include <QMutex>
#include <QFile>
#include <QTextStream>

#include "singleton.h"

#define DEFAULT_LOG_FILE "update.log"

class MprLog
{
public:
    /**
     * @brief 在使用日志功能之前执行此函数，比如main函数的开始处
     */
    static void initialize();
private:
    MprLog();
    ~MprLog();
    QFile _file;
    QTextStream _out;
    QMutex _mutex;
    QtMessageHandler _oriMsgHandler;
    static void msgHandle(QtMsgType type, const QMessageLogContext &context, const QString &msg);
    DECLARE_SINGLETON_CLASS(MprLog)
};

#endif // MPRLOG_H
