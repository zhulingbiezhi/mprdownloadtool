﻿#include "mprlog.h"
#include <QDateTime>
#include <QDir>
#include <QApplication>

MprLog::MprLog():_oriMsgHandler(NULL)
{
}

MprLog::~MprLog()
{
    qInstallMessageHandler(_oriMsgHandler);
}

void MprLog::initialize()
{
    MprLog *p = Singleton<MprLog>::getInstance();
    if (p)
    {
        QDir dir(QApplication::applicationDirPath());
        p->_file.setFileName(dir.absoluteFilePath(DEFAULT_LOG_FILE));
        p->_file.open(QIODevice::WriteOnly|QIODevice::Append|QIODevice::Text);
        p->_out.setDevice(&p->_file);
        p->_oriMsgHandler = qInstallMessageHandler(msgHandle);
    }
}

void MprLog::msgHandle(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    MprLog *p = Singleton<MprLog>::getInstance();
    if (!p) return;

    QString strBuff;
    switch (type) {
    case QtDebugMsg:
        strBuff = QString("%1 Debug:%2").arg(QDateTime::currentDateTime().toString("yy-MM-dd hh:mm:ss"), msg);
        break;
    case QtWarningMsg:
        strBuff = QString("%1 Warning:%2").arg(QDateTime::currentDateTime().toString("yy-MM-dd hh:mm:ss"), msg);
        break;
    case QtCriticalMsg:
        strBuff = QString("%1 Critical:%2").arg(QDateTime::currentDateTime().toString("yy-MM-dd hh:mm:ss"), msg);
        break;
    case QtFatalMsg:
        strBuff = QString("%1 Fatal:%2").arg(QDateTime::currentDateTime().toString("yy-MM-dd hh:mm:ss"), msg);
        break;
    }

    p->_mutex.lock();
    p->_out << strBuff << endl;
    p->_mutex.unlock();
    if (p->_oriMsgHandler)
        p->_oriMsgHandler(type, context, strBuff);
}
