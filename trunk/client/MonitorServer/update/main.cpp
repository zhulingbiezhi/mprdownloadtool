﻿#include "update.h"
#include <QApplication>
#include <QDebug>
#include "mprlog.h"
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MprLog::initialize();
   /* for(int i=0;i<argc;i++)
        qDebug() << argv[i];*/
    if(argc != 7)
    {
        qDebug() << "argc error";
        exit(0);
    }
    updateDialog w;
    w.sInstallPackageSaveDir = argv[1];
    w.sDownloadUrl = argv[2];
    w.sVcode = argv[3];
    w.sDesc = argv[4];
    w.sForceInstall = argv[5];
    w.sUpdateType = argv[6];
    w.startUpdate();
    w.hide();

    qDebug() << "---------Update-----------------------------------";
    qDebug() << "InstallPackageSaveDir  :" << w.sInstallPackageSaveDir;
    qDebug() << "DownloadUrl            :" << w.sDownloadUrl;
    qDebug() << "Vcode                  :" << w.sVcode;
    qDebug() << "Desc                   :" << w.sDesc;
    qDebug() << "ForceInstall           :" << w.sForceInstall;
    qDebug() << "UpdateType             :" << w.sUpdateType;
    qDebug() << "-----------------------------------------------------";

    return a.exec();
}
