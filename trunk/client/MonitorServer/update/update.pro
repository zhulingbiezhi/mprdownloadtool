TEMPLATE = app
QT += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG(release, debug|release):TARGET = update
else:TARGET = updated

QMAKE_TARGET_OS = xp

SOURCES += main.cpp \
           update.cpp \
           downloadmanager.cpp \
           mprlog.cpp \
           serversocket.cpp

HEADERS += update.h \
           downloadmanager.h \
           mprlog.h \
           serversocket.h \
           singleton.h

win32{
    COPY = xcopy /D/Y
    MKDIR = mkdir
}else{
    COPY = cp -f
    MKDIR = mkdir -p
}
    
