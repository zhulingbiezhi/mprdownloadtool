﻿#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include "downloadmanager.h"

class serverSocket;
class updateDialog : public QDialog
{
    Q_OBJECT
    
public:
    updateDialog(QWidget *parent = 0);
    ~updateDialog();

private:
    DownloadManager *pDownloadManager;
private slots:
    void softwareDownloadFinish();
    void softwareDownloadError();
    void firmwareDownloadFinish();
    void firmwareDownloadError();

public:
    void startUpdate();

    QString sInstallPackageSaveDir;
    QString sDownloadUrl;
    QString sVcode;
    QString sDesc;
    QString sForceInstall;
    QString sUpdateType;

    serverSocket* softwareServer;
    serverSocket* firewareServer;
};

#endif // DIALOG_H
