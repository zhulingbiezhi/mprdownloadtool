﻿#include "update.h"
#include <QApplication>
#include <iostream>
#include "serversocket.h"
#include <QDebug>
updateDialog::updateDialog(QWidget *parent)
    : QDialog(parent)
{
    softwareServer = new serverSocket("SoftwareUpdateServer");
    firewareServer = new serverSocket("FirmwareUpdateServer");
}

updateDialog::~updateDialog()
{
    if(softwareServer!= NULL)
    {
        delete softwareServer;
        softwareServer = NULL;
    }
    if(firewareServer!= NULL)
    {
        delete firewareServer;
        firewareServer = NULL;
    }
}

void updateDialog::startUpdate()
{
    pDownloadManager = new DownloadManager(this->parent(),sInstallPackageSaveDir);
    UpdateTask* pUpdateTask;
    if(sUpdateType == "software")
    {
        pUpdateTask = pDownloadManager->addTask(sDownloadUrl,sVcode,sDesc,
                                                sForceInstall,UpdateTask::TypeSoftwareupdate);
        connect(pUpdateTask,SIGNAL(taskFinish()),this,SLOT(softwareDownloadFinish()));
        connect(pUpdateTask,SIGNAL(taskError()),this,SLOT(softwareDownloadError()));
    }

    else if(sUpdateType == "firmware")
    {
        pUpdateTask = pDownloadManager->addTask(sDownloadUrl,sVcode,sDesc,
                                                sForceInstall,UpdateTask::TypeFirmwareupdate);
        connect(pUpdateTask,SIGNAL(taskFinish()),this,SLOT(firmwareDownloadFinish()));
        connect(pUpdateTask,SIGNAL(taskError()),this,SLOT(firmwareDownloadError()));
    }
}


void updateDialog::softwareDownloadFinish()
{
    qDebug() << __FUNCTION__;
    softwareServer->send(0);
    qApp->quit();
}

void updateDialog::softwareDownloadError()
{
    qDebug() << __FUNCTION__;
    softwareServer->send(1);
    qApp->quit();
}

void updateDialog::firmwareDownloadFinish()
{
    qDebug() << __FUNCTION__;
    firewareServer->send(2);
    qApp->quit();
}

void updateDialog::firmwareDownloadError()
{
    qDebug() << __FUNCTION__;
    firewareServer->send(3);
    qApp->quit();
}
