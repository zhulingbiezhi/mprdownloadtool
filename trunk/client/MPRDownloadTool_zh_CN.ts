<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>QObject</name>
    <message>
        <location filename="src/model/dumpcore/TDumpCore.cpp" line="51"/>
        <source>Program an exception occurs, exception information has been saved to the desktop, FileName:</source>
        <translation>软件异常，异常信息已保存于桌面！</translation>
    </message>
    <message>
        <location filename="src/model/dumpcore/TDumpCore.cpp" line="52"/>
        <source>Progress Error</source>
        <translation>软件异常</translation>
    </message>
    <message>
        <location filename="src/device/TDeviceEject.cpp" line="157"/>
        <source>Media in Drive %c has been ejected safely.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/device/TDeviceEject.cpp" line="163"/>
        <source>Media in Drive %c can be safely removed.
</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TBaseMessageBox</name>
    <message>
        <location filename="src/view/main/TBaseMessageBox.cpp" line="42"/>
        <source>OK</source>
        <translation>确认</translation>
    </message>
    <message>
        <location filename="src/view/main/TBaseMessageBox.cpp" line="43"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>TDeviceInfoWidget</name>
    <message>
        <location filename="src/view/main/TDeviceInfoWidget.cpp" line="20"/>
        <source>Device Type:</source>
        <translation>识读器型号：</translation>
    </message>
    <message>
        <location filename="src/view/main/TDeviceInfoWidget.cpp" line="23"/>
        <source>Device Id:</source>
        <translation>品牌ID：</translation>
    </message>
    <message>
        <location filename="src/view/main/TDeviceInfoWidget.cpp" line="27"/>
        <source>Capacity:</source>
        <translation>容量：</translation>
    </message>
    <message>
        <location filename="src/view/main/TDeviceInfoWidget.cpp" line="35"/>
        <location filename="src/view/main/TDeviceInfoWidget.cpp" line="157"/>
        <source>Used:%1,Total:%2</source>
        <translation>已用%1，总共%2</translation>
    </message>
    <message>
        <location filename="src/view/main/TDeviceInfoWidget.cpp" line="84"/>
        <source>Used:%1%2,Total:%3%4</source>
        <translation>已用%1%2，总共%3%4</translation>
    </message>
</context>
<context>
    <name>TDeviceTabWidget</name>
    <message>
        <location filename="src/view/main/TDeviceTabWidget.cpp" line="73"/>
        <source>  </source>
        <translation> </translation>
    </message>
    <message>
        <location filename="src/view/main/TDeviceTabWidget.cpp" line="73"/>
        <source>Book Name</source>
        <translation>书名</translation>
    </message>
    <message>
        <location filename="src/view/main/TDeviceTabWidget.cpp" line="73"/>
        <source>Publisher Name</source>
        <translation>出版社</translation>
    </message>
    <message>
        <location filename="src/view/main/TDeviceTabWidget.cpp" line="73"/>
        <source>Format</source>
        <translation>文件类型</translation>
    </message>
    <message>
        <location filename="src/view/main/TDeviceTabWidget.cpp" line="73"/>
        <source>Download Time</source>
        <translation>下载时间</translation>
    </message>
    <message>
        <location filename="src/view/main/TDeviceTabWidget.cpp" line="73"/>
        <source>File Size</source>
        <translation>大小</translation>
    </message>
    <message>
        <location filename="src/view/main/TDeviceTabWidget.cpp" line="73"/>
        <source>Operate</source>
        <translation>操作</translation>
    </message>
    <message>
        <location filename="src/view/main/TDeviceTabWidget.cpp" line="187"/>
        <source>delete mpr files</source>
        <translation>删除文件</translation>
    </message>
    <message>
        <location filename="src/view/main/TDeviceTabWidget.cpp" line="187"/>
        <source>Are you sure to delete this MPF file?</source>
        <translation>您是否确认删除MPR文件？</translation>
    </message>
</context>
<context>
    <name>THeadWidget</name>
    <message>
        <location filename="src/view/main/THeadWidget.cpp" line="23"/>
        <source>Setting</source>
        <translation>设置</translation>
    </message>
    <message>
        <location filename="src/view/main/THeadWidget.cpp" line="27"/>
        <source>Version Info</source>
        <translation>版本</translation>
    </message>
    <message>
        <location filename="src/view/main/THeadWidget.cpp" line="32"/>
        <source>minimumSize</source>
        <translation>最小化</translation>
    </message>
    <message>
        <location filename="src/view/main/THeadWidget.cpp" line="36"/>
        <source>close</source>
        <translation>关闭</translation>
    </message>
</context>
<context>
    <name>TLeftTabWidget</name>
    <message>
        <location filename="src/view/main/TLeftTabWidget.cpp" line="20"/>
        <source>My Device</source>
        <translation>我的设备</translation>
    </message>
    <message>
        <location filename="src/view/main/TLeftTabWidget.cpp" line="23"/>
        <source>My Task</source>
        <translation>任务列表</translation>
    </message>
</context>
<context>
    <name>TNetMPRSearcher</name>
    <message>
        <location filename="src/model/network/TNetMPRSearcher.cpp" line="186"/>
        <location filename="src/model/network/TNetMPRSearcher.cpp" line="193"/>
        <source>download data error!</source>
        <translation>下载数据错误！</translation>
    </message>
</context>
<context>
    <name>TPageNaviBar</name>
    <message>
        <location filename="src/view/main/TPageNaviBar.cpp" line="28"/>
        <source>Previous Page</source>
        <translation>前一页</translation>
    </message>
    <message>
        <location filename="src/view/main/TPageNaviBar.cpp" line="74"/>
        <source>Next Page</source>
        <translation>下一页</translation>
    </message>
    <message>
        <location filename="src/view/main/TPageNaviBar.cpp" line="94"/>
        <source>Page</source>
        <translation>页</translation>
    </message>
    <message>
        <location filename="src/view/main/TPageNaviBar.cpp" line="334"/>
        <source>Total %1 pages / Total %2 records, To</source>
        <translation>共%1页/共%2条记录,跳转</translation>
    </message>
</context>
<context>
    <name>TPopupDialog</name>
    <message>
        <location filename="src/view/main/TPopupDialog.cpp" line="43"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="src/view/main/TPopupDialog.cpp" line="97"/>
        <source>Ok</source>
        <translation>确认</translation>
    </message>
    <message>
        <location filename="src/view/main/TPopupDialog.cpp" line="104"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="src/view/main/TPopupDialog.cpp" line="110"/>
        <source>Yes</source>
        <translation>是</translation>
    </message>
    <message>
        <location filename="src/view/main/TPopupDialog.cpp" line="117"/>
        <source>No</source>
        <translation>否</translation>
    </message>
</context>
<context>
    <name>TSystemSettingDlg</name>
    <message>
        <location filename="src/view/main/TSystemSettingDlg.cpp" line="53"/>
        <source>select save directory</source>
        <oldsource>select import directory</oldsource>
        <translation>缓存路径</translation>
    </message>
    <message>
        <location filename="src/view/main/TSystemSettingDlg.cpp" line="72"/>
        <source>setting</source>
        <translation>设置</translation>
    </message>
    <message>
        <location filename="src/view/main/TSystemSettingDlg.cpp" line="80"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="src/view/main/TSystemSettingDlg.cpp" line="99"/>
        <source>browse</source>
        <translation>浏览</translation>
    </message>
    <message>
        <location filename="src/view/main/TSystemSettingDlg.cpp" line="100"/>
        <source>save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="src/view/main/TSystemSettingDlg.cpp" line="101"/>
        <source>cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="src/view/main/TSystemSettingDlg.cpp" line="118"/>
        <source>save path:</source>
        <translation>本地存储路径：</translation>
    </message>
    <message>
        <location filename="src/view/main/TSystemSettingDlg.cpp" line="128"/>
        <source>It does effective when next start software!</source>
        <translation>软件重启后生效！</translation>
    </message>
</context>
<context>
    <name>TTaskTabWidget</name>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="33"/>
        <source>Warm prompt: Using USB data lin to connet device with computer first, and make computer staying connected to the Internet.
 Then your can download mpr files by touch mpr book using touch and talk pen!</source>
        <translation>温馨提示：请通过USB数据线将识读器连接到电脑，保持电脑接入互联网，使用识读器点击图书中发声的区域开始声音文件下载</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="38"/>
        <source>There is no download task, please touch reading the MPR book</source>
        <translation>暂无下载任务，请点读MPR出版物下载</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="77"/>
        <source>  </source>
        <translation> </translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="77"/>
        <source>Book Name</source>
        <translation>书名</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="77"/>
        <source>Publisher Name</source>
        <translation>出版社</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="77"/>
        <source>Format</source>
        <translation>格式</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="77"/>
        <source>Status</source>
        <translation>状态</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="77"/>
        <source>Operate</source>
        <translation>操作</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="253"/>
        <source>start</source>
        <translation>开始</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="254"/>
        <source>pause</source>
        <translation>暂停</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="255"/>
        <source>delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="341"/>
        <source>Prepare</source>
        <translation>准备下载</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="347"/>
        <source>Download</source>
        <translation>下载中</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="353"/>
        <source>Pause</source>
        <translation>暂停中</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="359"/>
        <source>Copyfile</source>
        <translation>拷贝中</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="368"/>
        <source>Finished</source>
        <translation>已完成</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="376"/>
        <source>Not Enough Space</source>
        <translation>空间不足</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="380"/>
        <source>Copy File Failed</source>
        <translation>拷贝失败</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="384"/>
        <source>Download File Failed</source>
        <translation>下载失败</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="470"/>
        <source>file download error</source>
        <translation>下载失败</translation>
    </message>
</context>
<context>
    <name>TUpdateVersionManager</name>
    <message>
        <location filename="src/control/TUpdateVersionManager.cpp" line="200"/>
        <source>Prompt</source>
        <translation>升级</translation>
    </message>
    <message>
        <location filename="src/control/TUpdateVersionManager.cpp" line="200"/>
        <source>Firmware update success,please pull out the device and wait to reboot</source>
        <translation>软件升级成功，请拔出设备，等待设备重启</translation>
    </message>
    <message>
        <location filename="src/control/TUpdateVersionManager.cpp" line="184"/>
        <location filename="src/control/TUpdateVersionManager.cpp" line="210"/>
        <source>Firmware upgrade failure</source>
        <translation>硬件升级失败</translation>
    </message>
    <message>
        <location filename="src/control/TUpdateVersionManager.cpp" line="210"/>
        <source>Upgrade package download failed</source>
        <translation>硬件升级包下载失败</translation>
    </message>
    <message>
        <location filename="src/control/TUpdateVersionManager.cpp" line="185"/>
        <location filename="src/control/TUpdateVersionManager.cpp" line="211"/>
        <source>Retry</source>
        <translation>重试</translation>
    </message>
    <message>
        <location filename="src/control/TUpdateVersionManager.cpp" line="184"/>
        <source>Upgrade package copy failed</source>
        <translation>硬件升级包拷贝失败</translation>
    </message>
    <message>
        <location filename="src/control/TUpdateVersionManager.cpp" line="234"/>
        <source>file is downloading, do not pull out the device</source>
        <translation>正在下载新版固件，请勿断开连接</translation>
    </message>
    <message>
        <location filename="src/control/TUpdateVersionManager.cpp" line="342"/>
        <location filename="src/control/TUpdateVersionManager.cpp" line="351"/>
        <source>Firmware upgrade tips</source>
        <translation>硬件升级</translation>
    </message>
    <message>
        <location filename="src/control/TUpdateVersionManager.cpp" line="342"/>
        <source>Reading pen firmware exist new version, whether to download upgrade?</source>
        <translation>设备存在硬件升级版本，是否升级？</translation>
    </message>
    <message>
        <location filename="src/control/TUpdateVersionManager.cpp" line="351"/>
        <source>current firmwarevision is incompaticatable, you need to download upgrade!</source>
        <translation>当前硬件版本不兼容，请升级！</translation>
    </message>
</context>
<context>
    <name>TWaittingDlg</name>
    <message>
        <location filename="src/view/main/TWaittingDlg.cpp" line="87"/>
        <source>Info</source>
        <translation>提示</translation>
    </message>
</context>
<context>
    <name>TWebServiceInterface</name>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="669"/>
        <source>Success</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="672"/>
        <source>Parameter error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="675"/>
        <source>Equipment loss</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="678"/>
        <source>Not identify user</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="681"/>
        <source>Failed to get ticket</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="684"/>
        <source>Please connect equipment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="687"/>
        <source>Resource was not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="690"/>
        <source>Account inactive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="693"/>
        <source>Account exception</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="696"/>
        <source>Account does not exist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="699"/>
        <source>Account or password error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="702"/>
        <source>Equipment account binding relationship does not exist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="705"/>
        <source>Equipment abnormal binding</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="708"/>
        <source>Equipment has bound</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="711"/>
        <source>Equipment wasn&apos;t register</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="714"/>
        <source>Inconsistent binding account login account and equipment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="717"/>
        <source>The device did not buy the goods</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="720"/>
        <source>No purchase record fee content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="723"/>
        <source>Equipment validation failure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="726"/>
        <source>A system exception</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="1248"/>
        <source>Failure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="1248"/>
        <source>The search timeout, please try again</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
