﻿@echo off
::警告 请不要任意改动和编辑代码，如有配置问题，请进入到脚本_project_setting.bat中进行设定

echo.[Loading Settings] ......
    set local=%~dp0
    call "%local%../_project_setting.bat"
echo. 

echo.[Translate zh_CN] ......
    cd /d "%ProClientDir%"
    set Translate_zh_CN=%ProName%_zh_CN
    lupdate "%ProName%.pro" -ts "%Translate_zh_CN%.ts"
    lrelease "%Translate_zh_CN%.ts"
echo. 

echo.[Translate en_EN] ......
    set Translate_en_EN=%ProName%_en_EN
    lupdate "%ProName%.pro" -ts "%Translate_en_EN%.ts"
    lrelease "%Translate_en_EN%.ts"
echo. 

echo.[Copy Files] ......
    echo. copy "%Translate_zh_CN%.qm"
        copy "%Translate_zh_CN%.qm" ".\bin\debug_x86\%Translate_zh_CN%.qm"
	copy "%Translate_zh_CN%.qm" ".\bin\release_x86\%Translate_zh_CN%.qm"
	::copy "%Translate_zh_CN%.qm" ".\bin\debug_x64\%Translate_zh_CN%.qm"
	::copy "%Translate_zh_CN%.qm" ".\bin\release_x64\%Translate_zh_CN%.qm"

    echo. copy "%Translate_en_EN%.qm"
        copy "%Translate_en_EN%.qm" ".\bin\debug_x86\%Translate_en_EN%.qm"
        copy "%Translate_en_EN%.qm" ".\bin\release_x86\%Translate_en_EN%.qm"
        ::copy "%Translate_en_EN%.qm" ".\bin\debug_x64\%Translate_en_EN%.qm"
        ::copy "%Translate_en_EN%.qm" ".\bin\release_x64\%Translate_en_EN%.qm"
echo.

echo.[Delete Files] ......
    del "%Translate_zh_CN%.qm"
    del "%Translate_en_EN%.qm"
echo.

if "%Package%" EQU "true" (exit /B 1)
if "%Generate%" EQU "true" (exit  /B 1)
pause