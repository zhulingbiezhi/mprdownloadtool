#ifndef _TCOPYFILETASK_H_
#define _TCOPYFILETASK_H_

#include "TCopyFileState.h"

class TFileScopedCloser
{
public:
    TFileScopedCloser(QFile* pFile);
    ~TFileScopedCloser();

private:
    QFile* mpFile;
};

class TCopyFileTask : public QObject
{
    Q_OBJECT

public:
    TCopyFileTask(const QString& srcPath, const QString& dstPath);
    ~TCopyFileTask();

    void Stop();
    bool IsStopRequsted();

    bool CheckDiskSize();

    QString GetSrcPath() const;
    QString GetDstPath() const;

public slots:
    void Start();
    
signals:
    void CopyProgressChanged(qint64 bytesCopied, qint64 bytesTotal);
    void CopyStateChanged(CopyFileState);

private:
    QString mSrcPath;
    QString mDstPath;
    QString mTempDstPath;
    QFile*  mpSrcFile;
    QFile*  mpDstFile;
    qint64              mnByteCopied;
    volatile bool       mbStop;
};

#endif