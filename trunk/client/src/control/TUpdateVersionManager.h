﻿#ifndef _TUPDATEVERSIONMANAGER_H_
#define _TUPDATEVERSIONMANAGER_H_
#include "TWebServiceInterface.h"
#include "TCopyFileState.h"

class TMPRDeviceDesc;
class TLocalSocket;
class TPopupDialog;
class TWaittingDlg;
class TCopyFileTask;
class TUpdateVersionManager : public QObject
{
    Q_OBJECT
public:
    enum TUpdateType
    {
        TSoftware_Update,
        TFirmware_Update
    };
    enum ReturnCode
    {
        ExistNewVer = 0,
        NoExistNewVer = 1
    };
    explicit TUpdateVersionManager(QObject *parent = 0);
    ~TUpdateVersionManager();

    void CheckFirmwareVersion(TMPRDeviceDesc desc);
    void CheckSPIFirmwareVersion(QString strFirmwareInfo, QString strRootDir);
    void CloseWaittingDlg();

    static TUpdateVersionManager& Instance();

private:
    void CopyUpdatefileToFirmware();
    void FirmwareDownloadError();
    void NewUpdatePackageDownload(TUpdateType type);
    void CreateUpdateProcess(TUpdateType type);
    void UpdateDownloadPrompt(TUpdateType type, const TFetchLatestVersionResponse &resData);
    QString GetLocalFirmwareUpdatePackageName();



public slots:
    void OnReadUpdateServerMessSlot(const QString& text);
    void OnCopyUpdataPackageStateChange(CopyFileState);

private slots:
    void OnFetchFirmwareLatestVerDone(QObject *requester, const TFetchLatestVersionResponse &resData, TWebServiceInterface::ResponseCode code);

private:

    QString         mstrUpdateFileName;
    QString         mstrFirmwarePackageDir;
    QString         mstrDeviceRootDir;
    QString         mstrFirmwareDeviceTempPath;
    QString         mstrFirmwareDevicePath;
    QString         mstrUpdateExePath;
    TLocalSocket    *mpFirmwareSocket;
    TWaittingDlg*   mpWaitDlg;
    TWebServiceInterface*       mpServiceInterFace;
    TCopyFileTask*              mpCurrentTask;
    TFetchLatestVersionResponse mFirmwareResData;

};


#endif
