﻿#include "TUpdateVersionManager.h"
#include "TMPRDeviceDesc.h"
#include "TLocalSocket.h"
#include "TAppDataDirManager.h"
#include "TAppCommon.h"
#include "TCopyFileTask.h"
#include "TCopyFileManager.h"
#include "TPopupDialog.h"
#include "TWaittingDlg.h"

#ifdef Q_OS_WIN
#include <Windows.h>
#endif


#if _MSC_VER >= 1600
#pragma execution_character_set("utf-8")
#endif
#ifdef UNICODE
#define QStringToTCHAR(x)     (const wchar_t*)x.utf16()
#else
#define QStringToTCHAR(x)     x.local8Bit().constData()
#endif

#define SPI_DEVICE_NAME "MPR-1026SPI"
#define SPI_DEVICE_TYPE "1"

TUpdateVersionManager::TUpdateVersionManager(QObject *parent) 
    : QObject(parent)
    , mpFirmwareSocket(NULL)
    , mpServiceInterFace(NULL)
    , mstrDeviceRootDir("")
    , mstrFirmwarePackageDir("")
    , mstrUpdateFileName("")
    , mstrFirmwareDevicePath("")
    , mstrFirmwareDeviceTempPath("")
    , mstrUpdateExePath("")
    , mpWaitDlg(NULL)
    , mpCurrentTask(NULL)
{
    mpServiceInterFace = new TWebServiceInterface();
    connect(mpServiceInterFace, &TWebServiceInterface::fetchFirmwareLatestVerDone, this, &TUpdateVersionManager::OnFetchFirmwareLatestVerDone);
    QDir dir;
    dir.setCurrent(QApplication::applicationDirPath());
#ifdef QT_NO_DEBUG
    mstrUpdateExePath = dir.absoluteFilePath("update.exe");
#else
    mstrUpdateExePath = dir.absoluteFilePath("updated.exe");
#endif

}

TUpdateVersionManager::~TUpdateVersionManager()
{
    if (mpServiceInterFace)
    {
        delete mpServiceInterFace;
        mpServiceInterFace = NULL;
    }
}

void TUpdateVersionManager::CheckFirmwareVersion(TMPRDeviceDesc desc)
{
    if (desc.mDeviceSN.isEmpty())
    {
        return;
    }

    if (mpFirmwareSocket == NULL)
    {
        mpFirmwareSocket = new TLocalSocket("FirmwareUpdateServer", this);
    }

    if (!mpFirmwareSocket->ConnectToServer())
    {
        //检测版本
        TFetchFirmLatestVersionRequest request;
        request.firmwareVersion = desc.mFirmwareVersion;
        request.deviceModel = desc.mDeviceName;
        request.deviceType = QString::number(desc.mType, 10);
        mstrDeviceRootDir = desc.mDeviceMediaPath.left(desc.mDeviceMediaPath.lastIndexOf("\\"));
        mpServiceInterFace->fetchFirmwareLatestVer(this, request);
    }
    connect(mpFirmwareSocket, &TLocalSocket::readUpdateServerMess, this, &TUpdateVersionManager::OnReadUpdateServerMessSlot);
}

void TUpdateVersionManager::CheckSPIFirmwareVersion(QString strFirmwareInfo, QString strRootDir)
{
    if (strFirmwareInfo.isEmpty())
    {
        return;
    }

    if (mpFirmwareSocket == NULL)
    {
        mpFirmwareSocket = new TLocalSocket("FirmwareUpdateServer", this);
    }

    if (!mpFirmwareSocket->ConnectToServer())
    {
        //检测版本
        TFetchFirmLatestVersionRequest request;
        request.firmwareVersion = strFirmwareInfo;
        request.deviceModel = SPI_DEVICE_NAME;
        request.deviceType = SPI_DEVICE_TYPE;
        mstrDeviceRootDir = strRootDir;
        mpServiceInterFace->fetchFirmwareLatestVer(this, request);
    }
    connect(mpFirmwareSocket, &TLocalSocket::readUpdateServerMess, this, &TUpdateVersionManager::OnReadUpdateServerMessSlot);
}

void TUpdateVersionManager::OnReadUpdateServerMessSlot(const QString& text)
{
    if (text == "firmware success")
    {
        //复制文件到硬件中
        CopyUpdatefileToFirmware();
    }
    else if (text == "firmware error")
    {
        //出错处理
        CloseWaittingDlg();
        FirmwareDownloadError();
    }
}

void TUpdateVersionManager::CopyUpdatefileToFirmware()
{
    QString strDeviceUpdateDir = mstrDeviceRootDir + "\\update";
    QDir dir(strDeviceUpdateDir);
    if (!dir.exists())
    {
        QDir::current().mkpath(strDeviceUpdateDir);
    }
    else
    {
        dir.cd(strDeviceUpdateDir);
    }

    QString sLocalFirmwareUpdatePackageName = GetLocalFirmwareUpdatePackageName();
    if (sLocalFirmwareUpdatePackageName == NULL)
    {
        CloseWaittingDlg();
        qDebug() << __FUNCTION__ << "LocalFirmwareUpdatePackageName is empty";
        return;
    }

    mstrFirmwareDevicePath = strDeviceUpdateDir + "\\" + mstrUpdateFileName;
    mstrFirmwareDeviceTempPath = strDeviceUpdateDir + "\\" + sLocalFirmwareUpdatePackageName + ".tmp";
    QString strFwareUpdateFileLocalpath = mstrFirmwarePackageDir + "/" + sLocalFirmwareUpdatePackageName;

    //复制文件
    //if (mpCopyFileThread == NULL)
    //{
    //    mpCopyFileThread = new TCopyFileThread();
    //    connect(mpCopyFileThread, &TCopyFileThread::copyStarted, this, &TUpdateVersionManager::copyUpdatePackageStarted);
    //    connect(mpCopyFileThread, &TCopyFileThread::copyEnded, this, &TUpdateVersionManager::OnCopyUpdataPackageEnded);
    //}

    //if (!mpCopyFileThread->copyFile(FwareUpdateFileLocalpath, mFirmwareDeviceTempPath))
    //{
    //    qDebug() << __FUNCTION__ << "copy file error" << FwareUpdateFileLocalpath << mFirmwareDeviceTempPath;
    //    return;
    //}

    mpCurrentTask = TCopyFileManager::Instance().AddTask(strFwareUpdateFileLocalpath, mstrFirmwareDeviceTempPath);
    //connect(pTask, &TCopyFileTask::CopyProgressChanged, this, &TUpdateVersionManager::copyUpdatePackageStarted);
    connect(mpCurrentTask, &TCopyFileTask::CopyStateChanged, this, &TUpdateVersionManager::OnCopyUpdataPackageStateChange);
    TCopyFileManager::Instance().ActivateTask(mpCurrentTask, false);

}

void TUpdateVersionManager::OnCopyUpdataPackageStateChange(CopyFileState state)
{
    if (CSInit != state
        && CSWaitForCopy != state
        && CSCopying != state)
    {
        CloseWaittingDlg();
        TCopyFileManager::Instance().RemoveTask(static_cast<TCopyFileTask*>(sender()));
    }
    if (static_cast<TCopyFileTask*>(sender()) != mpCurrentTask)
    {
        return;
    }
    if (state & CSError)
    {
        QFile::remove(mstrFirmwareDeviceTempPath);
        TPopupDialog msg(TPopupDialog::Ok | TPopupDialog::Cancel, tr("Firmware upgrade failure"), tr("Upgrade package copy failed"), TAppCommon::Instance().GetMainWindow(), TPopupDialog::Error);
        msg.SetButtonText(TPopupDialog::Ok, tr("Retry"));
        msg.exec();
        if (msg.GetResult() == TPopupDialog::Ok)
        {
            NewUpdatePackageDownload(TFirmware_Update);
        }
    }
    else if (state == CSCopySucceeded)
    {
        if (QFile::exists(mstrFirmwareDevicePath))
        {
            QFile::remove(mstrFirmwareDevicePath);
        }
        if (!QFile::rename(mstrFirmwareDeviceTempPath, mstrFirmwareDevicePath))
        {
            qDebug() << __FUNCTION__ << "rename file error";
        }
        else
        {
            TPopupDialog msg(TPopupDialog::Ok, tr("Prompt"), tr("Firmware update success,please pull out the device and wait to reboot"), TAppCommon::Instance().GetMainWindow());
            msg.exec();
        }
    }


}

void TUpdateVersionManager::FirmwareDownloadError()
{
    TPopupDialog msg(TPopupDialog::Ok | TPopupDialog::Cancel, tr("Firmware upgrade failure"), tr("Upgrade package download failed"), TAppCommon::Instance().GetMainWindow(), TPopupDialog::Error);
    msg.SetButtonText(TPopupDialog::Ok, tr("Retry"));
    msg.exec();
    if (msg.GetResult() == TPopupDialog::Ok)
    {
        NewUpdatePackageDownload(TFirmware_Update);
    }
}

void TUpdateVersionManager::NewUpdatePackageDownload(TUpdateType type)
{
    CreateUpdateProcess(type);
    if (type == TSoftware_Update)
    {
        //mpSoftwareSocket->ConnectToServer();
    }
    else if (type == TFirmware_Update)
    {
        mpFirmwareSocket->ConnectToServer();
    }
    if (!mpWaitDlg)
    {
        mpWaitDlg = new TWaittingDlg(TAppCommon::Instance().GetMainWindow());
    }
    mpWaitDlg->ShowText(tr("file is downloading, do not pull out the device"));
}

QString TUpdateVersionManager::GetLocalFirmwareUpdatePackageName()
{
    QSettings configIniRead(mstrFirmwarePackageDir + "/firmwareupdate.ini", QSettings::IniFormat);
    QString strName = configIniRead.value("firmware/name").toString();
    return strName;
}

void TUpdateVersionManager::CreateUpdateProcess(TUpdateType type)
{
    STARTUPINFO si;
    memset(&si, 0, sizeof(STARTUPINFO));
    si.cb = sizeof(STARTUPINFO);
    si.dwFlags = STARTF_USESHOWWINDOW;
    si.wShowWindow = SW_SHOW;
    PROCESS_INFORMATION pi;
    TCHAR lpszCmdLine[_MAX_PATH * 2] = { 0 };
    QString arguments;
    if (type == TSoftware_Update)
    {
 /*       arguments = QString("%1 \"%2\" \"%3\" \"%4\" \"%5\" \"%6\" \"software\" ")
            .arg(mUpdateExePath).arg(mSoftwarePackageDir)
            .arg(mSoftwareResData.url).arg(mSoftwareResData.vCode)
            .arg(mSoftwareResData.desc).arg(mSoftwareResData.force);*/
    }
    else if (type == TFirmware_Update)
    {
        arguments = QString("%1 \"%2\" \"%3\" \"%4\" \"%5\" \"%6\" \"firmware\" ")
            .arg(mstrUpdateExePath).arg(mstrFirmwarePackageDir)
            .arg(mFirmwareResData.url).arg(mFirmwareResData.vCode)
            .arg(mFirmwareResData.desc).arg(mFirmwareResData.force);
    }
    qDebug() << __FUNCTION__ << arguments;
#ifdef UNICODE
    wcsncpy(lpszCmdLine, QStringToTCHAR(arguments), _MAX_PATH * 2 - 1);
#else
    strncpy(lpszCmdLine, QStringToTCHAR(arguments), _MAX_PATH * 2 - 1);
#endif
    if (!CreateProcess(NULL, lpszCmdLine, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi))
    {
        qDebug() << __FUNCTION__ << "CreateProcess Failure!";
        return;
    }

    // It's a good habit to close unused handle
    CloseHandle(pi.hThread);
    CloseHandle(pi.hProcess);
}

void TUpdateVersionManager::OnFetchFirmwareLatestVerDone(QObject *requester, const TFetchLatestVersionResponse &resData, TWebServiceInterface::ResponseCode code)
{
    Q_UNUSED(requester)
        Q_UNUSED(code)
        qDebug() << __FUNCTION__ << resData.rcode << resData.vCode << resData.url;

    if (resData.rcode.isEmpty())
    {
        return;
    }

    if (resData.rcode.toInt() == ExistNewVer)
    {
        QDir dir;
        mstrUpdateFileName = resData.fileName;
        dir.setCurrent(TAppDataDirManager::Instance().GetAppDataDir());
        if (!dir.cd("firmwareUpdate"))
        {
            if (dir.mkdir("firmwareUpdate"))
            {
                mstrFirmwarePackageDir = dir.absolutePath() + QDir::separator() + "firmwareUpdate";
                UpdateDownloadPrompt(TFirmware_Update, resData);
            }
            else
            {
                qDebug() << __FUNCTION__ << "mkdir firmwareUpdate fail.";
            }
        }
        else
        {
            mstrFirmwarePackageDir = dir.absolutePath();
            QString sLocalFirmwareUpdatePackageName = GetLocalFirmwareUpdatePackageName();
            dir.remove(sLocalFirmwareUpdatePackageName);
            dir.remove("firmwareupdate.ini");
            UpdateDownloadPrompt(TFirmware_Update, resData);
        }
    }
}

void TUpdateVersionManager::UpdateDownloadPrompt(TUpdateType type, const TFetchLatestVersionResponse &resData)
{
    if (type == TSoftware_Update)
    {
        //mSoftwareResData = resData;
        //mSoftwarePromptType = Download;
        //TPopupDialog msg(TPopupDialog::Yes | TPopupDialog::No, tr("Software upgrade tips"), tr("Software version exist new version, whether to download upgrade?"), mpMainWindow, TPopupDialog::Warning);
        //msg.exec();
        //if (msg.GetResult() == TPopupDialog::Yes)
        //{
        //SoftwarePromptDlg();
        //}
    }
    else if (type == TFirmware_Update)
    {
        mFirmwareResData = resData;
        if (mFirmwareResData.force == "0")
        {
            TPopupDialog msg(TPopupDialog::Yes | TPopupDialog::No, tr("Firmware upgrade tips"), tr("Reading pen firmware exist new version, whether to download upgrade?"), TAppCommon::Instance().GetMainWindow(), TPopupDialog::Warning);
            msg.exec();
            if (msg.GetResult() == TPopupDialog::Yes)
            {
                NewUpdatePackageDownload(TFirmware_Update);
            }
        }
        else
        {
            TPopupDialog msg(TPopupDialog::Yes, tr("Firmware upgrade tips"), tr("current firmwarevision is incompaticatable, you need to download upgrade!"), TAppCommon::Instance().GetMainWindow(), TPopupDialog::Warning);
            msg.exec();
            if (msg.GetResult() == TPopupDialog::Yes)
            {
                NewUpdatePackageDownload(TFirmware_Update);
            }
        }

    }
}

TUpdateVersionManager& TUpdateVersionManager::Instance()
{
    static TUpdateVersionManager obj;
    return obj;
}

void TUpdateVersionManager::CloseWaittingDlg()
{
    if (mpWaitDlg && mpWaitDlg->isVisible())
    {
        mpWaitDlg->Close();
    }
}


