#include "TSTDPreCompiledHeader.h"
#include "TLocalLibHandle.h"
#include "TFileLib.h"
#include "TWorkStateConfig.h"
#include "TGlobleCtl.h"
#include "TAppCommon.h"

TLocalLibHandle::TLocalLibHandle()   
{
    if (NULL != mFileLibLocal){
        delete mFileLibLocal;
        mFileLibLocal = NULL;
    }

//     TWorkStateConfig::Instance()->BeginGroup(DirSetting);
//     QString localFileLibDir = TWorkStateConfig::Instance()->GetValue(LOCAL_LIB_DIR, TAppCommon::Instance().GetDownloadSavePath()).toString();
//     TWorkStateConfig::Instance()->EndGroup();
    QString localFileLibDir = TAppCommon::Instance().GetDownloadSavePath();
    mFileLibLocal = new TFileLib(localFileLibDir);
}

TLocalLibHandle::~TLocalLibHandle()
{
}

TLocalLibHandle& TLocalLibHandle::Instance()
{
    static TLocalLibHandle ins;
    return ins;
}

TFileLib* TLocalLibHandle::GetLocalLib()
{
    return mFileLibLocal;
}

void TLocalLibHandle::ClearData()
{
    if (NULL != mFileLibLocal){
        delete mFileLibLocal;
        mFileLibLocal = NULL;
    }
}
