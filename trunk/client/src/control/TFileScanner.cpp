#include "TFileScanner.h"
#include "mprMimeDataFetcher.h"

TFileScanner::TFileScanner(char disk)
: mbStop(false)
, mcDisk(disk)
{
    mFileItemList.clear();
}

TFileScanner::~TFileScanner()
{

}

void TFileScanner::StartScanning()
{
    QDir dir;
    dir.setFilter(QDir::Files);
    QStringList nameFilters;
    nameFilters << "*.mpr";
    dir.setNameFilters(nameFilters);

    QString strMediaPath = QString("%1:/MPR").arg(mcDisk);
    dir.setPath(strMediaPath);
    QStringList fileList = dir.entryList();

    for each (QString fileName in fileList)
    {
        if (mbStop)
        {
            break;
        }
        QString strFilePath = strMediaPath + "/" + fileName;
        //获取MPR文件信息
        MPR_StdHeadInfo info;
        int nRet = getMPRStdHeadInfo(strFilePath.toStdWString().c_str(), &info);
        if (nRet >= 0)
        {
            TMPRFileInfoItem item;
            item.tBookName = QString::fromUtf16(info.bookName);
            item.tPublisher = QString::fromUtf16(info.publishing);
            item.tPrefixCode = QString("%1").arg(info.prefixNo, 10, 10, QChar('0'));
            item.tVersion = QString("%1").arg(info.langVersion, 5, 10, QChar('0'));;
            item.tFileType = FT_MPR;
            QFileInfo fileInfo(strFilePath);
            item.tFileSize = fileInfo.size();
            item.tFilePath = strFilePath;
            item.tLicencePath = strMediaPath+ "/" + fileInfo.completeBaseName() + ".lic";
            item.tFileCreateTime = fileInfo.created().toString("yyyy-MM-dd hh:mm:ss");
            mFileItemList.append(item);
        }
    } 
    emit finished(mcDisk);
}

bool TFileScanner::GetMPRFileInfo(QString filePath, QString& strVersion, QString& strPreCode)
{
    MPR_StdHeadInfo info;
    int nRet = getMPRStdHeadInfo(filePath.toStdWString().c_str(), &info);
    if (nRet >= 0)
    {     
        strPreCode = QString("%1").arg(info.prefixNo, 10, 10, QChar('0'));
        strVersion = QString("%1").arg(info.langVersion, 5, 10, QChar('0'));
    }
    return (nRet >= 0 ? true : false);
}
