#ifndef __TLOCALLIBHANDLE_H_
#define __TLOCALLIBHANDLE_H_

class TFileLib;
class TLocalLibHandle : public QObject
{
public:
    static  TLocalLibHandle& Instance();

    TFileLib*               GetLocalLib();
    void                    ClearData();

private:
    TLocalLibHandle();
    ~TLocalLibHandle();

private:
    TFileLib*           mFileLibLocal;
};


#endif //__TLOCALLIBHANDLE_H_