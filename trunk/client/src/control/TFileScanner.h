#ifndef _TFILESCANNER_H
#define _TFILESCANNER_H
#include "TFileEnum.h"
class TFileScanner : public QObject
{
    Q_OBJECT
public:
    TFileScanner(char disk);
    ~TFileScanner();

    void SetMediaPath(const QList<QString> list) { mstrMediaPathList = list; }
    void AddMediaPath(const QList<QString> list) { mstrMediaPathList.append(list); }
    void Stop() { mbStop = true; }
    const QList<TMPRFileInfoItem>& GetAllFiles() const { return mFileItemList;}

public slots:
    void StartScanning();

signals:
    void finished(char);

public: 
    static bool GetMPRFileInfo(QString filePath, QString& strVersion, QString& strPreCode);

private:
    bool               mbStop;
    char               mcDisk;
    QList<QString>     mstrMediaPathList;
    QList<TMPRFileInfoItem>   mFileItemList;

};


#endif