#include "TSTDPreCompiledHeader.h"
#include "TCopyFileTask.h"


TFileScopedCloser::TFileScopedCloser( QFile* pFile )
    : mpFile(pFile)
{
}

TFileScopedCloser::~TFileScopedCloser()
{
    if (mpFile->isOpen())
    {
        mpFile->close();
    }
}

TCopyFileTask::TCopyFileTask( const QString& srcPath, const QString& dstPath )
    : mSrcPath(srcPath)
    , mDstPath(dstPath)
    , mpSrcFile(0)
    , mpDstFile(0)
    , mnByteCopied(0)
    , mbStop(false)
{
    mpSrcFile = new QFile(mSrcPath, this);
    mTempDstPath = mDstPath + ".temp";
    mpDstFile = new QFile(mTempDstPath, this);
}

TCopyFileTask::~TCopyFileTask()
{
    Stop();
}

void TCopyFileTask::Start()
{
    if (mbStop)
    {
        emit CopyStateChanged(CSInterrupted);
        return;
    }

    qint64 totalSize = 0;
    CopyFileState state = CSWaitForCopy;

    { //加个作用域,自动关闭文件
        if (QFile::exists(mDstPath))
        {
            if (0 == mnByteCopied || QFileInfo(mDstPath).size() != mnByteCopied)
            {
                if (!QFile::remove(mDstPath))
                {
                    qDebug() << "CopyError" << __FUNCTION__ << CSOpenFileError;
                    emit CopyStateChanged(CSOpenFileError);
                    return;
                }
                mnByteCopied = 0;
            }
        }

        if (!mpSrcFile->isOpen() && !mpSrcFile->open(QFile::ReadOnly))
        {
            qDebug() << "CopyError" << __FUNCTION__ << CSOpenFileError;
            emit CopyStateChanged(CSOpenFileError);
            return;
        }
        TFileScopedCloser srcFileCloser(mpSrcFile);

        if (!mpDstFile->isOpen() && !mpDstFile->open(QFile::ReadWrite | QFile::Append))
        {
            qDebug() << "CopyError" << __FUNCTION__ << CSOpenFileError;
            emit CopyStateChanged(CSOpenFileError);
            return;
        }
        TFileScopedCloser dstFileCloser(mpDstFile);

        emit CopyStateChanged(CSCopying);
        totalSize = mpSrcFile->size();

        mpSrcFile->seek(mnByteCopied);

        qint64 last_BytesCopied= -1;

        while (mnByteCopied < totalSize)
        {
            int block = 1024 * 512;
            if (block > totalSize - mnByteCopied)
                block = totalSize - mnByteCopied;

            mnByteCopied += mpDstFile->write(mpSrcFile->read(block));

            if (last_BytesCopied == mnByteCopied) //防止设备拔出后死循环
            { 
                mnByteCopied = 0;
                mbStop = true;
            }
            last_BytesCopied = mnByteCopied;

            if (mbStop)
            {
                state = CSInterrupted;
                if (mpDstFile->isOpen())
                    mpDstFile->close();
                mpDstFile->remove();
                break;
            }
            emit CopyProgressChanged(mnByteCopied, totalSize);
        }
    }

    if (mnByteCopied >= totalSize) {
        if(QFile::exists(mDstPath))
            QFile::remove(mDstPath);
        mpDstFile->close();
        mpDstFile->rename(mDstPath);
        state = CSCopySucceeded;
    }

    emit CopyStateChanged(state);

    if (state!= CSCopySucceeded){
    }
}

void TCopyFileTask::Stop()
{
    mbStop = true;
}

bool TCopyFileTask::CheckDiskSize()
{
    QString DstDiskName = mDstPath.left(3);

    LPCWSTR lpcwstrDriver = (LPCWSTR)DstDiskName.utf16();

    ULARGE_INTEGER liFreeBytesAvailable, liTotalBytes, liTotalFreeBytes;

    if(!GetDiskFreeSpaceEx(lpcwstrDriver, &liFreeBytesAvailable, &liTotalBytes, &liTotalFreeBytes))  
    {
        return false;
    }
    return (quint64)mpSrcFile->size() < liTotalFreeBytes.QuadPart;
}

QString TCopyFileTask::GetSrcPath() const
{
    return mSrcPath;
}

QString TCopyFileTask::GetDstPath() const
{
    return mDstPath;
}

bool TCopyFileTask::IsStopRequsted()
{
    return mbStop;
}

