#ifndef _TCOPYFILESTATE_H_
#define _TCOPYFILESTATE_H_

enum CopyFileState
{
    CSNone = 0x0000,
    CSInit = 0x0001,
    CSWaitForCopy = 0x0002,
    CSCopying = 0x0004,
    CSCopySucceeded = 0x0008,

    CSInterrupted = 0x0010,
    CSOpenFileError = 0x0020,
    CSCheckDiskSpace = 0x0040,
    CSNotEnoughDiskSpace = 0x0080,
    CSDeviceNotExist = 0x0100,

    CSError = CSInterrupted | CSOpenFileError | CSCheckDiskSpace | CSNotEnoughDiskSpace | CSDeviceNotExist,
};
Q_DECLARE_METATYPE(CopyFileState)

#endif