#include "TSTDPreCompiledHeader.h"
#include "TMPRDeviceDescFileReader.h"
#include "MPRDeviceEnum.h"


TMPRDeviceDescFileReader::TMPRDeviceDescFileReader(const QString& fielPath)
    : mFilePath(fielPath)
{
    ParseFile();
}

TMPRDeviceDescFileReader::~TMPRDeviceDescFileReader()
{

}

TMPRDeviceDesc TMPRDeviceDescFileReader::GetDeviceDesc()
{
    return mDesc;
}

void TMPRDeviceDescFileReader::ParseFile()
{
    QSettings setting(mFilePath, QSettings::IniFormat);
    setting.setIniCodec(QTextCodec::codecForLocale());

    QString deviceType  = setting.value("DeviceDesc/deviceType").toString();
    QString deviceSn    = setting.value("DeviceDesc/deviceSn").toString();
    QString deviceModel = setting.value("DeviceDesc/deviceModel").toString();
    QString brandId     = setting.value("DeviceDesc/brandId").toString();
    QString deviceUid   = setting.value("DeviceDesc/deviceUid").toString();
    QString version     = setting.value("DeviceDesc/version").toString();
    QString mediaFormat = setting.value("DeviceDesc/mediaFormat").toString();
    QString symmetrical = setting.value("DeviceDesc/symmetrical").toString();
    QString asymmetric  = setting.value("DeviceDesc/asymmetric").toString();

    if ("MPRDevice_PenReader" == deviceType) {
        mDesc.mType = TMPRDeviceDesc::MPRDevice_PenReader;
    }
    mDesc.mDeviceSN = deviceSn;
    mDesc.mDeviceName = deviceModel;
    mDesc.mBrandId = brandId.toLatin1();
    mDesc.mUuid = deviceUid.toLatin1();
    mDesc.mFirmwareVersion = version;

    if (mediaFormat.compare("OGG", Qt::CaseInsensitive) == 0) {
        mDesc.mSupportMediaFormat = OGG;
    }
    else {
        mDesc.mSupportMediaFormat = MP3;
    }

    if (symmetrical.compare("AES_ECB", Qt::CaseInsensitive) == 0) {
        mDesc.mSupportEncryptType = AES_ECB;
    }
    else if (symmetrical.compare("AES_CBC", Qt::CaseInsensitive) == 0) {
        mDesc.mSupportEncryptType = AES_CBC;
    }
    else if (symmetrical.compare("AES_CTR", Qt::CaseInsensitive) == 0) {
        mDesc.mSupportEncryptType = AES_CTR;
    }
    else if (symmetrical.compare("SM4_ECB", Qt::CaseInsensitive) == 0) {
        mDesc.mSupportEncryptType = SM4_ECB;
    }
    else if (symmetrical.compare("SM4_CBC", Qt::CaseInsensitive) == 0) {
        mDesc.mSupportEncryptType = SM4_CBC;
    }
    else if (symmetrical.compare("SM4_CTR", Qt::CaseInsensitive) == 0) {
        mDesc.mSupportEncryptType = SM4_CTR;
    }

    mDesc.mDeviceId = mDesc.mBrandId + mDesc.mUuid;
}

bool TMPRDeviceDescFileReader::IsValid() const
{
    return !mDesc.mDeviceId.isEmpty();
}

