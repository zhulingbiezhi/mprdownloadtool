#include "TMPRDeviceInfo.h"

void TMPRDeviceInfo::ParserDeviceDsc( const unsigned char * inBuf, MPRDeviceDesc& rDevDesc )
{
    memset(&rDevDesc, 0x00, sizeof(MPRDeviceDesc));    
    int dev_type_pos                  = inBuf[ 4] + inBuf[ 4 + 1] * 0x100;
    int dev_type_len                  = inBuf[ 6] + inBuf[ 6 + 1] * 0x100;
    int dev_sn_pos                    = inBuf[ 8] + inBuf[ 8 + 1] * 0x100;
    int dev_sn_len                    = inBuf[10] + inBuf[10 + 1] * 0x100;
    int dev_name_pos                  = inBuf[12] + inBuf[12 + 1] * 0x100;
    int dev_name_len                  = inBuf[14] + inBuf[14 + 1] * 0x100;
    int dev_brand_id_pos              = inBuf[16] + inBuf[16 + 1] * 0x100;
    int dev_brand_id_len              = inBuf[18] + inBuf[18 + 1] * 0x100;
    int dev_uuid_pos                  = inBuf[20] + inBuf[20 + 1] * 0x100;
    int dev_uuid_len                  = inBuf[22] + inBuf[22 + 1] * 0x100;
    int dev_firmware_version_pos      = inBuf[24] + inBuf[24 + 1] * 0x100;
    int dev_firmware_version_len      = inBuf[26] + inBuf[26 + 1] * 0x100;
    int dev_media_path_pos            = inBuf[28] + inBuf[28 + 1] * 0x100;
    int dev_media_path_len            = inBuf[30] + inBuf[30 + 1] * 0x100;
    int dev_audio_format_pos          = inBuf[32] + inBuf[32 + 1] * 0x100;
    int dev_audio_format_len          = inBuf[34] + inBuf[34 + 1] * 0x100;
    int dev_encrypt_algorithm_pos     = inBuf[36] + inBuf[36 + 1] * 0x100;
    int dev_encrypt_algorithm_len     = inBuf[38] + inBuf[38 + 1] * 0x100;
    int dev_pki_algorithm_pos         = inBuf[40] + inBuf[40 + 1] * 0x100;
    int dev_pki_algorithm_len         = inBuf[42] + inBuf[42 + 1] * 0x100;

    if(dev_type_pos && dev_type_len){
        if(!strcmp((char *)(inBuf + dev_type_pos), "MPRDevice_PenReader")){
            rDevDesc.dev_type = MPRDevice_PenReader;
        }
        else if(!strcmp((char *)(inBuf + dev_type_pos), "MPRDevice_BookDisk")){
            rDevDesc.dev_type = MPRDevice_BookDisk;
        }
    }

    if(dev_sn_pos && dev_sn_len){
        memcpy(rDevDesc.dev_sn, inBuf + dev_sn_pos, dev_sn_len);
    }

    if(dev_name_pos && dev_name_len){
        memcpy(rDevDesc.dev_name, inBuf + dev_name_pos, dev_name_len);
    }

    if(dev_brand_id_pos && dev_brand_id_len){
        rDevDesc.dev_brand_id = (inBuf[dev_brand_id_pos + 0] << 24) + (inBuf[dev_brand_id_pos + 1] << 16) + (inBuf[dev_brand_id_pos + 2] << 8) + (inBuf[dev_brand_id_pos + 3]);
    }

    if(dev_uuid_pos && dev_uuid_len){
        memcpy(rDevDesc.dev_uuid, inBuf + dev_uuid_pos, dev_uuid_len);
    }

    if(dev_firmware_version_pos && dev_firmware_version_len){
        memcpy(rDevDesc.dev_firmware_version, inBuf + dev_firmware_version_pos, dev_firmware_version_len);
    }

    if(dev_media_path_pos && dev_media_path_len){
        memcpy(rDevDesc.dev_media_path,  inBuf + dev_media_path_pos, dev_media_path_len);
    }

    if(dev_audio_format_pos && dev_audio_format_len){
        if(!strcmp((char *)(inBuf + dev_audio_format_pos), "WAV")){
            rDevDesc.dev_audio_format = WAV;
        }     
        else if(!strcmp((char *)(inBuf + dev_audio_format_pos), "MP3")){
            rDevDesc.dev_audio_format = MP3;
        }
        else if(!strcmp((char *)(inBuf + dev_audio_format_pos), "OGG")){
            rDevDesc.dev_audio_format = OGG;
        }
    }

    if(dev_encrypt_algorithm_pos && dev_encrypt_algorithm_len){
        if(!strcmp((char *)(inBuf + dev_encrypt_algorithm_pos), "AES_ECB")){
            rDevDesc.dev_encrypt_algorithm = AES_ECB;
        }
        else if(!strcmp((char *)(inBuf + dev_encrypt_algorithm_pos), "AES_CBC")){
            rDevDesc.dev_encrypt_algorithm = AES_CBC;
        }
        else if(!strcmp((char *)(inBuf + dev_encrypt_algorithm_pos), "AES_CTR")){
            rDevDesc.dev_encrypt_algorithm = AES_CTR;
        }   
        else if(!strcmp((char *)(inBuf + dev_encrypt_algorithm_pos), "SM4_ECB")){
            rDevDesc.dev_encrypt_algorithm = SM4_ECB;
        } 
        else if(!strcmp((char *)(inBuf + dev_encrypt_algorithm_pos), "SM4_CBC")){
            rDevDesc.dev_encrypt_algorithm = SM4_CBC;
        } 
        else if(!strcmp((char *)(inBuf + dev_encrypt_algorithm_pos), "SM4_CTR")){
            rDevDesc.dev_encrypt_algorithm = SM4_CTR;
        } 
    }

    if(dev_pki_algorithm_pos && dev_pki_algorithm_len){
        if(!strcmp((char *)(inBuf + dev_pki_algorithm_pos), "RSA")){
            rDevDesc.dev_pki_algorithm = RSA;
        }
        if(!strcmp((char *)(inBuf + dev_pki_algorithm_pos), "SM2")){
            rDevDesc.dev_pki_algorithm = SM2;
        }
    }
}


bool TMPRDeviceInfo::HexToAscii(string hexData, char* asciiData)
{
    if(hexData.length() % 2 != 0 && hexData.length() > 0)
    {
        return false;
    }
    size_t i = 0;
    char c = 0;
    for(;i < hexData.length(); ++i)
    {
        if('0' <= hexData[i] && '9' >= hexData[i])
        {
            if(i % 2 == 0)
            {
                c += (hexData[i] - '0') << 4; 
            }
            else
            {
                c += (hexData[i] - '0');
            }
        }
        else if('a' <= hexData[i] && 'f' >= hexData[i])
        {
            if(i % 2 == 0)
            {
                c += (hexData[i] - 'a' + 10) << 4; 
            }
            else
            {
                c += (hexData[i] - 'a' + 10);
            }
        }
        else if('A' <= hexData[i] && 'F' >= hexData[i])
        {
            if(i % 2 == 0)
            {
                c += (hexData[i] - 'A' + 10) << 4; 
            }
            else
            {
                c += (hexData[i] - 'A' + 10);
            }
        }
        else
        {
            return false;
        }
        if(i % 2 != 0)
        {
            asciiData[i / 2] = c;
            c = 0;
        }

    }
    return true;
}

