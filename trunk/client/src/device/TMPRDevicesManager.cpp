#include "TSTDPreCompiledHeader.h"
#include "TMPRDevicesManager.h"
#include "TMPRDeviceUSBInfo.h"
#include "TMPRDeviceUSBComm.h"
#include "MPRDeviceCmd.h"
#include "TMPRDeviceCommData.h"
#include "TMPRDeviceHandler.h"
#include <Dbt.h>
#include "TDefaultMPRDeviceManager.h"
#include "TMPRDeviceDescFileReader.h"

TMPRDevicesManager::TMPRDevicesManager()
{
   mpCaptureCodeTimer = new QTimer(this);
   mpCaptureCodeTimer->setInterval(300);
   connect(mpCaptureCodeTimer, SIGNAL(timeout()), this, SLOT(OnCaptureCodeTimeout()));
   mpCodeCaptureThread = new QThread(this);
}

TMPRDevicesManager::~TMPRDevicesManager()
{
}

TMPRDevicesManager& TMPRDevicesManager::Instance()
{
    static TMPRDevicesManager ins;
    return ins;
}

void TMPRDevicesManager::HandleDeviceChanged(MSG* msg)
{
    if (msg->wParam == DBT_DEVICEARRIVAL ||
        msg->wParam == DBT_DEVICEREMOVECOMPLETE)
    {
        PDEV_BROADCAST_HDR pDevBroadcastHdr = 0; 
        PDEV_BROADCAST_VOLUME pDevBroadcastVolume = 0;
        char disk;

        pDevBroadcastHdr = (PDEV_BROADCAST_HDR)msg->lParam; 
        if (pDevBroadcastHdr->dbch_devicetype == DBT_DEVTYP_VOLUME) 
        { 
            pDevBroadcastVolume = (PDEV_BROADCAST_VOLUME)pDevBroadcastHdr;
            for (unsigned char i = 0; i < 26; i++)
            {
                if (pDevBroadcastVolume->dbcv_unitmask & (1 << i))
                {
                    disk = 'A' + i;
                }
            }
        }

        if (msg->wParam == DBT_DEVICEARRIVAL)
        {
            if (!TMPRDeviceUSBInfo::device_map.empty()) {
               return;
            }
            char sn_tmp[1024] = {0};
            TMPRDeviceUSBInfo::EnumConnectedDevice(sn_tmp);
            if (TMPRDeviceUSBInfo::device_map.empty()) {
                return;
            }

            if (TMPRDeviceUSBInfo::device_map.find(disk) == TMPRDeviceUSBInfo::device_map.end())
            {
                disk = disk + 1;
            }
            if (TMPRDeviceUSBInfo::device_map.find(disk) == TMPRDeviceUSBInfo::device_map.end())
            {
                return;
            }
            string sn = TMPRDeviceUSBInfo::device_map[disk];

            if(sn.empty())  {
                qDebug() << __FUNCTION__ << disk << "plug in sn is empty";
                return;
            }

            qDebug() << __FUNCTION__ << disk << QString(sn.c_str()) << "plug in";
            if (!TMPRDeviceUSBInfo::spi_device_map[disk])
            {
                StartCaptureMPRCode(disk);
            }

            emit MPRDevicePlugIn(disk);
        }
        else if (msg->wParam == DBT_DEVICEREMOVECOMPLETE)
        {
            if (TMPRDeviceUSBInfo::device_map.find(disk) == TMPRDeviceUSBInfo::device_map.end())
            {
                disk = disk + 1;
            }
            if (TMPRDeviceUSBInfo::device_map.find(disk) == TMPRDeviceUSBInfo::device_map.end())
            {
                return;
            }
            string sn = TMPRDeviceUSBInfo::device_map[disk];
            if(sn.empty()) {
                qDebug() << __FUNCTION__ << disk << "plug out sn is empty";
                return;
            }
            qDebug() << __FUNCTION__ << disk << QString(sn.c_str()) << "plug out";
            char sn_tmp[1024] = {0};
            TMPRDeviceUSBInfo::EnumConnectedDevice(sn_tmp);
            DetachMPRDevice(disk);
            emit MPRDevicePlugOut(disk);

        }
    }
}

void TMPRDevicesManager::EnumConnectedMPRDevice()
{
    char sn_tmp[1024] = {0};
    TMPRDeviceUSBInfo::EnumConnectedDevice(sn_tmp);

    for (auto it = TMPRDeviceUSBInfo::device_map.begin(); it != TMPRDeviceUSBInfo::device_map.end(); it++)
    {
        string sn = it->second;
        if (!sn.empty())
        {
            qDebug() << __FUNCTION__ << it->first << QString(sn.c_str()) << "plug in";
            if (!TMPRDeviceUSBInfo::spi_device_map[it->first])
            {
                StartCaptureMPRCode(it->first);
            }
            emit MPRDevicePlugIn(it->first);
        }
    }
}

void TMPRDevicesManager::StartCaptureMPRCode(char disk)
{
    auto it = mMPRDeviceHandlerMap.find(disk);
    if (it != mMPRDeviceHandlerMap.end()) {
        return;
    }

    TMPRDeviceHandler* handler = new TMPRDeviceHandler(disk);
    if (NULL == handler) {
        return;
    }

    handler->moveToThread(mpCodeCaptureThread);
    connect(handler, SIGNAL(MPRCodeCaptured(char, QString)), this, SIGNAL(MPRCodeCaptured(char, QString)));
    if (handler->StartCaptureMPRCode() || true) 
    {
        mMPRDeviceHandlerMap[disk] = handler;
        if (1 == mMPRDeviceHandlerMap.count()) {
            mpCaptureCodeTimer->start();
        }

    } else {
        delete handler;
    }
}

void TMPRDevicesManager::DetachMPRDevice( char disk)
{
    auto it = mMPRDeviceHandlerMap.find(disk);
    if (it == mMPRDeviceHandlerMap.end())
        return;

    TMPRDeviceHandler* devComm =  mMPRDeviceHandlerMap[disk];
    mMPRDeviceHandlerMap.remove(disk);
    delete devComm;

    if (mMPRDeviceHandlerMap.count() == 0)
    {
        mpCaptureCodeTimer->stop();
    }
}

void TMPRDevicesManager::OnCaptureCodeTimeout()
{
    if (!mpCodeCaptureThread->isRunning())
        mpCodeCaptureThread->start();

    for (auto it = mMPRDeviceHandlerMap.begin(); it != mMPRDeviceHandlerMap.end(); it++)
    {
        TMPRDeviceHandler* handler = it.value();

        QMetaObject::invokeMethod(handler, "TryCaptureMPRCode", Qt::UniqueConnection);
    }
}

bool TMPRDevicesManager::GetMPRDeviceDesc(char disk, TMPRDeviceDesc& desc)
{
    if (0 == disk)
    {
        QString path = TDefaultMPRDeviceManager::Instance().GetPath();
        TMPRDeviceDescFileReader reader(path);
        if (reader.IsValid())
        {
            desc = reader.GetDeviceDesc();
            return true;
        }
        return false;
    }

    auto it = mMPRDeviceHandlerMap.find(disk);
    if (it == mMPRDeviceHandlerMap.end())
        return false;

    TMPRDeviceHandler* handler = it.value();
    if (handler->GetDeviceDesc(desc))
    {
        return true;
    }
    return false;
}


QString TMPRDevicesManager::GetSPIFirmwareVersionInfo(char disk)
{
    int outSize = 1024;
    char* pszOutData = new char[outSize];
    memset(pszOutData, 0x00, outSize);
    bool bRet = TMPRDeviceUSBInfo::ReadSPIFirmwareVersionInfo(disk, pszOutData, outSize);
    QString strRet("");
    if (bRet)
    {
        QStringList strList = QString(pszOutData).split("\r\n");
        for each (QString str in strList)
        {
            if (str.startsWith("SVN", Qt::CaseInsensitive))
            {
                QStringList svnInfoList = str.split(":");
                if (svnInfoList.size() == 2)
                {
                    strRet = svnInfoList.at(1);
                }
                break;
            }
        }
    }
    delete pszOutData;
    return strRet;
}

void TMPRDevicesManager::ClearData()
{
    if (mpCodeCaptureThread->isRunning()) {
        mpCodeCaptureThread->quit();
        mpCodeCaptureThread->wait();
    }

    for (auto it = mMPRDeviceHandlerMap.begin(); it != mMPRDeviceHandlerMap.end(); it++) {
        delete it.value();
    }
    mMPRDeviceHandlerMap.clear();
}

bool TMPRDevicesManager::GetMPRDeviceCapacityInfo(char disk, qint64& qTotal, qint64& qFree)
{
#ifdef Q_OS_WIN
    unsigned long long freeBytesToCaller = 0, totalBytes = 0, freeBytes = 0;
    bool bRet = GetDiskFreeSpaceEx(QString("%1:/").arg(disk).toStdWString().c_str(), (PULARGE_INTEGER)&freeBytesToCaller,
        (PULARGE_INTEGER)&totalBytes, (PULARGE_INTEGER)&freeBytes);
    qDebug() << (QString("b=%1,freeBytesToCaller=%2,totalBytes=%3,freeBytes=%4,PULARGE_INTEGER type=%5, freeBytesToCaller type =%6, sizeof(freeBytesToCaller)=%7")
        .arg(bRet).arg(freeBytesToCaller).arg(totalBytes).arg(freeBytes)
        .arg(typeid(PULARGE_INTEGER).name()).arg(typeid(unsigned _int64).name())
        .arg(sizeof(freeBytesToCaller))
        );
    if (bRet)
    {
        qTotal = totalBytes;
        qFree = freeBytesToCaller;
    }
    return bRet;
#else
    return false;
#endif // Q_OS_WIN
}


