#ifndef __TMPRDEVICEHANDLER_H__
#define __TMPRDEVICEHANDLER_H__

#include "TMPRDeviceDesc.h"
class TMPRDeviceUSBComm;

class TMPRDeviceHandler : public QObject
{
    Q_OBJECT
public:
    TMPRDeviceHandler(char diskName);
    ~TMPRDeviceHandler();

    bool                StartCaptureMPRCode();
    char                GetDiskName();
    bool                GetDeviceDesc(TMPRDeviceDesc& devDesc);

private:
    QByteArray          HandleCodeBytes(const char* data, int /*len*/);

public slots:
    void                TryCaptureMPRCode();

signals:
    void                MPRCodeCaptured(char disk, QString mprCode);

private:
     TMPRDeviceUSBComm* mpDevComm;
     TMPRDeviceDesc*    mpMPRDevDes;
};

#endif


