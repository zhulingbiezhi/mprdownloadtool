#ifndef __TMPRDEVICDESC_H__
#define __TMPRDEVICDESC_H__

class TMPRDeviceDesc
{
public:
    enum DeviceType
    {
        MPRDevice_Invalid,     //无效的MPR设备
        MPRDevice_PenReader,   //MPR识读器（点读笔）
        MPRDevice_BookDisk,    //MRR书盘（UKey）
        MPRDevice_Dongle       //USB dongle,无媒体存储能力
    };

    DeviceType  mType;

    QString     mDeviceSN;
    QString     mDeviceName;
    QString     mDeviceMediaPath;
    QString     mFirmwareVersion;

    QByteArray  mBrandId;
    QByteArray  mUuid;
    QByteArray  mDeviceId;

    quint32     mSupportMediaFormat; //MPRAudioFormat按位或
    quint32     mSupportEncryptType; //EncryptAlgorithm按位或
};

#endif