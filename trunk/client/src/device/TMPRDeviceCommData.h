#ifndef __TMPRDEVICECOMMDATA_H__
#define __TMPRDEVICECOMMDATA_H__

#define DATA_ERROR -1
#define DATA_OK     0

struct TMPRDeviceCommData{
    unsigned int            len;
    unsigned int            id;
    unsigned const char *   data;     
};

int MPRDeviceProcessCommData(TMPRDeviceCommData* deviceData,const unsigned char *data, int dataMaxLen);

#endif