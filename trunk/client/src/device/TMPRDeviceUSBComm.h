#ifndef __TMPRDEVICEUSBCOMM_H__
#define __TMPRDEVICEUSBCOMM_H__

#include <windows.h>
#include "TMPRDeviceComm.h"

class TMPRDeviceUSBComm : public TMPRDeviceComm
{
public:
    TMPRDeviceUSBComm( char diskName );
    virtual ~TMPRDeviceUSBComm();

protected:
    HANDLE              m_hDisk;

public:   
    BOOL                ReadData(int* inLen, unsigned char* inData, int inMaxLen);
    BOOL                WriteData(int outLen, const unsigned char* outData);

private:
   
    BOOL                OpenDisk(char diskName);
    void                CloseDisk(); 
};

#endif
