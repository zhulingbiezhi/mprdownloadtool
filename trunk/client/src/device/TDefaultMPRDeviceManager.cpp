#include "TSTDPreCompiledHeader.h"
#include "TDefaultMPRDeviceManager.h"
#include "TWorkStateConfig.h"
#include "TGlobleCtl.h"

TDefaultMPRDeviceManager::TDefaultMPRDeviceManager()
{
    TWorkStateConfig::Instance()->BeginGroup(DirSetting);
    mDefuatlPath = TWorkStateConfig::Instance()->GetValue(DEFAULT_MPR_DEVICE_DESC_FILE_PATH, "").toString();
    TWorkStateConfig::Instance()->EndGroup();

    if (!QFile::exists(mDefuatlPath)) {
        mDefuatlPath = qApp->applicationDirPath() + QDir::separator() + "DefaultMPRDeviceDesc.ini";
    }
    mDefuatlPath = QDir::toNativeSeparators(mDefuatlPath);
}

TDefaultMPRDeviceManager::~TDefaultMPRDeviceManager()
{

}

TDefaultMPRDeviceManager& TDefaultMPRDeviceManager::Instance()
{
    static TDefaultMPRDeviceManager ins;
    return ins;
}

void TDefaultMPRDeviceManager::SetPath( const QString& path )
{
    mDefuatlPath = path;
    TWorkStateConfig::Instance()->SetValue(DEFAULT_MPR_DEVICE_DESC_FILE_PATH, mDefuatlPath);
}

QString TDefaultMPRDeviceManager::GetPath() const
{
    return mDefuatlPath;
}
