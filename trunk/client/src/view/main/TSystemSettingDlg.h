#ifndef _TSYSTEMSETTINGDLG_H
#define _TSYSTEMSETTINGDLG_H

#include "TBaseDialog.h"
class TSystemSettingDlg : public TBaseDialog
{
    Q_OBJECT
public:
    TSystemSettingDlg(QWidget* parent = NULL, Qt::WindowFlags f = 0);
    ~TSystemSettingDlg();

private:
    void CreateUI();
    void InitData();
    void InitConnect();
    void CreateTitleWidget();
    void CreateCenterWidget();

private slots:
    void OnBrowseBtnClicked();
    void OnOkBtnClicked();


private:
    QLineEdit*       mpLineEdit;
    QPushButton*     mpBrowseBtn;
    QPushButton*     mpOkBtn;
    QPushButton*     mpCancelBtn;
    QWidget*         mpTitleWidget;
    QWidget*         mpCenterWidget;



};

#endif