#include "src/TSTDPreCompiledHeader.h"
#include "TSplashScreen.h"

TSplashScreen::TSplashScreen(const QPixmap& pixmap, bool isProgress) 
	: QSplashScreen(pixmap)
    , mProgressBar(nullptr)
{
    if (!isProgress){
        return;
    }

    QSize nSize = size();
    QRect nRect(nSize.width()*1/10, nSize.height()*8/9,nSize.width()*8/10,nSize.height()*1/20);

    mProgressBar = new QProgressBar(this); // 父类为TSplashScreen
    mProgressBar->setGeometry(nRect);
    mProgressBar->setRange(0, 100);
    mProgressBar->setValue(0);
    mProgressBar->setTextVisible(false);

    connect(mProgressBar, SIGNAL(valueChanged(int)), this, SLOT(OnProgressChanged(int))); //值改变时，立刻repaint
}

TSplashScreen::~TSplashScreen()
{
}

void TSplashScreen::SetProgress(int value)
{
    if (NULL == mProgressBar){
        return;
    }
    mProgressBar->setValue(value);
}


void TSplashScreen::OnProgressChanged(int)
{
    repaint();
}
