#ifndef _MAINWINDOW_H_
#define _MAINWINDOW_H_

//class TUpgradeClientUI;
class THeadWidget;
class TLeftTabWidget;
class TRightDisplayWidget;
class TDownloadManager;
class TSearchHandle;
class TMPRSearchRepliedInfo;

#include "TFileEnum.h"


class TMainWindow : public QFrame
{
    Q_OBJECT

public:
    TMainWindow(QWidget* parent = 0);
    ~TMainWindow();

private:
    bool nativeEvent(const QByteArray &eventType, void *message, long *result);
private:
    //void moveEvent(QMoveEvent *);
    //void MoveToBottomRight(QDialog* dlg);
    bool IsInHeadBarArea(QPoint pos);

private:
	void CreateUI();
    void InitConnect();
    void InitData();
    void InitBorderStyle();

private slots:
    //void ShowUpgradeUI(bool);
    void OnExit();
    void OnSystemSetting();
    void OnMPRDevicePlugIn(char);
    void OnMPRDevicePlugOut(char);
    void OnMPRCodeCaptured(char disk, QString mprCode);
    void OnFetchDownloadUrlReplied(const TMPRFileInfoItem&, const TMPRSearchRepliedInfo&, const QString&, const QString&, const QString&);
    
private:
    //TUpgradeClientUI*      mpUpgradeClientUI;
    THeadWidget*               mpHeadWidget;
    TLeftTabWidget*            mpCustomerTabWidget;
    TRightDisplayWidget*       mpDisPlayWidget;
    TDownloadManager*          mpDownloadManager;
    TSearchHandle*             mpSearchHandle;
    QLibrary*                  mpDwmapiLib;
};

#endif