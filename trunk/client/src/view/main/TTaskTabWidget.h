#ifndef _TTASKTABWIDGET_H
#define _TTASKTABWIDGET_H
#include "TFileEnum.h"
#include "TCopyFileState.h"
class TDeviceInfoWidget;
class TCopyFileTask;
typedef QHash<QString, QVariant> TSVHashMap;
class TTaskTabWidget : public QFrame
{
    Q_OBJECT
public:
    enum TTaskState{
        TS_PREPARE,
        TS_DOWNLOAD,
        TS_PAUSE,
        TS_COPYFILE,
        TS_FINISHE,
        TS_ERROR
    };
    struct TTaskInfo
    {
        int                 nDownloadTaskId;
        int                 downloadProgress;
        CopyFileState       copyMprState;
        CopyFileState       copyLicState;
        TTaskState          taskState;
        TMPRFileInfoItem    downloadInfo;
        TCopyFileTask*      pCopyMprTask;
        TCopyFileTask*      pCopyLicenseTask;
        QProgressBar*       pDownloadProgressBar;
        QLabel*             pLabel;
        QPushButton*        pDeleteBtn;
        QPushButton*        pPauseBtn;
        QPushButton*        pStartBtn;
    };

    TTaskTabWidget(QWidget* parent = NULL);
    ~TTaskTabWidget();
    void ResetDeviceInfo();
    void ShowSPIInfo(QString);

private:
    int  GetClickedRow();
    int  GetTaskIndex(int nDownloadTaskId);
    int  GetTaskIndex(TCopyFileTask* pTask, bool bMpr = true);
    void CreateUI();
    void InitTable();
    void InitConnect();
    void ResetTaskTable();
    void AddRow(TTaskInfo& taskInfo);
    void UpdatetTaskShow(TTaskInfo& taskInfo);
    void AddCopyTask(TTaskInfo& taskInfo);
    void UpdateCopyTaskState(TTaskInfo& taskInfo);
    void HandleProgressMsg(int nDownloadTaskId, int nProgerss);
    void HandleStateMsg(int nDownloadTaskId, int nState, TSVHashMap& extraParams);
    void HandleErrorMsg(int nDownloadTaskId, QString strError);
    QWidget* InitStateCellWidget(TTaskInfo& taskInfo);
    QWidget* InitOptCellWidget(TTaskInfo& taskInfo);


public slots:
    void UpdateDeviceInfo();
    void OnAddTask(int nTaskId, TMPRFileInfoItem item);

private slots:
    void OnHandleDownloadMsg(int taskID, int msgType, const QVariant& msgBody, TSVHashMap& extraParams);
    void OnDeleteTask();
    void OnPauseTask();
    void OnStartTask();
    void OnCopyMprStateChange(CopyFileState state);
    void OnCopyLicStateChange(CopyFileState state);
    void OnCopyProgressChanged(qint64 qCopyed, qint64 qTotal);

signals:
    void capacityChange();

private:
    TDeviceInfoWidget*    mpDeviceInfoWidget;
    QTableWidget*         mpTaskTableWidget;
    QLabel*               mpTipsLabel;
    QLabel*               mpNoTaskLabel;
    QStackedLayout*       mpStackedLayout;
    QList<TTaskInfo>      mTaskInfoList;
};
#endif