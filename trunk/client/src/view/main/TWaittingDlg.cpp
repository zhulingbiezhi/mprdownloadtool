#include "TWaittingDlg.h"

TWaittingDlg::TWaittingDlg(QWidget* parent /*= NULL*/, Qt::WindowFlags f /*= 0*/)
: TBaseDialog(parent, f)
{
    CreateUI();
    InitTimer();
}

TWaittingDlg::~TWaittingDlg()
{

}

void TWaittingDlg::CreateUI()
{
    setObjectName("TWaittingDlg");
    CreateTitleWidget();
    CreateCenterWidget();
    QVBoxLayout* pMainLayout = new QVBoxLayout();
    pMainLayout->addWidget(mpTitleWidget);
    pMainLayout->addStretch();
    pMainLayout->addWidget(mpCenterWidget);
    pMainLayout->addStretch();
    pMainLayout->setContentsMargins(0, 0, 0, 0);
    pMainLayout->setSpacing(0);
    SetLayout(pMainLayout);
    setMinimumSize(452, 267);

}

void TWaittingDlg::InitTimer()
{
    mpTimer = new QTimer(this);
    mpTimer->setInterval(1000);
    connect(mpTimer, &QTimer::timeout, this, &TWaittingDlg::OnTimerTricked);
}

void TWaittingDlg::CreateCenterWidget()
{
    mpCenterWidget = new QWidget(this);
    mpCenterWidget->setObjectName("CenterWidget");
    mpTipsLabel = new QLabel(this);
    mpTipsLabel->setAlignment(Qt::AlignRight);
    mpWaitingLabel = new QLabel(this);
    mpWaitingLabel->setAlignment(Qt::AlignLeft);
    QHBoxLayout* pLayout = new QHBoxLayout();
    pLayout->setContentsMargins(100, 0, 100, 0);
    pLayout->setSpacing(0);
    pLayout->addWidget(mpTipsLabel);
    pLayout->addWidget(mpWaitingLabel);

    QVBoxLayout* pCenterLayout = new QVBoxLayout(mpCenterWidget);
    pCenterLayout->setContentsMargins(0, 0, 0, 0);
    pCenterLayout->addStretch();
    pCenterLayout->addLayout(pLayout);
    pCenterLayout->addStretch();

   
}

void TWaittingDlg::OnTimerTricked()
{
     if (mnTrickCount % 3 == 0)
     {
         mnTrickCount = 0;
     }
     mnTrickCount++;
     QString strText = QString("%1").arg(".", mnTrickCount, '.');
     for (int i = mnTrickCount; i < 3; i ++)
     {
         strText += " ";
     }
     mpWaitingLabel->setText(strText);
}

void TWaittingDlg::ShowText(QString str)
{
    mpTipsLabel->setText(str);
    mnTrickCount = 0;
    mpTimer->start();
    exec();
}

void TWaittingDlg::Close()
{
    if (mpTimer->isActive())
    {
        mpTimer->stop();
    }
    hide();
}

void TWaittingDlg::CreateTitleWidget()
{
    mpTitleWidget = new QWidget(this);
    mpTitleWidget->setObjectName("TitleWidget");
    mpTitleWidget->setFixedHeight(35);

    QLabel *toptitle = new QLabel(tr("Info"), mpTitleWidget);
    toptitle->setAlignment(Qt::AlignVCenter);
    toptitle->setObjectName("title");
   

    QHBoxLayout *toplayout = new QHBoxLayout;
    toplayout->addWidget(toptitle);
    toplayout->addStretch();
    toplayout->setContentsMargins(10, 0, 0, 0);
    mpTitleWidget->setLayout(toplayout);
}
