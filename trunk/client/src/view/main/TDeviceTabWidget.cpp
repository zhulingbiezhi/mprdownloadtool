#include "TDeviceTabWidget.h"
#include "TDeviceInfoWidget.h"
#include "TDeviceDataManager.h"
#include "TCommonHelper.h"
#include "TFileEnum.h"
#include "TPageNaviBar.h"
#include "TPopupDialog.h"
#include "TAppCommon.h"

const int PER_PAGE_COUNT = 10;
TDeviceTabWidget::TDeviceTabWidget(QWidget* parent /*= NULL*/)
: QFrame(parent)
, mnCurrentPage(1)
{
    CreateUI();
    InitConnect();
}

TDeviceTabWidget::~TDeviceTabWidget()
{

}

void TDeviceTabWidget::CreateUI()
{
    setObjectName("TDeviceTabWidget");
    mpDeviceInfoWidget = new TDeviceInfoWidget(this);

    mpBookInfoTable = new QTableWidget(this);

    mpPageNaviBar = new TPageNaviBar(this);


    QVBoxLayout* pMainLayout = new QVBoxLayout();
    pMainLayout->setContentsMargins(0, 0, 0, 0);
    pMainLayout->setSpacing(0);

    pMainLayout->addWidget(mpDeviceInfoWidget);
    pMainLayout->addWidget(mpBookInfoTable);

    QHBoxLayout* pBottomLayout = new QHBoxLayout();
    pBottomLayout->setContentsMargins(0, 0, 50, 0);
    pBottomLayout->addStretch();
    pBottomLayout->addWidget(mpPageNaviBar);

    pMainLayout->addLayout(pBottomLayout);
    pMainLayout->addSpacing(50);

    setLayout(pMainLayout);

    InitTable();
}

void TDeviceTabWidget::InitConnect()
{
    connect(&TDeviceDataManager::Instance(), &TDeviceDataManager::sigScanFileFinished, this, &TDeviceTabWidget::OnRefreshAllData);
    connect(&TDeviceDataManager::Instance(), &TDeviceDataManager::addNewFile, this, &TDeviceTabWidget::OnNewFileAdded);
    connect(mpPageNaviBar, &TPageNaviBar::sigJumpToPage, this, &TDeviceTabWidget::JumpToPage);
}

void TDeviceTabWidget::InitTable()
{
    mpBookInfoTable->horizontalHeader()->setStretchLastSection(true);
    mpBookInfoTable->setEditTriggers(QAbstractItemView::NoEditTriggers);
    mpBookInfoTable->verticalHeader()->setHidden(true);
    mpBookInfoTable->setSelectionBehavior(QAbstractItemView::SelectRows);
    mpBookInfoTable->setSelectionMode(QAbstractItemView::SingleSelection);
    mpBookInfoTable->horizontalHeader()->setHighlightSections(false);
    mpBookInfoTable->setFocusPolicy(Qt::NoFocus);
    mpBookInfoTable->setShowGrid(false);

    QStringList headers;
    headers << tr("  ") << tr("Book Name") << tr("Publisher Name") << tr("Format") << tr("Download Time") << tr("File Size") << tr("Operate");

    mpBookInfoTable->setColumnCount(headers.size());
    mpBookInfoTable->setHorizontalHeaderLabels(headers);
    mpBookInfoTable->horizontalHeader()->setDefaultAlignment(Qt::AlignHCenter | Qt::AlignVCenter);

    mpBookInfoTable->setColumnWidth(0, 30);
    mpBookInfoTable->setColumnWidth(1, 200);
    mpBookInfoTable->setColumnWidth(2, 150);
    mpBookInfoTable->setColumnWidth(3, 100);
    mpBookInfoTable->setColumnWidth(4, 170);
    mpBookInfoTable->setColumnWidth(5, 80);

}

void TDeviceTabWidget::UpdateDeviceInfo()
{
    mpDeviceInfoWidget->UpdateShowInfo();
}

void TDeviceTabWidget::ShowSPIInfo(QString strDeviceSn)
{
    mpDeviceInfoWidget->ShowSPIInfo(strDeviceSn);
}

void TDeviceTabWidget::ResetDeviceInfo()
{
    ClearTable();
    mpPageNaviBar->setVisible(false);
    mpDeviceInfoWidget->ResetDeviceInfo();
}

void TDeviceTabWidget::OnRefreshAllData(char)
{
    UpdatePageNavBar();
    JumpToPage(1);
}

void TDeviceTabWidget::RefreshTable()
{
    int nOffest = 1;
    int nBegin = (mnCurrentPage - 1) * 10;
    for each (TMPRFileInfoItem fileInfo in mShowFileInfoList)
    {
        int id = nBegin + nOffest;
        nOffest++;
        AddRowToTable(id, fileInfo);
    }
}

void TDeviceTabWidget::AddRowToTable(const int& id, const TMPRFileInfoItem& fileInfo)
{
    mpBookInfoTable->insertRow(mpBookInfoTable->rowCount());
    //mpBookInfoTable->setCellWidget(mpBookInfoTable->rowCount() - 1, 0, InitIdWidget(infoItem.nWorkFlowID, infoItem.nPreProStatu, infoItem.nWorkStatu));

    QTableWidgetItem *item0 = new QTableWidgetItem(QString("  %1").arg(id));
    item0->setTextAlignment(Qt::AlignLeft | Qt::AlignVCenter);
    mpBookInfoTable->setItem(mpBookInfoTable->rowCount() - 1, 0, item0);

    QTableWidgetItem *item1 = new QTableWidgetItem(QString("  %1").arg(fileInfo.tBookName));
    item1->setTextAlignment(Qt::AlignLeft | Qt::AlignVCenter);
    mpBookInfoTable->setItem(mpBookInfoTable->rowCount() - 1, 1, item1);

    QTableWidgetItem *item2 = new QTableWidgetItem(QString("  %1").arg(fileInfo.tPublisher));
    item1->setTextAlignment(Qt::AlignLeft | Qt::AlignVCenter);
    mpBookInfoTable->setItem(mpBookInfoTable->rowCount() - 1, 2, item2);

    QTableWidgetItem *item3 = new QTableWidgetItem(QString("  %1").arg("MPR"));
    item3->setTextAlignment(Qt::AlignLeft | Qt::AlignVCenter);
    mpBookInfoTable->setItem(mpBookInfoTable->rowCount() - 1, 3, item3);

    QTableWidgetItem *item4 = new QTableWidgetItem(QString("  %1").arg(fileInfo.tFileCreateTime));
    item4->setTextAlignment(Qt::AlignLeft | Qt::AlignVCenter);
    mpBookInfoTable->setItem(mpBookInfoTable->rowCount() - 1, 4, item4);

    //QDate tempDate = QDate::fromString(infoItem.strCreateTime.left(10), "yyyy-MM-dd");
    //QTime tempTime = QTime::fromString(infoItem.strCreateTime.right(8), "hh:mm:ss");
    //QDateTime tempDataTime(tempDate, tempTime, Qt::UTC);

    QTableWidgetItem *item5 = new QTableWidgetItem(QString("  %1").arg(TCommonHelper::Instance().StitchSizeDec(fileInfo.tFileSize)));
    item5->setTextAlignment(Qt::AlignLeft | Qt::AlignVCenter);
    mpBookInfoTable->setItem(mpBookInfoTable->rowCount() - 1, 5, item5);


    QLabel* pLabel = new QLabel(this);
    QHBoxLayout* pLayout = new QHBoxLayout(pLabel);
    QPushButton* pDeleteBtn = new QPushButton(this);
    pDeleteBtn->setFixedSize(22, 22);
    pDeleteBtn->setObjectName("RemoveBtn");
    pLayout->addWidget(pDeleteBtn, Qt::AlignCenter);
    pLayout->setContentsMargins(0, 0, 0, 0);
    connect(pDeleteBtn, &QPushButton::clicked, this, &TDeviceTabWidget::OnDeleteBtnClicked);
    mpBookInfoTable->setCellWidget(mpBookInfoTable->rowCount() - 1, 6, pLabel);
}

void TDeviceTabWidget::ClearTable()
{
    mpBookInfoTable->setRowCount(0);
    mpBookInfoTable->clearContents();
}

void TDeviceTabWidget::JumpToPage(int nPage)
{
    ClearTable();
    mnCurrentPage = nPage;
    mShowFileInfoList.clear();
    mShowFileInfoList.append(TDeviceDataManager::Instance().GetFileInfoItems(mnCurrentPage));
    RefreshTable();
}

void TDeviceTabWidget::OnDeleteBtnClicked()
{
    int nRow = GetClickedRow();
    mpBookInfoTable->selectRow(nRow);
    TPopupDialog msg(TPopupDialog::Yes | TPopupDialog::No, tr("delete mpr files"), tr("Are you sure to delete this MPF file?"), TAppCommon::Instance().GetMainWindow(), TPopupDialog::Warning);
    msg.exec();
    if (msg.GetResult() == TPopupDialog::Yes)
    {
        TDeviceDataManager::Instance().RemoveDeviceFile(mShowFileInfoList.at(nRow).tPrefixCode, mShowFileInfoList.at(nRow).tVersion);
        UpdatePageNavBar();
        mpBookInfoTable->removeRow(nRow);
        if (mpBookInfoTable->rowCount() > 0)
        {
            JumpToPage(mnCurrentPage);
        }
        else
        {
            JumpToPage(mnCurrentPage - 1);
        }
        UpdateDeviceInfo();
        emit capacityChange();
    }
}

void TDeviceTabWidget::UpdatePageNavBar()
{
    int nCount = TDeviceDataManager::Instance().GetFilesCount();
    if (nCount > 0)
    {
        int nRemaind = nCount % PER_PAGE_COUNT;
        int nTotalPage = nCount / PER_PAGE_COUNT + (nRemaind > 0 ? 1 : 0);
        mpPageNaviBar->setVisible(true);
        mpPageNaviBar->setCurPageInfo(1, nTotalPage, nCount);
    }
    else
    {
        mpPageNaviBar->setVisible(false);
    }
}

void TDeviceTabWidget::OnNewFileAdded()
{
    UpdatePageNavBar();
    JumpToPage(mnCurrentPage);
}

int TDeviceTabWidget::GetClickedRow()
{
    int nRow = 0;
    int nTempRow = mpBookInfoTable->indexAt(mpBookInfoTable->mapFromGlobal(QCursor::pos())).row();
    if (nTempRow < 0) {
        nRow = mpBookInfoTable->rowCount() - 1;
    }
    else {
        nRow = nTempRow - 1;
    }
    return nRow;
}




