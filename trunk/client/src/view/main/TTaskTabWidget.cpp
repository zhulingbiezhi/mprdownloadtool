#include "TTaskTabWidget.h"
#include "TDeviceInfoWidget.h"
#include "TDownloadManager.h"
#include "TCopyFileManager.h"
#include "TCopyFileTask.h"
#include "TDeviceDataManager.h"
#include "TAppCommon.h"


TTaskTabWidget::TTaskTabWidget(QWidget* parent /*= NULL*/)
: QFrame(parent)
{
    CreateUI();
    InitConnect();
}

TTaskTabWidget::~TTaskTabWidget()
{

}

void TTaskTabWidget::CreateUI()
{
    setObjectName("TTaskTabWidget");
    mpDeviceInfoWidget = new TDeviceInfoWidget(this);

    mpTaskTableWidget = new QTableWidget(this);


    mpTipsLabel = new QLabel(this);
    mpTipsLabel->setFixedHeight(50);
    mpTipsLabel->setAlignment(Qt::AlignHCenter);
    mpTipsLabel->setText(tr("Warm prompt: Using USB data lin to connet device with computer first, and make computer staying connected to the Internet.\n Then your can download mpr files by touch mpr book using touch and talk pen!"));

    mpNoTaskLabel = new QLabel(this);
    mpNoTaskLabel->setAlignment(Qt::AlignCenter);
    mpNoTaskLabel->setObjectName("NoTaskLabel");
    mpNoTaskLabel->setText(tr("There is no download task, please touch reading the MPR book"));

    mpStackedLayout = new QStackedLayout();
    mpStackedLayout->setContentsMargins(0, 0, 0, 0);
    mpStackedLayout->addWidget(mpNoTaskLabel);
    mpStackedLayout->addWidget(mpTaskTableWidget);

    QVBoxLayout* pMainLayout = new QVBoxLayout();
    pMainLayout->setContentsMargins(0, 0, 0, 0);
    pMainLayout->setSpacing(0);

    pMainLayout->addWidget(mpDeviceInfoWidget);
    pMainLayout->addLayout(mpStackedLayout);
    pMainLayout->addSpacing(10);
    pMainLayout->addWidget(mpTipsLabel);

    setLayout(pMainLayout);
    InitTable();
}


void TTaskTabWidget::InitConnect()
{
    connect(TDownloadManager::Instance(), &TDownloadManager::sigDownloadMessage, this, &TTaskTabWidget::OnHandleDownloadMsg);
    connect(TDownloadManager::Instance(), &TDownloadManager::sigAddTaskToTable, this, &TTaskTabWidget::OnAddTask);
}


void TTaskTabWidget::InitTable()
{
    mpTaskTableWidget->horizontalHeader()->setStretchLastSection(true);
    mpTaskTableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
    mpTaskTableWidget->verticalHeader()->setHidden(true);
    mpTaskTableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
    mpTaskTableWidget->setSelectionMode(QAbstractItemView::SingleSelection);
    mpTaskTableWidget->horizontalHeader()->setHighlightSections(false);
    mpTaskTableWidget->setFocusPolicy(Qt::NoFocus);
    mpTaskTableWidget->setShowGrid(false);

    QStringList headers;
    headers << tr("  ") << tr("Book Name") << tr("Publisher Name") << tr("Format") << tr("Status") << tr("Operate");

    mpTaskTableWidget->setColumnCount(headers.size());
    mpTaskTableWidget->setHorizontalHeaderLabels(headers);
    mpTaskTableWidget->horizontalHeader()->setDefaultAlignment(Qt::AlignHCenter | Qt::AlignVCenter);

    mpTaskTableWidget->setColumnWidth(0, 30);
    mpTaskTableWidget->setColumnWidth(1, 200);
    mpTaskTableWidget->setColumnWidth(2, 150);
    mpTaskTableWidget->setColumnWidth(3, 100);
    mpTaskTableWidget->setColumnWidth(4, 250);


}

void TTaskTabWidget::UpdateDeviceInfo()
{
    mpDeviceInfoWidget->UpdateShowInfo();
}

void TTaskTabWidget::ShowSPIInfo(QString strDeviceSn)
{
    mpDeviceInfoWidget->ShowSPIInfo(strDeviceSn);
}
void TTaskTabWidget::ResetDeviceInfo()
{
    ResetTaskTable();
    mpDeviceInfoWidget->ResetDeviceInfo();
}

void TTaskTabWidget::OnAddTask(int nTaskId, TMPRFileInfoItem item)
{
    TTaskInfo task;
    task.nDownloadTaskId = nTaskId;
    task.copyLicState = CSNone;
    task.copyMprState = CSNone;
    if (nTaskId < 0)
    {
        task.taskState = TS_COPYFILE;
        task.downloadProgress = 100;
        AddCopyTask(task);
    }
    else
    {
        task.downloadProgress = 0;
        task.taskState = TS_PREPARE;
    }
    task.downloadInfo = item;
    AddRow(task);
    mTaskInfoList.append(task);
    mpStackedLayout->setCurrentWidget(mpTaskTableWidget);
}

void TTaskTabWidget::OnHandleDownloadMsg(int taskID, int msgType, const QVariant& msgBody, TSVHashMap& extraParams)
{
    switch (msgType) {
    case HTM_PROGRESS:   //进度消息
        HandleProgressMsg(taskID, msgBody.toInt());
        break;
    case HTM_STATE:     //状态切换消息
        HandleStateMsg(taskID, msgBody.toInt(), extraParams);
        break;
    case HTM_ERROR:     //错误消息
        HandleErrorMsg(taskID, msgBody.toString());
        break;
    case HTM_TEXT:      //文本消息
        break;
    case HTM_SIZE:      //任务大小反馈
        break;
    case HTM_PHASE:     //任务子阶段反馈
        break;
    default:
        break;
    }
}

void TTaskTabWidget::AddRow(TTaskInfo& taskInfo)
{
    mpTaskTableWidget->insertRow(mpTaskTableWidget->rowCount());
    //mpBookInfoTable->setCellWidget(mpBookInfoTable->rowCount() - 1, 0, InitIdWidget(infoItem.nWorkFlowID, infoItem.nPreProStatu, infoItem.nWorkStatu));

    QTableWidgetItem *item0 = new QTableWidgetItem(QString("  %1").arg(mpTaskTableWidget->rowCount()));
    item0->setTextAlignment(Qt::AlignLeft | Qt::AlignVCenter);
    mpTaskTableWidget->setItem(mpTaskTableWidget->rowCount() - 1, 0, item0);

    QTableWidgetItem *item1 = new QTableWidgetItem(QString("  %1").arg(taskInfo.downloadInfo.tBookName));
    item1->setTextAlignment(Qt::AlignLeft | Qt::AlignVCenter);
    mpTaskTableWidget->setItem(mpTaskTableWidget->rowCount() - 1, 1, item1);

    QTableWidgetItem *item2 = new QTableWidgetItem(QString("  %1").arg(taskInfo.downloadInfo.tPublisher));
    item1->setTextAlignment(Qt::AlignLeft | Qt::AlignVCenter);
    mpTaskTableWidget->setItem(mpTaskTableWidget->rowCount() - 1, 2, item2);

    QTableWidgetItem *item3 = new QTableWidgetItem(QString("  %1").arg("MPR"));
    item3->setTextAlignment(Qt::AlignLeft | Qt::AlignVCenter);
    mpTaskTableWidget->setItem(mpTaskTableWidget->rowCount() - 1, 3, item3);


    //QDate tempDate = QDate::fromString(infoItem.strCreateTime.left(10), "yyyy-MM-dd");
    //QTime tempTime = QTime::fromString(infoItem.strCreateTime.right(8), "hh:mm:ss");
    //QDateTime tempDataTime(tempDate, tempTime, Qt::UTC);
    mpTaskTableWidget->setCellWidget(mpTaskTableWidget->rowCount() - 1, 4, InitStateCellWidget(taskInfo));
    mpTaskTableWidget->setCellWidget(mpTaskTableWidget->rowCount() - 1, 5, InitOptCellWidget(taskInfo));

    UpdatetTaskShow(taskInfo);

}

void TTaskTabWidget::ResetTaskTable()
{
    mpTaskTableWidget->clearContents();
    mpTaskTableWidget->setRowCount(0);
    for each (TTaskInfo task in mTaskInfoList)
    {
        if (task.taskState == TS_DOWNLOAD && task.nDownloadTaskId > 0)
        {
            TDownloadManager::Instance()->StopTask(task.nDownloadTaskId);
        }
        if (task.taskState == TS_COPYFILE)
        {
            if (task.pCopyLicenseTask != NULL)
            {
                TCopyFileManager::Instance().RemoveTask(task.pCopyLicenseTask);
            }
            if (task.pCopyMprTask != NULL)
            {
                TCopyFileManager::Instance().RemoveTask(task.pCopyMprTask);
            }
        }
    }
    mTaskInfoList.clear();
    mpStackedLayout->setCurrentWidget(mpNoTaskLabel);
}

int TTaskTabWidget::GetClickedRow()
{
    int nRow = 0;
    int nTempRow = mpTaskTableWidget->indexAt(mpTaskTableWidget->mapFromGlobal(QCursor::pos())).row();
    if (nTempRow < 0) {
        nRow = mpTaskTableWidget->rowCount() - 1;
    }
    else {
        nRow = nTempRow - 1;
    }
    return nRow;
}

QWidget* TTaskTabWidget::InitStateCellWidget(TTaskInfo& taskInfo)
{
    QWidget*  pWidget = new QWidget(this);
    QProgressBar* pProgress = new QProgressBar(this);
    pProgress->setObjectName("TaskProgressBar");
    pProgress->setFixedSize(180, 16);
    pProgress->setMaximum(100);

    QLabel* pStateLabel = new QLabel(this);
    QHBoxLayout* pLayout = new QHBoxLayout(pWidget);
    pLayout->setContentsMargins(0, 0, 0, 0);
    pLayout->addWidget(pProgress);
    pLayout->addWidget(pStateLabel);
    taskInfo.pDownloadProgressBar = pProgress;
    taskInfo.pLabel = pStateLabel;
    return pWidget;
}

QWidget* TTaskTabWidget::InitOptCellWidget(TTaskInfo& taskInfo)
{
    QWidget*  pWidget = new QWidget(this);
    QPushButton* pPauseBtn = new QPushButton(this);
    QPushButton* pStartBtn = new QPushButton(this);
    QPushButton* pDeleteBtn = new QPushButton(this);

    pPauseBtn->setFixedSize(22, 22);
    pStartBtn->setFixedSize(22, 22);
    pDeleteBtn->setFixedSize(22, 22);

    pStartBtn->setToolTip(tr("start"));
    pPauseBtn->setToolTip(tr("pause"));
    pDeleteBtn->setToolTip(tr("delete"));

    pStartBtn->setObjectName("PauseOpBtn");
    pPauseBtn->setObjectName("StartOpBtn");
    pDeleteBtn->setObjectName("RemoveBtn");


    taskInfo.pStartBtn = pStartBtn;
    taskInfo.pDeleteBtn = pDeleteBtn;
    taskInfo.pPauseBtn = pPauseBtn;

    connect(pPauseBtn, &QPushButton::clicked, this, &TTaskTabWidget::OnPauseTask);
    connect(pDeleteBtn, &QPushButton::clicked, this, &TTaskTabWidget::OnDeleteTask);
    connect(pStartBtn, &QPushButton::clicked, this, &TTaskTabWidget::OnStartTask);

    pStartBtn->setVisible(false);
    QHBoxLayout* pLayout = new QHBoxLayout(pWidget);
    pLayout->setContentsMargins(0, 0, 0, 0);
    pLayout->addStretch();
    pLayout->addWidget(pStartBtn);
    pLayout->addWidget(pPauseBtn);
    pLayout->addSpacing(5);
    pLayout->addWidget(pDeleteBtn);
    pLayout->addStretch();
    return pWidget;

}

void TTaskTabWidget::OnDeleteTask()
{
    int nRow = GetClickedRow();
    if (nRow != -1 && nRow < mTaskInfoList.size())
    {
        if (mTaskInfoList[nRow].nDownloadTaskId > 0)
        {
            TDownloadManager::Instance()->RemoveTask(mTaskInfoList[nRow].nDownloadTaskId);
        }
        if (mTaskInfoList[nRow].taskState == TS_COPYFILE)
        {
            if (mTaskInfoList[nRow].pCopyLicenseTask != NULL)
            {
                TCopyFileManager::Instance().RemoveTask(mTaskInfoList[nRow].pCopyLicenseTask);
            }
            if (mTaskInfoList[nRow].pCopyMprTask != NULL)
            {
                TCopyFileManager::Instance().RemoveTask(mTaskInfoList[nRow].pCopyMprTask);
            }
        }
    }
    mpTaskTableWidget->removeRow(nRow);
    mTaskInfoList.removeAt(nRow);
    if (mTaskInfoList.isEmpty())
    {
        mpStackedLayout->setCurrentWidget(mpNoTaskLabel);
    }
}

void TTaskTabWidget::OnPauseTask()
{
    int nRow = GetClickedRow();
    if (nRow != -1 && nRow < mTaskInfoList.size())
    {
        if (mTaskInfoList[nRow].taskState == TS_DOWNLOAD && mTaskInfoList[nRow].nDownloadTaskId > 0)
        {
            TDownloadManager::Instance()->StopTask(mTaskInfoList[nRow].nDownloadTaskId);
        }
    }
}

void TTaskTabWidget::OnStartTask()
{
    int nRow = GetClickedRow();
    if (nRow != -1 && nRow < mTaskInfoList.size())
    {
        if (mTaskInfoList[nRow].taskState == TS_PAUSE && mTaskInfoList[nRow].nDownloadTaskId > 0)
        {
            TDownloadManager::Instance()->StartTask(mTaskInfoList[nRow].nDownloadTaskId);
        }
    }
}

void TTaskTabWidget::UpdatetTaskShow(TTaskInfo& taskInfo)
{
    switch (taskInfo.taskState)
    {
    case TS_PREPARE:
        taskInfo.pLabel->setText(tr("Prepare"));
        taskInfo.pPauseBtn->setVisible(true);
        taskInfo.pStartBtn->setVisible(false);
        taskInfo.pPauseBtn->setEnabled(false);
        break;
    case TS_DOWNLOAD:
        taskInfo.pLabel->setText(tr("Download"));
        taskInfo.pPauseBtn->setVisible(true);
        taskInfo.pStartBtn->setVisible(false);
        taskInfo.pPauseBtn->setEnabled(true);
        break;
    case TS_PAUSE:
        taskInfo.pLabel->setText(tr("Pause"));
        taskInfo.pStartBtn->setVisible(true);
        taskInfo.pPauseBtn->setVisible(false);
        taskInfo.pStartBtn->setEnabled(true);
        break;
    case TS_COPYFILE:
        taskInfo.pLabel->setText(tr("Copyfile"));
        taskInfo.pPauseBtn->setVisible(true);
        taskInfo.pStartBtn->setVisible(false);
        taskInfo.pPauseBtn->setEnabled(false);
        break;
    case TS_FINISHE:
        taskInfo.pPauseBtn->setVisible(true);
        taskInfo.pStartBtn->setVisible(false);
        taskInfo.pPauseBtn->setEnabled(false);
        taskInfo.pLabel->setText(tr("Finished"));
        break;
    case TS_ERROR:
        taskInfo.pStartBtn->setVisible(true);
        taskInfo.pPauseBtn->setVisible(false);
        taskInfo.pStartBtn->setEnabled(false);
        if (taskInfo.copyLicState & CSNotEnoughDiskSpace || taskInfo.copyLicState & CSNotEnoughDiskSpace)
        {
            taskInfo.pLabel->setText(tr("Not Enough Space"));
        }
        else if (taskInfo.copyLicState & CSError || taskInfo.copyLicState & CSError)
        {
            taskInfo.pLabel->setText(tr("Copy File Failed"));
        }
        else
        {
            taskInfo.pLabel->setText(tr("Download File Failed"));
        }

    default:
        break;
    }
    if (taskInfo.taskState == TS_FINISHE)
    {
        taskInfo.pDownloadProgressBar->setValue(taskInfo.pDownloadProgressBar->maximum());
    }
    else
    {
        taskInfo.pDownloadProgressBar->setValue(taskInfo.downloadProgress);
    }

}

void TTaskTabWidget::AddCopyTask(TTaskInfo& taskInfo)
{
    QString strMediaPath = TDeviceDataManager::Instance().GetMediaPath();
    QString strMprFileName = taskInfo.downloadInfo.tBookName + "_" + taskInfo.downloadInfo.tVersion + "_" + taskInfo.downloadInfo.tPrefixCode;
    QString strMprDestPath = strMediaPath + "/" + strMprFileName + ".mpr";
    QString strLicDestPath = strMediaPath + "/" + strMprFileName + ".lic";
    TCopyFileTask* pMprTask = TCopyFileManager::Instance().AddTask(taskInfo.downloadInfo.tFilePath, strMprDestPath);
    TCopyFileTask* pLicTask = TCopyFileManager::Instance().AddTask(taskInfo.downloadInfo.tLicencePath, strLicDestPath);
    connect(pLicTask, &TCopyFileTask::CopyStateChanged, this, &TTaskTabWidget::OnCopyLicStateChange);
    connect(pMprTask, &TCopyFileTask::CopyStateChanged, this, &TTaskTabWidget::OnCopyMprStateChange);
    connect(pMprTask, &TCopyFileTask::CopyProgressChanged, this, &TTaskTabWidget::OnCopyProgressChanged);
    taskInfo.pCopyMprTask = pMprTask;
    taskInfo.pCopyLicenseTask = pLicTask;
    taskInfo.downloadInfo.tFilePath = strMprDestPath;
    taskInfo.downloadInfo.tLicencePath = strLicDestPath;
    TCopyFileManager::Instance().ActivateTask(pLicTask, false);
    TCopyFileManager::Instance().ActivateTask(pMprTask, false);
}

void TTaskTabWidget::HandleProgressMsg(int nDownloadTaskId, int nProgerss)
{
    int nIndex = GetTaskIndex(nDownloadTaskId);
    if (nIndex != -1)
    {
        mTaskInfoList[nIndex].downloadProgress = nProgerss;
        mTaskInfoList[nIndex].pDownloadProgressBar->setValue(nProgerss);
    }

}


void TTaskTabWidget::HandleStateMsg(int nDownloadTaskId, int nState, TSVHashMap& extraParams)
{
    int nIndex = GetTaskIndex(nDownloadTaskId);
    if (nIndex != -1)
    {
        switch (nState)
        {
        case HTS_PAUSED:
            mTaskInfoList[nIndex].taskState = TS_PAUSE;
            break;
        case HTS_PREPARED:
            mTaskInfoList[nIndex].taskState = TS_PREPARE;
            break;
        case HTS_RUNNING:
            mTaskInfoList[nIndex].taskState = TS_DOWNLOAD;
            break;
        case HTS_FAILED:
            mTaskInfoList[nIndex].taskState = TS_ERROR;
            break;
        case  HTS_FINISHED:
            mTaskInfoList[nIndex].taskState = TS_COPYFILE;
            mTaskInfoList[nIndex].downloadInfo.tFilePath = extraParams.value(MPRPATH).toString();
            mTaskInfoList[nIndex].downloadInfo.tVersion = extraParams.value(VERSION).toString();
            mTaskInfoList[nIndex].downloadInfo.tLicencePath = extraParams.value(LICENSEPATH).toString();
            AddCopyTask(mTaskInfoList[nIndex]);
            break;
        }
        UpdatetTaskShow(mTaskInfoList[nIndex]);
    } 
}

void TTaskTabWidget::HandleErrorMsg(int nDownloadTaskId, QString strError)
{
    qDebug() << __FUNCTION__ << nDownloadTaskId << strError;
    int nIndex = GetTaskIndex(nDownloadTaskId);
    if (nIndex != -1)
    {
        mTaskInfoList[nIndex].taskState = TS_ERROR;
        mTaskInfoList[nIndex].pLabel->setText(tr("file download error"));
    }

}

int TTaskTabWidget::GetTaskIndex(int nDownloadTaskId)
{
    for (int i = 0; i < mTaskInfoList.size(); i ++)
    {
        if (mTaskInfoList.at(i).nDownloadTaskId == nDownloadTaskId)
        {
            return i;
        }
    }
    return -1;
}

int TTaskTabWidget::GetTaskIndex(TCopyFileTask* pTask, bool bMpr /*= true*/)
{
    for (int i = 0; i < mTaskInfoList.size(); i++)
    {
        if (bMpr)
        {
            if (mTaskInfoList.at(i).pCopyMprTask == pTask)
            {
                return i;
            }
        }
        else
        {
            if (mTaskInfoList.at(i).pCopyLicenseTask == pTask)
            {
                return i;
            }
        }
    }
    return -1;
}

void TTaskTabWidget::OnCopyMprStateChange(CopyFileState state)
{
    TCopyFileTask* pSender = static_cast<TCopyFileTask*>(sender());
    int nIndex = GetTaskIndex(pSender, true);
    if (nIndex != -1)
    {
        mTaskInfoList[nIndex].copyMprState = state;
        UpdateCopyTaskState(mTaskInfoList[nIndex]);
        if (state & CSError || state & CSCopySucceeded)
        {
            TCopyFileManager::Instance().RemoveTask(mTaskInfoList[nIndex].pCopyMprTask);
        }
    }

}

void TTaskTabWidget::OnCopyProgressChanged(qint64 qCopyed, qint64 qTotal)
{
    TCopyFileTask* pSender = static_cast<TCopyFileTask*>(sender());
    int nIndex = GetTaskIndex(pSender, true);
    if (nIndex != -1)
    {
        mTaskInfoList[nIndex].pDownloadProgressBar->setMaximum(qTotal);
        mTaskInfoList[nIndex].pDownloadProgressBar->setValue(qCopyed);
    }
}


void TTaskTabWidget::OnCopyLicStateChange(CopyFileState state)
{
    TCopyFileTask* pSender = static_cast<TCopyFileTask*>(sender());
    int nIndex = GetTaskIndex(pSender, false);
    if (nIndex != -1)
    {
        mTaskInfoList[nIndex].copyLicState = state;
        UpdateCopyTaskState(mTaskInfoList[nIndex]);
        if (state & CSError || state & CSCopySucceeded)
        {
            TCopyFileManager::Instance().RemoveTask(mTaskInfoList[nIndex].pCopyLicenseTask);
        }
    }
}

void TTaskTabWidget::UpdateCopyTaskState(TTaskInfo& taskInfo)
{
    if (taskInfo.copyLicState == CSCopySucceeded && taskInfo.copyMprState == CSCopySucceeded)
    {
        taskInfo.taskState = TS_FINISHE;
        taskInfo.downloadInfo.tFileCreateTime = QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss");
        taskInfo.downloadInfo.tFileSize = QFileInfo(taskInfo.downloadInfo.tFilePath).size();    
        UpdatetTaskShow(taskInfo);
        UpdateDeviceInfo();
        TDeviceDataManager::Instance().AddDeviceFile(taskInfo.downloadInfo);
        emit capacityChange();
    }
    else
    {
        if (taskInfo.copyLicState & CSError || taskInfo.copyMprState & CSError)
        {
            taskInfo.taskState = TS_ERROR;
            UpdatetTaskShow(taskInfo);
        }

    }
}




