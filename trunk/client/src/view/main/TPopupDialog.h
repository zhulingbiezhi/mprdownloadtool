#ifndef _TPOPUPDIALOG_H_
#define _TPOPUPDIALOG_H_

#include "TBaseDialog.h"

class TPopupDialog : public TBaseDialog
{
    Q_OBJECT
public:
    explicit TPopupDialog(int buttons, QString title, const QString& text, QWidget* parent = 0, int type = Prompt, Qt::WindowFlags f = 0);
    ~TPopupDialog();

    enum TMessageBoxButton
    {
        Ok = 0x00000400,
        Cancel = 0x00400000,
        Yes = 0x00004000,
        No = 0x00010000,
        Close = 0x00200000
    };

    enum TMessageType
    {
        Prompt,
        Warning,
        Error
    };

    typedef TMessageBoxButton MsgBtnFlag;

    void CreateTitleWidget(QString title);
    void CreateCenterWidget(int type, QString mString, int button);

    int  GetResult();
    void AutoCloseDialog(int mtime = 3);

    void SetButtonText(TMessageBoxButton, QString);

signals:
    void ClickedBnt();

private slots:
    void OnOKAction();
    void OnCancelAction();
    void OnYesAction();
    void OnNoAction();
    void OnClose();
    void OnTimeOut();

private:
    QWidget     *mpTitleWidget;
    QWidget     *mpCenterWidget;

    QPushButton *mpBtnOk;
    QPushButton *mpBtnCancel;
    QPushButton *mpBtnYes;
    QPushButton *mpBtnNO;

    QTimer      *mpCloseTimer;
    int          mCloseTime;
    int          mResult;

    QShortcut   *mpMainEnterKey;
    QShortcut   *mpSmallEnterKey;

    double opacityInc;
    double maxOpacity;
    bool isDisplay;
    int displayTime;
    int displayInterval;
};

#endif


