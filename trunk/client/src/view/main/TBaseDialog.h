#ifndef _T_BASE_DIALOG_H_
#define _T_BASE_DIALOG_H_

#define SHADOWFRAMEWIDTH 36

class TBaseDialog : public QDialog
{
    Q_OBJECT
public:
    TBaseDialog(QWidget* parent = NULL, Qt::WindowFlags f = 0);
    ~TBaseDialog();

    void SetLayout(QLayout* lyt);
    void SetFixedSize(int nWidth, int nHeight);
    void SetMinimumSize(int nMinWidth, int nMinHeight);
    void SetMaximumSize(int nMaxWidth, int nMaxHeight);
    void SetStyleSheet(const QString& styleSheet);
    void SetShake(bool flag) { mbShake = flag; }

protected:
    virtual void paintEvent(QPaintEvent *);
    virtual void mousePressEvent(QMouseEvent *event);
    virtual void mouseMoveEvent(QMouseEvent *event);
    virtual void mouseReleaseEvent(QMouseEvent *event);
    virtual void closeEvent(QCloseEvent *);

private:
    void CreateUI();
    void MoveDialog(QPoint curMousePoint);

private:
    QFrame *mpMainFrame;
    
    QImage  mImage;
    QImage  mImageShake;
    bool    mbShake;
    bool    mbMouseLeftKeyDown;
    QPoint  mCurTopLeftPt;
    QPoint  mCurMovPt;
};

#endif