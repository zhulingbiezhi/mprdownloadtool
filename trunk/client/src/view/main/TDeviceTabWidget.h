#ifndef _TDEVICETABWIDGET_H
#define _TDEVICETABWIDGET_H
class TDeviceInfoWidget;
class TPageNaviBar;
struct TMPRFileInfoItem;
class TDeviceTabWidget : public QFrame
{
    Q_OBJECT
public:
    TDeviceTabWidget(QWidget* parent = NULL);
    ~TDeviceTabWidget();
    void ResetDeviceInfo();
    void ShowSPIInfo(QString);

private:
    void CreateUI();
    void InitConnect();
    void InitTable();
    void RefreshTable();
    void UpdatePageNavBar();
    void ClearTable();
    void AddRowToTable(const int& id,const TMPRFileInfoItem& fileInfo);
    int  GetClickedRow();

public slots:
    void UpdateDeviceInfo();

private slots:
    void OnRefreshAllData(char);
    void OnNewFileAdded();
    void JumpToPage(int);
    void OnDeleteBtnClicked();

signals:
    void capacityChange();


private:
    QTableWidget*            mpBookInfoTable;
    TDeviceInfoWidget*       mpDeviceInfoWidget;
    TPageNaviBar*            mpPageNaviBar;
    QList<TMPRFileInfoItem>  mShowFileInfoList;
    int                      mnCurrentPage;

};

#endif