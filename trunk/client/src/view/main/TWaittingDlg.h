#ifndef _TWAITTINGDLG_H
#define _TWAITTINGDLG_H
#include "TBaseDialog.h"
class TWaittingDlg : public TBaseDialog
{
    Q_OBJECT
public:
    TWaittingDlg(QWidget* parent = NULL, Qt::WindowFlags f = 0);
    ~TWaittingDlg();
    void ShowText(QString str);
    void Close();

private:
    void CreateUI();
    void InitTimer();
    void CreateCenterWidget();
    void CreateTitleWidget();

private slots:
    void OnTimerTricked();
private:
    int        mnTrickCount;
    QWidget*   mpCenterWidget;
    QWidget*   mpTitleWidget;
    QTimer*    mpTimer;
    QLabel*    mpWaitingLabel;
    QLabel*    mpTipsLabel;

};

#endif