#ifndef _TDEVICEINFOWIDGET_H
#define _TDEVICEINFOWIDGET_H
class TDeviceInfoWidget : public QFrame
{
    Q_OBJECT
public:
    TDeviceInfoWidget(QWidget* parent = NULL);
    ~TDeviceInfoWidget();

public:
    void UpdateShowInfo();
    void ResetDeviceInfo();
    void ShowSPIInfo(QString strDeviceSn);
private:
    void CreateUI();
    void UpdateCapacityInfo(qint64 qTotal, qint64 qUsed);

private:
    QLabel*           mpDeviceTypeLabel;
    QLabel*           mpDeviceIdLabel;
    QProgressBar*     mpCapacityProgress;
    QLabel*           mpCapacityLabel;

};

#endif