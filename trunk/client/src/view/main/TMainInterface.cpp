#include "TMainInterface.h"
#include "TUIStyleManager.h"
#include "TSplashScreen.h"
#include "TMainWindow.h"
#include "TOperationHandle.h"
#include "TMessageBox.h"
#include "TFileEncryptAndDecrypt.h"
#include "TCommonHelper.h"
#include "TDeleteFileThread.h"

TMainInterface::TMainInterface(QObject * pParent)
: QObject(pParent)
, mpMainWindows(nullptr)
, mbInitiative(true)
, mThread(nullptr)
{
}

TMainInterface::~TMainInterface()
{
    if (mpMainWindows) {
        delete mpMainWindows;
        mpMainWindows = NULL;
    }
}

void TMainInterface::Run()
{
    if (NULL == mpMainWindows) {
        mpMainWindows = new TMainWindow();
    }
    mpMainWindows->show();
}

void TMainInterface::OnLoginSuccess()
{
    mpWgtLogin->hide();
    TFileEncryptAndDecrypt::Instance().InitKey(TOperationHandle::Instance()->GetCurUserInfo().userName);
    if (mbInitiative) {
        if (NULL != mpMainWindows) {
            disconnect(mpMainWindows, &TMainWindow::SigMainWindowClose, this, &TMainInterface::OnMainWindowClose);
            disconnect(mpMainWindows, &TMainWindow::SigMainWindowExit, this, &TMainInterface::OnExit);
            delete mpMainWindows;
            mpMainWindows = NULL;
        }

    } else {
        if (NULL == mpMainWindows) {
            return;
        }

        if (mpWgtLogin->IsForceToLogin()) {
            disconnect(mpMainWindows, &TMainWindow::SigMainWindowClose, this, &TMainInterface::OnMainWindowClose);
            disconnect(mpMainWindows, &TMainWindow::SigMainWindowExit, this, &TMainInterface::OnExit);
            delete mpMainWindows;
            mpMainWindows = NULL;

        } else {
            mpMainWindows->show();
			TInfoManage::Instance()->SetInfo("", CLEAR_INFO);
            return;
        }
    }

    QPixmap pixmap(TUIStyleManager::Instance().GetCurrentStyleDir() + "splash.png");

    TSplashScreen *splash = new TSplashScreen(pixmap);
    splash->show();
    qApp->processEvents();
    QThread::usleep(800000);
    
    mpMainWindows = new TMainWindow();
    connect(mpMainWindows, &TMainWindow::SigMainWindowClose, this, &TMainInterface::OnMainWindowClose);
    connect(mpMainWindows, &TMainWindow::SigMainWindowExit, this, &TMainInterface::OnExit);

    mpMainWindows->UpdateShow();
   
    //bug: window frame repaint error
    if (!mpMainWindows->isMaximized())
    {
        mpMainWindows->setWindowState((mpMainWindows->windowState() & ~(Qt::WindowMinimized | Qt::WindowFullScreen))
            | Qt::WindowMaximized);
        mpMainWindows->showNormal();
    }
    else
    {
        mpMainWindows->show();
    }

    if (!mThread)
    {
        mThread = new TDeleteFileThread();
    }
    if (mThread->isRunning())
    {
        mThread->quit();
        mThread->wait();
    }
    mThread->start();
    mThread->setPriority(QThread::HighPriority);

    if (NULL != splash) {
        splash->finish(mpMainWindows);
        delete splash;
        splash = NULL;
    }
    //TFileEncryptAndDecrypt::Instance().InitKey();
    return;
}

void TMainInterface::OnExit()
{
    qApp->exit(0);
}

void TMainInterface::OnMainWindowClose(bool bInitiative)
{
    mbInitiative = bInitiative;

    mpWgtLogin->SetWarningTips(false);
    if (bInitiative) {
        mpMainWindows->SaveState();
        mpMainWindows->hide();

    } else {
        mpWgtLogin->SetWarningTips(true);
    }
    mpWgtLogin->show();
}
