#ifndef __TBASE_MESSAGEBOX_H__
#define __TBASE_MESSAGEBOX_H__

#include "TCommonDialog.h"

class TBaseMessageBox :public TCommonDialog
{
	Q_OBJECT
public:
	enum Btn_Type
	{
		Type_OneBtn,
		Type_TwoBtn
	};

	enum TYPE_CLICKID
	{
		FIRST_BTN_CLICKED = 0,
		SECOND_BTN_CLICKED
	};
public:
	TBaseMessageBox(QWidget* parent = NULL, const QString& strMsgText = "", Btn_Type btnType = Type_TwoBtn);
	~TBaseMessageBox();
signals:
	void btnClickedSignal(int);
private slots:
	void OnBtnClicked();
public:
	void SetMsgText(const QString& strMsgText);
	void SetBtnText(int iIndex, const QString& strText);//0: first btn 1:second btn
	int GetClikedBtnId()const;
private:
	void CreateUI();
	void CreateLayOut();
	void CreateConnect();
	void MoveToParentCenter(QWidget* parent);
private:
	int mnBtnID;
	QLabel*  mpImageLbl;
	QLabel*  mpTipsMsgLbl;
	QPushButton* mpOkBtn;
	QPushButton* mpCancelBtn;
	Btn_Type  mBtnType;
	QFrame* mpTipsFrame;
};

#endif