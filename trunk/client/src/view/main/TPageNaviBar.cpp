#include "TPageNaviBar.h"


TPageNaviBar::TPageNaviBar(QWidget *pParent)
: QWidget(pParent)
, mTotalPageCnt(0)
, mCurPageIndex(0)
, mPageGrpStartIdx(2)
, mbNextBatchToInt(false)
, mbPrevBatchToInt(false)
{
    setObjectName("TPageNaviBar");
    setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
    setFixedHeight(30);
    createwidget();
    createlayout();
    createconnect();
    setCurPageInfo(0, 0, 0);
}

void TPageNaviBar::createwidget()
{
    mPrevPageBtn = new QPushButton(this);
    mPrevPageBtn->setFlat(true);
    mPrevPageBtn->setFixedSize(23, 23);
    mPrevPageBtn->setFocusPolicy(Qt::NoFocus);
    mPrevPageBtn->setObjectName("NaviPrevBtn");
    mPrevPageBtn->setToolTip(tr("Previous Page"));

    mFirstPageBtn = new QPushButton(this);
    mFirstPageBtn->setFlat(true);
    mFirstPageBtn->setText(" 1 ");
    mFirstPageBtn->setCheckable(true);
    mFirstPageBtn->setFixedSize(23, 23);
    mFirstPageBtn->setFocusPolicy(Qt::NoFocus);
    mFirstPageBtn->setObjectName("SpecPageBtn");

    mPrevBatchBtn = new QPushButton(this);
    mPrevBatchBtn->setFlat(true);
    mPrevBatchBtn->setText("...");
    mPrevBatchBtn->setFixedSize(23, 23);
    mPrevBatchBtn->setFocusPolicy(Qt::NoFocus);
    mPrevBatchBtn->setObjectName("BatchNaviBtn");

    for (int i = 0; i < 5; i++) {
        mPageBtnGroup[i] = new QPushButton(this);
        mPageBtnGroup[i]->setFlat(true);
        mPageBtnGroup[i]->setCheckable(true);
        mPageBtnGroup[i]->setFixedSize(23, 23);
        mPageBtnGroup[i]->setFocusPolicy(Qt::NoFocus);
        mPageBtnGroup[i]->setObjectName("SpecPageBtn");
    }

    mNextBatchBtn = new QPushButton(this);
    mNextBatchBtn->setFlat(true);
    mNextBatchBtn->setText("...");
    mNextBatchBtn->setFixedSize(23, 23);
    mNextBatchBtn->setFocusPolicy(Qt::NoFocus);
    mNextBatchBtn->setObjectName("BatchNaviBtn");

    mLastPageBtn = new QPushButton(this);
    mLastPageBtn->setFlat(true);
    mLastPageBtn->setText(" 1 ");
    mLastPageBtn->setCheckable(true);
    mLastPageBtn->setFixedSize(23, 23);
    mLastPageBtn->setFocusPolicy(Qt::NoFocus);
    mLastPageBtn->setObjectName("SpecPageBtn");

    mNextPageBtn = new QPushButton(this);
    mNextPageBtn->setFlat(true);
    mNextPageBtn->setFixedSize(23, 23);
    mNextPageBtn->setFocusPolicy(Qt::NoFocus);
    mNextPageBtn->setObjectName("NaviNextBtn");
    mNextPageBtn->setToolTip(tr("Next Page"));

    mJmpPageEdit = new QLineEdit(this);
    mJmpPageEdit->setFixedWidth(36);
    mJmpPageEdit->setFixedHeight(23);
    mJmpPageEdit->setFocusPolicy(Qt::ClickFocus);
    mJmpPageEdit->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    mJmpPageEdit->setObjectName("JumpToPageEdit");
    QIntValidator* validator = new QIntValidator(0, 65536, mJmpPageEdit);
    mJmpPageEdit->setValidator(validator);

    mPageInfoLabel1 = new QLabel(this);
    mPageInfoLabel1->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
    mPageInfoLabel1->setMinimumWidth(16);
    //mPageInfoLabel1->setText(tr("Total %1 pages / Total %2 records, To").arg(1).arg(2));
    mPageInfoLabel1->setObjectName("PageInfoLabel");

    mPageInfoLabel2 = new QLabel(this);
    mPageInfoLabel2->setAlignment(Qt::AlignLeft | Qt::AlignVCenter);
    mPageInfoLabel2->setMinimumWidth(16);
    mPageInfoLabel2->setText(tr("Page"));
    mPageInfoLabel2->setObjectName("PageInfoLabel");

    QButtonGroup* btnGrp = new QButtonGroup(this);
    btnGrp->addButton(mFirstPageBtn);
    btnGrp->addButton(mPageBtnGroup[0]);
    btnGrp->addButton(mPageBtnGroup[1]);
    btnGrp->addButton(mPageBtnGroup[2]);
    btnGrp->addButton(mPageBtnGroup[3]);
    btnGrp->addButton(mPageBtnGroup[4]);
    btnGrp->addButton(mLastPageBtn);
    btnGrp->setExclusive(true);
}

void TPageNaviBar::createlayout()
{
    QHBoxLayout* layout = new QHBoxLayout(this);
     
    layout->setSpacing(6);
    layout->setContentsMargins(0, 0, 0, 0); 
    layout->addWidget(mPrevPageBtn);
    layout->addWidget(mFirstPageBtn);
    layout->addWidget(mPrevBatchBtn);
    layout->addWidget(mPageBtnGroup[0]);
    layout->addWidget(mPageBtnGroup[1]);
    layout->addWidget(mPageBtnGroup[2]);
    layout->addWidget(mPageBtnGroup[3]);
    layout->addWidget(mPageBtnGroup[4]);
    layout->addWidget(mNextBatchBtn);
    layout->addWidget(mLastPageBtn);
    layout->addWidget(mNextPageBtn);
    layout->addWidget(mPageInfoLabel1);
    layout->addWidget(mJmpPageEdit);
    layout->addWidget(mPageInfoLabel2);
}

void TPageNaviBar::createconnect()
{
    connect(mPrevPageBtn, SIGNAL(clicked()), this, SLOT(onPrevPageBtnClicked()));
    connect(mFirstPageBtn, SIGNAL(clicked()), this, SLOT(onFirstPageBtnClicked()));
    connect(mPrevBatchBtn, SIGNAL(clicked()), this, SLOT(onPrevBatchBtnClicked()));
    connect(mPageBtnGroup[0], SIGNAL(clicked()), this, SLOT(onPageBtnGroup0Clicked()));
    connect(mPageBtnGroup[1], SIGNAL(clicked()), this, SLOT(onPageBtnGroup1Clicked()));
    connect(mPageBtnGroup[2], SIGNAL(clicked()), this, SLOT(onPageBtnGroup2Clicked()));
    connect(mPageBtnGroup[3], SIGNAL(clicked()), this, SLOT(onPageBtnGroup3Clicked()));
    connect(mPageBtnGroup[4], SIGNAL(clicked()), this, SLOT(onPageBtnGroup4Clicked()));
    connect(mNextBatchBtn, SIGNAL(clicked()), this, SLOT(onNextBatchBtnClicked()));
    connect(mLastPageBtn, SIGNAL(clicked()), this, SLOT(onLastPageBtnClicked()));
    connect(mNextPageBtn, SIGNAL(clicked()), this, SLOT(onNextPageBtnClicked()));
    connect(mJmpPageEdit, SIGNAL(returnPressed()), this, SLOT(onPageEditFinished()));
}

void TPageNaviBar::onPrevPageBtnClicked()
{
    emit sigJumpToPage(mCurPageIndex - 1);
}

void TPageNaviBar::onFirstPageBtnClicked()
{
    emit sigJumpToPage(1);
}

void TPageNaviBar::onPrevBatchBtnClicked()
{   
    if (mbPrevBatchToInt)
    {
        int index = mPrevBatchBtn->text().toInt();
        emit sigJumpToPage(index);
        return;
    }
    emit sigJumpToPage(mCurPageIndex - 5);
}

void TPageNaviBar::onPageBtnGroup0Clicked()
{
    QPushButton * revObj = (QPushButton *)(sender());
    int index = revObj->text().toInt();
    emit sigJumpToPage(index);
}

void TPageNaviBar::onPageBtnGroup1Clicked()
{
    QPushButton * revObj = (QPushButton *)(sender());
    int index = revObj->text().toInt();
    emit sigJumpToPage(index);
}

void TPageNaviBar::onPageBtnGroup2Clicked()
{
    QPushButton * revObj = (QPushButton *)(sender());
    int index = revObj->text().toInt();
    emit sigJumpToPage(index);
}

void TPageNaviBar::onPageBtnGroup3Clicked()
{
    QPushButton * revObj = (QPushButton *)(sender());
    int index = revObj->text().toInt();
    emit sigJumpToPage(index);
}

void TPageNaviBar::onPageBtnGroup4Clicked()
{
    QPushButton * revObj = (QPushButton *)(sender());
    int index = revObj->text().toInt();
    emit sigJumpToPage(index);
}

void TPageNaviBar::onNextBatchBtnClicked()
{
    if (mbNextBatchToInt)
    {
        int index = mNextBatchBtn->text().toInt();
        emit sigJumpToPage(index);
        return;
    }
    emit sigJumpToPage(mCurPageIndex + 5);
}

void TPageNaviBar::onLastPageBtnClicked()
{
    emit sigJumpToPage(mTotalPageCnt);
}

void TPageNaviBar::onNextPageBtnClicked()
{
    emit sigJumpToPage(mCurPageIndex + 1);
}

void TPageNaviBar::onPageEditFinished()
{
    emit sigJumpToPage(mJmpPageEdit->text().toInt());
}

void TPageNaviBar::setCurPageInfo(int curPage, int totalPageCnt, int totalCnt)
{
    if (totalPageCnt <= 1) {
        setVisible(false);
        return;
    }

    setVisible(true);

    mCurPageIndex = curPage;
    mTotalPageCnt = totalPageCnt;


    setVisibleAll(false);


    mFirstPageBtn->setVisible(true);
    mFirstPageBtn->setChecked(true);

    for (int i = 0; i < 3 && i < mTotalPageCnt - 1; i++)
    {
        mPageBtnGroup[i]->setVisible(true);
        mPageBtnGroup[i]->setText(QString::number(i + 2));
    }

    if (mTotalPageCnt > 4)
    {
        int leftCnt = 0;
        int rightCnt = 0;

        if (curPage < 4)
        {
            leftCnt = 4;
        }  
        if (mTotalPageCnt - curPage < 4)
        {
            rightCnt = mTotalPageCnt - 2;
        }
        
        if (rightCnt !=0 && leftCnt != 0)
        {
            curPage = (leftCnt + rightCnt) / 2;
        }
        else if (leftCnt != 0)
        {
            curPage = leftCnt;
        }
        else if (rightCnt != 0)
        {
            curPage = rightCnt;
        }
        else
        {
            ;
        }
        
        leftCnt = curPage;
        rightCnt = mTotalPageCnt - curPage;

        if (leftCnt > 4)
        {
            mPrevBatchBtn->setVisible(true);
            mPrevBatchBtn->setText("...");
            mbPrevBatchToInt = false;
            for (int i = 0; i < 3; i++)
            {
                mPageBtnGroup[i]->setVisible(true);
                mPageBtnGroup[i]->setText(QString::number(curPage + i - 2));
            }
        }

        if (rightCnt > 3)
        {
            for (int i = 3; i < 5; i++)
            {
                mPageBtnGroup[i]->setVisible(true);
                mPageBtnGroup[i]->setText(QString::number(curPage + i - 2));
            }
            mNextBatchBtn->setVisible(true);
            mNextBatchBtn->setText("...");
            mbNextBatchToInt = false;
            mLastPageBtn->setVisible(true);
            mLastPageBtn->setText(QString::number(mTotalPageCnt));

        }
        else
        {
            for (int i = 3; i < 5 && i - 3 < mTotalPageCnt - curPage; i++)
            {
                if (curPage + i - 2 <= 4)
                {
                    continue;
                }
                mPageBtnGroup[i]->setVisible(true);
                mPageBtnGroup[i]->setText(QString::number(curPage + i - 2));
            }
            if (rightCnt == 3)
            {
                mLastPageBtn->setVisible(true);
                mLastPageBtn->setText(QString::number(mTotalPageCnt));
            }
        }
    }

    mPrevPageBtn->setEnabled(mCurPageIndex > 1);
    mNextPageBtn->setEnabled(mCurPageIndex < mTotalPageCnt);
    mPageInfoLabel1->setText(tr("Total %1 pages / Total %2 records, To").arg(totalPageCnt).arg(totalCnt));
    mJmpPageEdit->setText(QString::number(mCurPageIndex));

    if (mPageBtnGroup[0]->text().compare(QString()) != 0)
    {
        int first = mFirstPageBtn->text().toInt();
        int first1 = mPageBtnGroup[0]->text().toInt();
        if (first1 - first == 2)
        {
            mPrevBatchBtn->setText(QString::number(2));
            mbPrevBatchToInt = true;
        }
    }
    
    if (mPageBtnGroup[4]->text().compare(QString()) != 0)
    {
        int last = mLastPageBtn->text().toInt();
        int last1 = mPageBtnGroup[4]->text().toInt();
        if (last - last1 == 2)
        {
            mNextBatchBtn->setText(QString::number(mTotalPageCnt - 1));
            mbNextBatchToInt = true;
        }
    }

    for (int i = 0; i < 5; i++ )
    {
        mPageBtnGroup[i]->setChecked(false);
        if (mPageBtnGroup[i]->text().toInt() == mCurPageIndex)
        {
            mPageBtnGroup[i]->setChecked(true);
            mFirstPageBtn->setChecked(false);
            break;
        }
        if (mbPrevBatchToInt)
        {
            mPrevBatchBtn->setChecked(false);
        }
        if (mbNextBatchToInt)
        {
            mNextBatchBtn->setChecked(false);
        }
    }
    mPrevBatchBtn->setCheckable(mbPrevBatchToInt);
    mNextBatchBtn->setCheckable(mbNextBatchToInt);
    mPrevBatchBtn->setProperty("Batch", mbPrevBatchToInt);
    mNextBatchBtn->setProperty("Batch", mbNextBatchToInt);
    if (style())
    {
        style()->polish(mPrevBatchBtn);
        style()->polish(mNextBatchBtn);
    }
}

void TPageNaviBar::setVisibleAll(bool /*state*/)
{
    for (int i = 0; i < 5; i++) 
    {
        mPageBtnGroup[i]->setVisible(false);
        mPageBtnGroup[i]->setText(QString());
    }

    //mPrevPageBtn->setVisible(false);
    mFirstPageBtn->setVisible(false);
    mPrevBatchBtn->setVisible(false);
    mNextBatchBtn->setVisible(false);
    mLastPageBtn->setVisible(false);
    //mNextPageBtn->setVisible(false);
}
