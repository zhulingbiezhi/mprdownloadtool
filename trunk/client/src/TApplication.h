#ifndef __TAPPLICATION__H__
#define __TAPPLICATION__H__

#ifndef tApp
    #define tApp (static_cast<TApplication *>(qApp))
#endif

class QLocalServer;
class TApplication : public QApplication
{
    Q_OBJECT

public:
    enum ESoftwareLanguage  {
        ESL_English,
        ESL_Chinesesimp
    };

    TApplication(int &argc, char **argv, const QString& companyname, const QString& appname);
    ~TApplication();

    bool                        IsRunning();

    const QString&              GetAppName() const { return mAppName; }
    const QString&              GetCompanyName() const { return mCompanyName; }

    void                        SetLanguage(ESoftwareLanguage lang);
    ESoftwareLanguage           GetLanguage();
    
signals:
    void ActivateMainWindow();

private slots:  
    void NewLocalConnection();  

private:
    QLocalServer *              mpLocalServer;  
    bool                        mbRunning;
    QString                     mAppName;
    QString                     mCompanyName;
    ESoftwareLanguage           mlanguage;
};

#endif // __TAPPLICATION__H__