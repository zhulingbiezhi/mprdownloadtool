#include "src/TSTDPreCompiledHeader.h"
#include "TLogRecorder.h"
#include "TAppDataDirManager.h"

TLogRecorder::TLogRecorder()
    : mpFile(0)
    , mpMutex(0)
    , mpTextStream(0)
{
   
}

TLogRecorder::~TLogRecorder()
{
    if (mpFile){
        delete mpFile;
        mpFile = 0;
    }
    if (mpTextStream){
        delete mpTextStream;
        mpTextStream = 0;
    }
    if (mpMutex){
        delete mpMutex;
        mpMutex = 0;
    }
}

TLogRecorder& TLogRecorder::Instance()
{
    static TLogRecorder ins;
    return ins;
}

void TLogRecorder::StartRecord()
{
    QDir appDir(TAppDataDirManager::Instance().GetAppDataDir());
    mpFile = new QFile(appDir.absoluteFilePath("MPRDistributer.log"));
    if (mpFile->size() > 10 * 1024 * 1024)
    {
        QFile::remove(mpFile->fileName());
    }
    mpFile->open(QFile::WriteOnly | QFile::Append | QFile::Text);
    mpTextStream = new QTextStream(mpFile);
    mpMutex = new QMutex;

    mOriMsgHandler = qInstallMessageHandler(MsgHandler);
}

void TLogRecorder::MsgHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg )
{
    TLogRecorder::Instance().HandleMsg(type, context, msg);
}

void TLogRecorder::HandleMsg( QtMsgType type, const QMessageLogContext &context, const QString &msg )
{
    QString strBuff;
    switch (type) {
    case QtDebugMsg:
        strBuff = QString("%1 %2 Debug:%3").arg(QDateTime::currentDateTime().toString("yy-MM-dd hh:mm:ss")).
            arg((qint64)QThread::currentThreadId()).arg(msg);
        break;
    case QtWarningMsg:
        strBuff = QString("%1 %2 Warning:%3").arg(QDateTime::currentDateTime().toString("yy-MM-dd hh:mm:ss")).
            arg((qint64)QThread::currentThreadId()).arg(msg);
        break;
    case QtCriticalMsg:
        strBuff = QString("%1 %2 Critical:%3").arg(QDateTime::currentDateTime().toString("yy-MM-dd hh:mm:ss")).
            arg((qint64)QThread::currentThreadId()).arg(msg);
        break;
    case QtFatalMsg:
        strBuff = QString("%1 %2 Fatal:%3").arg(QDateTime::currentDateTime().toString("yy-MM-dd hh:mm:ss")).
            arg((qint64)QThread::currentThreadId()).arg(msg);
        break;
    }

    mpMutex->lock();
    *mpTextStream << strBuff << endl;
    mpMutex->unlock();
    if (mOriMsgHandler)
    {
        mOriMsgHandler(type, context, msg);
    }
}



