#ifndef __TWorkStateConfig_H__
#define __TWorkStateConfig_H__

#include <QString>
#include <QSettings>

class TWorkStateConfig : QObject
{
public:
    TWorkStateConfig(QObject* parant);
    ~TWorkStateConfig();

    static TWorkStateConfig* Instance();

    QVariant        GetValue(const QString& key, const QVariant& default_value = QVariant()) const;
    void            SetValue(const QString& key, const QVariant& value) const;

    void            SetFileName(const QString& file_name);

    void            BeginGroup(const QString& group);
    void            BeginFileGroup();
    void            EndGroup();

    void            CloneKeysToFile(const QString fileName);
   
private:
    QSettings*      mpWorkState;
    QString         mFileName;
    QMutex          mMutex;
};
#endif