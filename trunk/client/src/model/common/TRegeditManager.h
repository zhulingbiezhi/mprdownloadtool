#ifndef __TREGEDITMANAGER_H_
#define __TREGEDITMANAGER_H_

#include <QString>
#include <QSettings>

class TRegeditManager : QObject
{
public:
    TRegeditManager(QObject* parant = 0);
    ~TRegeditManager();

    static TRegeditManager& Instance();
    void            ClearData();

    QVariant        GetValue(const QString& key, const QVariant& default_value = QVariant()) const;
    void            SetValue(const QString& key, const QVariant& value) const;

    void            SetFileName(const QString& file_name);

    void            BeginGroup(const QString& group);
    void            BeginFileGroup();
    void            EndGroup();
   
private:
    QSettings*      mpWorkState;
    QString         mFileName;
    QMutex          mMutex;
    
};
#endif //__TREGEDITMANAGER_H_