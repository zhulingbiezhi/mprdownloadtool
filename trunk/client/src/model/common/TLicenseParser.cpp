﻿#include "TLicenseParser.h"
#include "licParser.h"


TLicenseParser::TLicenseParser()
    : mbIsLoaded(false)
{

}

TLicenseParser::~TLicenseParser()
{

}

bool TLicenseParser::LoadContent( const QByteArray& content )
{
    QXmlStreamReader reader(content);
    if (reader.readNextStartElement())
    {
        if (reader.name() == "MPRLicense")
        {
            ReadMPRLicense(reader);
        }
    }
    if (reader.error())
        return false;

    mbIsLoaded = true;
    return true;
}

void TLicenseParser::ReadMPRLicense(QXmlStreamReader &reader)
{
    Q_ASSERT(reader.isStartElement() && reader.name() == "MPRLicense");

    while (reader.readNextStartElement())
    {
        if (reader.name() == "MPRLicenseInfo")
            ReadMPRLicenseInfo(reader);
         else if (reader.name() == "ResourceEncryptedKeyRels")
             ReadResourceEncryptedKeyRels(reader);
         else if (reader.name() == "EncrytionMethodOfKey")
             ReadEncrytionMethodOfKey(reader);
         else if (reader.name() == "ResourceEncryptedKeys")
             ReadResourceEncryptedKeys(reader);
         else if (reader.name() == "Authorizations")
             ReadAuthorizations(reader);
         else
            reader.skipCurrentElement();
    }
}

void TLicenseParser::ReadMPRLicenseInfo(QXmlStreamReader &reader)
{
    Q_ASSERT(reader.isStartElement() && reader.name() == "MPRLicenseInfo");
    MprLicenseInfo info;

    while (reader.readNextStartElement())
    {
        if (reader.name() == "Version")
            info.version = reader.readElementText();
        else if (reader.name() == "UUID")
            info.uuid = reader.readElementText();
        else if (reader.name() == "IssuedFrom")
            info.issuedFrom = reader.readElementText();
        else if (reader.name() == "IssuedTo")
            info.issuedTo = reader.readElementText();
        else if (reader.name() == "ExpireDate")
            info.expireDate = QDateTime::fromString(reader.readElementText(), "yyyyMMdd");
        else
            reader.skipCurrentElement();
    }
    mLicenseInfo = info;
}

void TLicenseParser::ReadResourceEncryptedKeyRels(QXmlStreamReader &reader)
{
    Q_ASSERT(reader.isStartElement() && reader.name() == "ResourceEncryptedKeyRels");

    mMapResSym.clear();
    while (reader.readNextStartElement())
    {
        if (reader.name() != "ResourceEncryptedKeyRel")
        {
            reader.skipCurrentElement();
            continue;
        }
        QString resId;
        QString keyId;
        while (reader.readNextStartElement())
        {
            if (reader.name() == "ResourceUUID")
                resId = reader.readElementText();
            else if (reader.name() == "KeyUUID")
                keyId = reader.readElementText();
            else
                reader.skipCurrentElement();
        }
        if (!resId.isEmpty() && !keyId.isEmpty())
            mMapResSym.insert(resId, keyId);
    }
}

void TLicenseParser::ReadEncrytionMethodOfKey(QXmlStreamReader &reader)
{
    Q_ASSERT(reader.isStartElement() && reader.name() == "EncrytionMethodOfKey");

    struct AsymmetricEncryption asyEncryption;
    while (reader.readNextStartElement())
    {
        if (reader.name() == "Algorithm")
            asyEncryption.algorithm = reader.readElementText();
        else if (reader.name() == "EncodedMode")
            asyEncryption.encodedMode = reader.readElementText();
        else if (reader.name() == "Length")
            asyEncryption.length = reader.readElementText().toInt();
        else
            reader.skipCurrentElement();
    }
    mAsymmetricEncryption = asyEncryption;
}

void TLicenseParser::ReadResourceEncryptedKeys(QXmlStreamReader &reader)
{
    Q_ASSERT(reader.isStartElement() && reader.name() == "ResourceEncryptedKeys");
    while (reader.readNextStartElement())
    {
        if (reader.name() != "ResourceEncryptedKey")
        {
            reader.skipCurrentElement();
            continue;
        }
        SymmetricEncryption symEncryption;
        while (reader.readNextStartElement())
        {
            if (reader.name() == "UUID")
                symEncryption.uuid = reader.readElementText();
            else if (reader.name() == "Algorithm")
            {
                QString algorithm = reader.readElementText();
                int enAlgorithm = AES_ECB;
                if (algorithm.compare("AES_ECB", Qt::CaseInsensitive) == 0)
                    enAlgorithm = AES_ECB;
                else if (algorithm.compare("AES_CTR", Qt::CaseInsensitive) == 0)
                    enAlgorithm = AES_CTR;
                else if (algorithm.compare("AES_CBC", Qt::CaseInsensitive) == 0)
                    enAlgorithm = AES_CBC;
                else if (algorithm.compare("SM4_ECB", Qt::CaseInsensitive) == 0)
                    enAlgorithm = SM4_ECB;
                else if (algorithm.compare("SM4_CTR", Qt::CaseInsensitive) == 0)
                    enAlgorithm = SM4_CTR;
                else if (algorithm.compare("SM4_CBC", Qt::CaseInsensitive) == 0)
                    enAlgorithm = SM4_CBC;

                symEncryption.algorithm = enAlgorithm;
            }
            else if (reader.name() == "EncodedMode")
                symEncryption.encodeMode = reader.readElementText();
            else if (reader.name() == "Length")
                symEncryption.length = reader.readElementText().toUInt();
            else if (reader.name() == "Key")
                symEncryption.key = reader.readElementText();
            else if (reader.name() == "Signature")
                symEncryption.signature = reader.readElementText();
            else if (reader.name() == "Parameters")
                ReadParameters(reader, symEncryption.parameters);
            else
                reader.skipCurrentElement();
        }
        mMapSymmetricEncryption.insert(symEncryption.uuid, symEncryption);
    }
}

void TLicenseParser::ReadParameters(QXmlStreamReader &reader, QMap<QString, QString> &parameters)
{
    Q_ASSERT(reader.isStartElement());

    parameters.clear();
    while (reader.readNextStartElement())
    {
        if (reader.name() != "Parameter")
        {
            reader.skipCurrentElement();
            continue;
        }

        QString key;
        QString value;
        while (reader.readNextStartElement())
        {
            if (reader.name() == "Key")
                key = reader.readElementText();
            else if (reader.name() == "Value")
                value = reader.readElementText();
        }
        if (!key.isEmpty() && !value.isEmpty())
            parameters.insert(key, value);
    }
}

void TLicenseParser::ReadAuthorizations(QXmlStreamReader &reader)
{
    Q_ASSERT(reader.isStartElement() && reader.name() == "Authorizations");
    mAuthorizations.clear();
    while (reader.readNextStartElement())
    {
        if (reader.name() != "Authorization")
        {
            reader.skipCurrentElement();
            continue;
        }
        Authorization auth;
        while (reader.readNextStartElement())
        {
            if (reader.name() == "Parameters")
                ReadAuthorization(reader, auth);
            else
                reader.skipCurrentElement();
        }
        mAuthorizations.push_back(auth);
    }
}

void TLicenseParser::ReadAuthorization(QXmlStreamReader &reader, Authorization &authorization)
{
    Q_ASSERT(reader.isStartElement());

    authorization.parameters.clear();
    while (reader.readNextStartElement())
    {
        if (reader.name() != "Parameter")
        {
            reader.skipCurrentElement();
            continue;
        }

        QString key;
        QString value;
        while (reader.readNextStartElement())
        {
            if (reader.name() == "Key")
                key = reader.readElementText();
            else if (reader.name() == "Value")
                value = reader.readElementText();
        }
        if (!key.isEmpty() && !value.isEmpty())
        {
            if (key.compare("SecurityRuleID") == 0)
                authorization.type.push_back(value);
            else
                authorization.parameters.insert(key, value);
        }
    }
}

bool TLicenseParser::Save2Bin( QByteArray& out, const QString &symGuid)
{
    if (!mbIsLoaded)
        return false;

    if (mMapSymmetricEncryption.isEmpty())
        return false;

    const SymmetricEncryption *symEnc = NULL;
    if (symGuid.isEmpty())
    {
        symEnc = &mMapSymmetricEncryption.begin().value();
    }
    else if (mMapSymmetricEncryption.contains(symGuid))
    {
        symEnc = &mMapSymmetricEncryption.value(symGuid);
    }
    else
    {
        return false;
    }

    QByteArray key = QByteArray::fromHex(symEnc->key.toLatin1());
    if (symEnc->algorithm == AES_CBC || symEnc->algorithm == SM4_CBC)
    {
        QString strIv = symEnc->parameters.value("Iv");
        if (!strIv.isEmpty())
        {
            QByteArray iv = QByteArray::fromBase64(strIv.toLatin1());
            key.append(iv);
        }
    }
    QByteArray signature = QByteArray::fromHex(symEnc->signature.toLatin1());

    MPRLicense_s license;
    memset(&license, 0, sizeof(license));
    //version
    QRegExp rx("(\\d+)");
    int pos = rx.indexIn(mLicenseInfo.version);
    if (pos >= 0)
    {
        license.Version = static_cast<unsigned char>(rx.cap(1).toUInt());
    }
    //AsymType
    if (mAsymmetricEncryption.algorithm.compare("RSA", Qt::CaseInsensitive) == 0)
        license.AsymType = RSA;
    else if (mAsymmetricEncryption.algorithm.compare("SM2", Qt::CaseInsensitive) == 0)
        license.AsymType = SM2;
    else if (mAsymmetricEncryption.algorithm.compare("ECC", Qt::CaseInsensitive) == 0)
        license.AsymType = SM2;
    else
        license.AsymType = SM2;

    //SymType
    license.SymType = (EncryptAlgorithm)symEnc->algorithm;

    //GUID
    QByteArray uuid = QByteArray::fromHex(mLicenseInfo.uuid.toLatin1());
    memcpy(license.GUID, uuid.constData(), 16);

    //KeyParaLength
    license.KeyParaLength = key.length();

    //KeyPara
    memcpy(license.KeyPara, key.constData(), qMin(key.length(), 256));

    //ExpireDate
    QByteArray expireDate = mLicenseInfo.expireDate.toString("yyyyMMdd").toLatin1();
    memcpy(license.ExpireDate, expireDate.constData(), 8);

    //IssuedType
    license.IssuedType = GetIssuedType();

    //IssuedUser
    QByteArray userId = GetUserId().toUtf8();
    memset(license.IssuedUser, 0, sizeof(license.IssuedUser));

    memcpy((char*)license.IssuedUser, userId.constData(), userId.length());
    license.IssuedUser[userId.length() + 1]= '\0';

    //IssuedFrom
    QByteArray issuedFrom = QByteArray::fromHex(mLicenseInfo.issuedFrom.toLatin1());
    memcpy(license.IssuedFrom, issuedFrom.constData(), sizeof(license.IssuedFrom));

    //IssuedTo
    QByteArray issuedTo = QByteArray::fromHex(mLicenseInfo.issuedTo.toLatin1());
    memcpy(license.IssuedTo, issuedTo.constData(), sizeof(license.IssuedTo));

    //SignatureLength
    license.SignatureLength = signature.length();

    //SignatureData
    memcpy(license.SignatureData, signature.constData(), qMin(256, signature.length()));

    char buffer[512];
    int len = 512;
    if (MPRLicense_Pack(&license, (unsigned char*)buffer, &len) != 0)
        return false;

    out = QByteArray(buffer, len);
    return true;
}


int TLicenseParser::GetIssuedType() const
{
    if (mbIsLoaded)
    {
        if (mAuthorizations.isEmpty())
            return 0;
        const Authorization &auth = *mAuthorizations.begin();
        if (auth.type.isEmpty())
            return 0;
        return auth.type.begin()->toInt();
    }
    return 0;
}

QString TLicenseParser::GetUserId() const
{
    if (mbIsLoaded)
    {
        if (mAuthorizations.isEmpty())
            return QString();
        const Authorization &auth = *mAuthorizations.begin();
        if (auth.type.contains("1"))
            return auth.parameters.value("ContentID");
        else if (auth.type.contains("4"))
            return auth.parameters.value("BrandID");
        else if (auth.type.contains("8"))
            return auth.parameters.value("UserID");
        else if (auth.type.contains("2"))
        {
            if (mLicenseInfo.issuedFrom == mLicenseInfo.issuedTo)
                return QString();
            else
                return auth.parameters.value("DeviceID");
        }
    }
    return QString();
}

TLicenseParser::MprLicenseInfo TLicenseParser::GetLicenseInfo() const
{
    return mLicenseInfo;
}
