#ifndef _TPROXYSTYLE_H__
#define _TPROXYSTYLE_H__

#include <QProxyStyle>

class TProxyStyle : public QProxyStyle
{
public:
    TProxyStyle(QStyle *style = 0);
    ~TProxyStyle();
   
public:
    int styleHint(StyleHint hint, const QStyleOption *option = 0, const QWidget *widget = 0, QStyleHintReturn *returnData = 0) const;
    void drawPrimitive(PrimitiveElement pe, const QStyleOption *opt, QPainter *p,
        const QWidget *w = 0) const;
};

#endif