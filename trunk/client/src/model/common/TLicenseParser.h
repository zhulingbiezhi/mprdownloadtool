﻿#ifndef _TLICENSEPARSER_H_
#define _TLICENSEPARSER_H_

class TLicenseParser 
{
public:
    struct MprLicenseInfo
    {
        QString version;
        QString uuid;
        QString issuedFrom;
        QString issuedTo;
        QDateTime expireDate;
    };

    struct AsymmetricEncryption
    {
        QString algorithm;
        QString encodedMode;
        int length;
    };
    struct SymmetricEncryption
    {
        QString uuid;
        int algorithm;
        QString encodeMode;
        QString key;
        quint32 length;
        QString signature;
        QMap<QString, QString> parameters;
    };

    struct Authorization
    {
        QStringList type;
        QMap<QString, QString> parameters;
    };

    TLicenseParser();
    ~TLicenseParser();

    bool LoadContent(const QByteArray& content);
    bool Save2Bin(QByteArray& out, const QString &symGuid = "");
    MprLicenseInfo GetLicenseInfo() const;

private:
    void ReadMPRLicense(QXmlStreamReader &reader);
    void ReadMPRLicenseInfo(QXmlStreamReader &reader);
    void ReadResourceEncryptedKeyRels(QXmlStreamReader &reader);
    void ReadEncrytionMethodOfKey(QXmlStreamReader &reader);
    void ReadResourceEncryptedKeys(QXmlStreamReader &reader);
    void ReadParameters(QXmlStreamReader &reader, QMap<QString, QString> &parameters);
    void ReadAuthorizations(QXmlStreamReader &reader);
    void ReadAuthorization(QXmlStreamReader &reader, Authorization &authorization);

    int GetIssuedType() const;
    QString GetUserId() const;

private:
    bool                                mbIsLoaded;
    MprLicenseInfo                      mLicenseInfo;
    QMap<QString, QString>              mMapResSym;
    AsymmetricEncryption                mAsymmetricEncryption;
    QMap<QString, SymmetricEncryption>  mMapSymmetricEncryption;
    QList<Authorization>                mAuthorizations;
};

#endif 