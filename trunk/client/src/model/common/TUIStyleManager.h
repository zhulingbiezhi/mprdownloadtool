#ifndef _TUISTYLEMANAGER_H__
#define _TUISTYLEMANAGER_H__

#include <QObject>
#include <QHash>

enum EUI_Style
{
    EUI_Default     = 0,
    EUI_Black_Style = 1,
    EUI_Light_Style = 2
};

struct TResourceInfo
{
    QString  mQssPath;
};

class TUIStyleManager : public QObject
{
    Q_OBJECT

public:
    static TUIStyleManager& Instance();

    bool                    LoadUIStyle(EUI_Style style = EUI_Default);
    const int               GetCurrentStyle();

signals:
    void                    CurrentUIStyleChanged(EUI_Style);

private:
    TUIStyleManager(QObject* parent = 0);
    ~TUIStyleManager();

    const QString           GetUIStyleName(EUI_Style style);
    bool  RegisterResource(EUI_Style style);

private:
    EUI_Style               mCurrentUIStyle;
    EUI_Style               mDefaultUIStyle;

    QHash<QString, QString> mRegisterFile;
};

#endif//_TUISTYLEMANAGER_H__