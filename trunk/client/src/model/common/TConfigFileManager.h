#ifndef _TCONFIGFILEMANGER_H_
#define _TCONFIGFILEMANGER_H_

class TConfigFileManager
{
 private:
    TConfigFileManager();
    ~TConfigFileManager();
 
public:
    static  TConfigFileManager& Instance();

    QString GetConvigValue(const QString& str) const;
    QVariant GetConfigValue(const QString& str) const;
    void SetConfigValue(const QString& str, int style);

private:
    QSettings* mpSetting;
};

#endif