#include "TUIStyleManager.h"
#include "TWorkStateConfig.h"
#include "TApplication.h"

TUIStyleManager::TUIStyleManager(QObject* parent)
    : QObject(parent)
{
    TWorkStateConfig::Instance()->BeginGroup("Common");
    mDefaultUIStyle = static_cast<EUI_Style>(TWorkStateConfig::Instance()->GetValue("Ui_Style").toInt());
    TWorkStateConfig::Instance()->EndGroup();

    mRegisterFile.clear();
}

TUIStyleManager::~TUIStyleManager()
{

}

TUIStyleManager& TUIStyleManager::Instance()
{
    static TUIStyleManager style_manager;
    return style_manager;
}

bool TUIStyleManager::LoadUIStyle(EUI_Style style)
{   
    if (!RegisterResource(style))
    {
        qDebug() << "Error:" << "Register Resource Fail !!!";
        return false;
    }
    
    QString strStyles = QString();
    QStringList fileList = mRegisterFile.keys();
    for (int index = 0; index < fileList.size(); index++)
    {
           QString strRoot = ":/" + fileList.at(index) + "/";
            //load qss
           QFile strQssFileConfig(strRoot + "qss_file_list.ini");
            if (!strQssFileConfig.exists()) {
                qDebug() << "Error:" << "current qss_file_list.ini is missing.";
                return false;
            }

            bool bOpen = strQssFileConfig.open(QIODevice::ReadOnly | QIODevice::Text);
            if (!bOpen) {
                qDebug() << "Error:" << "open file [" << strQssFileConfig.fileName() << "] error";
                return false;
            }

            QStringList strQssFileNameList;
            QTextStream stream(&strQssFileConfig);
            while (!stream.atEnd())
            {
                QString strQSSFileName = stream.readLine();
                if (!strQSSFileName.isEmpty() && 2 <= strQSSFileName.length() && 0 == strQSSFileName.left(2).compare("//")) {
                    continue;
                }
                QFileInfo tmpFile(strQSSFileName);
                if (0 == tmpFile.suffix().toLower().compare("qss")) {
                    strQssFileNameList.append(strQSSFileName);
                }
            }

            //load qss content;

            foreach(QString qssFileName, strQssFileNameList) {
                QFile styleFile(strRoot + qssFileName);
                bool bOpen = styleFile.open(QIODevice::ReadOnly);
                if (!bOpen) {
                    qDebug() << "Error:" << "open file [" << styleFile.fileName() << "] error";
                    continue;
                }

                QByteArray byteAll = styleFile.readAll();
                strStyles += QString(byteAll);
            }
    }
    qApp->setStyleSheet(strStyles);

    EUI_Default == style ? mCurrentUIStyle = mDefaultUIStyle : mCurrentUIStyle = style;

    TWorkStateConfig::Instance()->BeginGroup("Common");
    TWorkStateConfig::Instance()->SetValue("Ui_Style", mCurrentUIStyle);
    TWorkStateConfig::Instance()->EndGroup();

    return true;
}

const int TUIStyleManager::GetCurrentStyle()
{
    return mCurrentUIStyle;
}

const QString TUIStyleManager::GetUIStyleName(EUI_Style style)
{
    switch (style)
    {
        case EUI_Black_Style: {
            return QString("classic_black");
        }

        case EUI_Light_Style: {
            return QString("fresh_blue");
        }

        default: {
            return QString("classic_black");
        }
    }

    return QString("");
}

bool TUIStyleManager::RegisterResource(EUI_Style style)
{       
    QString strStyleName = GetUIStyleName(EUI_Default == style ? mDefaultUIStyle : style);

    QString style_dir = qApp->applicationDirPath() + "/resource/ui/";
    QDir dir(style_dir);
    QFileInfoList rcc_files = dir.entryInfoList(QDir::Files | QDir::Readable);

    for (int i = 0; i < rcc_files.size(); i++)
    {        
        QString fileName = rcc_files[i].fileName();
        if (0 == fileName.right(4).compare(".rcc") && (fileName.contains(tApp->GetAppName())))
        {          
            QString basicFileName = fileName.mid(0, fileName.length() - 4);
            QString registerRoot = "/MPRDownloadTool/ui";

            //unregister
            if (!mRegisterFile[basicFileName].isEmpty() && !QResource::unregisterResource(mRegisterFile[basicFileName], registerRoot))
            {
                qDebug() << "Error: QResource::unregisterResource(" << mRegisterFile[basicFileName] << ")";
                return false;
            }
            //register
            
            if (!QResource::registerResource(rcc_files[i].absoluteFilePath(), registerRoot))
            {
                qDebug() << "Error: QResource::registerResource(" << rcc_files[i].absoluteFilePath() << ")";
                return false;
            }
            mRegisterFile[basicFileName] = rcc_files[i].absoluteFilePath();
        }
    }
    return true;
}

