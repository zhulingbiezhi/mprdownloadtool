#include "TAppCommon.h"

TAppCommon& TAppCommon::Instance()
{
    static TAppCommon obj;
    return obj;
}

TAppCommon::TAppCommon()
{
    QDir dir;
    dir.setCurrent(QApplication::applicationDirPath());
    QString strAppPath = dir.absolutePath();
    QString strConfPath = dir.absoluteFilePath("config.ini");

    QSettings setting(strConfPath, QSettings::IniFormat);
    setting.setIniCodec(QTextCodec::codecForLocale());

    QString strUpgradeServer = setting.value("url/upgrade_url").toString();
    QString strSearchServer = setting.value("url/search_url").toString();

    mstrSavePath = setting.value("save_path").toString();
    mstrSetPath = mstrSavePath;
    mstrCheckFirmwareVersionUrl = strUpgradeServer + "/web/ports/mvc/port/firmware";
    //mstrSearchrUrl = WorldServer + "/web/shop/mvc/store/pc/isbnGoodslist";
    mstrSearchrUrl = strSearchServer + "/web/ports/mvc/port/doSearchByClient";
}

TAppCommon::~TAppCommon()
{

}

void TAppCommon::ChangeSavePath(QString strSavePath)
{
    if (mstrSavePath != strSavePath)
    {
        QDir dir;
        dir.setCurrent(QApplication::applicationDirPath());
        QString strAppPath = dir.absolutePath();
        QString strConfPath = dir.absoluteFilePath("config.ini");

        QSettings setting(strConfPath, QSettings::IniFormat);
        setting.setIniCodec(QTextCodec::codecForLocale());
        setting.setValue("save_path", strSavePath);
        mstrSetPath = strSavePath;
    }
}
