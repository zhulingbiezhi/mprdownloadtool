#ifndef _TAPPCOMMON_H
#define _TAPPCOMMON_H

#define  VERSION "version"
#define  MPRPATH "mprPath"
#define  LICENSEPATH "licensePath"

class TAppCommon
{
public:
    static TAppCommon& Instance();

    void SetMainWindow(QWidget* pWidget) { mpMainWindow = pWidget; }
    void ChangeSavePath(QString strSavePath);
    QWidget* GetMainWindow() const { return mpMainWindow; }

    const QString& GetSearchUrl() const { return mstrSearchrUrl; }
    const QString& GetCheckFirmwareVersionUrl() const { return mstrCheckFirmwareVersionUrl; }
    const QString& GetDownloadSavePath() const { return mstrSavePath; }
    const QString& GetCurrentSetSavePath() const { return mstrSetPath; }

private:
    TAppCommon();
    ~TAppCommon();

private:
    QWidget*     mpMainWindow;
    QString      mstrSavePath;
    QString      mstrSetPath;
    QString      mstrSearchrUrl;
    QString      mstrCheckFirmwareVersionUrl;

};
#endif