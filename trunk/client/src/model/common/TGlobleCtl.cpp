#include "TSTDPreCompiledHeader.h"
#include "TGlobleCtl.h"

TGlobleCtl::TGlobleCtl()
{

}

TGlobleCtl::~TGlobleCtl()
{

}

void TGlobleCtl::CopyLicenseToDisk( const TMPRFileInfoItem& item, const TMPRDeviceDesc& desc )
{
    QString srcFilePath = item.tLicencePath;
    QFileInfo srcFileInfo(srcFilePath);
    QString desFilePath = desc.mDeviceMediaPath  + srcFileInfo.fileName();
    if (QFile::exists(desFilePath)) {
        QFile::remove(desFilePath);
    }
    QFile::copy(srcFilePath, desFilePath);
}

//转换文件类型 为字符串描述信息
QString TGlobleCtl::SwitchFileType( FileType fileType)
{
    if (fileType & FT_OGG){
        return QString("OGG");
    }

    if (fileType & FT_MP3){
        return QString("MP3");
    }

    if (fileType & FT_MPRX){
        return QString("MPRX");
    }
    return QString();
}
