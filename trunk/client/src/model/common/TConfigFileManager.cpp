#include "src/TSTDPreCompiledHeader.h"
#include "TConfigFileManager.h"
#include "TAppDefine.h"

TConfigFileManager::TConfigFileManager()
{
    QString confPath = g_ConfigFilePath;
    mpSetting = new QSettings(confPath, QSettings::IniFormat);
    mpSetting->setIniCodec(QTextCodec::codecForLocale());
}

TConfigFileManager::~TConfigFileManager()
{
    if (NULL != mpSetting){
        delete mpSetting;
        mpSetting = NULL;
    }
}

TConfigFileManager& TConfigFileManager::Instance()
{
    static TConfigFileManager ins;
    return ins;
}

QString TConfigFileManager::GetConvigValue(const QString& str) const
{
    return mpSetting->value(str).toString();
}

QVariant TConfigFileManager::GetConfigValue(const QString& str) const
{
    return mpSetting->value(str);
}

void TConfigFileManager::SetConfigValue(const QString& str, int style)
{
    mpSetting->setValue(str, style);
}
