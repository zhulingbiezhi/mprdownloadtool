#ifndef __TDISTASKFILEPARSER_H_
#define __TDISTASKFILEPARSER_H_

#include "TFileEnum.h"
#include "TDownloadTask.h"
#include <QDomDocument>

const QString xmlRoot = "root";
const QString xmlTasks = "tasks";
const QString xmlTaskItem = "taskItem";

const QString xmlTaskRef = "taskRef";
const QString xmlRemoteUrl = "remoteUrl";
const QString xmlTotalBytes = "totalBytes";
const QString xmlTransferdBytes = "transferdBytes";
const QString xmlSavePath = "savePath";
const QString xmlSegOffsetList = "segOffsetList";


//const QString xmlFileName = "fileName";
const QString xmlPrefixCode = "prefixCode";
//const QString xmlVersion = "version";
const QString xmlFileUuid = "fileUuid";
const QString xmlBookName = "bookName";
const QString xmlPublisher = "publisher";
//const QString xmlFileRef = "fileRef";
const QString xmlFilePath = "filePath";

//const QString xmlLicenceName = "lecenceName";
const QString xmlLicenceUuid = "licenceUuid";
const QString xmlLicenceBrandID = "licenceBrandid";
const QString xmlLicenceDeviceID = "licenceDeviceID";
const QString xmlLicenceDeviceSN = "licenceDeviceSN";
const QString xmlLicencePath = "licencePath";

class TTaskDescXml : public QObject
{
public:
    TTaskDescXml(QString filePath);
    ~TTaskDescXml();
    QList<TBasicInfo> QueryAll(QString deviceID);
    void SaveToXMLFile();
    void UpdateTaskInfo(TBasicInfo infoItem);
    void DeleteTaskInfo(TBasicInfo infoItem);

private:
    void InitFile();
    void InitData();
    void ParseDescXml();
    void ParseDescInfo(QDomElement);
    void AddDescHeader();
    bool AddDescInfo(QDomElement *, TBasicInfo);
    void InsertChild(QDomElement& tmpItem, QString xml, QString xmlData);
    void ReWriteXmlFile();


    void ClearData();
private:
    QDomDocument*                   mdomXmlDoc;
    QHash<QString, TBasicInfo>      mTaskHash;       //prefixcode
    QString                         mstrInfoDir;
    QString                         mstrDescFilePath;
};

#endif