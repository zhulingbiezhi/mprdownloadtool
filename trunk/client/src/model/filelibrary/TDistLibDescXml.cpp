#include "TDistLibDescXml.h"
#include "TGlobleCtl.h"


TTaskDescXml::TTaskDescXml(QString filePath)
: mdomXmlDoc(NULL)
{
    mstrInfoDir = QDir::fromNativeSeparators(filePath);
    InitFile();
    InitData();
}

TTaskDescXml::~TTaskDescXml()
{
    mTaskHash.clear();
    if (NULL != mdomXmlDoc){
        delete mdomXmlDoc;
        mdomXmlDoc = NULL;
    }
}


void TTaskDescXml::InitFile()
{
    mstrDescFilePath = mstrInfoDir + gDescXmlTaskName;
    QFile file(mstrDescFilePath);
    if (file.exists(mstrDescFilePath)){
        return;
    }

    QString strBackupDescFilePath = mstrInfoDir + gDescXmlTaskName + ".bak";
    QFile backupFile(strBackupDescFilePath);
    if (backupFile.exists(strBackupDescFilePath)){
        backupFile.copy(strBackupDescFilePath, mstrDescFilePath);
        return;
    }

    file.open(QIODevice::WriteOnly);
    file.close();

    AddDescHeader();
    ReWriteXmlFile();
}

void TTaskDescXml::InitData()
{
    QFile file(mstrDescFilePath);
    if (!file.open(QIODevice::ReadOnly)){
        qDebug() << "open file error:" << mstrDescFilePath;
        return;
    }

    if (NULL != mdomXmlDoc){
        delete mdomXmlDoc;
        mdomXmlDoc = NULL;
    }

    mdomXmlDoc = new QDomDocument(mstrDescFilePath);
    if (!mdomXmlDoc->setContent(&file)) {
        qDebug() << "desc-xml file is not complete:" << mstrDescFilePath;
        file.close();
        return;
    }

    file.close();
    ParseDescXml();

    if (NULL != mdomXmlDoc){
        delete mdomXmlDoc;
        mdomXmlDoc = NULL;
    }

    QString strBackupDescFilePath = mstrDescFilePath + ".bak";
    QFile::remove(strBackupDescFilePath);
    QFile::copy(mstrDescFilePath, strBackupDescFilePath);
    return;
}

void TTaskDescXml::ClearData()
{

}

void TTaskDescXml::ParseDescXml()
{
    mTaskHash.clear();

    QDomElement docElem = mdomXmlDoc->documentElement();
    QDomNode n = docElem.firstChild();
    while (!n.isNull()) {
        QDomElement e = n.toElement();
        if (!e.isNull() && xmlTasks == e.tagName()){
            ParseDescInfo(e);   //parse files
        }
        n = n.nextSibling();
    }
}

void TTaskDescXml::ParseDescInfo(QDomElement docElem)
{
    QDomNode n = docElem.firstChild();
    while (!n.isNull()) {
        QDomElement e = n.toElement();
        if (e.isNull() || xmlTaskItem != e.tagName()){
            n = n.nextSibling();
            continue;
        }

        TBasicInfo tmpItem;
        QDomNode t = e.firstChild();
        while (!t.isNull()){
            QDomElement te = t.toElement();
            if (xmlTaskRef == te.tagName()){
                tmpItem.strTaskRef = te.text();

            }
            else if (xmlRemoteUrl == te.tagName()){
                tmpItem.strRemoteUrl = te.text();

            }
            else if (xmlTotalBytes == te.tagName()){
                tmpItem.totalBytes = te.text().toLongLong();

            }
            else if (xmlTransferdBytes == te.tagName()){
                tmpItem.transferedBytes = te.text().toLongLong();

            }
            else if (xmlSavePath == te.tagName()){
                tmpItem.strSavePath = te.text();

            }
            else if (xmlSegOffsetList == te.tagName()){
                tmpItem.progressList.clear();
                tmpItem.progressList = te.text().split('|');

            }
            else if (xmlPrefixCode == te.tagName()){
                tmpItem.itemInfo.tPrefixCode = te.text();

            }
            else if (xmlFileUuid == te.tagName()){
                tmpItem.itemInfo.tFileUuid = te.text();

            }
            else if (xmlBookName == te.tagName()){
                tmpItem.itemInfo.tBookName = te.text();

            }
            else if (xmlPublisher == te.tagName()){
                tmpItem.itemInfo.tPublisher = te.text();

            }
            else if (xmlFilePath == te.tagName()){
                tmpItem.itemInfo.tFilePath = te.text();

            }
            else if (xmlLicenceUuid == te.tagName()){
                tmpItem.itemInfo.tLiceceUuid = te.text();

            }
            else if (xmlLicenceBrandID == te.tagName()){
                tmpItem.itemInfo.tBrandID = te.text();

            }
            else if (xmlLicencePath == te.tagName()){
                tmpItem.itemInfo.tLicencePath = te.text();

            }   
            else if (xmlLicenceDeviceID == te.tagName()){
                tmpItem.strDeviceID = te.text();

            }
            else if (xmlLicenceDeviceSN == te.tagName()){
                tmpItem.strDeviceSN = te.text();

            }
            else{

            }
            t = t.nextSibling();
        }

        QFileInfo tmpFileInfo(tmpItem.itemInfo.tFilePath);
        QFileInfo tmpLicenceInfo(tmpItem.itemInfo.tLicencePath);
        if (tmpFileInfo.exists() && tmpLicenceInfo.exists()){
            mTaskHash.insert(tmpItem.strTaskRef, tmpItem);         //如果有desc的xml文件有重复的话会导致内存泄漏   
        }
        n = n.nextSibling();
    }
}

void TTaskDescXml::AddDescHeader()
{
    if (NULL != mdomXmlDoc){
        delete mdomXmlDoc;
        mdomXmlDoc = NULL;
    }
    mdomXmlDoc = new QDomDocument("myTaskDescFile");
    QDomElement domRoot = mdomXmlDoc->createElement(xmlRoot);
    QDomElement domTask = mdomXmlDoc->createElement(xmlTasks);

    domRoot.appendChild(domTask);
    mdomXmlDoc->appendChild(domRoot);
}

bool TTaskDescXml::AddDescInfo(QDomElement *docElem, TBasicInfo infoItem)
{
    if (NULL == mdomXmlDoc){
        qDebug() << "xml parse error!";
        return false;
    }
    QDomElement tmpItem = mdomXmlDoc->createElement(xmlTaskItem);

    InsertChild(tmpItem, xmlTaskRef, infoItem.strTaskRef);
    InsertChild(tmpItem, xmlRemoteUrl, infoItem.strRemoteUrl);
    InsertChild(tmpItem, xmlTotalBytes, QString::number(infoItem.totalBytes, 10));
    InsertChild(tmpItem, xmlTransferdBytes, QString::number(infoItem.transferedBytes, 10));
    QString segList = infoItem.progressList.join('|');
    InsertChild(tmpItem, xmlSegOffsetList, segList);
    InsertChild(tmpItem, xmlSavePath, infoItem.strSavePath);
    InsertChild(tmpItem, xmlPrefixCode, infoItem.itemInfo.tPrefixCode);
    InsertChild(tmpItem, xmlFileUuid, infoItem.itemInfo.tFileUuid);
    InsertChild(tmpItem, xmlBookName, infoItem.itemInfo.tBookName);
    InsertChild(tmpItem, xmlPublisher, infoItem.itemInfo.tPublisher);
    InsertChild(tmpItem, xmlFilePath, infoItem.itemInfo.tFilePath);
    InsertChild(tmpItem, xmlLicenceUuid, infoItem.itemInfo.tLiceceUuid);
    InsertChild(tmpItem, xmlLicenceBrandID, infoItem.itemInfo.tBrandID);
    InsertChild(tmpItem, xmlLicencePath, infoItem.itemInfo.tLicencePath);
    InsertChild(tmpItem, xmlLicenceDeviceID, infoItem.strDeviceID);
    InsertChild(tmpItem, xmlLicenceDeviceSN, infoItem.strDeviceSN);

    docElem->appendChild(tmpItem);
    return true;
}

void TTaskDescXml::InsertChild(QDomElement& tmpItem, QString xml, QString xmlData)
{
    QDomElement item = mdomXmlDoc->createElement(xml);
    QDomText valueText = mdomXmlDoc->createTextNode(xmlData);
    item.appendChild(valueText);
    tmpItem.appendChild(item);
}

void TTaskDescXml::ReWriteXmlFile()
{
    if (NULL == mdomXmlDoc){
        qDebug() << "domdocument is null.";
        return;
    }

    QFile file(mstrDescFilePath);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Truncate | QIODevice::Text)){
        return;
    }

    QTextStream out(&file);

    out.setCodec("UTF-8");

    mdomXmlDoc->save(out, 4, QDomNode::EncodingFromTextStream);

    file.close();
}

void TTaskDescXml::SaveToXMLFile()
{
    AddDescHeader();
    QHashIterator<QString, TBasicInfo> it(mTaskHash);
    while (it.hasNext()){
        TBasicInfo infoItem = it.next().value();
        QDomElement docElem = mdomXmlDoc->documentElement();
        QDomNode n = docElem.firstChild();
        while (!n.isNull()) {
            QDomElement e = n.toElement();
            if (!e.isNull() && xmlTasks == e.tagName()){
                AddDescInfo(&e, infoItem);
            }
            n = n.nextSibling();
        }
    }

    ReWriteXmlFile();
}

QList<TBasicInfo> TTaskDescXml::QueryAll(QString deviceID)
{
    QList<TBasicInfo> taskList;
    QHashIterator<QString, TBasicInfo> it(mTaskHash);
    while (it.hasNext()){
        TBasicInfo tmpItem = it.next().value();
        if (tmpItem.strDeviceID == deviceID)
        {
            taskList.push_back(tmpItem);
        }       
    }
    return taskList;
}

void TTaskDescXml::UpdateTaskInfo(TBasicInfo infoItem)
{
    QString taskRef = infoItem.strTaskRef;
    TBasicInfo hashItem = mTaskHash.value(taskRef);
    if (hashItem.strRemoteUrl.isEmpty())
    {
        mTaskHash.insert(taskRef, infoItem);
    }
    else
    {
        mTaskHash.remove(taskRef);
        mTaskHash.insert(taskRef, infoItem);
    }
}

void TTaskDescXml::DeleteTaskInfo(TBasicInfo infoItem)
{
    QString taskRef = infoItem.strTaskRef;
    TBasicInfo hashItem = mTaskHash.value(taskRef);
    if (!hashItem.strRemoteUrl.isEmpty())
    {
        mTaskHash.remove(taskRef);
    }
}
