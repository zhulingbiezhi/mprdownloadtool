#ifndef __TDUMPCORE_H_
#define __TDUMPCORE_H_

#include <windows.h>
#include <Dbghelp.h>
#include <tchar.h>
#include <QTime>

class TDumpCore
{
private:
    TDumpCore();
    ~TDumpCore();
    
public:
    static TDumpCore&  Instance();
    long CrashHandler(EXCEPTION_POINTERS *pException);

private:
    QString     CreateDumpFilePath();
    void        CreateDumpFile(LPCWSTR lpstrDumpFilePathName, EXCEPTION_POINTERS *pException);
    void        ShowMessage();

private:
    QString     m_qstrLocalName ;
};

#endif //__TDUMPCORE_H_