#include <QNetworkReply>
#include <QNetworkRequest>
#include <QNetworkAccessManager>
#include "TDownloadTaskProber.h"

TDownloadTaskProber::TDownloadTaskProber(QNetworkAccessManager* NAM, QObject* parent)
: QObject(parent)
, mTotalSize(-1)
, mFromBeginning(true)
, mReply(nullptr)
, mNAM(NAM)
{

}

TDownloadTaskProber::~TDownloadTaskProber()
{
    OnStop();
}

QString TDownloadTaskProber::GetObjETag() const
{
    return mETag;
}

QString TDownloadTaskProber::GetObjLastModify() const
{
    return mLastModify;
}

qint64 TDownloadTaskProber::GetObjTotalSize() const
{
    return mTotalSize;
}

bool TDownloadTaskProber::IsDownloadFromBeginning() const
{
    return mFromBeginning;
}

void TDownloadTaskProber::Stop()
{
    if (mReply) {
        QMetaObject::invokeMethod(this, "OnStop", Qt::QueuedConnection);
    }
}

void TDownloadTaskProber::OnStop()
{
    if (mReply)
    {
        if (mReply->isRunning()) {
            mReply->abort();
        }
        mReply->deleteLater();
        mReply = nullptr;
    }
}

void TDownloadTaskProber::Start(const QString& url)
{
    Start(url, QString(), QString());
}

void TDownloadTaskProber::Start(const QString& url, const QString& eTag, const QString& lastModify)
{
    QMetaObject::invokeMethod(this, "OnStart", Qt::QueuedConnection, Q_ARG(QString, url), Q_ARG(QString, eTag), Q_ARG(QString, lastModify));
}

void TDownloadTaskProber::OnStart(const QString& url, const QString& eTag, const QString& lastModify)
{
    mUrl = url;
    mETag = eTag;
    mLastModify = lastModify;
    mTotalSize = -1;
    mFromBeginning = mETag.isEmpty() || mLastModify.isEmpty();

    QNetworkRequest request(mUrl);
    request.setRawHeader("User-Agent", "IHttpUpDownloader");
    request.setRawHeader("Range", "bytes=0-0");
    request.setRawHeader("Accept-Encoding", "*");
    //request.setRawHeader("Accept-Language", "*");
    //request.setRawHeader("Cache-Control", "no-cache");
    //request.setRawHeader("Accept", "*/*");
    if (!mETag.isEmpty()) {
        request.setRawHeader("If-Range", mETag.toUtf8());
    }
    if (!mLastModify.isEmpty()) {
        request.setRawHeader("Unless-Modified-Since", mLastModify.toUtf8());
    }

    OnStop();
    mReply = mNAM->get(request);
    if (mReply) {
        mReply->ignoreSslErrors();
        connect(mReply, SIGNAL(finished()), this, SLOT(OnProbeTaskFinished()));
    }
}

void TDownloadTaskProber::OnProbeTaskFinished()
{
    QObject* reply = sender();
    if (reply != mReply) {
        reply->deleteLater();
        return;
    }

    if (mReply)
    {
        int error = mReply->error();
        QString errorStr = mReply->errorString();
        if (error == QNetworkReply::NoError) {
            mETag = mReply->rawHeader("ETag");
            mLastModify = mReply->rawHeader("Last-Modified");
            mStatus = mReply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
            if (mStatus == 200) {
                mFromBeginning = true;
                mTotalSize = mReply->header(QNetworkRequest::ContentLengthHeader).toLongLong();
            }
            else if (mStatus == 206) {
                QString contentRange = mReply->rawHeader("Content-Range");
                int pos = contentRange.lastIndexOf('/');
                if (pos != -1) {
                    mTotalSize = contentRange.mid(pos + 1).trimmed().toLongLong();
                }
            }
            else
            {
                qDebug() << __FUNCTION__ << mStatus;
            }
        }

        mReply->deleteLater();
        mReply = nullptr;
        emit sigProbeTaskFinished(error, errorStr);
    }
}

