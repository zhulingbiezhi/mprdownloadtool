#ifndef __TDOWNLOADTASKPROBER_INC__
#define __TDOWNLOADTASKPROBER_INC__

#include <QString>
#include <QObject>

class QNetworkReply;
class QNetworkAccessManager;
class TDownloadTaskProber : public QObject
{
    Q_OBJECT

public:
    TDownloadTaskProber(QNetworkAccessManager* NAM, QObject* parent = nullptr);
    ~TDownloadTaskProber();

    void    Stop();
    void    Start(const QString& url);
    void    Start(const QString& url, const QString& eTag, const QString& lastModify);

    QString GetObjETag() const;
    QString GetObjLastModify() const;
    qint64  GetObjTotalSize() const;
    bool    IsDownloadFromBeginning() const;

signals:
    void    sigProbeTaskFinished(int code, QString errStr);

private slots:
    void    OnStop();
    void    OnStart(const QString& url, const QString& eTag, const QString& lastModify);
    void    OnProbeTaskFinished();

private:
    int                             mStatus;
    bool                            mFromBeginning;
    qint64                          mTotalSize;
    QString                         mUrl;
    QString                         mLastModify;
    QString                         mETag;
    QNetworkReply*                  mReply;
    QNetworkAccessManager*          mNAM;
};


#endif //__TDOWNLOADTASKPROBER_INC__


