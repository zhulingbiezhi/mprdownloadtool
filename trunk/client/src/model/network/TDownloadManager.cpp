#include "TDownloadTask.h"
#include "TDownloadManager.h"
#include "TFileLib.h"
#include "TWorkStateConfig.h"
#include "TGlobleCtl.h"
#include "mprMimeDataFetcher.h"
#include "TAppCommon.h"
#include "TLocalLibHandle.h"

const int THREAD_NUM = 3;
const int MAX_RUNNING_NUM = 3;

#define  PAUSE_TASK 1
#define  REMOVE_TASK 2
#define  FINISHED_TASK 3

TDownloadManager::TDownloadManager()
: mnRunningTaskNum(0)
, mnTaskIDNumber(0)
{
    mpLocalFileLib = TLocalLibHandle::Instance().GetLocalLib();
    mpSharedThread = new QThread;
    mpSharedNAM = new QNetworkAccessManager;
    mpSharedNAM->moveToThread(mpSharedThread);
    mpSharedThread->start();
}

TDownloadManager::~TDownloadManager()
{
    TDownloadTask * pDownloadTask;
    for (int i = 0; i < mDownloadTaskList.size(); i++)
    {
        pDownloadTask = mDownloadTaskList.at(i);
        pDownloadTask->deleteLater();
    }
    if (mpSharedThread)
    {
        if (mpSharedThread->isRunning())
        {
            mpSharedThread->quit();
            mpSharedThread->wait();
        }
        mpSharedThread->deleteLater();
        mpSharedThread = nullptr;
    }
    if (mpSharedNAM)
    {
        mpSharedNAM->deleteLater();
        mpSharedNAM = nullptr;
    }
}

TDownloadManager* TDownloadManager::Instance()
{
    static TDownloadManager downloadMgr;
    return &downloadMgr;
}


void TDownloadManager::AddTask(int task_id, TBasicInfo basicInfo)
{
    TDownloadTask * pDownloadTask = new TDownloadTask(this, task_id, THREAD_NUM, basicInfo);
    mTaskInfoHash.insert(task_id, basicInfo);
    mDownloadTaskList.append(pDownloadTask);
    SaveTaskInfo(task_id, PAUSE_TASK);
    StartTask(task_id);
}

int TDownloadManager::StartTask(int task_id)
{
    int ret = -1;
    int index = FindByID(task_id);
    if (index != -1)
    {
        TDownloadTask * pDownloadTask = mDownloadTaskList.at(index);
        if (mnRunningTaskNum < MAX_RUNNING_NUM) 
        {
            if (pDownloadTask->IsPause()) {
                pDownloadTask->RestartTask();
                mnRunningTaskNum++;
            } else if (!pDownloadTask->IsRuning()) {
                pDownloadTask->StartTask();
                mnRunningTaskNum++;
            } else {
                qDebug() << __FUNCTION__ << "Warning : the task is running, you can't start it again !" << task_id;
            }
            ret = 1;
        }
        else
        {
            if (pDownloadTask->IsPause())
            {
                pDownloadTask->SetDownloadState(HTS_PREPARED);
            }            
            ret = 0;
        }        
    }
    else
    {
        qDebug() << __FUNCTION__ << "Warning : can't find the task by id !" << task_id;
    }   
    
    return ret;
}

void TDownloadManager::StopTask(int task_id)
{
    int index = FindByID(task_id);
    if (index != -1)
    {
        TDownloadTask * pDownloadTask = mDownloadTaskList.at(index);
        if (pDownloadTask->IsRuning())
        {          
            mnRunningTaskNum--;
            pDownloadTask->StopTask();      
            SaveTaskInfo(task_id, PAUSE_TASK);
        }
        else
        {
            qDebug() << __FUNCTION__ << "Warning : the task is not start, you can't stop it !" << task_id;
        }
    }
    else
    {
        qDebug() << __FUNCTION__ << "Warning : can't find the task by id !" << task_id;
    }
}

void TDownloadManager::RemoveTask(int task_id)
{
    int index = FindByID(task_id);
    if (index != -1)
    {
        TDownloadTask * pDownloadTask = mDownloadTaskList.at(index);
        if (pDownloadTask->IsRuning())
        {                        
            pDownloadTask->StopTask();      
        }
        mnRunningTaskNum--;
        SaveTaskInfo(task_id, REMOVE_TASK);
        mDownloadTaskList.removeOne(pDownloadTask);
        delete pDownloadTask;
    }
    else
    {
        qDebug() << __FUNCTION__ << "Warning : can't find the task by id !" << task_id;
    }
}

int TDownloadManager::FindByID(int task_id)
{
    for (int i = 0; i < mDownloadTaskList.size(); i++)
    {
        TDownloadTask *pDownloadTask = mDownloadTaskList.at(i);
        if (pDownloadTask->GetTaskID() == task_id)
        {
            return i;
        }
    }
    return -1;
}

void TDownloadManager::AutoStartTask()
{
    TDownloadTask * pDownloadTask = nullptr;
    for (int i = 0; i < mDownloadTaskList.size(); i++)  //�Ƚ��ȳ�
    {
        pDownloadTask = mDownloadTaskList.at(i);
        if (pDownloadTask->IsWaiting())
        {
            //pDownloadTask->StartTask();
            StartTask(pDownloadTask->GetTaskID());
            if (mnRunningTaskNum >= MAX_RUNNING_NUM)
            {
                break;
            }           
        }
    }
}

void TDownloadManager::SendMessage(int taskID, int msgType, const QVariant& msgBody, TSVHashMap& extraParams)
{
    if (msgType == HTM_STATE)
    {
        int state = msgBody.toInt();
        if (state == HTS_FINISHED || state == HTS_PAUSED || state == HTS_FAILED)
        {
            if (state == HTS_FINISHED)
            {                
                RemoveTask(taskID);
                SaveTaskInfo(taskID, FINISHED_TASK);
                extraParams.insert(VERSION, mTaskInfoHash[taskID].itemInfo.tVersion);
                extraParams.insert(MPRPATH, mTaskInfoHash[taskID].itemInfo.tFilePath);
                extraParams.insert(LICENSEPATH, mTaskInfoHash[taskID].itemInfo.tLicencePath);
                mTaskInfoHash.remove(taskID);
            }
            AutoStartTask();
        }
    }
    emit sigDownloadMessage(taskID, msgType, msgBody, extraParams);  
}

int TDownloadManager::GetValidTaskID()
{
    return ++mnTaskIDNumber;
}

void TDownloadManager::SaveTaskInfo(int task_id, int state)
{
    TMPRFileInfoItem& infoItem = mTaskInfoHash[task_id].itemInfo;
    if (state == FINISHED_TASK || state == REMOVE_TASK)
    {
        mpLocalFileLib->DeleteTaskInfo(mTaskInfoHash[task_id]);
        if (state == FINISHED_TASK)
        {
            unsigned short version = 0;
            if (0 == getMPRLanguageVersionW((wchar_t*)infoItem.tFilePath.utf16(), &version)) {
                infoItem.tVersion = QString("%1").arg(version, 5, 10, QChar('0'));
            }
            mpLocalFileLib->UpdateFileInfo(infoItem);
        }
    }
    else
    {
        int index = FindByID(task_id);
        if (index != -1)
        {
            mDownloadTaskList.at(index)->SaveTaskInfo(mTaskInfoHash[task_id]);
        }
        mpLocalFileLib->UpdateTaskInfo(mTaskInfoHash[task_id]);
    }
}

void TDownloadManager::LoadTaskInfo(QString deviceID)
{
    QList<TBasicInfo> taskList;
    taskList = mpLocalFileLib->FetchAllTasks(deviceID);
    for (int i = 0; i < taskList.size(); i++)
    {
        int task_id = GetValidTaskID();
        emit sigAddTaskToTable(task_id, taskList.at(i).itemInfo);
        TDownloadTask * pDownloadTask = new TDownloadTask(this, task_id, THREAD_NUM, taskList.at(i));
        pDownloadTask->LoadTaskInfo(taskList.at(i));
        mTaskInfoHash.insert(task_id, taskList.at(i));
        mDownloadTaskList.append(pDownloadTask);
    }
}

void TDownloadManager::RemoveAllTask()
{
    TDownloadTask * pDownloadTask;
    for (int i = 0; i < mDownloadTaskList.size(); i++)
    {
        pDownloadTask = mDownloadTaskList.at(i);
        delete pDownloadTask;
    }
    mDownloadTaskList.clear();
    mnRunningTaskNum = 0;
    mTaskInfoHash.clear();
}

