#ifndef _TDOWNLOADTASK_H_
#define _TDOWNLOADTASK_H_

#include "THttpTask.h"
#include "TFileEnum.h"
#include <QThread>
#include <QMap>

class TDownloadManager;
class TDownloadTaskProber;


typedef struct  _TSegmentArea
{
    qint64 startOffset;
    qint64 endOffset;
    _TSegmentArea()
    {

    }
    _TSegmentArea(const qint64& start, const qint64& end)
    {
        startOffset = start;
        endOffset = end;
    }
}TSegmentArea;

typedef struct _TBasicInfo
{
    int         task_id;
    int         percent;
    int         rate;
    int         usedTime;
    int         state;
    qint64      totalBytes;
    qint64      transferedBytes;
    QString     strRemoteUrl;
    QString     strTaskRef;
    QString     strSavePath;
    QString     strDeviceID;
    QString     strDeviceSN;
    QStringList progressList;
    TMPRFileInfoItem   itemInfo;
    _TBasicInfo()
    {
        task_id = -1;
        percent = 0;
        rate = 0;
        usedTime = 0;
        transferedBytes = 0;
        state = HTS_UNKNOWN;
    }  
}TBasicInfo;


typedef struct _TSegtaskInfo
{
    int             http_id;
    QThread*        pThread;
    THttpSegment*   pHttpSegment;
    bool operator==(const _TSegtaskInfo& info)
    {
        return (http_id == info.http_id) && (pThread == info.pThread) && (pHttpSegment == info.pHttpSegment);
    }
    _TSegtaskInfo()
    {
        http_id = -1;
        pThread = nullptr;
        pHttpSegment = nullptr;
    }
}TSegtaskInfo;

class TDownloadTask : public QObject 
{
    Q_OBJECT
public:
    TDownloadTask(TDownloadManager* pDownloadMgr, int task_id, int threadNum, TBasicInfo basicInfo);
    ~TDownloadTask();

    int GetTaskID();
    void StopTask();
    bool IsRuning();
    bool IsPause();
    bool IsWaiting();
    void RestartTask();
    void StartTask();
    void SaveTaskInfo(TBasicInfo& info);
    void LoadTaskInfo(TBasicInfo info);
    void SetDownloadState(EHttpTaskState state);

private:
    int              FindByID(int http_id);
    void             StartFromBeginning();

public slots:
    void    OnSegmentTaskFinished(int http_id, int code, QString errStr);
    void    OnSegmentDataRead(int http_id, qint64 length);
    void    OnProbeTaskFinished(int code, QString errStr);
private:   
    int                     mnThreadNum;
    TBasicInfo              mBasicInfo;
    TDataStream*            mpDataStream;
    TDownloadManager*       mpDownloadMgr;
    QList<TSegtaskInfo>     mlstTaskSegmentInfo;
    QMap<int, TSegmentArea> mSegmentArea;
    TDownloadTaskProber*    mpTaskProber;
};

#endif