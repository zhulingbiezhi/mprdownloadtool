#ifndef _TDOWNLOAD_MANAGER_H_
#define _TDOWNLOAD_MANAGER_H_

#include "TDownloadTask.h"
typedef QHash<QString, QVariant> TSVHashMap;
class TThreadManager;
class TFileLib;

class TDownloadManager : public QObject
{
    friend class TDownloadTask;
    Q_OBJECT
public:
    TDownloadManager();
    ~TDownloadManager();
    static TDownloadManager* Instance();

public:
    void AddTask(int task_id, TBasicInfo basicInfo);
    int  StartTask(int task_id);
    void StopTask(int task_id);
    void RemoveTask(int task_id);
    void SendMessage(int taskID, int msgType, const QVariant& msgBody, TSVHashMap& extraParams = TSVHashMap());
    int GetValidTaskID();
    void LoadTaskInfo(QString deviceID);
    void RemoveAllTask();

private:
    void SaveTaskInfo(int task_id, int state);
    void AutoStartTask();
    void InitTasks(QList<TBasicInfo> infoList);
    int  FindByID(int task_id);

signals:
    void sigDownloadMessage(int taskID, int msgType, const QVariant& msgBody, TSVHashMap& extraParams);
    void sigAddTaskToTable(int task_id, TMPRFileInfoItem infoItem);

private:
    QList<TDownloadTask *>  mDownloadTaskList;
    int    mnRunningTaskNum;
    int    mnTaskIDNumber;
    TFileLib*   mpLocalFileLib;
    QMap<int, TBasicInfo>    mTaskInfoHash;
    QThread*                        mpSharedThread;
    QNetworkAccessManager*          mpSharedNAM;
};
#endif