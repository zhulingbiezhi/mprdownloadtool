#include "TDataStream.h"
#include "TDownloadManager.h"
#include "TDownloadTask.h"
#include "TDownloadTaskProber.h"
const qint64 minTaskSegmentSize = 1024 * 1024 * 5;

TDownloadTask::TDownloadTask(TDownloadManager* pDownloadMgr, int task_id, int threadNum, TBasicInfo basicInfo)
: mnThreadNum(threadNum)
, mBasicInfo(basicInfo)
, mpDownloadMgr(pDownloadMgr)
{
    mBasicInfo.state = HTS_PREPARED;
    mBasicInfo.task_id = task_id;
    mpDownloadMgr->SendMessage(mBasicInfo.task_id, HTM_STATE, mBasicInfo.state);
    mpDataStream = new TDataStream(mBasicInfo.strSavePath);
    mpTaskProber = new TDownloadTaskProber(pDownloadMgr->mpSharedNAM);
    mpTaskProber->moveToThread(pDownloadMgr->mpSharedThread);
    connect(mpTaskProber, SIGNAL(sigProbeTaskFinished(int, QString)), this, SLOT(OnProbeTaskFinished(int, QString)));
}

TDownloadTask::~TDownloadTask()
{
    for (int i = 0; i < mlstTaskSegmentInfo.size(); i++)
    {
        mlstTaskSegmentInfo[i].pHttpSegment->PauseSegTask(true);
        if (mlstTaskSegmentInfo[i].pThread->isRunning())
        {
            mlstTaskSegmentInfo[i].pThread->quit();
            mlstTaskSegmentInfo[i].pThread->wait();
        }
        mlstTaskSegmentInfo[i].pHttpSegment->Stop();
        mlstTaskSegmentInfo[i].pThread->deleteLater();
        mlstTaskSegmentInfo[i].pHttpSegment->deleteLater();
    }
    mlstTaskSegmentInfo.clear();
    if (mpDataStream)
    {
        delete mpDataStream;
        mpDataStream = nullptr;
    }

    if (mpTaskProber)
    {
        mpTaskProber->deleteLater();
        mpTaskProber = NULL;
    }
}

void TDownloadTask::StartTask()
{
    mpTaskProber->Start(mBasicInfo.strRemoteUrl, "", "");
}

void TDownloadTask::OnProbeTaskFinished(int code, QString errStr)
{
    if (code != QNetworkReply::NoError)
    {
        int state = (code == QNetworkReply::OperationCanceledError) ? HTS_PAUSED : HTS_FAILED;
        if (mBasicInfo.state != state) {
            mBasicInfo.state = state;
            mpDownloadMgr->SendMessage(mBasicInfo.task_id, HTM_STATE, mBasicInfo.state);
        }
    }
    else
    {
        mBasicInfo.totalBytes = mpTaskProber->GetObjTotalSize();
        StartFromBeginning();
    }
}

void TDownloadTask::StartFromBeginning()
{
    if (mnThreadNum == 0 || mBasicInfo.totalBytes <= 0)
    {
        qDebug() << __FUNCTION__ << "error : the thread number is 0 !" << mnThreadNum << mBasicInfo.totalBytes;
        return;
    }
    qint64 segTaskSize = mBasicInfo.totalBytes / mnThreadNum;
    while (segTaskSize < minTaskSegmentSize) {
        if (mnThreadNum > 1) {
            mnThreadNum--;
            segTaskSize = mBasicInfo.totalBytes / mnThreadNum;
        }
        else {
            break;
        }
    }
    qint64 beginOffset = 0;
    for (int i = 0; i < mnThreadNum; i++) {
        qint64 endOffset = (i < mnThreadNum - 1) ? (beginOffset + segTaskSize - 1) : (mBasicInfo.totalBytes - 1);
        THttpSegment* segmentTask = new THttpSegment(i, mBasicInfo.strRemoteUrl, beginOffset, endOffset, mpDataStream);
        connect(segmentTask, SIGNAL(sigSegmentDataTransferred(int, qint64)), this, SLOT(OnSegmentDataRead(int, qint64)));
        connect(segmentTask, SIGNAL(sigSegmentTaskFinished(int, int, QString)), this, SLOT(OnSegmentTaskFinished(int, int, QString)));
        mSegmentArea.insert(i, TSegmentArea(beginOffset, endOffset));
        beginOffset += segTaskSize;

        TSegtaskInfo info;
        info.http_id = i;
        info.pHttpSegment = segmentTask;
        info.pThread = new QThread();
        info.pHttpSegment->moveToThread(info.pThread);
        info.pThread->start();
        QMetaObject::invokeMethod(info.pHttpSegment, "Start");
        mlstTaskSegmentInfo.append(info);
    }
    mBasicInfo.state = HTS_RUNNING;
    mpDownloadMgr->SendMessage(mBasicInfo.task_id, HTM_STATE, mBasicInfo.state);
}


void TDownloadTask::RestartTask()
{
    for (int i = 0; i < mlstTaskSegmentInfo.size(); i++)
    {
        mlstTaskSegmentInfo[i].pHttpSegment->PauseSegTask(false);
        mlstTaskSegmentInfo[i].pThread->start();
        QMetaObject::invokeMethod(mlstTaskSegmentInfo[i].pHttpSegment, "Start");
    }
    mBasicInfo.state = HTS_RUNNING;
    mpDownloadMgr->SendMessage(mBasicInfo.task_id, HTM_STATE, mBasicInfo.state);
}

void TDownloadTask::StopTask()
{
    for (int i = 0; i < mlstTaskSegmentInfo.size(); i++)
    {
        mlstTaskSegmentInfo[i].pHttpSegment->PauseSegTask(true);
        if (mlstTaskSegmentInfo[i].pThread->isRunning())
        {
            mlstTaskSegmentInfo[i].pThread->quit();
            mlstTaskSegmentInfo[i].pThread->wait();
        }
        mlstTaskSegmentInfo[i].pHttpSegment->Stop();
    }
    mpTaskProber->Stop();
    mBasicInfo.state = HTS_PAUSED;
    mpDownloadMgr->SendMessage(mBasicInfo.task_id, HTM_STATE, mBasicInfo.state);
}

bool TDownloadTask::IsRuning()
{
    return mBasicInfo.state & HTS_RUNNING;
}

int TDownloadTask::GetTaskID()
{
    return mBasicInfo.task_id;
}

bool TDownloadTask::IsPause()
{
    return mBasicInfo.state & HTS_PAUSED;
}

bool TDownloadTask::IsWaiting()
{
    return mBasicInfo.state & HTS_PREPARED;
}


void TDownloadTask::OnSegmentTaskFinished(int http_id, int code, QString errStr)
{
    if (code != QNetworkReply::NoError)
    {
        qDebug() << __FUNCTION__ << "Error : the segment task failed !" << http_id << "---code : " << code << errStr;
        StopTask();
        mBasicInfo.state = HTS_FAILED;
        mpDownloadMgr->SendMessage(mBasicInfo.task_id, HTM_STATE, mBasicInfo.state);
    }
    else
    {
        int nIndex = FindByID(http_id);
        if (nIndex != -1)
        {
            if (mlstTaskSegmentInfo[nIndex].pThread->isRunning())
            {
                mlstTaskSegmentInfo[nIndex].pThread->quit();
                mlstTaskSegmentInfo[nIndex].pThread->wait();
            }
            mlstTaskSegmentInfo[nIndex].pThread->deleteLater();
            mlstTaskSegmentInfo[nIndex].pHttpSegment->deleteLater();
            mlstTaskSegmentInfo.removeAt(nIndex);
            mSegmentArea.remove(http_id);
        }
        else
        {
            qDebug() << __FUNCTION__ << "Error : can't find the segment task by the ID !" << http_id;
        }
        if (mlstTaskSegmentInfo.isEmpty())
        {
            mpDownloadMgr->SendMessage(mBasicInfo.task_id, HTM_PROGRESS, mBasicInfo.percent);
            mBasicInfo.state = HTS_FINISHED;
            mpDownloadMgr->SendMessage(mBasicInfo.task_id, HTM_STATE, mBasicInfo.state);
        }
    }
}

void TDownloadTask::OnSegmentDataRead(int http_id, qint64 length)
{
    mSegmentArea[http_id].startOffset += length;
    mBasicInfo.transferedBytes += length;
    //qDebug() << __FUNCTION__ << http_id << length;
    int msgMask = 0;
    int percent = mBasicInfo.transferedBytes * 100 / mBasicInfo.totalBytes;

    if (mBasicInfo.percent < percent) {
        mBasicInfo.percent = percent;
        msgMask |= HTM_PROGRESS;
    }
    //qDebug() << __FUNCTION__ << http_id << length << percent << "%";

    if (msgMask & HTM_PROGRESS) {
        //qDebug() << __FUNCTION__ << "emit progress signals";
        mpDownloadMgr->SendMessage(mBasicInfo.task_id, HTM_PROGRESS, mBasicInfo.percent);
    }

    if (msgMask & HTM_RATE) {
        //qDebug() << __FUNCTION__ << "emit Rate signals";
        mpDownloadMgr->SendMessage(mBasicInfo.task_id, HTM_RATE, mBasicInfo.rate);
    }

    if (msgMask & HTM_STATE) {
        mpDownloadMgr->SendMessage(mBasicInfo.task_id, HTM_STATE, mBasicInfo.state);
    }
}

int TDownloadTask::FindByID(int http_id)
{
    for (int i = 0; i < mlstTaskSegmentInfo.size(); i++)
    {
        if (mlstTaskSegmentInfo[i].http_id == http_id)
        {
            return i;
        }
    }
    return -1;
}

void TDownloadTask::SaveTaskInfo(TBasicInfo& info)
{
    QList<TSegmentArea> areaList = mSegmentArea.values();
    info.progressList.clear();
    for each (TSegmentArea area in areaList)
    {
        info.progressList.push_back(QString::number(area.startOffset));
        info.progressList.push_back(QString::number(area.endOffset));
        qDebug() << __FUNCTION__ << area.startOffset << area.endOffset;
    }
    info.transferedBytes = mBasicInfo.transferedBytes;
    info.state = mBasicInfo.state;
    info.percent = mBasicInfo.transferedBytes * 100 / mBasicInfo.totalBytes;
    info.task_id = mBasicInfo.task_id;

}

void TDownloadTask::LoadTaskInfo(TBasicInfo info)
{
    QStringList progressList = info.progressList;
//     if (progressList.isEmpty()) {
//         return;
//     }
    int index = 0;
    mSegmentArea.clear();
    mlstTaskSegmentInfo.clear();

    for (int i = 0; i < progressList.size(); i += 2)
    {
        qint64 beginOffset = progressList.at(i).toLongLong();
        qint64 endOffset = progressList.at(i + 1).toLongLong();

        mSegmentArea.insert(index, TSegmentArea(beginOffset, endOffset));
        THttpSegment* segmentTask = new THttpSegment(index, mBasicInfo.strRemoteUrl, beginOffset, endOffset, mpDataStream);
        connect(segmentTask, SIGNAL(sigSegmentDataTransferred(int, qint64)), this, SLOT(OnSegmentDataRead(int, qint64)));
        connect(segmentTask, SIGNAL(sigSegmentTaskFinished(int, int, QString)), this, SLOT(OnSegmentTaskFinished(int, int, QString)));
        TSegtaskInfo info;
        info.http_id = index;
        info.pHttpSegment = segmentTask;
        info.pThread = new QThread();
        info.pHttpSegment->moveToThread(info.pThread);
        mlstTaskSegmentInfo.append(info);
        index++;
        mnThreadNum = index;
    }
   
    mBasicInfo.percent = mBasicInfo.transferedBytes * 100 / mBasicInfo.totalBytes;
    mBasicInfo.state = HTS_PAUSED;
    mpDownloadMgr->SendMessage(mBasicInfo.task_id, HTM_STATE, mBasicInfo.state);
    mpDownloadMgr->SendMessage(mBasicInfo.task_id, HTM_PROGRESS, mBasicInfo.percent);
    if (index == 0)
    {
        mBasicInfo.state = HTS_PREPARED;
    }
}

void TDownloadTask::SetDownloadState(EHttpTaskState state)
{
    mBasicInfo.state = state | HTS_PAUSED;
    mpDownloadMgr->SendMessage(mBasicInfo.task_id, HTM_STATE, state);
}
