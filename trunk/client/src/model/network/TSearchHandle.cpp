#include "TSTDPreCompiledHeader.h"
#include "TSearchHandle.h"
#include "TLocalLibHandle.h"
#include "TNetMPRSearcher.h"
#include "TMPRDeviceDesc.h"
#include "TMPRDevicesManager.h"
#include "TLicenseParser.h"
#include "TGlobleCtl.h"
#include "TDeviceDataManager.h"


TSearchHandle::TSearchHandle(char diskName, QObject *parent)   
    : QObject(parent)
    , mDisk(diskName)
    , mMPRFileLib(0)
{
    mFileType = FT_None;

    TMPRDeviceDesc desc;
    TMPRDevicesManager::Instance().GetMPRDeviceDesc(mDisk, desc);

    if (desc.mSupportMediaFormat & OGG || desc.mSupportMediaFormat & MP3) {
        mFileType = FT_MPR;
    }

    mBrandID = desc.mBrandId;
    mDeviceID = desc.mDeviceId;

    mMPRFileLib = TLocalLibHandle::Instance().GetLocalLib();

    mpNetMprSearcher = new TNetMPRSearcher(this);
    connect(mpNetMprSearcher, SIGNAL(SearchReplied()), this, SLOT(OnNetMPRSearchReplied()));
    connect(mpNetMprSearcher, SIGNAL(FetchDownloadUrlReplied(const QString&, const TNetMPRUrl&)), 
        this, SLOT(OnFetchDownloadUrlReplied(const QString&, const TNetMPRUrl&)));
}

TSearchHandle::~TSearchHandle()
{

}

/*
To Do: 
bug说明：
搜索bug： 网络搜索时会返回prefixcode isbn goodname bookname cn_code keyword相关结果
但本地指挥显示bookname， prefixcode的项目， 匹配过程会有出入，导致数据重复下载，但数据库已经存在此数据，并不会进行更新，
导致重复下载过程，从而tmp文件缓存文件夹越来越大
临时解决方法： 每次启动之后清空tmp缓存文件夹
最终： 本地匹配方案同步网络匹配，新增其他各种搜索条件，后续进行添加
*/

void TSearchHandle::FetchFiles(const QString& keywork, const QString& prefixCode)
{
    mlstFetchLocal.clear();
    mlstFetchFiles.clear();
    UpdateDeviceInfo();

    QList<TBasicInfo> downLoadInfoList = mMPRFileLib->FetchAllTasks(mDeviceID);
    for (int i = 0; i < downLoadInfoList.size(); i++)
    {
        mlstFetchLocal.append(downLoadInfoList.at(i).itemInfo);
    }
//     mlstFetchLocal = mMPRFileLib->FetchFilesByBookName(keywork, mFileType);    
//     mlstFetchLocal += mMPRFileLib->FetchFilesByPrefixCode(prefixCode, mFileType);
//     mlstFetchLocal += mMPRFileLib->FetchFilesByFileRef(keywork, mFileType);
    //mlstFetchFiles = mlstFetchLocal;
    //FilteLocalByBrandid(mBrandID);

    mpNetMprSearcher->SearchMPRFromNetwork(mDeviceID, keywork, prefixCode);
}

void TSearchHandle::FilteLocalByBrandid(QString ibrandid)
{
    QString brandid = ibrandid.rightJustified(8, '0');
    int totalSize = mlstFetchLocal.size()-1;
    for (int i=totalSize; i>=0; i--){
        QString tmpBrandid = mlstFetchLocal.at(i).tBrandID;
        if (0 == tmpBrandid.compare(brandid)){
            TMPRFileInfoItem tmpItem = mlstFetchLocal.at(i);
            mlstFetchFiles.append(tmpItem);
            mlstFetchLocal.removeAt(i);
        }        
    }
}

void TSearchHandle::OnNetMPRSearchReplied()
{
    mMPRSearchInfoMap.clear();
    const QList<TMPRSearchRepliedInfo>& mprInfoList = mpNetMprSearcher->GetReachResult();

    QListIterator<TMPRSearchRepliedInfo> it(mprInfoList);
    while(it.hasNext()) {
        TMPRSearchRepliedInfo repliedItem = it.next();

        TMPRFileInfoItem fileItem = MPRNetFileInfo2LocalFileInfo(repliedItem);
        FilteFetchResult(fileItem);
        mMPRSearchInfoMap[repliedItem.downloadId] = repliedItem;
        OnDownloadTriggered(fileItem);
    }
    //emit FetchFilesEnd();
}

TMPRFileInfoItem TSearchHandle::MPRNetFileInfo2LocalFileInfo(const TMPRSearchRepliedInfo& repliedItem)
{
    TMPRFileInfoItem fileItem;
    fileItem.tFileLocation = Network;

    if (repliedItem.fileFormat.compare("MPR", Qt::CaseInsensitive) == 0){
        fileItem.tFileType = mFileType;
    }
    else if(repliedItem.fileFormat.compare("MPRX", Qt::CaseInsensitive) == 0){
        fileItem.tFileType = FT_MPRX;
    }

    fileItem.tBookName = repliedItem.goodsName;
    fileItem.tPublisher = repliedItem.publishing;
    fileItem.tPrefixCode = repliedItem.mprPrefixCode;
    fileItem.tFileUuid = repliedItem.downloadId;
    fileItem.tLiceceUuid = repliedItem.downloadId;
    fileItem.tBrandID = repliedItem.brandId;
    fileItem.tVersion = repliedItem.versionCode;    //注：第一次搜索回来时是空值，当下载完成后，进行解析包，将此数值解析为正确的包版本号以存入本地库描述文件！
    return fileItem;
}

//for test server , the files is not regular
bool TSearchHandle::FilteNetErrorResult( TMPRFileInfoItem fileItem )
{
    for (int i = mlstFetchFiles.size() - 1; i >= 0; i--){
        TMPRFileInfoItem tmpFileItem = mlstFetchFiles.at(i);
        if (tmpFileItem.tPrefixCode == fileItem.tPrefixCode
            && tmpFileItem.tVersion == fileItem.tVersion
            && tmpFileItem.tBrandID == fileItem.tBrandID)
        {
            return false;
        }
    }
    return true;
}

//预留：无版本号的处理方式，网络端暂时不支持版本号
void TSearchHandle::FilteFetchResult(TMPRFileInfoItem& fileItem)    
{
    for (int i=mlstFetchLocal.size()-1; i>=0 ;i--){
        TMPRFileInfoItem tmpFiltItem = mlstFetchLocal.at(i);
        if (0 == tmpFiltItem.tFileUuid.compare(fileItem.tFileUuid))
        {
            fileItem.tFileLocation = Local;        
            return;
        }
    }
    mlstFetchFiles.append(fileItem);
}

//预留：有版本号的处理方式
/*void TSearchHandle::FilteFetchResult(TMPRFileInfoItem& fileItem)    
{
    for (int i=mlstFetchFiles.size()-1; i>=0 ;i--){
        TMPRFileInfoItem tmpFiltItem = mlstFetchFiles.at(i);

        if (Network == tmpFiltItem.tFileLocation){
            continue;
        }

        if (0 == tmpFiltItem.tPrefixCode.compare(fileItem.tPrefixCode)
            && 0 == tmpFiltItem.tVersion.compare(fileItem.tVersion)
            && 0 == tmpFiltItem.tBrandID.compare(fileItem.tBrandID))
        {
            if (0 != tmpFiltItem.tFileUuid.compare(fileItem.tFileUuid))
            {
                mlstFetchFiles.removeAt(i);
                mlstFetchFiles.append(fileItem);
                return;
            }

            if (0 == tmpFiltItem.tLiceceUuid.compare(fileItem.tFileUuid))
            {
                return;
            }

            if (0 == tmpFiltItem.tFileUuid.compare(fileItem.tFileUuid)
                && 0 != tmpFiltItem.tLiceceUuid.compare(fileItem.tFileUuid))
            {
                fileItem.tFilePath = tmpFiltItem.tFilePath;
                mlstFetchFiles.removeAt(i);
                mlstFetchFiles.append(fileItem);
                return;
            }
        }
    }

    for (int i=mlstFetchLocal.size()-1; i>=0; i--){
        TMPRFileInfoItem otherItem = mlstFetchLocal.at(i);

        if (0 == otherItem.tPrefixCode.compare(fileItem.tPrefixCode)
            && 0 == otherItem.tVersion.compare(fileItem.tVersion))
        {
            if (0 != otherItem.tFileUuid.compare(fileItem.tFileUuid)){
                mlstFetchFiles.removeAt(i);
                mlstFetchFiles.append(fileItem);
                return;
            }

            if (0 == otherItem.tFileUuid.compare(fileItem.tFileUuid)){
                fileItem.tFilePath = otherItem.tFilePath;
                mlstFetchFiles.removeAt(i);
                mlstFetchFiles.append(fileItem);
                return;
            }
        }
    }

    mlstFetchFiles.append(fileItem);
}*/

QList<TMPRFileInfoItem> TSearchHandle::GetFetchResult()
{
    return mlstFetchFiles;
}

void TSearchHandle::OnDownloadTriggered( const TMPRFileInfoItem& info)
{
    TMPRDeviceDesc desc;
    TMPRDevicesManager::Instance().GetMPRDeviceDesc(mDisk, desc);

    TMPRSearchRepliedInfo tmpInfo = mMPRSearchInfoMap[info.tFileUuid];
    QString queryString = mpNetMprSearcher->CreateQueryString(desc, tmpInfo);
    mpNetMprSearcher->FetchDownloadUrl(tmpInfo.interfaceUrl, queryString);

    mSearchingItemMap[info.tFileUuid] = info;
}

void TSearchHandle::OnFetchDownloadUrlReplied( const QString& uuid, const TNetMPRUrl& netMprUrl)
{
    if (uuid.isEmpty()){
        //TMainStatusHandle::Instance().ShowNormalTips(netMprUrl.msg);
        return;
    }
    auto it = mSearchingItemMap.find(uuid);
    if (it == mSearchingItemMap.end()) {
        return;
    }

    TMPRSearchRepliedInfo info = mMPRSearchInfoMap[uuid];
    QString downloadDir = mMPRFileLib->GetFileLibDir();

    QString fileName = QUuid::createUuid().toString();

    QString storageMprFilePath = downloadDir + "/" + fileName + "." + info.fileFormat;
    QString licensePath;
    SaveLicense(info, netMprUrl.licenseXml, fileName, licensePath);
    it.value().tLicencePath = licensePath;
    it.value().tFilePath = storageMprFilePath;
    emit FetchDownloadUrlReplied(it.value(), info, netMprUrl.mprUrl, storageMprFilePath, licensePath);

    mSearchingItemMap.remove(uuid);
}

bool TSearchHandle::SaveLicense(const TMPRSearchRepliedInfo& info, const QString& license, const QString& mprFileName, QString& licensePath)
{
    QByteArray licenseData;

    TLicenseParser licParser;
    if (!licParser.LoadContent(license.toUtf8()))
        return false;

    QString licenseDir =  mMPRFileLib->GetFileLibDir();
    QDir dir(licenseDir);
    if (!dir.exists()){
        dir.mkpath(licenseDir);
    }

    if (info.fileFormat.compare("MPR", Qt::CaseInsensitive) == 0)
    {
        licensePath = licenseDir + "/" + mprFileName + ".lic";
        if (!licParser.Save2Bin(licenseData))
            return false;
    }
    else
    {
        TLicenseParser::MprLicenseInfo licInfo = licParser.GetLicenseInfo();
        licensePath = licenseDir + "/" + licInfo.uuid + ".xml";
        licenseData = license.toUtf8();
    }

    if(QFile::exists(licensePath))
        QFile::remove(licensePath);

    QFile file(licensePath);
    if (!file.open(QFile::ReadWrite))
        return false;

    file.write(licenseData);
    file.close();
    return true;
}

QString TSearchHandle::ToAvailableFileName( const QString& fileName )
{
    QString temp = fileName;
    temp = temp.remove('\\').remove('/').remove(':').remove('*').remove('\"').remove('>').remove('|');
    return temp;
}

void TSearchHandle::UpdateDeviceInfo()
{
    mDisk = TDeviceDataManager::Instance().GetDiskName();
    TMPRDeviceDesc desc;
    TMPRDevicesManager::Instance().GetMPRDeviceDesc(mDisk, desc);
    mDeviceID = desc.mDeviceId;
    mBrandID = desc.mBrandId;
}
