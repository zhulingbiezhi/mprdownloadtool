#ifndef _TDataStream_H_
#define _TDataStream_H_

#include <QByteArray>
#include <QMutex>

class TDataStream
{
public:
    TDataStream(const QString& savePath);
    ~TDataStream();
    void WriteData(const qint64 pos, const QByteArray& data);
    
private:
    QFile mFile;
    QMutex  mMutex;
    QString mstrSavePath;
};

#endif