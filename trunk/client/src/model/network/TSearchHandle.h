#ifndef __TSEARCHHANDLE_H_
#define __TSEARCHHANDLE_H_

#include "TFileEnum.h"
#include "TFileLib.h"
#include "TMPRSearhRepliedInfo.h"

class TNetMPRSearcher;
class TSearchHandle : public QObject
{
    Q_OBJECT;

public:
    TSearchHandle(char diskName, QObject *parent = 0);
    ~TSearchHandle();

    //Search fileInfo
    void                                    FetchFiles(const QString& keywork, const QString& prefixCode);
    QList<TMPRFileInfoItem>                 GetFetchResult();
    TMPRFileInfoItem                        MPRNetFileInfo2LocalFileInfo(const TMPRSearchRepliedInfo& repliedItem);
    void                                    UpdateDeviceInfo();
public slots:
    void                                    OnDownloadTriggered(const TMPRFileInfoItem&);

signals:
    void                                    FetchFilesEnd();
    void                                    FetchDownloadUrlReplied(const TMPRFileInfoItem&, const TMPRSearchRepliedInfo&, const QString&, const QString&, const QString&);

private:
    bool                                    SaveLicense(const TMPRSearchRepliedInfo& info, const QString& license, const QString& mprFileName, QString& licensePath);
    void                                    FilteLocalByBrandid(QString brandid);
    void                                    FilteFetchResult(TMPRFileInfoItem& fileItem);
    bool                                    FilteNetErrorResult(TMPRFileInfoItem fileItem);
    QString                                 ToAvailableFileName(const QString& fileName);
    
private slots:
    void                                    OnNetMPRSearchReplied();
    void                                    OnFetchDownloadUrlReplied(const QString& uuid, const TNetMPRUrl& );

private:
    TFileLib*                               mMPRFileLib;

    char                                    mDisk;
    QString                                 mDeviceID;      //硬件ID
    QString                                 mBrandID;       //硬件品牌ID
    FileType                                mFileType;      //硬件支持的文件格式 , 仅仅使用与MPR文件

    QList<TMPRFileInfoItem>                 mlstFetchLocal;
    QList<TMPRFileInfoItem>                 mlstFetchFiles;

    TNetMPRSearcher*                        mpNetMprSearcher;

    QMap<QString, TMPRSearchRepliedInfo>    mMPRSearchInfoMap;
    QMap<QString, TMPRFileInfoItem>         mSearchingItemMap;
};

#endif //__TSEARCHHANDLE_H_