#include "TDataStream.h"
#include<QFileInfo>

TDataStream::TDataStream(const QString& savePath)
: mstrSavePath(savePath)
{
    QFileInfo info(savePath);
    QString dirPath = info.absoluteDir().absolutePath();
    QDir mprDir(dirPath);
    if (!mprDir.exists()){
        mprDir.mkpath(dirPath);
    }
    mFile.setFileName(mstrSavePath);
    if (!mFile.open(QIODevice::ReadWrite))
    {
        qDebug() << __FUNCTION__ << "error : open failed !";
    }
}

TDataStream::~TDataStream()
{
    mFile.close();
}

void TDataStream::WriteData(const qint64 pos, const QByteArray& data)
{
    QMutexLocker locker(&mMutex);
    mFile.seek(pos);
    mFile.write(data, data.size());
}
