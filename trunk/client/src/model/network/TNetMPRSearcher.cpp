#include "TSTDPreCompiledHeader.h"
#include "TNetMPRSearcher.h"
#include "TConfigFileManager.h"
#include "TAppCommon.h"

//#include "TMainStatusHandle.h"

TNetMPRSearcher::TNetMPRSearcher(QObject* parent)
    : QObject(parent)
    , mpCurrentSeachReply(0)
    , mpFetchUrlReply(0)
{
    mpNetMgr = new QNetworkAccessManager();
}

TNetMPRSearcher::~TNetMPRSearcher()
{
    delete mpNetMgr;
}

void TNetMPRSearcher::SearchMPRFromNetwork( const QString& deviceId, const QString& keywork, const QString& prefixCode)
{
    QString strUlr = TAppCommon::Instance().GetSearchUrl();
    QUrl url(strUlr);
    QUrlQuery urlQuery;
    urlQuery.addQueryItem("deviceId", deviceId);

    if (!keywork.isEmpty()) {
        urlQuery.addQueryItem("searchText", keywork);

    } else if (!prefixCode.isEmpty()) {
        urlQuery.addQueryItem("mprPrefixCode", prefixCode);

    } else {
        //TMainStatusHandle::Instance().ShowTimerTips(tr("param error"));
        return;
    }
    url.setQuery(urlQuery);

    QNetworkRequest request;
    request.setUrl(url);

    qDebug() << __FUNCTION__ << __LINE__ << "Searche Infos: " <<  url.toString();

    if (mpCurrentSeachReply) {
        mpCurrentSeachReply->abort();
    }

    mMPRSearchRepliedInfoList.clear();
    mDeviceId = deviceId;
    mpCurrentSeachReply = mpNetMgr->get(request);
    connect(mpCurrentSeachReply, &QNetworkReply::finished, this, &TNetMPRSearcher::OnSearchReply);
    connect(mpCurrentSeachReply, static_cast<void (QNetworkReply:: *)(QNetworkReply::NetworkError)>(&QNetworkReply::error), this, &TNetMPRSearcher::OnSearchError);
}

void TNetMPRSearcher::OnSearchReply()
{
    QNetworkReply* reply = static_cast<QNetworkReply*>(sender());
    if (reply != mpCurrentSeachReply) {
        reply->deleteLater();
        return;
    }

    QString errString;
    QByteArray data = reply->readAll();
    mpCurrentSeachReply->deleteLater();
    mpCurrentSeachReply = 0;

    qDebug() << __FUNCTION__ << __LINE__ << "Search Result: " << data;

    QJsonParseError parseError;
    QJsonDocument doc = QJsonDocument::fromJson(data, &parseError);
    if (parseError.error != QJsonParseError::NoError) {
        emit SearchReplied();
        return;
    }

    QJsonArray mprList = doc.object().value("list").toArray();

    mMPRSearchRepliedInfoList.clear();
    for(int i = 0; i < mprList.size(); i++)
    {
        TMPRSearchRepliedInfo info;
        QJsonObject item = mprList[i].toObject();

        info.ticket = item.value("ticket").toString();
        info.interfaceUrl = item.value("interfaceUrl").toString();

        QJsonObject productObj = item.value("downloadProduct").toObject();
        info.language       = productObj.value("language").toString();
        info.size           = productObj.value("size").toString();
        info.brandId        = productObj.value("brandId").toString();
        info.goodsId        = productObj.value("goodsId").toString();
        info.goodsName      = productObj.value("goodsName").toString();
        info.publicationName= productObj.value("publicationName").toString();
        info.publishing     = productObj.value("publishing").toString();
        info.languageId     = productObj.value("languageId").toString();
        info.publisher      = productObj.value("publisher").toString();
        info.fileFormat     = productObj.value("fileFormat").toString().toLower();
        info.producer       = productObj.value("producer").toString();
        info.downloadId     = productObj.value("downloadId").toString();
        info.versionCode    = productObj.value("versionCode").toString();
        info.author         = productObj.value("author").toString();
        info.mprPrefixCode  = productObj.value("mprPrefixCode").toString();
        info.productType    = productObj.value("productType").toString();
        info.uuid           = productObj.value("uuid").toString();
        info.isMPR          = productObj.value("isMPR").toString();
        info.isDiy          = productObj.value("isDiy").toString();
        info.productImgUrl  = productObj.value("productImgUrl").toString();
        info.timeBuying     = productObj.value("timeBuying").toString();

        if (info.fileFormat.compare("mpr", Qt::CaseInsensitive) == 0)               
        {
            mMPRSearchRepliedInfoList.push_back(info);
        }
        else if (info.fileFormat.compare("mprx", Qt::CaseInsensitive) == 0)
        {
            
        }
    }
    emit SearchReplied();
}

void TNetMPRSearcher::OnSearchError(QNetworkReply::NetworkError err)
{
    qDebug() << __FUNCTION__ << __LINE__ << "Error Code: " << err;
}

const QList<TMPRSearchRepliedInfo>& TNetMPRSearcher::GetReachResult() const
{
    return mMPRSearchRepliedInfoList;
}

QString TNetMPRSearcher::CreateQueryString(const TMPRDeviceDesc& devDesc, const TMPRSearchRepliedInfo& info)
{
    QUrlQuery urlQuery;
    QString ticket = info.ticket;
    ticket = ticket.replace("+", "%2B");
    urlQuery.addQueryItem("ticket", ticket);
    urlQuery.addQueryItem("contentId", info.uuid);
    urlQuery.addQueryItem("downloadId", info.downloadId);
    urlQuery.addQueryItem("owner", devDesc.mDeviceId);
    urlQuery.addQueryItem("deviceId", devDesc.mDeviceId);
    urlQuery.addQueryItem("deviceModel", devDesc.mDeviceName);
    urlQuery.addQueryItem("sysCode", "68071ea8c5b94e9cbe89c30382dc0fdc");
    urlQuery.addQueryItem("brandId", info.brandId);
    urlQuery.addQueryItem("sn", devDesc.mDeviceSN);
    urlQuery.addQueryItem("goodsId", info.goodsId);
    urlQuery.addQueryItem("replCode", "1");
    urlQuery.addQueryItem("mprxType", "0");
    return urlQuery.toString();
}

void TNetMPRSearcher::FetchDownloadUrl(const QString& strUrl, const QString& queryString)
{
    QUrl url(strUrl);
    url.setQuery(queryString);

    QNetworkRequest request;
    request.setUrl(url);

    qDebug() << __FUNCTION__ << __LINE__ << "DownloadURl: " << url.toString();

    mpFetchUrlReply = mpNetMgr->get(request);
    connect(mpFetchUrlReply, SIGNAL(finished()), this, SLOT(OnFetchUrlReply()));
    connect(mpFetchUrlReply, static_cast<void (QNetworkReply:: *)(QNetworkReply::NetworkError)>(&QNetworkReply::error), this, &TNetMPRSearcher::OnSearchError);
}

void TNetMPRSearcher::OnFetchUrlReply()
{
    QString uuid = QString();
    TNetMPRUrl downloadUrl;

    QNetworkReply* reply = static_cast<QNetworkReply*>(sender());
    QUrl url = reply->url();
    QString errString;
    QByteArray data = reply->readAll();
    reply->deleteLater();

    qDebug() << __FUNCTION__ << data;

    QJsonParseError parseError;
    QJsonDocument doc = QJsonDocument::fromJson(data, &parseError);
    if (parseError.error != QJsonParseError::NoError) {
        //用于出错回馈机制
        downloadUrl.msg = tr("download data error!");
        emit FetchDownloadUrlReplied(uuid, downloadUrl);
        return;
    }
    QJsonObject obj = doc.object();
    if ("000000" != obj.value("repcode").toString()) {
        //用于出错回馈机制
        downloadUrl.msg = tr("download data error!");
        emit FetchDownloadUrlReplied(uuid, downloadUrl);
        return;
    }

    downloadUrl.mprUrl = obj.value("address").toString();
    downloadUrl.licenseXml = obj.value("license").toString();

    uuid = QUrlQuery(url.query()).queryItemValue("downloadId");
    emit FetchDownloadUrlReplied(uuid, downloadUrl);
}

void TNetMPRSearcher::OnFetchUrlError(QNetworkReply::NetworkError err)
{
    qDebug() << __FUNCTION__ << __LINE__ << "Error Code: " << err;
}
