#include "TSTDPreCompiledHeader.h"
#include "TApplication.h"
#include "TDumpCore.h"
#include "TProxyStyle.h"
#include "TLogRecorder.h"
#include "TUIStyleManager.h"
#include "TMainWindow.h"
#include "TSplashScreen.h"

void AddTranslater()
{
    QTranslator* pTranslator = new QTranslator();
    QLocale locale;
    if (locale.language() == QLocale::Chinese)  //获取系统语言环境  
    {
        if (pTranslator->load(qApp->applicationDirPath() + "/" + tApp->GetAppName() + "_zh_CN"))
        {
            qApp->installTranslator(pTranslator);

            QTranslator* pQtTranslator = new QTranslator();
            if (pQtTranslator->load(qApp->applicationDirPath() + "/qt_zh_CN",
                QLibraryInfo::location(QLibraryInfo::TranslationsPath)))
            {
                qApp->installTranslator(pQtTranslator);
            }

        }
    }
    else
    {
        if (pTranslator->load(qApp->applicationDirPath() + "/" + tApp->GetAppName() + "_en_EN"))
        {
            qApp->installTranslator(pTranslator);

            QTranslator* pQtTranslator = new QTranslator();
            if (pQtTranslator->load(qApp->applicationDirPath() + "/qt_en",
                QLibraryInfo::location(QLibraryInfo::TranslationsPath)))
            {
                qApp->installTranslator(pQtTranslator);
            }
        }
    }
   
}

int MainFunc(int argc, char* argv[])
{
    TApplication app(argc, argv, "MPRTimes", "MPRDownloadTool");
    app.setStyle(new TProxyStyle(app.style()));
    if (app.IsRunning()){
        return 0;
    }

    TLogRecorder::Instance().StartRecord();

    TUIStyleManager::Instance().LoadUIStyle();

    AddTranslater();

    TMainWindow w;
    w.show();

    return app.exec();
}

int main(int argc, char* argv[])
{
#ifdef _DEBUG
    _CrtSetDbgFlag(_CrtSetDbgFlag(_CRTDBG_REPORT_FLAG) | _CRTDBG_LEAK_CHECK_DF);
    MainFunc(argc, argv);
    return 0;
#endif

    __try {
        MainFunc(argc, argv);
    }
    __except (TDumpCore::Instance().CrashHandler(GetExceptionInformation())) {
        qApp->exit();
    }
    return 0;
}