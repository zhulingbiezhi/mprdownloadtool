TEMPLATE     = app
TARGET       = MPRDownloadTool
CONFIG      -= flat
QT          += widgets network xml core gui

QMAKE_LFLAGS_CONSOLE    = /SUBSYSTEM:CONSOLE,"5.01"
QMAKE_LFLAGS_WINDOWS    = /SUBSYSTEM:WINDOWS,"5.01"
QMAKE_CFLAGS            = /D _USING_V110_SDK71 -nologo -Zc:wchar_t
QMAKE_CXXFLAGS          = /D _USING_V110_SDK71 $$QMAKE_CFLAGS
QMAKE_CXXFLAGS_DEBUG += /Od /W4  /wd4127 /wd4512 /wd4481
QMAKE_CFLAGS_RELEASE += -O2 -MD -Zi
QMAKE_LFLAGS_RELEASE += /INCREMENTAL:NO /DEBUG
QMAKE_CXXFLAGS_RELEASE += -O2 -MD -Zi
QMAKE_CXXLFLAGS_RELEASE += /INCREMENTAL:NO /DEBUG
DEFINES += _CRT_SECURE_NO_WARNINGS
DEFINES += SUPPORT_OS_XP


INCLUDEPATH += ./src \
                src/model src/model/common src/model/dumpcore src/model/logger src/model/network src/model/filelibrary \
                src/view src/view/common src/view/main \
                src/control \
                src/device \
                ./libs/Common/include \
                ./libs/libMPRLicense/include \
                ./libs/MFL/libMPRMimeDataFetcher/include
                

win32{
    contains(QT_ARCH, i386) {
        CONFIG(debug, debug|release) {
            LIBS += ./libs/libMPRLicense/lib/win32/licParser_v120_xp_x86_Debug.lib \
                    ./libs/MFL/libMPRMimeDataFetcher/lib/MPRMimeDataFetcher_v120_xp_x86_Debug.lib
            DESTDIR = bin/debug_x86
            OBJECTS_DIR = temp/debug_x86
        }   
        
        CONFIG(release, debug|release) {
            LIBS += ./libs/libMPRLicense/lib/win32/licParser_v120_xp_x86_Release.lib \
            ./libs/MFL/libMPRMimeDataFetcher/lib/MPRMimeDataFetcher_v120_xp_x86_Release.lib
            DESTDIR = bin/release_x86
            OBJECTS_DIR = temp/release_x86
        }   
        
    } else {
        CONFIG(debug, debug|release) {
            LIBS += ./libs/libMPRLicense/lib/win32/licParser_v120_xp_x64_Debug.lib \     
            
            DESTDIR = bin/debug_x64
            OBJECTS_DIR = temp/debug_x64
        }   
        
        CONFIG(release, debug|release) {
            LIBS += ./libs/libMPRLicense/lib/win32/licParser_v120_xp_x64_Release.lib \    
            
            DESTDIR = bin/release_x64
            OBJECTS_DIR = temp/release_x64
        }   
    }
}

LIBS += DbgHelp.Lib shell32.lib Advapi32.lib User32.lib

MOC_DIR = $${OBJECTS_DIR}/moc
RCC_DIR = $${OBJECTS_DIR}/rcc

RC_FILE = MPRDownloadTool.rc

# Use Precompiled headers (PCH)
PRECOMPILED_HEADER  = src/TSTDPreCompiledHeader.h \

HEADERS += \
        src/TApplication.h \
        src/view/main/TMainWindow.h \
        src/view/main/TSplashScreen.h \
        src/view/main/TBaseMessageBox.h\
        src/view/main/TCommonDialog.h\
        src/view/main/THeadWidget.h\
        src/view/main/TLeftTabWidget.h\
        src/view/main/TRightDisplayWidget.h\
        src/view/main/TTaskTabWidget.h\
        src/view/main/TDeviceTabWidget.h\
        src/view/main/TDeviceInfoWidget.h\
        src/view/main/TBaseDialog.h\
        src/view/main/TPopupDialog.h\
        src/view/main/TPageNaviBar.h\
        src/view/main/TSystemSettingDlg.h\
		src/view/main/TWaittingDlg.h\
		src/view/main/TDwmapi.h\
        src/model/common/TAppDefine.h \
        src/model/common/TAppDataDirManager.h \
        src/model/common/TUIStyleManager.h\ 
        src/model/common/TProxyStyle.h\
        src/model/common/TCommonHelper.h \
        src/model/common/TRegeditManager.h \
        src/model/common/TWorkStateConfig.h \
        src/model/common/TConfigFileManager.h \
        src/model/common/TGlobleCtl.h \
        src/model/common/TLicenseParser.h \
        src/model/common/TAppCommon.h\
        src/model/network/TDataStream.h \
        src/model/network/TDownloadManager.h \
        src/model/network/TDownloadTask.h \
        src/model/network/THttpTask.h \
        src/model/network/TLocalSocket.h \
        src/model/network/TMPRSearhRepliedInfo.h \
        src/model/network/TWebServiceInterface.h \
        src/model/network/TNetMPRSearcher.h \
        src/model/network/TSearchHandle.h \
		src/model/network/TDownloadTaskProber.h\
        src/model/filelibrary/TFileLib.h \
        src/model/filelibrary/TFileLibDescXml.h \ 
        src/model/filelibrary/TDistLibDescXml.h \ 
        src/model/filelibrary/TDisTaskFileData.h \   
        src/model/dumpcore/TDumpCore.h \
        src/model/logger/TLogRecorder.h \
        src/device/TMPRDeviceUSBComm.h \
        src/device/TMPRDeviceHandler.h \
        src/device/TMPRDeviceComm.h \
        src/device/TMPRDevicesManager.h \
        src/device/TMPRDeviceUSBInfo.h \
        src/device/TMPRDeviceInfo.h \
        src/device/MPRDeviceCmd.h \
        src/device/TDeviceEject.h \
        src/device/TMPRDeviceCommData.h \
        src/device/TMPRDeviceDesc.h \
        src/device/TDefaultMPRDeviceManager.h \
        src/device/TMPRDeviceDescFileReader.h \
        src/control/TFileEnum.h\
        src/control/TCopyFileState.h\
        src/control/TCopyFileTask.h\
        src/control/TCopyFileManager.h\
        src/control/TDeviceDataManager.h\
        src/control/TUpdateVersionManager.h\
        src/control/TLocalLibHandle.h \
        src/control/TFileScanner.h



SOURCES += \
        src/main.cpp \
        src/TApplication.cpp \
        src/view/main/TMainWindow.cpp \
        src/view/main/TSplashScreen.cpp \   
        src/view/main/TBaseMessageBox.cpp\
        src/view/main/TCommonDialog.cpp\
        src/view/main/THeadWidget.cpp\
        src/view/main/TLeftTabWidget.cpp\
        src/view/main/TRightDisplayWidget.cpp\
        src/view/main/TTaskTabWidget.cpp\
        src/view/main/TDeviceTabWidget.cpp\
        src/view/main/TDeviceInfoWidget.cpp\
        src/view/main/TBaseDialog.cpp\
        src/view/main/TPopupDialog.cpp\
        src/view/main/TPageNaviBar.cpp\
        src/view/main/TSystemSettingDlg.cpp\
		src/view/main/TWaittingDlg.cpp\
        src/model/common/TAppDataDirManager.cpp \
        src/model/common/TUIStyleManager.cpp\
        src/model/common/TProxyStyle.cpp\
        src/model/common/TCommonHelper.cpp \
        src/model/common/TRegeditManager.cpp \
        src/model/common/TWorkStateConfig.cpp \
        src/model/common/TConfigFileManager.cpp \
        src/model/common/TGlobleCtl.cpp \
        src/model/common/TLicenseParser.cpp \
        src/model/common/TAppCommon.cpp\
        src/model/network/TDataStream.cpp \
        src/model/network/TDownloadManager.cpp \
        src/model/network/TDownloadTask.cpp \
        src/model/network/THttpTask.cpp \
        src/model/network/TLocalSocket.cpp \
        src/model/network/TNetMPRSearcher.cpp \
        src/model/network/TWebServiceInterface.cpp \
        src/model/network/TSearchHandle.cpp \
		src/model/network/TDownloadTaskProber.cpp\
        src/model/filelibrary/TFileLib.cpp \     
        src/model/filelibrary/TFileLibDescXml.cpp \    
        src/model/filelibrary/TDistLibDescXml.cpp \  
        src/model/dumpcore/TDumpCore.cpp \
        src/model/logger/TLogRecorder.cpp \
        src/device/TMPRDeviceUSBComm.cpp \
        src/device/TMPRDeviceHandler.cpp \
        src/device/TMPRDeviceComm.cpp \
        src/device/TMPRDevicesManager.cpp \
        src/device/TMPRDeviceUSBInfo.cpp \
        src/device/TMPRDeviceInfo.cpp \
        src/device/TDeviceEject.cpp \
        src/device/TMPRDeviceCommData.cpp \
        src/device/TDefaultMPRDeviceManager.cpp \
        src/device/TMPRDeviceDescFileReader.cpp \
        src/control/TCopyFileTask.cpp\
        src/control/TCopyFileManager.cpp\
        src/control/TDeviceDataManager.cpp\
        src/control/TUpdateVersionManager.cpp \
        src/control/TLocalLibHandle.cpp \
        src/control/TFileScanner.cpp