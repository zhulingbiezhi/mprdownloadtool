﻿@echo off
::警告 请不要任意改动和编辑代码，如有配置问题，请进入到脚本_project_setting.bat中进行设定

:InitConfig
    echo.------------InitConfig------------
    set Package=true
    call _project_setting.bat
    echo. 

:BuildPro
    echo.------------BuildPro------------
    cd "%ProClientDir%"
    echo.
    call _generate_for_project_x86.bat

:Package
    echo.------------Package-------------
    cd /d "%ProPackageDir%"
    call package_zh_CN_x86.bat
    echo.
