<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>QObject</name>
    <message>
        <location filename="src/model/dumpcore/TDumpCore.cpp" line="51"/>
        <source>Program an exception occurs, exception information has been saved to the desktop, FileName:</source>
        <translation>Software exception, the exception information has been saved on the desktop!</translation>
    </message>
    <message>
        <location filename="src/model/dumpcore/TDumpCore.cpp" line="52"/>
        <source>Progress Error</source>
        <translation>Software exception</translation>
    </message>
    <message>
        <location filename="src/device/TDeviceEject.cpp" line="157"/>
        <source>Media in Drive %c has been ejected safely.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/device/TDeviceEject.cpp" line="163"/>
        <source>Media in Drive %c can be safely removed.
</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TBaseMessageBox</name>
    <message>
        <location filename="src/view/main/TBaseMessageBox.cpp" line="42"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="src/view/main/TBaseMessageBox.cpp" line="43"/>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
</context>
<context>
    <name>TDeviceInfoWidget</name>
    <message>
        <location filename="src/view/main/TDeviceInfoWidget.cpp" line="20"/>
        <source>Device Type:</source>
        <translation>Model of device:</translation>
    </message>
    <message>
        <location filename="src/view/main/TDeviceInfoWidget.cpp" line="24"/>
        <source>Device Id:</source>
        <translation>Device ID:</translation>
    </message>
    <message>
        <location filename="src/view/main/TDeviceInfoWidget.cpp" line="29"/>
        <source>Capacity:</source>
        <translation>Capacity:</translation>
    </message>
    <message>
        <location filename="src/view/main/TDeviceInfoWidget.cpp" line="39"/>
        <location filename="src/view/main/TDeviceInfoWidget.cpp" line="161"/>
        <source>Used:%1,Total:%2</source>
        <translation>Used: %1, tatal: %2</translation>
    </message>
    <message>
        <location filename="src/view/main/TDeviceInfoWidget.cpp" line="88"/>
        <source>Used:%1%2,Total:%3%4</source>
        <translation>Used: %1%2, tatal: %3%4</translation>
    </message>
</context>
<context>
    <name>TDeviceTabWidget</name>
    <message>
        <location filename="src/view/main/TDeviceTabWidget.cpp" line="73"/>
        <source>  </source>
        <translation> </translation>
    </message>
    <message>
        <location filename="src/view/main/TDeviceTabWidget.cpp" line="73"/>
        <source>Book Name</source>
        <translation>Book name</translation>
    </message>
    <message>
        <location filename="src/view/main/TDeviceTabWidget.cpp" line="73"/>
        <source>Publisher Name</source>
        <translation>Publisher</translation>
    </message>
    <message>
        <location filename="src/view/main/TDeviceTabWidget.cpp" line="73"/>
        <source>Format</source>
        <translation>Format</translation>
    </message>
    <message>
        <location filename="src/view/main/TDeviceTabWidget.cpp" line="73"/>
        <source>Download Time</source>
        <translation>Download time</translation>
    </message>
    <message>
        <location filename="src/view/main/TDeviceTabWidget.cpp" line="73"/>
        <source>File Size</source>
        <translation>File size</translation>
    </message>
    <message>
        <location filename="src/view/main/TDeviceTabWidget.cpp" line="73"/>
        <source>Operate</source>
        <translation>Operate</translation>
    </message>
    <message>
        <location filename="src/view/main/TDeviceTabWidget.cpp" line="187"/>
        <source>delete mpr files</source>
        <translation>Delete MPR file(s)</translation>
    </message>
    <message>
        <location filename="src/view/main/TDeviceTabWidget.cpp" line="187"/>
        <source>Are you sure to delete this MPF file?</source>
        <translation>Are you sure to delete the MPR file(s)?</translation>
    </message>
</context>
<context>
    <name>THeadWidget</name>
    <message>
        <location filename="src/view/main/THeadWidget.cpp" line="24"/>
        <source>Setting</source>
        <translation>Setting</translation>
    </message>
    <message>
        <source>Version Info</source>
        <translation type="vanished">Version</translation>
    </message>
    <message>
        <location filename="src/view/main/THeadWidget.cpp" line="20"/>
        <source>MPR DownloadTool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/view/main/THeadWidget.cpp" line="29"/>
        <source>Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/view/main/THeadWidget.cpp" line="34"/>
        <source>minimumSize</source>
        <translation>Minimize</translation>
    </message>
    <message>
        <location filename="src/view/main/THeadWidget.cpp" line="38"/>
        <source>close</source>
        <translation>Close</translation>
    </message>
</context>
<context>
    <name>TLeftTabWidget</name>
    <message>
        <location filename="src/view/main/TLeftTabWidget.cpp" line="20"/>
        <source>My Device</source>
        <translation>My device</translation>
    </message>
    <message>
        <location filename="src/view/main/TLeftTabWidget.cpp" line="23"/>
        <source>My Task</source>
        <translation>Download list</translation>
    </message>
</context>
<context>
    <name>TMainWindow</name>
    <message>
        <location filename="src/view/main/TMainWindow.cpp" line="332"/>
        <source>Software version: V</source>
        <translation>Software version</translation>
    </message>
    <message>
        <source>
Update date: 
</source>
        <translation type="vanished">
Date of update</translation>
    </message>
    <message>
        <source>
Copyright: shen zhen tian lang shi dai 
</source>
        <oldsource>Copyright: shen zhen tian lang shi dai 
</oldsource>
        <translation type="obsolete">
Copyright © 2016 Shenzhen MPR Technology Co., Ltd. All Rights Reserved.
</translation>
    </message>
    <message>
        <location filename="src/view/main/TMainWindow.cpp" line="337"/>
        <source>Copyright: shen zhen tian lang shi dai </source>
        <oldsource>Copyright: shen zhen tian lang shi dai 
</oldsource>
        <translation>Copyright © 2016 Shenzhen MPR Technology Co., Ltd. All Rights Reserved.</translation>
    </message>
    <message>
        <location filename="src/view/main/TMainWindow.cpp" line="339"/>
        <source>current version is the latest version</source>
        <translation>Your software is up to date</translation>
    </message>
    <message>
        <location filename="src/view/main/TMainWindow.cpp" line="343"/>
        <source>Version Information</source>
        <translation>Version information</translation>
    </message>
</context>
<context>
    <name>TNetMPRSearcher</name>
    <message>
        <location filename="src/model/network/TNetMPRSearcher.cpp" line="125"/>
        <source>no file is exit !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TNetMPRSearcher.cpp" line="125"/>
        <source>Tips</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TNetMPRSearcher.cpp" line="193"/>
        <location filename="src/model/network/TNetMPRSearcher.cpp" line="200"/>
        <source>download data error!</source>
        <translation>Download failed!</translation>
    </message>
</context>
<context>
    <name>TPageNaviBar</name>
    <message>
        <location filename="src/view/main/TPageNaviBar.cpp" line="28"/>
        <source>Previous Page</source>
        <translation>Previous</translation>
    </message>
    <message>
        <location filename="src/view/main/TPageNaviBar.cpp" line="74"/>
        <source>Next Page</source>
        <translation>Next</translation>
    </message>
    <message>
        <location filename="src/view/main/TPageNaviBar.cpp" line="94"/>
        <source>Page</source>
        <translation>Page</translation>
    </message>
    <message>
        <location filename="src/view/main/TPageNaviBar.cpp" line="334"/>
        <source>Total %1 pages / Total %2 records, To</source>
        <translation>Total %1 page / Total %2 records, jump to</translation>
    </message>
</context>
<context>
    <name>TPopupDialog</name>
    <message>
        <location filename="src/view/main/TPopupDialog.cpp" line="43"/>
        <source>Close</source>
        <translation>Close</translation>
    </message>
    <message>
        <location filename="src/view/main/TPopupDialog.cpp" line="97"/>
        <source>Ok</source>
        <translation>OK!</translation>
    </message>
    <message>
        <location filename="src/view/main/TPopupDialog.cpp" line="104"/>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="src/view/main/TPopupDialog.cpp" line="110"/>
        <source>Yes</source>
        <translation>Yes</translation>
    </message>
    <message>
        <location filename="src/view/main/TPopupDialog.cpp" line="117"/>
        <source>No</source>
        <translation>No</translation>
    </message>
</context>
<context>
    <name>TSystemSettingDlg</name>
    <message>
        <location filename="src/view/main/TSystemSettingDlg.cpp" line="53"/>
        <source>select save directory</source>
        <oldsource>select import directory</oldsource>
        <translation>Cache path</translation>
    </message>
    <message>
        <location filename="src/view/main/TSystemSettingDlg.cpp" line="72"/>
        <source>setting</source>
        <translation>Setting</translation>
    </message>
    <message>
        <location filename="src/view/main/TSystemSettingDlg.cpp" line="80"/>
        <source>Close</source>
        <translation>Close</translation>
    </message>
    <message>
        <location filename="src/view/main/TSystemSettingDlg.cpp" line="99"/>
        <source>browse</source>
        <translation>Browse</translation>
    </message>
    <message>
        <location filename="src/view/main/TSystemSettingDlg.cpp" line="100"/>
        <source>save</source>
        <translation>Save</translation>
    </message>
    <message>
        <location filename="src/view/main/TSystemSettingDlg.cpp" line="101"/>
        <source>cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="src/view/main/TSystemSettingDlg.cpp" line="118"/>
        <source>save path:</source>
        <translation>Local storage path:</translation>
    </message>
    <message>
        <location filename="src/view/main/TSystemSettingDlg.cpp" line="128"/>
        <source>It does effective when next start software!</source>
        <translation>The setting will be active after restarting!</translation>
    </message>
</context>
<context>
    <name>TTaskTabWidget</name>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="31"/>
        <source>Warm prompt: Using USB data lin to connet device with computer first, and make computer staying connected to the Internet.
 Then your can download mpr files by touch mpr book using touch and talk pen!</source>
        <translation>Tips: Please connect your talking pen to the computer with USB cable and keep your computer connected to the Internet.
 Start the audio files downloading by touch reading the book.</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="36"/>
        <source>There is no download task, please touch reading the MPR book</source>
        <translation>No download,please touch read the book to download audio files</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="77"/>
        <source>  </source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="77"/>
        <source>Book Name</source>
        <translation>Book name</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="77"/>
        <source>Publisher Name</source>
        <translation>Publisher</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="77"/>
        <source>Format</source>
        <translation>Format</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="77"/>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="77"/>
        <source>Operate</source>
        <translation>Operate</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="249"/>
        <source>start</source>
        <translation>Start</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="250"/>
        <source>pause</source>
        <translation>Pause</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="251"/>
        <source>delete</source>
        <translation>Delete</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="344"/>
        <source>Prepare download</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="349"/>
        <source>Downloading</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="354"/>
        <source>Download pause</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="359"/>
        <source>Download finished</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="367"/>
        <source>Download failed</source>
        <oldsource>Download Error</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="373"/>
        <source>Prepare copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="381"/>
        <source>Copying</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="386"/>
        <source>Copy pause</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="392"/>
        <source>Copy finished</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="404"/>
        <source>Copy failed</source>
        <oldsource>Copy error</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Prepare</source>
        <translation type="vanished">Prepare to download</translation>
    </message>
    <message>
        <source>Download</source>
        <translation type="vanished">Downloading...</translation>
    </message>
    <message>
        <source>Pause</source>
        <translation type="vanished">Paused</translation>
    </message>
    <message>
        <source>Copyfile</source>
        <translation type="vanished">Copying</translation>
    </message>
    <message>
        <source>Finished</source>
        <translation type="vanished">Finished</translation>
    </message>
    <message>
        <source>Not Enough Space</source>
        <translation type="vanished">No enough space</translation>
    </message>
    <message>
        <source>Copy File Failed</source>
        <translation type="vanished">Copying failed</translation>
    </message>
    <message>
        <source>Download File Failed</source>
        <translation type="vanished">Download failed</translation>
    </message>
    <message>
        <source>file download error</source>
        <translation type="vanished">Download failed</translation>
    </message>
</context>
<context>
    <name>TUpdateVersionManager</name>
    <message>
        <location filename="src/control/TUpdateVersionManager.cpp" line="259"/>
        <source>Prompt</source>
        <translatorcomment>Update</translatorcomment>
        <translation>Update</translation>
    </message>
    <message>
        <location filename="src/control/TUpdateVersionManager.cpp" line="259"/>
        <source>Firmware update success,please pull out the device and wait to reboot</source>
        <translation>Firmware update succeccfully,please pull out the talking pen and wait for rebooting.</translation>
    </message>
    <message>
        <location filename="src/control/TUpdateVersionManager.cpp" line="239"/>
        <location filename="src/control/TUpdateVersionManager.cpp" line="280"/>
        <source>Firmware upgrade failure</source>
        <translation>Firmware update failed!</translation>
    </message>
    <message>
        <location filename="src/control/TUpdateVersionManager.cpp" line="269"/>
        <location filename="src/control/TUpdateVersionManager.cpp" line="280"/>
        <source>Upgrade package download failed</source>
        <translatorcomment>Downloading the firmware upgrade package failed!</translatorcomment>
        <translation>Downloading of the upgrade package failed!</translation>
    </message>
    <message>
        <location filename="src/control/TUpdateVersionManager.cpp" line="240"/>
        <location filename="src/control/TUpdateVersionManager.cpp" line="270"/>
        <location filename="src/control/TUpdateVersionManager.cpp" line="281"/>
        <source>Retry</source>
        <translation>Retry</translation>
    </message>
    <message>
        <location filename="src/control/TUpdateVersionManager.cpp" line="239"/>
        <source>Upgrade package copy failed</source>
        <translation>Copying of the upgrade package failed!</translation>
    </message>
    <message>
        <location filename="src/control/TUpdateVersionManager.cpp" line="269"/>
        <source>Software upgrade failure</source>
        <translation>Update failed</translation>
    </message>
    <message>
        <location filename="src/control/TUpdateVersionManager.cpp" line="306"/>
        <source>file is downloading, do not pull out the device</source>
        <translation>Firmware is downloding, do not pull out the talking pen</translation>
    </message>
    <message>
        <location filename="src/control/TUpdateVersionManager.cpp" line="310"/>
        <source>file is downloading, please do not close the program !</source>
        <translation>Downloading, please don&apos;t close MPR download tool</translation>
    </message>
    <message>
        <location filename="src/control/TUpdateVersionManager.cpp" line="471"/>
        <location filename="src/control/TUpdateVersionManager.cpp" line="529"/>
        <source>Software upgrade tips</source>
        <translation>Tips for software update</translation>
    </message>
    <message>
        <location filename="src/control/TUpdateVersionManager.cpp" line="471"/>
        <source>Software version exist new version, whether to download upgrade?</source>
        <translation>There is update for the software,update now?</translation>
    </message>
    <message>
        <location filename="src/control/TUpdateVersionManager.cpp" line="484"/>
        <location filename="src/control/TUpdateVersionManager.cpp" line="493"/>
        <source>Firmware upgrade tips</source>
        <translation>Firmware update</translation>
    </message>
    <message>
        <location filename="src/control/TUpdateVersionManager.cpp" line="484"/>
        <source>Reading pen firmware exist new version, whether to download upgrade?</source>
        <translation>There is  update for firmware,do you want to try?</translation>
    </message>
    <message>
        <location filename="src/control/TUpdateVersionManager.cpp" line="493"/>
        <source>current firmwarevision is incompaticatable, you need to download upgrade!</source>
        <translation>The firmware is not compatible with MPR World, please update!</translation>
    </message>
    <message>
        <location filename="src/control/TUpdateVersionManager.cpp" line="529"/>
        <source>Update download finished, restart or not?</source>
        <translation>Finish downloading update, restart now?</translation>
    </message>
    <message>
        <location filename="src/control/TUpdateVersionManager.cpp" line="530"/>
        <source>restart</source>
        <translation>restart</translation>
    </message>
</context>
<context>
    <name>TWaittingDlg</name>
    <message>
        <location filename="src/view/main/TWaittingDlg.cpp" line="115"/>
        <source>Close</source>
        <translation type="unfinished">Close</translation>
    </message>
    <message>
        <location filename="src/view/main/TWaittingDlg.cpp" line="123"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
</context>
<context>
    <name>TWebServiceInterface</name>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="670"/>
        <source>Success</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="673"/>
        <source>Parameter error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="676"/>
        <source>Equipment loss</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="679"/>
        <source>Not identify user</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="682"/>
        <source>Failed to get ticket</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="685"/>
        <source>Please connect equipment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="688"/>
        <source>Resource was not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="691"/>
        <source>Account inactive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="694"/>
        <source>Account exception</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="697"/>
        <source>Account does not exist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="700"/>
        <source>Account or password error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="703"/>
        <source>Equipment account binding relationship does not exist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="706"/>
        <source>Equipment abnormal binding</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="709"/>
        <source>Equipment has bound</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="712"/>
        <source>Equipment wasn&apos;t register</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="715"/>
        <source>Inconsistent binding account login account and equipment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="718"/>
        <source>The device did not buy the goods</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="721"/>
        <source>No purchase record fee content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="724"/>
        <source>Equipment validation failure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="727"/>
        <source>A system exception</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="1245"/>
        <source>Failure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="1245"/>
        <source>The search timeout, please try again</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>serverSocket</name>
    <message>
        <location filename="src/update/serversocket.cpp" line="48"/>
        <source>software success</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/update/serversocket.cpp" line="49"/>
        <source>software error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/update/serversocket.cpp" line="50"/>
        <source>firmware success</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/update/serversocket.cpp" line="51"/>
        <source>firmware error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/update/serversocket.cpp" line="52"/>
        <source>connect success</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
