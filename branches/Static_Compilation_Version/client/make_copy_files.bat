@echo off
::copy相关的外链文件，打包程序拷贝不在此

set local=%~dp0
call "%local%../_project_setting.bat"


echo.[Copy Libs files] ......
    ::copy ".\libs\MFL\libMPRUpgrade\bin\debug\libMPRUpgrade_v120_xp_x86_Debug.dll" ".\bin\debug_x86" /B /Y
    ::copy ".\libs\MFL\libMPRUpgrade\bin\release\libMPRUpgrade_v120_xp_x86_Release.dll" ".\bin\release_x86" /B /Y
    
    ::copy ".\libs\MFL\libMPRUpgrade\bin\debug\libMPRWebdwn_v120_xp_x86_Debug.dll" ".\bin\debug_x86" /B /Y  
    ::copy ".\libs\MFL\libMPRUpgrade\bin\release\libMPRWebdwn_v120_xp_x86_Release.dll" ".\bin\release_x86" /B /Y
    
    ::copy ".\libs\MFL\libMPRUpgrade\bin\debug\libMPRUpgrade_v120_xp_x64_Debug.dll" ".\bin\debug_x64" /B /Y
    ::copy ".\libs\MFL\libMPRUpgrade\bin\release\libMPRUpgrade_v120_xp_x64_Release.dll" ".\bin\release_x64" /B /Y
    
    ::copy ".\libs\MFL\libMPRUpgrade\bin\debug\libMPRWebdwn_v120_xp_x64_Debug.dll" ".\bin\debug_x64 /B /Y   
    ::copy ".\libs\MFL\libMPRUpgrade\bin\release\libMPRWebdwn_v120_xp_x64_Release.dll" ".\bin\release_x64 /B /Y
    
    copy ".\MonitorServer\updated.exe" ".\bin\debug_x86"
    copy ".\MonitorServer\update.exe" ".\bin\release_x86"
echo.

if "%Package%" EQU "true" (exit /B 1)

if "%Generate%" EQU "true" (exit  /B 1)

pause