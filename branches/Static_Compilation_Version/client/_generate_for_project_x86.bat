﻿@echo off

::警告 请不要任意改动和编辑代码，如有配置问题，请进入到脚本_project_setting.bat中进行设定
set Generate=true

:InitConfig
    echo.------------InitConfig------------
    set local=%~dp0
    call "%local%../_project_setting.bat"
    echo. 

:SetEnv
    echo.------------SetEnv----------------
    echo cd /d "%ProClientDir%"
    cd /d "%ProClientDir%"
    call make_setenv_x86.bat
    echo.
    
:MakeVersion	
    echo.------------MakeVersion------------
    echo cd /d "%ProClientDir%"
    cd /d "%ProClientDir%"
    call make_version.bat
    echo.
    
:MakeTranslate
    echo.------------MakeTranslate----------
    cd /d "%ProClientDir%"
    call make_translate.bat
    echo.

:::MakeQRC
    ::echo.------------MakeQRC----------
    ::cd /d "%ProClientDir%"
    ::call make_qrc.bat
    ::echo.

:::CopyFiles
    ::echo.------------CopyFiles--------------
    ::cd /d "%ProClientDir%"
    ::call make_copy_files.bat
    ::echo.
    
if "%Package%" EQU "true" (goto GenerateExe)
    
:MakePro
    echo.------------MakePro----------------
    cd /d "%ProClientDir%"
    echo.qmake -tp vc "%ProName%.pro"
        qmake -tp vc "%ProName%.pro"
    echo.
    goto MakeEnd

:GenerateExe
    echo.------------GenerateExe------------
    cd /d "%ProClientDir% "   
    echo.qmake "%ProName%.pro"
        qmake "%ProName%.pro"
    echo.nmake clean
        nmake clean
    echo.nmake release
        nmake release
    echo.

:MakeEnd
    if "%Package%" NEQ "true" ( pause exit )
    echo.
