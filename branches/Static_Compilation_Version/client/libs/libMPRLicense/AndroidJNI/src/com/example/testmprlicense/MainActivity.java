package com.example.testmprlicense;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Arrays;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;

import com.mpr.mfl.MPRLicense;

public class MainActivity extends Activity {

	private static final String TAG = "TestMPRLicense";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		/*******************************************打包生成License*****************************************/
		MPRLicense lic = new MPRLicense();
		lic.version = 0x01;
		lic.asymType = MPRLicense.RSA;
		lic.symType = MPRLicense.AES_CBC;
		lic.guid = new byte[16];
		lic.keyParaLength = 128;
		lic.keyPara = new byte[128];
		lic.expireDate = new byte[8];
		lic.issuedType = MPRLicense.ISSUED_TYPE_TO_ALL_DEVICES;
		lic.issuedUser = new byte[128];
		lic.issuedFrom = new byte[20];
		lic.issuedTo = new byte[20];
		lic.signatureLength = 64;
		lic.signatureData = new byte[64];
		Arrays.fill(lic.guid, (byte) 0x01);
		Arrays.fill(lic.keyPara, (byte) 0x02);
		Arrays.fill(lic.expireDate, (byte) 0x03);
		Arrays.fill(lic.issuedUser, (byte) 0x04);
		Arrays.fill(lic.issuedFrom, (byte) 0x05);
		Arrays.fill(lic.issuedTo, (byte) 0x06);
		Arrays.fill(lic.signatureData, (byte) 0x07);
		
		byte[] licbuf = lic.pack();

		try {
			FileOutputStream fos = null;
			fos = new FileOutputStream(new File("/sdcard/lictest.lic"));
			fos.write(licbuf);
			fos.close();
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}

		/*******************************************拆包License*****************************************/
		try {
			InputStream fis = null;
			fis = getAssets().open("9A0NOQQ.lic");

			int size = fis.available();
			licbuf = new byte[size];

			fis.read(licbuf);
			fis.close();
		} catch (Exception e){
			e.printStackTrace();
			return;
		}
		
		lic = new MPRLicense();
		int ret = lic.unPack(licbuf);
		Log.d(TAG, "ret=" + ret);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
