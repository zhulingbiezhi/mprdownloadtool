package com.mpr.mfl;

public class MPRLicense {
	
	public int version;							// License版本，固定为0x01
	public int asymType;						// 非对称加密类型
	public int symType;							// 对称加密类型
	public byte[] guid;							// license GUID, 与MPR文件头中的GUID匹配
	public int keyParaLength;					// KeyPara长度
	public byte[] keyPara;						// KeyPara字节流。正确解密后得到32个字节的原文。原文前16个字节为对称加密算法密钥（Key），后16个字节是IV（初始向量，仅在CBC模式下有效）
	public byte[] expireDate;					// 失效日期，格式YYYYMMDD
	public int issuedType;						// 授权类型
	public byte[] issuedUser;					// 授权用户
	public byte[] issuedFrom;					// License发布者，可以是网络平台或MPR设备
	public byte[] issuedTo;						// License被授权设备，可以是网络平台或MPR设备
	public int signatureLength;					// 签名数据长度
	public byte[] signatureData;				// 签名数据

	// 设备支持的非对称加密算法类型
    public static final int RSA = 0x01;           //RSA算法
    public static final int SM2 = 0x02;           //国密2算法
	
	// 设备支持的对称加密算法类型
	public static final int AES_ECB = 0x0001;     //AES ECB算法
	public static final int AES_CBC = 0x0002;     //AES CBC算法
	public static final int AES_CTR = 0x0004;     //AES CTR算法
	public static final int SM4_ECB = 0x0100;     //国密4 ECB算法
	public static final int SM4_CBC = 0x0200;     //国密4 CBC算法
	public static final int SM4_CTR = 0x0400;     //国密4 CTR算法

	// 授权类型
	public static final int ISSUED_TYPE_TO_ALL_DEVICES = 0x01;	//授权给全体设备
	public static final int ISSUED_TYPE_TO_DEVICE = 0x02;		//授权给单个设备
	public static final int ISSUED_TYPE_TO_BRAND = 0x04;		//授权给设备品牌
	public static final int ISSUED_TYPE_TO_USER = 0x08;			//授权给用户
	public static final int ISSUED_TYPE_TO_SERVER = 0x10;		//授权给网络平台

	// 返回错误代码
    public static final int DEVICE_OK = 0;  	  //接口函数调用成功
    public static final int BAD_PARAM = -1;       //传入的接口函数参数错误
    public static final int BAD_CALL  = -2;       //接口函数调用顺序错误

    public MPRLicense(int version,
			int asymType,
			int symType,
			byte[] guid,
			int keyParaLength,
			byte[] keyPara,
			byte[] expireDate,
			int issuedType,
			byte[] issuedUser,
			byte[] issuedFrom,
			byte[] issuedTo,
			int signatureLength,
			byte[] signatureData) {
		
		this.version = version;
		this.asymType = asymType;
		this.symType = symType;
		this.guid = guid;
		this.keyParaLength = keyParaLength;
		this.keyPara = keyPara;
		this.expireDate = expireDate;
		this.issuedType = issuedType;
		this.issuedUser = issuedUser;
		this.issuedFrom = issuedFrom;
		this.issuedTo = issuedTo;
		this.signatureLength = signatureLength;
		this.signatureData = signatureData;
	}
	
	public MPRLicense() {
		this.version = 0;
		this.asymType = 0;
		this.symType = 0;
		this.guid = null;
		this.keyParaLength = 0;
		this.keyPara = null;
		this.expireDate = null;
		this.issuedType = 0;
		this.issuedUser = null;
		this.issuedFrom = null;
		this.issuedTo = null;
		this.signatureLength = 0;
		this.signatureData = null;
	}

	public byte[] pack() {		
		
		if (version != 1 ||
			(asymType != RSA && asymType != SM2) ||
			(symType != AES_ECB && symType != AES_CBC && symType != AES_CTR && symType != SM4_ECB && symType != SM4_CBC && symType != SM4_CTR) ||
			guid == null || guid.length != 16 ||
			keyParaLength <= 0 ||
			keyPara == null || keyPara.length != keyParaLength ||
			expireDate == null || expireDate.length != 8 ||
			(issuedType !=  ISSUED_TYPE_TO_ALL_DEVICES && issuedType !=  ISSUED_TYPE_TO_DEVICE && issuedType !=  ISSUED_TYPE_TO_BRAND && issuedType !=  ISSUED_TYPE_TO_USER && issuedType !=  ISSUED_TYPE_TO_SERVER) ||
			issuedUser == null || issuedUser.length > 128 ||
			issuedFrom == null || issuedFrom.length != 20 ||
			issuedTo == null || issuedTo.length != 20 ||
			signatureLength <= 0 || signatureData == null || signatureData.length != signatureLength) return null;
			
		return nativePack(version,
				asymType,
				symType,
				guid,
				keyParaLength,
				keyPara,
				expireDate,
				issuedType,
				issuedUser,
				issuedFrom,
				issuedTo,
				signatureLength,
				signatureData);
	}
	
	public int unPack(byte[] buf) {
		
		return nativeUnPack(buf);
	}
	
	private native byte[] nativePack(int version,
			int asymType,
			int symType,
			byte[] guid,
			int keyParaLength,
			byte[] keyPara,
			byte[] expireDate,
			int issuedType,
			byte[] issuedUser,
			byte[] issuedFrom,
			byte[] issuedTo,
			int signatureLength,
			byte[] signatureData);
	
	private native int nativeUnPack(byte[] buf);
	
	static {
		System.loadLibrary("MPRLicenseJNI");
	}
}
