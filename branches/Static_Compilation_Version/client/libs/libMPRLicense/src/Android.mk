LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE    := libMPRLicense
LOCAL_CFLAGS    := -Werror
LOCAL_SRC_FILES := licParser.c
LOCAL_LDLIBS    := -llog
LOCAL_MODULE_TAGS := eng
include $(BUILD_STATIC_LIBRARY)