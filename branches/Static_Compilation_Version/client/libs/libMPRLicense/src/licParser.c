#include "../include/licParser.h"
#include <string.h>

int MPRLicense_Pack(const MPRLicense_s*  p_license_s,  //[IN]  传入license结构
                    unsigned char*       p_license,    //[OUT] 打包后的license首地址, 建议buf长度512
                    int*                 p_len         //[IN/OUT] 传入license buff长度, 传出实际打包后license长度            
                    )
{
    int pos = 0;
    if(!p_license_s || !p_license){
        return BAD_PARAM;
    }

    if(*p_len < 189 + p_license_s->KeyParaLength + p_license_s->SignatureLength){
        return BAD_PARAM;
    }

    p_license[0] = 0;
    strcpy((char*)p_license, "MPRLIC");
    pos += strlen("MPRLIC") + 1;

    p_license[pos] =  p_license_s->Version;
    pos += 1;

    p_license[pos] =  p_license_s->AsymType;
    pos += 1;


    p_license[pos] =  p_license_s->SymType / 256;
    p_license[pos + 1] =  p_license_s->SymType & 0xff;
    pos += 2;

    memcpy(p_license + pos, p_license_s->GUID, 16);
    pos += 16;

    p_license[pos] =  p_license_s->KeyParaLength / 256;
    p_license[pos + 1] =  p_license_s->KeyParaLength & 0xff;
    pos += 2;

    memcpy(p_license + pos, p_license_s->KeyPara, p_license_s->KeyParaLength);
    pos += p_license_s->KeyParaLength;

    memcpy(p_license + pos, p_license_s->ExpireDate, 8);
    pos += 8;

    p_license[pos] = p_license_s->IssuedType;
    pos += 1;

    memcpy(p_license + pos, p_license_s->IssuedUser, 128);
    pos += 128;

    memcpy(p_license + pos, p_license_s->IssuedFrom, 20);
    pos += 20;

    memcpy(p_license + pos, p_license_s->IssuedTo, 20);
    pos += 20;

    p_license[pos] =  p_license_s->SignatureLength / 256;
    p_license[pos + 1] =  p_license_s->SignatureLength & 0xff;
    pos += 2;

    memcpy(p_license + pos, p_license_s->SignatureData, p_license_s->SignatureLength);
    pos += p_license_s->SignatureLength;
    
    *p_len = pos;

    return 0;
}
int MPRLicense_UnPack(const unsigned char* p_license,    //[IN] 打包的license首地址
                      const int            len,          //[IN] 打包的license长度           
                      MPRLicense_s*        p_license_s   //[OUT]license结构
                      )
{
    int pos = 0;
    if(!p_license_s || !p_license || len <= 189){
        return BAD_PARAM;
    }

    pos += strlen("MPRLIC") + 1;

    p_license_s->Version = p_license[pos];
    pos += 1;

    p_license_s->AsymType = (PKI_Algorithm)p_license[pos];
    pos += 1;


    p_license_s->SymType = (EncryptAlgorithm)(p_license[pos] * 256 + p_license[pos + 1]);
    pos += 2;

    memcpy(p_license_s->GUID, p_license + pos, 16);
    pos += 16;

    p_license_s->KeyParaLength = p_license[pos] * 256 + p_license[pos + 1];
    pos += 2;

    memcpy(p_license_s->KeyPara, p_license + pos, p_license_s->KeyParaLength);
    pos += p_license_s->KeyParaLength;

    memcpy(p_license_s->ExpireDate, p_license + pos, 8);
    pos += 8;

    p_license_s->IssuedType = p_license[pos];
    pos += 1;

    memcpy(p_license_s->IssuedUser, p_license + pos, 128);
    pos += 128;

    memcpy(p_license_s->IssuedFrom, p_license + pos, 20);
    pos += 20;

    memcpy(p_license_s->IssuedTo, p_license + pos, 20);
    pos += 20;

    p_license_s->SignatureLength = p_license[pos] * 256 + p_license[pos + 1];
    pos += 2;

    memcpy(p_license_s->SignatureData, p_license + pos, p_license_s->SignatureLength);
    pos += p_license_s->SignatureLength;
    
    return 0;
}

#if 0

void main()
{
    MPRLicense_s lic = {0};
    MPRLicense_s lic_out = {0};
    unsigned char license[512];
    int len = 512;

    lic.Version  = 1;
    lic.AsymType = SM2;
    lic.SymType = AES_ECB;
    memcpy(lic.ExpireDate, "20130909", 8);
    memcpy(lic.GUID, "1234567890123456", 16);
    memcpy(lic.IssuedFrom, "12345678901234567890", 20);
    memcpy(lic.IssuedTo, "555555555555555555555555", 20);
    lic.IssuedType = 2;
    memcpy(lic.IssuedUser, "kkkkkkkkkk", 8);
    lic.KeyParaLength = 64;
    memcpy(lic.KeyPara, "sssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss", 64);
    lic.SignatureLength = 64;
    memcpy(lic.SignatureData, "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx", 64);
    
    MPRLicense_Pack(&lic, license, &len);
    MPRLicense_UnPack(license, len, &lic_out);
}

#endif