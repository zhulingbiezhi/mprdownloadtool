﻿@echo off
::警告 请不要任意改动和编辑代码，如有配置问题，请进入到脚本_project_setting.bat中进行设定

echo.[Loading Settings] ......
    call ..\_project_setting.bat
echo. 

echo.[Qrc build] ......
    cd /d %ProClientDir%\resource
	echo.rcc -binary MPRDownloadTool_default.qrc -o MPRDownloadTool.rcc
	rcc -binary MPRDownloadTool_default.qrc -o MPRDownloadTool.rcc

	::echo.rcc -binary MPRDownloadTool_en_EN.qrc -o MPRDownloadTool_en_EN.rcc
	::rcc -binary MPRDownloadTool_en_EN.qrc -o MPRDownloadTool_en_EN.rcc

echo. 

echo.[Copy Files] ......
   	echo.copy *.rcc ..\bin\debug_x86\resource\ui\*.rcc
    	copy *.rcc ..\bin\debug_x86\resource\ui\*.rcc

	echo.copy *.rcc ..\bin\release_x86\resource\ui\*.rcc
   	copy *.rcc ..\bin\release_x86\resource\ui\*.rcc
echo.

echo.[Delete Files] ......
	echo.del *.rcc
    del *.rcc
echo.

if "%Package%" EQU "true" (exit /B 1)
if "%Generate%" EQU "true" (exit  /B 1)
pause