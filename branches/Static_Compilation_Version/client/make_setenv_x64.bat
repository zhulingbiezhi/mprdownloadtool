@echo off
::警告 请不要任意改动和编辑代码，如有配置问题，请进入到脚本_project_setting.bat中进行设定

set local=%~dp0
call "%local%../_project_setting.bat"

SET QT_CREATOR_BIN=%QT_CREATOR_BIN_X64%

cd /d %ProClientDir%
echo.QtVersion:[%QT_Version_X64%]
wmic ENVIRONMENT where "name='QT_PATH' and username='<system>'" set VariableValue="%QT_Version_X64%"
wmic ENVIRONMENT where "name='QT_BIN' and username='<system>'" set VariableValue="%QT_Version_X64%\bin"    

echo.VSVersion:[%VS_Version%]
wmic ENVIRONMENT where "name='VS_PATH' and username='<system>'" set VariableValue="%VS_Version%"
wmic ENVIRONMENT where "name='VS_BIN' and username='<system>'" set VariableValue="%VS_Version%\bin"

call "%QT_Version_X86%\bin\qtenv2.bat"
call "%VS_Version%\vcvarsall.bat"

echo.