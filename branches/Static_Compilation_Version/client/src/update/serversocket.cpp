#include "serversocket.h"
#include <QDebug>
#include <QLocalServer>
#include <QLocalSocket>
#include <QDataStream>
serverSocket::serverSocket(const QString & servername,QObject *parent) :
    QObject(parent)
{
    server = NULL;
    clientConnection = NULL;
    _serverName = servername;
    initServer(_serverName);
}
serverSocket::~serverSocket()
{
    if(clientConnection != NULL)
    {
        clientConnection->disconnectFromServer();
        delete clientConnection;
        clientConnection = NULL;
    }

    if (server != NULL)
    {
        delete server;
        server = NULL;
    }
}

bool serverSocket::initServer(const QString & servername)
{
    if(isServerRun(servername))
    {
        qDebug() << __FUNCTION__<<"isServerRun" ;
        return false;
    }
    server = new QLocalServer(this);
    QLocalServer::removeServer(servername);
    if(server != NULL)
    {
        //server->setSocketOptions(QLocalServer::WorldAccessOption);
        if (!server->listen(servername))
        {
            qDebug() << __FUNCTION__ <<QString("Unable to start the server: %1.").arg(server->errorString());
            return false;
        }

        fortunes << tr("software success")
                 << tr("software error")
                 << tr("firmware success")
                 << tr("firmware error")
                 << tr("connect success");

        connect(server, SIGNAL(newConnection()), this, SLOT(sendFortune()));
    }
    return true;
}
bool serverSocket::isServerRun(const QString & servername)
{
    QLocalSocket ls;
    ls.connectToServer(servername);
    if (ls.waitForConnected(2000))
    {
        ls.disconnectFromServer();
        ls.close();
        return true;
    }
    return false;
}
void serverSocket::clientDisconnectedSlot()
{
    if(clientConnection != NULL)
    {
        clientConnection->deleteLater();
        clientConnection = NULL;
    }
}

void serverSocket::sendFortune()
{
    qDebug() << __FUNCTION__ ;
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_0);
    out << (quint16)0;
    out << fortunes.at(4);
    out.device()->seek(0);
    out << (quint16)(block.size() - sizeof(quint16));

    clientConnection = server->nextPendingConnection();
    connect(clientConnection, SIGNAL(disconnected()),this, SLOT(clientDisconnectedSlot()));

    clientConnection->write(block);
    clientConnection->flush();
}

void serverSocket::send(int site)
{
    qDebug() << __FUNCTION__;
    if(clientConnection != NULL)
    {
        qDebug() << __FUNCTION__<<"1";
        if(clientConnection->state() == QLocalSocket::ConnectedState)
        {
            qDebug() << __FUNCTION__<<fortunes.at(site) ;
            QByteArray block;
            QDataStream out(&block, QIODevice::WriteOnly);
            out.setVersion(QDataStream::Qt_4_0);
            out << (quint16)0;
            out << fortunes.at(site);
            out.device()->seek(0);
            out << (quint16)(block.size() - sizeof(quint16));

            clientConnection->write(block);
            clientConnection->flush();
        }
    }
}
