﻿#ifndef DOWNLOADMANAGER_H
#define DOWNLOADMANAGER_H

#include <QObject>
#include <QNetworkReply>
#include "singleton.h"
#include <QFile>
class QNetworkAccessManager;
class QTimer;


class UpdateTask : public QObject
{
	Q_OBJECT

public:
    /*!
     * \brief 任务类型
     */
    enum TaskType
    {
        TypeSoftwareupdate,
        TypeFirmwareupdate
    };

    /*!
     * \brief 任务信息
     */
    struct TaskInfo
    {
        TaskInfo():totalSize(0), received(0){}
        QString updatepackagePath;
        QString downloadurl;
        QString updatepackageName;
        QString vCode;
        QString desc;
        QString ForceInstall;
        quint64 totalSize;
        quint64 received;
        TaskType type;
    };
public:
    /*!
     * \brief 开始任务
	 */
    void start();
    /*!
     * \brief 获取任务信息
     */
    const TaskInfo& info()const;

    bool setOutTime(int s);
    int  getOutTime();
signals:
    void progressChanged(quint64 received, quint64 total);
    void taskFinish();
    void taskError();

private:
    friend class DownloadManager;
    UpdateTask(DownloadManager *pManager,const QString &downloadUrl,
               const QString &vCode,const QString &desc,const QString & sForceInstall,
               UpdateTask::TaskType type = UpdateTask::TypeSoftwareupdate);

    ~UpdateTask();
    bool checkFileValid();
    void downloadUpdatePackage();
    void endWithError();

    void writeUpdateIni();
    Q_DISABLE_COPY(UpdateTask)
private slots:
    void readyRead();
    void downloadUpdatepackageDone();
    void DownloadTimeout();
private:
    TaskInfo _info;
    QString _eTag;
    QString _lastModify;
    QFile _file;
    QNetworkReply *_downloadpackageReply;
    bool _firstReadPackage;
    DownloadManager *_manager;
    QTimer*  timer;
    int outTime;
};

class DownloadManager : public QObject
{
    Q_OBJECT

public:
    DownloadManager(QObject *parent = NULL,const QString &UpdatepackageDir = "");
    ~DownloadManager();

    UpdateTask *addTask(const QString &downloadUrl,const QString &vCode,
                        const QString &desc,const QString &sForceInstall,
                        UpdateTask::TaskType type = UpdateTask::TypeSoftwareupdate
                        );

    /*!
     * \brief 设置更新包保存的目录为  UpdatepackageDir
     */
    void setUpdatepackageDir(const QString &UpdatepackageDir);

    /*!
     * \brief 返回当前更新包保存的目录
     */
    const QString &getUpdatepackageDir()const;
Q_SIGNALS:
    /*!
     * \brief 在下载更新包任务完成时emit
     */
    void UpdateTaskFinish(const UpdateTask *task);

private:
    void taskFinish(UpdateTask *task);

private:
    friend class UpdateTask;
    QNetworkAccessManager *_networkManager;
    QString _UpdatepackageDir;
    DECLARE_SINGLETON_CLASS(DownloadManager)
};

#define PTR_DOWNLOAD_MANAGER Singleton<DownloadManager>::getInstance()

#endif // DOWNLOADMANAGER_H
