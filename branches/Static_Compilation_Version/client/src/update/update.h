﻿#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include "downloadmanager.h"

class serverSocket;
class TUpdateProcesser : public QObject
{
    Q_OBJECT
    
public:
    TUpdateProcesser(QObject *parent = 0);
    ~TUpdateProcesser();

private:
    DownloadManager *pDownloadManager;
private slots:
    void softwareDownloadFinish();
    void softwareDownloadError();
    void firmwareDownloadFinish();
    void firmwareDownloadError();

public:
    void startUpdate();

    QString sInstallPackageSaveDir;
    QString sDownloadUrl;
    QString sVcode;
    QString sDesc;
    QString sForceInstall;
    QString sUpdateType;

    serverSocket* softwareServer;
    serverSocket* firewareServer;
};

#endif // DIALOG_H
