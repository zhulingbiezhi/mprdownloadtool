﻿#include "downloadmanager.h"
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QFileInfo>
#include <QFile>
#include <QDir>
#include <QDebug>
#include <QSettings>
#include <QTimer>

#define DEFAULT_OUTTIME (60)
void UpdateTask::start()
{
    downloadUpdatePackage();
}

const UpdateTask::TaskInfo& UpdateTask::info()const
{
    return _info;
}

bool UpdateTask::checkFileValid()
{
    //only check the size
    QFileInfo fInfo(_info.updatepackagePath);
	if (fInfo.isFile() && fInfo.size() == _info.received)
		return true;

	return false;
}

void UpdateTask::downloadUpdatePackage()
{
    //TODO fetch ticket first
    qDebug() << __FUNCTION__;
    QNetworkRequest request(_info.downloadurl);
    request.setRawHeader("User-Agent", "Mpr_World_UPDATE 1.0");


    if (checkFileValid())
    {
        request.setRawHeader("Range", QString("bytes=%1-").arg(_info.received).toUtf8());
        if (!_eTag.isEmpty())
            request.setRawHeader("If-Range", _eTag.toUtf8());
        if (!_lastModify.isEmpty())
            request.setRawHeader("Unless-Modified-Since", _lastModify.toUtf8());
    }


    _firstReadPackage = true;

    _downloadpackageReply = _manager->_networkManager->get(request);
    if (_downloadpackageReply == NULL)
    {
        endWithError();
        return;
    }

    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(DownloadTimeout()));
    timer->start(outTime*1000);

    connect(_downloadpackageReply, SIGNAL(finished()), SLOT(downloadUpdatepackageDone()));
    connect(_downloadpackageReply, SIGNAL(readyRead()), SLOT(readyRead()));
}


bool UpdateTask::setOutTime(int s)
{
    if(timer != NULL)
    {
        outTime = s;
        timer->stop();
        timer->start(s*1000);
        return true;
    }
    return false;
}

int UpdateTask::getOutTime()
{
    return outTime;
}

void UpdateTask::endWithError()
{
    if (_downloadpackageReply != NULL)
    {
        _downloadpackageReply->abort();
        _downloadpackageReply->deleteLater();
        _downloadpackageReply = NULL;
    }
    if (_file.isOpen())
        _file.close();
    emit taskError();
}


void UpdateTask::readyRead()
{
    timer->stop();
    if (_firstReadPackage)
    {
        int statusCode = _downloadpackageReply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();

        if (statusCode == 200)
        {
            QDir dir(_manager->_UpdatepackageDir);
            if (dir.exists() || QDir::current().mkpath(_manager->_UpdatepackageDir))
            {
            }
            _file.setFileName(_info.updatepackagePath);
            _file.open(QIODevice::WriteOnly);
        }
        else if (statusCode == 206)
        {
            _file.setFileName(_info.updatepackagePath);
            _file.open(QIODevice::Append);
        }
        else
        {
            endWithError();
            return;
        }
        _eTag = _downloadpackageReply->rawHeader("ETag");
        _lastModify = _downloadpackageReply->rawHeader("Last-Modified");
        _info.totalSize = _downloadpackageReply->header(QNetworkRequest::ContentLengthHeader).toULongLong();
        _firstReadPackage = false;
    }
    _info.received += _file.write(_downloadpackageReply->readAll());
	emit progressChanged(_info.received, _info.totalSize);
    timer->start(outTime*1000);
}


void UpdateTask::downloadUpdatepackageDone()
{
    qDebug() << __FUNCTION__;
    timer->stop();
    Q_ASSERT(_downloadpackageReply == sender());
    int replayError = _downloadpackageReply->error();
    QString strError = _downloadpackageReply->errorString();
    _downloadpackageReply->deleteLater();
    _downloadpackageReply = NULL;
    if (_file.isOpen())
        _file.close();
    if (replayError == QNetworkReply::NoError)
    {
        writeUpdateIni();
        emit taskFinish();
    }
    else
    {
        if (replayError != QNetworkReply::OperationCanceledError)
        {
            qDebug() << __FUNCTION__ << "download error :" << replayError << "  "<<strError;
            endWithError();
        }
    }
}

void UpdateTask::DownloadTimeout()
{
    endWithError();
}

UpdateTask::UpdateTask(DownloadManager *pManager,const QString &downloadUrl,
                       const QString &vCode,const QString &desc,
                        const QString &sForceInstall, UpdateTask::TaskType type)
    :QObject(pManager),_manager(pManager),_downloadpackageReply(NULL)
{
    _info.downloadurl = downloadUrl;

    _info.updatepackageName = downloadUrl.right(downloadUrl.length()-downloadUrl.lastIndexOf("/")-1);
    _info.updatepackagePath = pManager->_UpdatepackageDir+"/"+_info.updatepackageName;
    _info.vCode = vCode;
    _info.desc = desc;
    _info.ForceInstall = sForceInstall;
    _info.type = type;

    timer = NULL;
    outTime = DEFAULT_OUTTIME;
}



UpdateTask::~UpdateTask()
{
    if (_downloadpackageReply)
    {
        _downloadpackageReply->abort();
        _downloadpackageReply->deleteLater();
        _downloadpackageReply = NULL;
    }
    if (_file.isOpen())
        _file.close();
}

void UpdateTask::writeUpdateIni()
{
    QSettings *pUpdateIniWrite = NULL;
    if(_info.type == TypeSoftwareupdate)
    {
        pUpdateIniWrite = new QSettings(_manager->_UpdatepackageDir+"/softwareupdate.ini", QSettings::IniFormat);
        pUpdateIniWrite->setValue("software/name", _info.updatepackageName);
        pUpdateIniWrite->setValue("software/vcode", _info.vCode);
        pUpdateIniWrite->setValue("software/desc", QStringLiteral("%1").arg(_info.desc));
        pUpdateIniWrite->setValue("software/force", _info.ForceInstall);
    }
    else if(_info.type == TypeFirmwareupdate)
    {
        pUpdateIniWrite = new QSettings(_manager->_UpdatepackageDir+"/firmwareupdate.ini", QSettings::IniFormat);
        pUpdateIniWrite->setValue("firmware/name", _info.updatepackageName);
        pUpdateIniWrite->setValue("firmware/vcode", _info.vCode);
        pUpdateIniWrite->setValue("firmware/desc", QStringLiteral("%1").arg(_info.desc));
        pUpdateIniWrite->setValue("firmware/force", _info.ForceInstall);
    }
    delete pUpdateIniWrite;
    pUpdateIniWrite = NULL;
}

/* DownloadManager ********************************/
UpdateTask *DownloadManager::addTask(const QString &downloadUrl,const QString &vCode,
                                     const QString &desc,const QString & sForceInstall,
                                     UpdateTask::TaskType type)
{
    UpdateTask *pTask = new UpdateTask(this, downloadUrl,vCode,desc,sForceInstall,type);
    if (pTask == NULL)
    {
        qDebug()<<__FUNCTION__<<"Application memory failure";
        return NULL;
    }
    pTask->start();
    return pTask;
}

void DownloadManager::setUpdatepackageDir(const QString &UpdatepackageDir)
{
    _UpdatepackageDir = UpdatepackageDir;
}

const QString &DownloadManager::getUpdatepackageDir()const
{
    return _UpdatepackageDir;
}

void DownloadManager::taskFinish(UpdateTask *task)
{
    emit UpdateTaskFinish(task);
}

DownloadManager::DownloadManager(QObject *parent,const QString &UpdatepackageDir)
    :QObject(parent),_UpdatepackageDir(UpdatepackageDir)
{
    _networkManager = new QNetworkAccessManager(this);
}

DownloadManager::~DownloadManager()
{
}

/* DownloadManager end ****************************/
