#ifndef _TAPPCOMMON_H
#define _TAPPCOMMON_H

#define  VERSION "version"
#define  MPRPATH "mprPath"
#define  LICENSEPATH "licensePath"
#define  FILESIZE "fileSize"

class TAppCommon
{
public:
    static TAppCommon& Instance();

    void SetMainWindow(QWidget* pWidget) { mpMainWindow = pWidget; }
    void SetExistVersion(QByteArray byteData) { mRetDataByte = byteData; mbExistNewVersion = true; }
    
    void ChangeSavePath(QString strSavePath);
    QWidget* GetMainWindow() const { return mpMainWindow; }

    const QString& GetSearchUrl() const { return mstrSearchrUrl; }
    const QString& GetCheckFirmwareVersionUrl() const { return mstrCheckFirmwareVersionUrl; }
    const QString& GetCheckSoftwareVersionUrl() const { return mstrCheckSoftwareVersionUrl; }

    const QString& GetDownloadSavePath() const { return mstrSavePath; }
    const QString& GetCurrentSetSavePath() const { return mstrSetPath; }
    const QString& GetCurrrentVersion() const{ return mstrVersionNum; }
    const bool& IsExistNewVersion() const{ return mbExistNewVersion; }
    const QByteArray& GetSoftPromateData() const { return mRetDataByte; }
    QString GetAppDataDir();
    void CopySettingData(QString srcPath, QString destPath);

private:
    TAppCommon();
    ~TAppCommon();

private:
    QWidget*     mpMainWindow;
    QString      mstrSavePath;
    QString      mstrSetPath;
    QString      mstrSearchrUrl;
    QString      mstrCheckFirmwareVersionUrl;
    QString      mstrCheckSoftwareVersionUrl;
    QString      mstrVersionNum;
    bool         mbExistNewVersion;
    QByteArray   mRetDataByte;

};
#endif