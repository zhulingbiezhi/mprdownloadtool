#include "src/TSTDPreCompiledHeader.h"
#include "TCommonHelper.h"

TCommonHelper::TCommonHelper()
{

}

TCommonHelper::~TCommonHelper()
{

}

TCommonHelper& TCommonHelper::Instance()
{
    static TCommonHelper ins;
    return ins;
}

QString TCommonHelper::StitchSizeDec(qint64 size)
{
    const qint64 cKB = 1024;
    const qint64 cMB = 1024 * 1024;
    const qint64 cGB = 1024ll * 1024 * 1024;

    if (size >= cGB) {
        return QString("%1%2").arg(static_cast<double>(size) / cGB, 0, 'f', 2).arg("GB");
    }
    else if (size >= cMB) {
        return QString("%1%2").arg(static_cast<double>(size) / cMB, 0, 'f', 2).arg("MB");
    }
    else if (size >= cKB) {
        return QString("%1%2").arg(static_cast<double>(size) / cKB, 0, 'f', 2).arg("KB");
    }
    else {
        return QString("%1%2").arg(size).arg("B");
    }
}

bool TCommonHelper::RemoveDirectory(QString dirName)
{
    QDir dir(dirName);
    QString tmpdir = "";
    if(!dir.exists()){
        return false;
    }

    QFileInfoList fileInfoList = dir.entryInfoList();
    foreach(QFileInfo fileInfo, fileInfoList){
        if(fileInfo.fileName() == "." || fileInfo.fileName() == "..")
            continue;

        if(fileInfo.isDir()){
            tmpdir = dirName + ("/") + fileInfo.fileName();
            RemoveDirectory(tmpdir);
            dir.rmdir(fileInfo.fileName()); /**< 移除子目录 */
        }
        else if(fileInfo.isFile()){
            QFile tmpFile(fileInfo.fileName());
            dir.remove(tmpFile.fileName()); /**< 删除临时文件 */
        }
    }

    dir.cdUp();            /**< 返回上级目录，因为只有返回上级目录，才可以删除这个目录 */
    if(dir.exists(dirName)){
        if(!dir.rmdir(dirName))
            return false;
    }
    return true;
}