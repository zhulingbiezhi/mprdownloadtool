#include "TProxyStyle.h"

TProxyStyle::TProxyStyle(QStyle* style) 
    : QProxyStyle(style)
{

}

TProxyStyle::~TProxyStyle()
{

}

int TProxyStyle::styleHint(StyleHint hint, const QStyleOption *option, const QWidget *widget , QStyleHintReturn *returnData ) const
{
    return QProxyStyle::styleHint(hint, option, widget, returnData);
}

void TProxyStyle::drawPrimitive( PrimitiveElement pe, const QStyleOption *opt, QPainter *p, const QWidget *w ) const
{
    QProxyStyle::drawPrimitive(pe, opt, p, w);
}
