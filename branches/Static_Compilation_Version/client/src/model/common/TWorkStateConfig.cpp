#include "TWorkStateConfig.h"
#include "TApplication.h"

TWorkStateConfig::TWorkStateConfig(QObject* parant)
: QObject(parant)
, mMutex(QMutex::Recursive)
{
    tApp->GetAppName();
    mpWorkState = new QSettings(/*QSettings::SystemFormat, QSettings::SystemScope, */tApp->GetCompanyName(), tApp->GetAppName(), parant);
}

TWorkStateConfig::~TWorkStateConfig()
{
    delete mpWorkState;
    mpWorkState = 0;
}

TWorkStateConfig* TWorkStateConfig::Instance()
{
    static TWorkStateConfig s_mpWorkStateConfig(0);
    return &s_mpWorkStateConfig;
}


QVariant TWorkStateConfig::GetValue(const QString& key, const QVariant& default_value) const
{
    return mpWorkState->value(key, default_value);
}

void TWorkStateConfig::SetValue(const QString& key, const QVariant& value) const
{
    mpWorkState->setValue(key, value);
}

void TWorkStateConfig::SetFileName(const QString& file_name)
{
    mFileName = QDir::toNativeSeparators(file_name);
}

void TWorkStateConfig::BeginGroup(const QString& group)
{
    mMutex.lock();
    mpWorkState->beginGroup(group);
}

void TWorkStateConfig::BeginFileGroup()
{
    mMutex.lock();
    mpWorkState->beginGroup(mFileName);
}

void TWorkStateConfig::EndGroup()
{
    mpWorkState->endGroup();
    mMutex.unlock();
}

void TWorkStateConfig::CloneKeysToFile(const QString file_name)
{
    BeginFileGroup();
    QStringList keylist = mpWorkState->childKeys();
    QVariantList valuelist;
    for (int i = 0; i < keylist.size(); i++)
    {
        valuelist.append(GetValue(keylist[i]));
    }
    EndGroup();

    BeginGroup(QDir::toNativeSeparators(file_name));
    for (int i = 0; i < keylist.size(); i++)
    {
        SetValue(keylist[i], valuelist[i]);
    }
    EndGroup();
}

