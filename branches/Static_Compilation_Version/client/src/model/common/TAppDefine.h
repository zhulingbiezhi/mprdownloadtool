#ifndef _TAPPDEFINE_H_
#define _TAPPDEFINE_H_

//regedit
static const QString g_Organization = "MPRTimes";
static const QString g_Application  = "MPRDownloadTool";

//config file path
static const QString g_ConfigFilePath = qApp->applicationDirPath() + "/config.ini";;

//dumpfile
static const QString g_DumpFileSaveDir = QStandardPaths::writableLocation(QStandardPaths::DesktopLocation);

#endif //_TAPPDEFINE_H_