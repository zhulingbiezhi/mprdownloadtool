#ifndef __TCOMMONHELPER_H_
#define __TCOMMONHELPER_H_

class TCommonHelper{

public:
    static TCommonHelper& Instance();

    //删除文件夹（清空所有）
    static bool     RemoveDirectory(QString dirName);

    //转化文件大小为描述，如 1024bytes 为 1kb
    static QString  StitchSizeDec(qint64 size);

private:
    TCommonHelper();
    ~TCommonHelper();

};
#endif //__TCOMMONHELPER_H_