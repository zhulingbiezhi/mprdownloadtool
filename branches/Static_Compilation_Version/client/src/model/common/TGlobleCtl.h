#ifndef __TGLOBLECTL_H_
#define __TGLOBLECTL_H_

//实际上的存储和licence有关，每一本

#define PREFIXCODE_LEN  10

//file format struct
const QString gDescXmlFileName = "/discription.xml";
const QString gDescXmlTaskName = "/task.xml";

// const QString gMPROggDirName = "/MPR/OGG";
// const QString gMPRMP3DirName = "/MPR/MP3";

const QString gMPRDirName = "/MPR";
const QString gMPRXDirName = "/MPRX";
//const QString gTempDirName = "/Temp";

// const QString gDataDirName = "/data";
// const QString gLicenceDirName = "/licence";
// const QString gTaskDirName = "/Tasks";
//const QString  gDefualtLocalLibDir = "C:/MPRDOWNLOAD";


//register setting
#define  DirSetting                             ("Dirsetting")
#define  LOCAL_LIB_DIR                          ("locallibdir")
#define  DEVICE_LIB_DIR                         ("devicelibdir")
#define  SYNC_LOCAL_DIFF                        ("SyncLocalLibDiff")
#define  SYNC_DEVICE_DIFF                       ("SyncDeviceLibDiff")
#define  DEFAULT_MPR_DEVICE_DESC_FILE_PATH      ("DefaultMPRDeviceDescFilePath")

enum EDistributionWgt{
    EDWL_LOCALLIB = 0,
    EDWL_STATE,
    EDWL_TASK,
};

#include "TFileEnum.h"
#include "TMPRDeviceDesc.h"

class TGlobleCtl{

public:
    TGlobleCtl();
    ~TGlobleCtl();

    static void             CopyLicenseToDisk( const TMPRFileInfoItem& item, const TMPRDeviceDesc& desc);
    static QString          SwitchFileType(FileType);
};
#endif //__TGLOBLECTL_H_