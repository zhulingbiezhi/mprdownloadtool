#include "src/TSTDPreCompiledHeader.h"
#include "TRegeditManager.h"
#include "TAppDefine.h"

TRegeditManager::TRegeditManager(QObject* parant)
: QObject(parant)
, mMutex(QMutex::Recursive)
{
    mpWorkState = new QSettings(g_Organization, g_Application, parant);
}

TRegeditManager::~TRegeditManager()
{
}

TRegeditManager& TRegeditManager::Instance()
{
    static TRegeditManager sRegisterCtl;
    return sRegisterCtl;
}

QVariant TRegeditManager::GetValue(const QString& key, const QVariant& default_value) const
{
    return mpWorkState->value(key, default_value);
}

void TRegeditManager::SetValue(const QString& key, const QVariant& value) const
{
    mpWorkState->setValue(key, value);
}

void TRegeditManager::SetFileName(const QString& file_name)
{
    mFileName = QDir::toNativeSeparators(file_name);
}

void TRegeditManager::BeginGroup(const QString& group)
{
    mMutex.lock();
    mpWorkState->beginGroup(group);
}

void TRegeditManager::BeginFileGroup()
{
    mMutex.lock();
    mpWorkState->beginGroup(mFileName);
}

void TRegeditManager::EndGroup()
{
    mpWorkState->endGroup();
    mMutex.unlock();
}

void TRegeditManager::ClearData()
{
    if (NULL != mpWorkState)
    {
        delete mpWorkState;
        mpWorkState = NULL;
    }
}
