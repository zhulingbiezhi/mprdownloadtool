#include "TAppCommon.h"
#include "TWorkStateConfig.h"
TAppCommon& TAppCommon::Instance()
{
    static TAppCommon obj;
    return obj;
}

TAppCommon::TAppCommon()
{
    QString strConfigPath = ":/MPRDownloadTool/config.ini";
    QString strConfigLocalPath = GetAppDataDir() + QDir::separator() + QApplication::applicationName() + ".ini";
    strConfigLocalPath = QDir::toNativeSeparators(strConfigLocalPath);

    QSettings *pLocalSetting = nullptr;
    QSettings setting(strConfigPath, QSettings::IniFormat);

    if (!QFile::exists(strConfigLocalPath))
    {
        //配置文件复制
        CopySettingData(strConfigPath, strConfigLocalPath);
    }
    else
    {
        //校验配置文件版本
        pLocalSetting = new QSettings(strConfigLocalPath, QSettings::IniFormat);
        int nLocalVersion = pLocalSetting->value("version").toInt();
        int nCurVersion = setting.value("version").toInt();
        if (nLocalVersion != nCurVersion)
        {
            ///配置文件复制
            if (pLocalSetting) {
                delete pLocalSetting;
                pLocalSetting = nullptr;
            }
            CopySettingData(strConfigPath, strConfigLocalPath);
        }
    }
    if (pLocalSetting == nullptr) {
        pLocalSetting = new QSettings(strConfigLocalPath, QSettings::IniFormat);
    }
    pLocalSetting->setIniCodec(QTextCodec::codecForLocale());

    QString strUpgradeServer = pLocalSetting->value("url/upgrade_url").toString();
    QString strSearchServer = pLocalSetting->value("url/search_url").toString();
    //QString strWorldServer = setting.value("url/worldserver").toString();
    //QString strWorldServer = "http://nemnp.isli.fm:8080";


    //mstrSavePath = setting.value("save_path").toString();
    TWorkStateConfig::Instance()->BeginGroup("Common");
    mstrSavePath = TWorkStateConfig::Instance()->GetValue("save_path", "C:/MPRDownloadTool").toString();
    TWorkStateConfig::Instance()->EndGroup();

    mstrSetPath = mstrSavePath;
    mstrCheckFirmwareVersionUrl = strUpgradeServer + "/web/ports/mvc/port/firmware";
    mstrCheckSoftwareVersionUrl = strUpgradeServer + "/web/ports/mvc/port/clientversion";
    mstrSearchrUrl = strSearchServer + "/web/ports/mvc/port/doSearchByClient";
    mbExistNewVersion = false;
    delete pLocalSetting;
#include "version.h"
    mstrVersionNum = CurrentVersion;
}

TAppCommon::~TAppCommon()
{

}

void TAppCommon::ChangeSavePath(QString strSavePath)
{
    if (mstrSavePath != strSavePath)
    {
        /*QDir dir;
        dir.setCurrent(QApplication::applicationDirPath());
        QString strAppPath = dir.absolutePath();
        QString strConfPath = dir.absoluteFilePath("config.ini");*/
        //QString strConfPath = ":/MPRDownloadTool/config.ini";
        //QSettings setting(strConfPath, QSettings::IniFormat);
        //setting.setIniCodec(QTextCodec::codecForLocale());
        //setting.setValue("save_path", strSavePath);
        TWorkStateConfig::Instance()->BeginGroup("Common");
        TWorkStateConfig::Instance()->SetValue("save_path", strSavePath);
        TWorkStateConfig::Instance()->EndGroup();
        mstrSetPath = strSavePath;
    }
}
QString TAppCommon::GetAppDataDir()
{
    QString strAppDir;
    char *pathvar;
    pathvar = getenv("APPDATA");
    QString strDataPath = QString(QLatin1String(pathvar));
    if (strDataPath.isEmpty())
    {
        QDir dir;
        dir.setCurrent(QApplication::applicationDirPath());
        strAppDir = dir.absolutePath();
    }
    else
    {
        strAppDir = strDataPath + QDir::separator() + QApplication::applicationName().replace("_", " ");
    }

    QDir dir(strAppDir);
    if (!dir.exists())
    {
        dir.mkpath(strAppDir);
    }
    return strAppDir;
}

void TAppCommon::CopySettingData(QString srcPath, QString destPath)
{
    QFile destFile(destPath);
    QFile srcFile(srcPath);
    qDebug() << __FUNCTION__ << destFile.open(QIODevice::ReadWrite);
    qDebug() << __FUNCTION__ << srcFile.open(QIODevice::ReadOnly);
    destFile.write(srcFile.readAll());
    destFile.close();
    srcFile.close();
}