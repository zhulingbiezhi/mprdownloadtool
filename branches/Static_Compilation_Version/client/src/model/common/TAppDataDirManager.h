#ifndef TAPPDATADIRMANAGER_H
#define TAPPDATADIRMANAGER_H


class TAppDataDirManager
{
 private:
    TAppDataDirManager();
    ~TAppDataDirManager();

    QString MakeAppDataDir();

public:
    static  TAppDataDirManager& Instance();
    QString GetAppDataDir();

private:
    QString     mstrAppDir;

};

#endif