#include "src/TSTDPreCompiledHeader.h"
#include "TAppDataDirManager.h"
#include "TAppDefine.h"

TAppDataDirManager::TAppDataDirManager()
{
    mstrAppDir = MakeAppDataDir();  
}

TAppDataDirManager::~TAppDataDirManager()
{

}

TAppDataDirManager& TAppDataDirManager::Instance()
{
    static TAppDataDirManager ins;
    return ins;
}

QString TAppDataDirManager::GetAppDataDir()
{
    return mstrAppDir;
}

QString TAppDataDirManager::MakeAppDataDir()
{
    QString appDataDir;

    size_t requiredSize = 0;
    getenv_s(&requiredSize, NULL, 0, "APPDATA");
    if (requiredSize != 0)
    {
        char *pathvar = (char*) malloc(requiredSize * sizeof(char));
        if (pathvar)
        {
            getenv_s(&requiredSize, pathvar, requiredSize, "APPDATA" );
            appDataDir = QString(QLatin1String(pathvar)) + "/" + g_Application;
            free(pathvar);
            QDir dir(appDataDir);
            if (dir.exists() || dir.mkpath(appDataDir))
            {
                return appDataDir;
            }
        }
    }
    return QDir::tempPath();
}
