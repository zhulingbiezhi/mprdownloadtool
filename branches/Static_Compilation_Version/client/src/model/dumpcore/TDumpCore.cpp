#include "TDumpCore.h"
#include "QMessageBox.h"
#include "TAppDefine.h"

TDumpCore::TDumpCore()
{
}

TDumpCore::~TDumpCore()
{

}

TDumpCore& TDumpCore::Instance()
{
    static TDumpCore zm;
    return zm;
}

// 作为except块中表达式的函数
LONG TDumpCore :: CrashHandler(EXCEPTION_POINTERS *pException)
{	
    // 在这里添加处理程序崩溃情况的代码

    // 创建Dump文件
    wchar_t fileName[128] = {0};
    CreateDumpFilePath().toWCharArray(fileName);    
    ShowMessage();
    CreateDumpFile(fileName, pException);

    return EXCEPTION_EXECUTE_HANDLER;
}

QString TDumpCore :: CreateDumpFilePath()
{ 
    QString strDumpFileDir = g_DumpFileSaveDir;
    strDumpFileDir = strDumpFileDir.replace("/","\\");
    m_qstrLocalName = "MPRDownloadTool-Dump-";
    m_qstrLocalName = m_qstrLocalName+"-TimeError"+ ".dmp";
    QString qstrFile = strDumpFileDir + "\\" + m_qstrLocalName ;    
    return qstrFile;
}

void TDumpCore :: ShowMessage()
{
    static int count = 1;

    if (!m_qstrLocalName.isNull() && count <= 3)
    {
        count++;
        QString strMsg = QObject::tr("Program an exception occurs, exception information has been saved to the desktop, FileName:") + m_qstrLocalName;
        QMessageBox::information(0, QObject::tr("Progress Error"), strMsg);
    }
}

// 创建Dump文件
void TDumpCore :: CreateDumpFile(LPCWSTR lpstrDumpFilePathName, EXCEPTION_POINTERS *pException)
{
    // 创建Dump文件
    //
    HANDLE hDumpFile = CreateFile(lpstrDumpFilePathName, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);

    // Dump信息
    //
    MINIDUMP_EXCEPTION_INFORMATION dumpInfo;
    dumpInfo.ExceptionPointers = pException;
    dumpInfo.ThreadId = GetCurrentThreadId();
    dumpInfo.ClientPointers = TRUE;

    // 写入Dump文件内容
    MiniDumpWriteDump(GetCurrentProcess(), GetCurrentProcessId(), hDumpFile, MiniDumpNormal, &dumpInfo, NULL, NULL);

    CloseHandle(hDumpFile);
}
