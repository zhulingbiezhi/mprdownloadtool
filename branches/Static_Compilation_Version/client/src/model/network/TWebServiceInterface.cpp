﻿#include "TWebServiceInterface.h"
#include <QNetworkCookie>
#include <QJsonArray>
#include <QSslConfiguration>
#include "TAppCommon.h"
#include "TPopupDialog.h"



#if _MSC_VER >= 1600
#pragma execution_character_set("utf-8")
#endif

static const char *EmnpCodeMap[] =
{
    "160000",
    "160001",
    "160002",
    "160003",
    "160004",
    "160005",
    "160006",
    "160007",
    "160008",
    "160009",
    "160010",
    "160011",
    "160012",
    "160013",
    "160014",
    "160015",
    "160027",
    "160028",
    "169998",
    "169999",
    NULL
};

static void appendArg(QString &out, const QString &name, const QString &value)
{
    if (value.isEmpty() || name.isEmpty())
        return;
    if (out.isEmpty())
        out = name + "=" + value;
    else
        out += "&" + name + "=" + value;
}

static void fillProInfo(QJsonObject &json, Mpr::MprProductInformation &proInfo)
{
    proInfo.productImgUrl = json.value("productImgUrl").toString();
    proInfo.productType = json.value("productType").toString().toInt();
    proInfo.publisher = json.value("publisher").toString();
    proInfo.author = json.value("author").toString();
    proInfo.timeBuying = QDateTime::fromString(json.value("timeBuying").toString(),
                                               "yyyy-MM-dd HH:mm:ss");
    if (!proInfo.timeBuying.isValid())
        proInfo.timeBuying = QDateTime::currentDateTime();
    proInfo.size = json.value("size").toVariant().toULongLong();
    proInfo.goodsId = json.value("goodsId").toString();
    proInfo.goodsName = json.value("goodsName").toString();
    proInfo.mprPrefixCode = json.value("mprPrefixCode").toString();
    proInfo.isMpr = json.value("isMPR").toString().toInt();
    proInfo.uuid = json.value("uuid").toString();
    proInfo.downloadId = json.value("downloadId").toString();
    proInfo.language = json.value("language").toString();
    proInfo.languageId = json.value("languageId").toString();
    proInfo.publishing = json.value("publishing").toString();
    proInfo.fileFormat = json.value("fileFormat").toString();
    proInfo.isDiy = json.value("isDiy").toBool();
    proInfo.versionCode = json.value("versionCode").toString();
    proInfo.producer = json.value("producer").toString();
    proInfo.brandId = json.value("brandId").toString();
    proInfo.publicationName = json.value("publicationName").toString();
}

QByteArray TUploadInfoRequest::toJson()const
{
    QString string = QString("userName=%1&deviceId=%2&softwareType=%3&hostSystem=%4"
                             "&clientVersion=%5&hostIP=%6&time=%7&terminalType=%8").arg(userName, deviceId, softwareType,
                                       hostSystem, clientVersion, hostIP, time, terminalType);
    qDebug() << __FUNCTION__ << string;
    return string.toUtf8();
}

bool TUploadInfoResponse::fromJson(const QByteArray &data)
{
    qDebug() <<__FUNCTION__ << QString("").append(data);
    QJsonParseError parseError;
    QJsonDocument doc = QJsonDocument::fromJson(data, &parseError);
    if (parseError.error != QJsonParseError::NoError)
    {
        qDebug() << __FUNCTION__ << "parse json error:" << parseError.error << parseError.errorString();
        return false;
    }

    if (doc.isObject())
    {
        QJsonObject object = doc.object();
        success = object.value("success").toString() == "Y" ? true : false;
        msgCode = object.value("msgCode").toString();
        message = object.value("message").toString();
        return true;
    }

    return false;
}

QByteArray TFetchOperatorRequest::toJson()const
{
    QString string = QString("mprCode=%1&sysId=%2&ids=%3&code=%4&flag=%5&versionCode=%6").arg(mprCode, sysId, ids, code, flag, versionCode);
    return string.toUtf8();
}

bool TFetchOperatorResponse::fromJson(const QByteArray &data)
{
    qDebug() <<__FUNCTION__ << QString("").append(data) << "#########";
    QJsonParseError parseError;
    QJsonDocument doc = QJsonDocument::fromJson(data,&parseError);
    if(parseError.error != QJsonParseError::NoError)
    {
        qDebug()<<__FUNCTION__<<parseError.errorString();
        return false;
    }

    if (doc.isObject())
    {
        QJsonObject object = doc.object();
        QJsonValue jsonValue = object.value("replist");
        if (jsonValue == QJsonValue::Undefined)
        {
            repcontent = object.value("repcontent").toString();
            mprCode += object.value("mprcode").toString() + ",";
            isNew = object.value("isNew").toBool();
            url = object.value("url").toString();
        }
        else
        {
            QJsonArray array = object.value("replist").toArray();
            foreach(QJsonValue item, array)
            {
                if (item.toObject().value("returnMsg").isObject())
                {
                    object = item.toObject().value("returnMsg").toObject();
                    QString repcode = object.value("repcode").toString();
                    if (repcode.compare("140000") == 0)
                    {
                        repcontent = object.value("repcontent").toString();
                        mprCode += object.value("mprcode").toString() + ",";
                        isNew = object.value("isNew").toBool();
                        url = object.value("url").toString();
                    }
                }
            }
        }         
                      
   }
  
    if (!mprCode.isEmpty())
        mprCode = mprCode.left(mprCode.size()-1);
    qDebug() << __FUNCTION__ << mprCode;
    return true;
}

QByteArray TFetchChallengePackageRequest::toJson()const
{
    QString string = QString("platformId=%1&deviceId=%2").arg(platformId, deviceId);
    qDebug() << __FUNCTION__ << string;
    return string.toUtf8();
}

bool TFetchChallengePackageResponse::fromJson(const QByteArray &data)
{
    qDebug() <<__FUNCTION__ << QString("").append(data) << "#########";
    QJsonParseError parseError;
    QJsonDocument doc = QJsonDocument::fromJson(data, &parseError);
    if (parseError.error != QJsonParseError::NoError)
    {
        qDebug() << __FUNCTION__ << "parse json error:" << parseError.error << parseError.errorString();
        return false;
    }

    if (doc.isObject())
    {
        QJsonObject object = doc.object();
        success = object.value("success").toString() == "Y" ? true : false;
        msgCode = object.value("msgCode").toString();
        challengePackage = object.value("challengePackage").toString();
        message = object.value("message").toString();
        return true;
    }

    return false;
}

QByteArray TCheckDeviceLegitimacyRequest::toJson()const
{
    QString string = QString("deviceId=%1&challengePackage=%2&responseChallengePackage=%3")
            .arg(deviceId).arg(challengePackage).arg(responseChallengePackage);
    qDebug() << __FUNCTION__ <<string;
    return string.toUtf8();
}

bool TCheckDeviceLegitimacyResponse::fromJson(const QByteArray &data)
{
    qDebug() << __FUNCTION__ <<data.data();
    QJsonParseError parseError;
    QJsonDocument doc = QJsonDocument::fromJson(data, &parseError);
    if (parseError.error != QJsonParseError::NoError)
    {
        qDebug() << __FUNCTION__ << "parse json error:" << parseError.error << parseError.errorString();
        return false;
    }

    if (doc.isObject())
    {
        QJsonObject object = doc.object();
        success = object.value("success").toString() == "Y" ? true : false;
        msgCode = object.value("msgCode").toString();
        bundUserName = object.value("bundUserName").toString();
        brandName = object.value("brandName").toString();
        message = object.value("message").toString();
        return true;
    }

    return false;
}

QByteArray TAuthenticateRequest::toJson()const
{
    QString string = QString("deviceId=%1&challengePackage=%2&responseChallengePackage=%3")
                     .arg(deviceId).arg(challengePackage).arg(responseChallengePackage);
    return string.toUtf8();
}

bool TAuthenticateResponse::fromJson(const QByteArray &data)
{
    QJsonParseError parseError;
    QJsonDocument doc = QJsonDocument::fromJson(data, &parseError);
    if (parseError.error != QJsonParseError::NoError)
    {
        qDebug() << __FUNCTION__ << "parse json error:" << parseError.error << parseError.errorString();
        return false;
    }

    if (doc.isObject())
    {
        QJsonObject object = doc.object();
        success = object.value("success").toString() == "Y" ? true : false;
        msgCode = object.value("msgCode").toString();
        bundUserName = object.value("bundUserName").toString();
        message = object.value("message").toString();
        return true;
    }
    return false;
}

QByteArray TLoginRequest::toJson()const
{
    QString string = QString("userName=%1&password=%2").arg(username).arg(password);
    string += QString("&out_trade_no=%1").arg(outtradeno);
    if (!deviceId.isEmpty())
        string += QString("&terminalId=%1&brandId=%2&model=%3").arg(deviceId)
                          .arg(brandId).arg(deviceModel);

    qDebug() << __FUNCTION__ << "*************" << string.toUtf8();
    return string.toUtf8();
}

bool TLoginResponse::fromJson(const QByteArray &data)
{
    qDebug() <<__FUNCTION__ << QString(data) << "#########";
    QJsonParseError parseError;
    QJsonDocument doc = QJsonDocument::fromJson(data, &parseError);
    if (parseError.error != QJsonParseError::NoError)
    {
        qDebug() << __FUNCTION__ << "parse json error:" << parseError.error << parseError.errorString();
        return false;
    }

    if (doc.isObject())
    {
        QJsonObject object = doc.object();
        //success = object.value("success").toBool();
        success = object.value("success").toString() == "Y" ? true : false;
        msgCode = object.value("msgCode").toString();
        accountId = object.value("accountId").toString();
        message = object.value("message").toString();
        return true;
    }

    return false;
}

QByteArray TLoginOutRequest::toJson()const
{
    QString string = QString("accountId=%1").arg(userName);
    qDebug() << __FUNCTION__ << string;
    return string.toUtf8();
}

bool TLoginOutResponse::fromJson(const QByteArray &data)
{
    qDebug() <<__FUNCTION__ << QString(data) << "#########";
    QJsonParseError parseError;
    QJsonDocument doc = QJsonDocument::fromJson(data, &parseError);
    if (parseError.error != QJsonParseError::NoError)
    {
        qDebug() << __FUNCTION__ << "parse json error:" << parseError.error << parseError.errorString();
        return false;
    }

    if (doc.isObject())
    {
        QJsonObject object = doc.object();
        success = object.value("success").toString() == "Y" ? true : false;
        msgCode = object.value("msgCode").toString();
        message = object.value("message").toString();
        return true;
    }

    return false;
}

QByteArray TProductInfoRequest::toJson()const
{
    QString body;
    appendArg(body, "userId", userName);
    appendArg(body, "deviceId", deviceId);
    appendArg(body, "pageSize", QString("%1").arg(pageSize));
    appendArg(body, "pageNo", QString("%1").arg(pageNo));
	qDebug() << __FUNCTION__ << body;
    return body.toUtf8();
}

bool TProductInfoResponse::fromJson(const QByteArray &data)
{
    qDebug() << __FUNCTION__ << data.data();
    QJsonParseError parseError;
    QJsonDocument doc = QJsonDocument::fromJson(data,&parseError);
    if(parseError.error != QJsonParseError::NoError)
    {
        qDebug()<<__FUNCTION__<<parseError.errorString();
        return false;
    }

    QJsonObject jsonObj=doc.object();
    success = jsonObj.value("success").toString() == "Y" ? true : false;
    msgCode = jsonObj.value("msgCode").toString();
    message = jsonObj.value("message").toString();

    if (!success)
        return false;

    QJsonArray proList = jsonObj.value("productList").toArray();
    for (QJsonArray::const_iterator it = proList.begin(); it != proList.end(); ++it)
    {
        QJsonObject proObj = (*it).toObject();
        Mpr::MprProductInformation info;
        fillProInfo(proObj, info);
        pInfo.push_back(info);
    }
    return true;
}

QByteArray TFetchBrandNameRequest::toJson()const
{
    QString string = QString("brandId=%1").arg(brandId);
    return string.toUtf8();
}

bool TFetchBrandNameResponse::fromJson(const QByteArray &data)
{
    qDebug() <<__FUNCTION__ <<QString("").append(data);
    QJsonParseError parseError;
    QJsonDocument doc = QJsonDocument::fromJson(data, &parseError);
    if (parseError.error != QJsonParseError::NoError)
    {
        qDebug() << __FUNCTION__ << "parse json error:" << parseError.error << parseError.errorString();
        return false;
    }

    if (doc.isObject())
    {
        QJsonObject object = doc.object();
        success = object.value("success").toString() == "Y" ? true : false;
        msgCode = object.value("msgCode").toString();
        brandName = object.value("brandName").toString();
        message = object.value("message").toString();
        return true;
    }

    return false;
}

bool TFetchStoreResponse::fromJson(const QByteArray &data)
{
    //这个日志太长了用外部调试工具看日志会很卡，先简化一点
    qDebug() << __FUNCTION__ << "size =" << data.size() << "(" << data.mid(0, 200).data() << "...)";
    QJsonParseError parseError;
    QJsonDocument doc = QJsonDocument::fromJson(data, &parseError);
    if (parseError.error != QJsonParseError::NoError)
    {
        qDebug() << __FUNCTION__ << "parse json error:" << parseError.error << parseError.errorString();
        return false;
    }

    QJsonObject jsonObj=doc.object();
    QVariantMap result = jsonObj.toVariantMap();
    success = result["success"].toBool();
    msgCode = result["msgCode"].toString();
    message = result["message"].toString();
    if (!success)
        return false;

    QVariantList storelist = result["classifyList"].toList();
    RecursiveAnalysis(storelist, store);
    return true;
}

void TFetchStoreResponse::RecursiveAnalysis(QVariantList list, QList<AllStoreUrl>& store)
{
    for (QVariantList::const_iterator it = list.constBegin(); it != list.constEnd(); ++it)
    {
        QVariantMap storeMap = it->toMap();
        AllStoreUrl storeUrl;
        storeUrl.classifyId = storeMap["classifyId"].toString();
        storeUrl.classifyName = storeMap["classifyName"].toString();
        storeUrl.url = storeMap["url"].toString();
        QVariantList childList = storeMap["classifyList"].toList();

        QList<AllStoreUrl> urlList;
        if (childList.size() != 0)
            RecursiveAnalysis(childList, urlList);
        storeUrl.storeUrlList = urlList;
        store.append(storeUrl);
    }
}

QByteArray TFetchTicketRequest::toJson()const
{
    QString body;
    appendArg(body, "uuid", uuid);
    appendArg(body, "goodsId", goodsId);
    appendArg(body, "accountId", accountId);
    appendArg(body, "deviceId", deviceId);
    return body.toUtf8();
}

bool TFetchTicketResponse::fromJson(const QByteArray &data)
{
    qDebug() << __FUNCTION__ << data;
    QJsonParseError parseError;
    QJsonDocument doc = QJsonDocument::fromJson(data, &parseError);
    if (parseError.error != QJsonParseError::NoError)
    {
        qDebug() << __FUNCTION__ << "parse json error:" << parseError.error << parseError.errorString();
        return false;
    }

    if (doc.isObject())
    {
        QJsonObject object = doc.object();
        ticket = object.value("ticket").toVariant().toByteArray();
        interfaceUrl = object.value("interfaceUrl").toString();
        message = object.value("message").toString();
        msgCode = object.value("msgCode").toString();
        success = object.value("success").toString() == "Y" ? true : false;
        QJsonObject proInfoObject = object.value("product").toObject();
        fillProInfo(proInfoObject, proInfo);
    }
    else
    {
        return false;
    }

    return true;
}

QByteArray TFetchSoftLatestVersionRequest::toJson()const
{
    QString body;
    appendArg(body, "curVCode", curVCode);
    appendArg(body, "terminalType", terminalType);
    appendArg(body, "softwareType", softwareType);
    return body.toUtf8();
}

QByteArray TFetchFirmLatestVersionRequest::toJson()const
{
    QString body;
    appendArg(body, "firmwareVersion", firmwareVersion);
    appendArg(body, "deviceModel", deviceModel);
    appendArg(body, "deviceType", deviceType);
    return body.toUtf8();
}

bool TFetchLatestVersionResponse::fromJson(const QByteArray &data)
{
    sourceData = data;
    qDebug() <<__FUNCTION__ << QString(data) << "#########";
    QJsonParseError parseError;
    QJsonDocument doc = QJsonDocument::fromJson(data, &parseError);
    if (parseError.error != QJsonParseError::NoError)
    {
        qDebug() << __FUNCTION__ << "parse json error:" << parseError.error << parseError.errorString();
        return false;
    }

    if (doc.isObject())
    {
        QJsonObject object = doc.object();
        rcode = object.value("rcode").toString();
        vCode = object.value("vCode").toString();
        url = object.value("url").toString();
        force = object.value("force").toString();
        desc = object.value("desc").toString();
        fileName = object.value("fileName").toString();
        return true;
    }

    return false;
}

QByteArray TFetchLicenseRequest::toJson()const
{
    QString body;
    appendArg(body, "uuid", uuid);
    appendArg(body, "mediaType", mediaType);
    appendArg(body, "accountId", accountId);
    appendArg(body, "deviceId", deviceId);
    return body.toUtf8();
}

bool TFetchLicenseResponse::fromJson(const QByteArray &data)
{
    qDebug() << __FUNCTION__ << data;
    QJsonParseError parseError;
    QJsonDocument doc = QJsonDocument::fromJson(data, &parseError);
    if (parseError.error != QJsonParseError::NoError)
    {
        qDebug() << __FUNCTION__ << "parse json error:" << parseError.error << parseError.errorString();
        return false;
    }

    if (doc.isObject())
    {
        QJsonObject object = doc.object();
        msgCode = object.value("msgCode").toString();
        ticket = object.value("ticket").toVariant().toByteArray();
        interfaceUrl = object.value("interfaceUrl").toString();
        message = object.value("message").toString();
        msgCode = object.value("msgCode").toString();
        success = object.value("success").toString() == "Y" ? true : false;
        QJsonObject proInfoObject = object.value("product").toObject();
        fillProInfo(proInfoObject, product);
    }
    else
    {
        return false;
    }

    return true;
}

QByteArray TValidSessionRequest::toJson()const
{
    QString body;
    appendArg(body, "userName", userName);
    return body.toUtf8();
}

bool TValidSessionResponse::fromJson(const QByteArray &data)
{
    qDebug() << __FUNCTION__ << data;
    QJsonParseError parseError;
    QJsonDocument doc = QJsonDocument::fromJson(data, &parseError);
    if (parseError.error != QJsonParseError::NoError)
    {
        qDebug() << __FUNCTION__ << "parse json error:" << parseError.error << parseError.errorString();
        return false;
    }

    if (doc.isObject())
    {
        QJsonObject object = doc.object();
        success = object.value("success").toString() == "Y" ? true : false;
    }
    else
    {
        return false;
    }

    return true;
}

bool TOpenBrowserResponse::fromJson(const QByteArray &data)
{
    qDebug() << __FUNCTION__ << data;
    QJsonParseError parseError;
    QJsonDocument doc = QJsonDocument::fromJson(data, &parseError);
    if (parseError.error != QJsonParseError::NoError)
    {
        qDebug() << __FUNCTION__ << "parse json error:" << parseError.error << parseError.errorString();
        return false;
    }

    if (doc.isObject())
    {
        QJsonObject object = doc.object();
        directUrl = object.value("directUrl").toString(); }
    else
    {
        return false;
    }

    return true;
}

TWebServiceInterface::TWebServiceInterface(QObject *parent)
    : QObject(parent), netManager(new QNetworkAccessManager(this))
    , mbTimeOut(true)
    , mVersionCode("")
    , mpMainWindow(NULL)
{   
    //need to modify
    //QNetworkCookieJar *cookieJar = TCommonApp::getInstance()->getCookieJar();
    //if (cookieJar != NULL)
    //{
    //    netManager->setCookieJar(cookieJar);
    //    cookieJar->setParent(NULL);
    //}
    qRegisterMetaType<TFetchTicketRequest>("TFetchTicketRequest");
    qRegisterMetaType<TFetchTicketResponse>("TFetchTicketResponse");
    qRegisterMetaType<TWebServiceInterface::ResponseCode>("TWebServiceInterface::ResponseCode");
    
    mpServerTimeOut = new QTimer(this);
    connect(mpServerTimeOut, &QTimer::timeout, this, &TWebServiceInterface::OnTimeOut);
}

TWebServiceInterface::~TWebServiceInterface()
{
    qDebug() << __FUNCTION__;
}

int TWebServiceInterface::getErrorCode(const QString &code)
{
    const char **p = EmnpCodeMap;
    while (*p != NULL)
    {
        if (*p == code)
            return p - EmnpCodeMap;
        else
             ++p;
    }
    return Mpr::EmnpECd_169999;
}

QString TWebServiceInterface::getErrorStr(int code)
{
    if (code < 0 || code > Mpr::EmnpECd_169999)
    {
        code = Mpr::EmnpECd_169999;
    }      

    QString mEmnpCodeStr;
    switch (code)
    {
    case Mpr::EmnpECd_160000:
        mEmnpCodeStr = tr("Success");
        break;
    case Mpr::EmnpECd_160001:
        mEmnpCodeStr = tr("Parameter error");
        break;
    case Mpr::EmnpECd_160002:
        mEmnpCodeStr = tr("Equipment loss");
        break;
    case Mpr::EmnpECd_160003:
        mEmnpCodeStr = tr("Not identify user");
        break;
    case Mpr::EmnpECd_160004:
        mEmnpCodeStr = tr("Failed to get ticket");
        break;
    case Mpr::EmnpECd_160005:
        mEmnpCodeStr = tr("Please connect equipment");
        break;
    case Mpr::EmnpECd_160006:
        mEmnpCodeStr = tr("Resource was not found");
        break;
    case Mpr::EmnpECd_160007:
        mEmnpCodeStr = tr("Account inactive");
        break;
    case Mpr::EmnpECd_160008:
        mEmnpCodeStr = tr("Account exception");
        break;
    case Mpr::EmnpECd_160009:
        mEmnpCodeStr = tr("Account does not exist");
        break;
    case Mpr::EmnpECd_160010:
        mEmnpCodeStr = tr("Account or password error");
        break;
    case Mpr::EmnpECd_160011:
        mEmnpCodeStr = tr("Equipment account binding relationship does not exist");
        break;
    case Mpr::EmnpECd_160012:
        mEmnpCodeStr = tr("Equipment abnormal binding");
        break;
    case Mpr::EmnpECd_160013:
        mEmnpCodeStr = tr("Equipment has bound");
        break;
    case Mpr::EmnpECd_160014:
        mEmnpCodeStr = tr("Equipment wasn't register");
        break;
    case Mpr::EmnpECd_160015:
        mEmnpCodeStr = tr("Inconsistent binding account login account and equipment");
        break;
    case Mpr::EmnpEcd_160027:
        mEmnpCodeStr = tr("The device did not buy the goods");
        break;
    case Mpr::EmnpEcd_160028:
        mEmnpCodeStr = tr("No purchase record fee content");
        break;
    case Mpr::EmnpECd_169998:
        mEmnpCodeStr = tr("Equipment validation failure");
        break;
    case Mpr::EmnpECd_169999:
        mEmnpCodeStr = tr("A system exception");
        break;
    default:
        mEmnpCodeStr = "";
        break;
    }

    return mEmnpCodeStr;    
}

QString TWebServiceInterface::getErrorStr(const QString &code)
{
    return getErrorStr(getErrorCode(code));
}

void TWebServiceInterface::setSebAddress(const QString &host, int port)
{
    sebHost = host;
    sebPort = port;
}

void TWebServiceInterface::fetchChallengePackage(QObject *requester, const TFetchChallengePackageRequest  & reqData)
{
    Q_UNUSED(requester);
    Q_UNUSED(reqData);
    /*qDebug() << __FUNCTION__ << TCommonApp::getInstance()->getChallengeUrl();
    call(requester, reqData, TCommonApp::getInstance()->getChallengeUrl(), SLOT(fetchChallengePackageFinish()));*/
}

void TWebServiceInterface::fetchChallengePackageFinish()
{   
    QObject *requester;
    TFetchChallengePackageResponse response;
    ResponseCode code = callFinish(response, requester);
    emit fetchChallengePackageDone(requester, response, code);
}

void TWebServiceInterface::authenticate(QObject *requester, const TAuthenticateRequest &reqData)
{
    call(requester, reqData, "xxxxx.action", SLOT(authenticateFinish()));
}

void TWebServiceInterface::authenticateFinish()
{   
    QObject *requester;
    TAuthenticateResponse response;
    if (callFinish(response, requester) == ResSuccess)
    {
        emit authenticateDone(requester, response);
    }
}


void TWebServiceInterface::login(QObject *requester, const TLoginRequest &reqData)
{
    Q_UNUSED(requester);
    Q_UNUSED(reqData);
    //call(requester, reqData, TCommonApp::getInstance()->getLoginButtonUrl(), SLOT(loginFinish()));
}

void TWebServiceInterface::loginFinish()
{
    QObject *requester;
    TLoginResponse response;
    if (loginCallFinish(response, requester))
    {
        emit loginDone(requester, response);
    }
}

void TWebServiceInterface::loginOut(QObject *requester, const TLoginOutRequest &reqData)
{
    Q_UNUSED(requester);
    Q_UNUSED(reqData);
    //call(requester, reqData, TCommonApp::getInstance()->getLoginOutUrl(), SLOT(loginOutFinish()));
}

void TWebServiceInterface::loginOutFinish()
{
    QObject *requester;
    TLoginOutResponse response;
    if (callFinish(response, requester) == ResSuccess)
    {
        emit loginOutDone(requester, response);
    }
}

void TWebServiceInterface::fetchProductInfo(QObject *requester, const TProductInfoRequest &reqData)
{
    Q_UNUSED(requester);
    Q_UNUSED(reqData);
    //call(requester, reqData, TCommonApp::getInstance()->getPurchaseRecordUrl(), SLOT(OnFetchProductInfoDone()));
}

void TWebServiceInterface::OnFetchProductInfoDone()
{
    QObject *requester;
    TProductInfoResponse response;
    if (callFinish(response, requester) == ResSuccess)
    {
        emit fetchProductInfoDone(requester, response);
    }
}

void TWebServiceInterface::fetchBrandName(QObject *requester, const TFetchBrandNameRequest &reqData)
{
    Q_UNUSED(requester);
    Q_UNUSED(reqData);
    //call(requester, reqData, TCommonApp::getInstance()->getBrandnameUrl(), SLOT(fetchBrandNameFinish()));
}

void TWebServiceInterface::fetchBrandNameFinish()
{
    QObject *requester;
    TFetchBrandNameResponse response;
    if (callFinish(response, requester) == ResSuccess)
    {
        emit fetchBrandNameDone(requester, response);
    }
}

void TWebServiceInterface::checkDeviceLegitimacy(QObject *requester, const TCheckDeviceLegitimacyRequest &reqData)
{
    Q_UNUSED(requester);
    Q_UNUSED(reqData);
    //call(requester, reqData, TCommonApp::getInstance()->getCheckPackageUrl(), SLOT(OnCheckDeviceLegitimacyDone()));
}

void TWebServiceInterface::OnCheckDeviceLegitimacyDone()
{
    QObject *requester;
    TCheckDeviceLegitimacyResponse response;
    ResponseCode code = callFinish(response, requester);
    emit checkDeviceLegitimacyDone(requester, response, code);
}

void TWebServiceInterface::fetchTicket(QObject *requester, const TFetchTicketRequest &reqData)
{
    Q_UNUSED(requester);
    Q_UNUSED(reqData);
    //call(requester, reqData, TCommonApp::getInstance()->getFetchTicketUrl(), SLOT(fetchTicketFinish()));
}

void TWebServiceInterface::fetchTicketFinish()
{
    //QObject *requester = NULL;
    //TFetchTicketResponse response;
    //ResponseCode code = callFinish(response, requester);
    //emit fetchTicketDone(requester, response, code);
    //if (code == ResSuccess && !response.success)
    //{
    //    int errCode = getErrorCode(response.msgCode);
    //    if (errCode == Mpr::EmnpECd_160003 || errCode == Mpr::EmnpECd_160005)
    //        QMetaObject::invokeMethod(PTR_LOGIN_MANAGER, "setTimeOut", Qt::QueuedConnection);
    //}
}

void TWebServiceInterface::fetchStoreUrl(QObject *requester, const TRequestData &reqData)
{
    Q_UNUSED(requester);
    Q_UNUSED(reqData);
    //call(requester, reqData, TCommonApp::getInstance()->getStoreUrl(), SLOT(OnFetchStoreUrlDone()));
}

void TWebServiceInterface::OnFetchStoreUrlDone()
{
    QObject *requester;
    TFetchStoreResponse response;
    if (callFinish(response, requester) == ResSuccess)
    {
        emit fetchStoreUrlDone(requester, response);
    }
}

void TWebServiceInterface::fetchOperator(QObject *requester, const TFetchOperatorRequest &reqData)
{
    Q_UNUSED(requester);
    Q_UNUSED(reqData);
   /* qDebug() << __FUNCTION__ << TCommonApp::getInstance()->getFetchSearchInfoUrl();
    qDebug() << __FUNCTION__ << "ouyl post1:" << reqData.toJson();
    if (reqData.versionCode != "")
    {
        mVersionCode = reqData.versionCode;
        call(requester, reqData, TCommonApp::getInstance()->getFetchSearchInfoUrl(), SLOT(fetchVersionFinish()));
        mbTimeOut = true;
        ServerRequestTimeout(5);
    }
    else
    {
        call(requester, reqData, TCommonApp::getInstance()->getFetchSearchInfoUrl(), SLOT(fetchOperatorFinish()));
        mbTimeOut = true;
        ServerRequestTimeout(5);
    }  */
}

void TWebServiceInterface::DoMetaDataSearchRequest(QObject *requester, const QString& mprCode)
{
    Q_UNUSED(requester);
    Q_UNUSED(mprCode);
	//if (NULL == requester){
	//	return;
	//}

	//QString strSerachByMPRCodeUrl("");
 //   strSerachByMPRCodeUrl = TCommonApp::getInstance()->GetMetaDataUrl();

	//QString strTempMprCode = mprCode/* + "99999"*/;
	//QString strMprCode = QString("?mprcode=%1&clientType=001").arg(strTempMprCode);
	//strSerachByMPRCodeUrl += strMprCode;

	//QNetworkRequest searchRequest(strSerachByMPRCodeUrl);
	//QNetworkReply *reply = netManager->get(searchRequest);
	//if (reply)
	//{
	//	mapReplyRequester.insert(reply, requester);
	//	connect(reply, SIGNAL(finished()), this, SLOT(OnMetaDataSeratchReply()));
	//}
}

void TWebServiceInterface::OnMetaDataSeratchReply()
{
	QNetworkReply *reply = qobject_cast<QNetworkReply*>(sender());
	if (reply == NULL)
		return ;
	if (QNetworkReply::NoError == reply->error())
	{
		emit sigSearchMetaDataUrl(reply->url().toString());
	}

    qDebug() << __FUNCTION__ << reply->errorString() << reply->url();

	mapReplyRequester.remove(reply);
	reply->deleteLater();
}

void TWebServiceInterface::fetchVersionFinish()
{
    /*TFetchOperatorRequest request;

    QObject *requesterWidget;
    TFetchOperatorResponse response;
    if (ResSuccess == callFinish(response, requesterWidget))
    {
        request.ids = "";
        request.code = response.mprCode;
        request.sysId = PLATFORMID;
        request.flag = "1";
        request.mprCode = response.mprCode;
        request.versionCode = mVersionCode;
        QString url = response.repcontent;
        
        call(requesterWidget, request, url, SLOT(fetchOperatorFinishTwo()));
        ServerRequestTimeout(5);
        mbTimeOut = true;
    }*/
}

void TWebServiceInterface::fetchOperatorFinish()
{    
/*    TFetchOperatorRequest request;
    
    QObject *requesterWidget;
    TFetchOperatorResponse response;
    if (ResSuccess == callFinish(response, requesterWidget))
    {
        if (mpServerTimeOut->isActive())
        {
            mpServerTimeOut->stop();
            mbTimeOut = false;
        }
        QList<Mpr::MprFileInfo> infors;
        int digtype = Mpr::DigMprx | Mpr::DigMpr;
        if (PTR_MPR_DIGITAL_FILE_MANAGER->getAllMprFile(infors, response.mprCode, digtype, false) == TMprDigitalFileManager::Success)
        {
            int i;
            request.ids = "";
            for (i = 0; i < infors.size(); ++i)
            {
                QString localgoods = infors.at(i).proInfo.goodsId;
                if (!localgoods.isEmpty())
                {
                    request.ids += localgoods;
                    request.ids += ",";
                }
            }
            if (i == 0)
            {
                request.ids = "";
            }
        }
        request.code = response.mprCode;
        request.sysId = PLATFORMID;
        request.flag = "1";
        request.mprCode = response.mprCode;
        request.versionCode = "";
        QString url = response.repcontent;
        qDebug() << __FUNCTION__ << "ouyl post:" << request.toJson();
        call(requesterWidget, request, url, SLOT(fetchOperatorFinishTwo()));
        ServerRequestTimeout(5);
        mbTimeOut = true;
    }*/     
}

void TWebServiceInterface::fetchOperatorFinishTwo()
{
    QObject *requester;
    TFetchOperatorResponse response;   
    mResponseResult = callFinish(response, requester);
    if (mResponseResult == ResSuccess)
    {
        if (mpServerTimeOut->isActive())
        {
            mpServerTimeOut->stop();
            mbTimeOut = false;
        }
        emit fetchOperatorDone(requester, response, mVersionCode);
        mVersionCode = "";
    }   
}

void TWebServiceInterface::fetchSofewareLatestVer(QObject *requester, const TFetchSoftLatestVersionRequest &reqData)
{
    call(requester, reqData, TAppCommon::Instance().GetCheckSoftwareVersionUrl(), SLOT(fetchSofewareLatestVerFinish()));
}

void TWebServiceInterface::fetchSofewareLatestVerFinish()
{
    QObject *requester;
    TFetchLatestVersionResponse response;
    ResponseCode code = callFinish(response, requester);
    emit fetchSofewareLatestVerDone(requester, response, code);
}

void TWebServiceInterface::fetchFirmwareLatestVer(QObject *requester, const TFetchFirmLatestVersionRequest &reqData)
{
    call(requester, reqData, TAppCommon::Instance().GetCheckFirmwareVersionUrl(), SLOT(fetchFirmwareLatestVerFinish()));
}

void TWebServiceInterface::fetchFirmwareLatestVerFinish()
{
    QObject *requester;
    TFetchLatestVersionResponse response;
    ResponseCode code = callFinish(response, requester);
    emit fetchFirmwareLatestVerDone(requester, response, code);
}

void TWebServiceInterface::uploadInfo(QObject *requester, const TUploadInfoRequest &reqData)
{
    Q_UNUSED(requester);
    Q_UNUSED(reqData);
    //call(requester, reqData, TCommonApp::getInstance()->getUploadInfoUrl(), SLOT(uploadInfoDone()));
}

void TWebServiceInterface::fetchLicense(QObject *requester, const TFetchLicenseRequest &reqData)
{
    Q_UNUSED(requester);
    Q_UNUSED(reqData);
    //call(requester, reqData, TCommonApp::getInstance()->getFetchLicenseUrl(), SLOT(fetchLicenseFinish()));
}

void TWebServiceInterface::validSession(QObject *requester, const TValidSessionRequest &reqData)
{
    Q_UNUSED(requester);
    Q_UNUSED(reqData);
    //call(requester, reqData, TCommonApp::getInstance()->getValidSessionUrl(), SLOT(validSessionFinish()));
}

void TWebServiceInterface::uploadInfoDone()
{
    QObject *requester;
    TUploadInfoResponse response;
    if (callFinish(response, requester) == ResSuccess)
    {
        emit uploadInfoFinish(requester, response);
    }
}

bool TWebServiceInterface::call(QObject *requester, const TRequestData &reqData,
	const QString &urlSuffix, const char *slot)
{
    if (requester == NULL)
    {
        qDebug() << __FUNCTION__ << "request is null";
        return false;
    }
		
	QNetworkRequest request;
	QNetworkReply *reply = NULL;

    QString strUrl = QString("%1").arg(urlSuffix);
	request.setUrl(QUrl(strUrl));
	request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
	QSslConfiguration conf = request.sslConfiguration();
	conf.setPeerVerifyMode(QSslSocket::VerifyNone);
	conf.setProtocol(QSsl::TlsV1SslV3);
	request.setSslConfiguration(conf);

    QByteArray byteArray = reqData.toJson(); 
    qDebug() << __FUNCTION__ << byteArray;

    //request.setHeader(QNetworkRequest::CookieHeader, qVariantFromValue(*TCommonApp::getInstance()->getNetworkCookie()));
    reply = netManager->post(request, byteArray);
    if (reply)
    {    
        mapReplyRequester.insert(reply, requester);
        connect(reply, SIGNAL(finished()), this, slot);        
    }

    return true;
}

TWebServiceInterface::ResponseCode TWebServiceInterface::callFinish(TResponseData &response, QObject *&requester)
{
    ResponseCode result = ResFail;
    QNetworkReply *reply = qobject_cast<QNetworkReply*>(sender());
    if (reply == NULL)
    {
        qDebug() << __FUNCTION__ << "reply is null";
        return result;
    }
        
	requester = mapReplyRequester.value(reply);
    if (reply->error() == QNetworkReply::NoError)
    {
	    qDebug() << __FUNCTION__ << "replyUrl:" << reply->url().toString();
        QByteArray tmpArry = reply->readAll();
        if (response.fromJson(tmpArry))
        {
            result = ResSuccess;
        }
        else
        {
            qCritical() << __FUNCTION__ << "json format error.";
        }
    }
    else if (reply->error() == QNetworkReply::OperationCanceledError)
    {
        qDebug() << __FUNCTION__ << "cancel";
        result = ResCancel;
    }
    else
    {
        qCritical() << __FUNCTION__ << "get response error" << reply->error() << reply->errorString();
        result = ResNetworkError;
    }

    mapReplyRequester.remove(reply);
    reply->deleteLater();
    reply = 0;
    return result;
}

bool TWebServiceInterface::loginCallFinish(TResponseData& response, QObject *&requester)
{
    bool result = false;
    QNetworkReply *reply = qobject_cast<QNetworkReply*>(sender());
    if (reply == NULL)
    {
        qDebug() << __FUNCTION__ << "reply is null";
        return result;
    }
        
    if (reply->error() == QNetworkReply::NoError)
    {
        if (response.fromJson(reply->readAll()))
        {
            requester = mapReplyRequester.value(reply);
            result = true;
        }
        else
        {
            emit checkNetwork();
            qDebug() << __FUNCTION__ << "json format error.";
        }
    }
    else
    {
        emit checkNetwork();
        qDebug() << __FUNCTION__ << "get response error" << reply->error() << reply->errorString();
    }

    mapReplyRequester.remove(reply);
    reply->deleteLater();
    return result;
}

void TWebServiceInterface::fetchLicenseFinish()
{
    //QObject *requester;
    //TFetchLicenseResponse response;
    //ResponseCode code = callFinish(response, requester);
    //emit fetchLicenseDone(requester, response, code);

    //if (code == ResSuccess && !response.success)
    //{
    //    int errCode = getErrorCode(response.msgCode);
    //    if (errCode == Mpr::EmnpECd_160003 || errCode == Mpr::EmnpECd_160005)
    //        QMetaObject::invokeMethod(PTR_LOGIN_MANAGER, "setTimeOut", Qt::QueuedConnection);
    //}
}

void TWebServiceInterface::validSessionFinish()
{
    QObject *requester;
    TValidSessionResponse response;
    ResponseCode code = callFinish(response, requester);
    emit validSessionDone(requester, response, code);
}

void TWebServiceInterface::OnTimeOut()
{   
    if (mpServerTimeOut->isActive())
    {
        mpServerTimeOut->stop();
    }
    
    if (mbTimeOut)
    {
        TPopupDialog msg(NULL, tr("Failure"), tr("The search timeout, please try again"), TAppCommon::Instance().GetMainWindow(), TPopupDialog::Error);
        msg.AutoCloseDialog();
        msg.exec();
    }
}

void TWebServiceInterface::ServerRequestTimeout(int mtime)
{
    int mTime = mtime * 1000;   
    mpServerTimeOut->start(mTime);
}

void TWebServiceInterface::SetMainWindow(QWidget *w)
{
    mpMainWindow = w;
}