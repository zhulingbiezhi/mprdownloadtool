#include "TDownloadTask.h"
#include "TDownloadManager.h"
#include "TFileLib.h"
#include "TWorkStateConfig.h"
#include "TGlobleCtl.h"
#include "mprMimeDataFetcher.h"
#include "TAppCommon.h"


const int THREAD_NUM = 3;
const int MAX_RUNNING_NUM = 3;

#define  PAUSE_TASK 1
#define  REMOVE_TASK 2
#define  FINISHED_TASK 3

TDownloadManager::TDownloadManager()
: mnRunningTaskNum(0)
, mbRemoveAll(false)
{
    mpSharedThread = new QThread;
    mpSharedNAM = new QNetworkAccessManager;
    mpSharedNAM->moveToThread(mpSharedThread);
    mpSharedThread->start();
}

TDownloadManager::~TDownloadManager()
{
    TDownloadTask * pDownloadTask;
    for (int i = 0; i < mDownloadTaskList.size(); i++)
    {
        pDownloadTask = mDownloadTaskList.at(i);
        pDownloadTask->deleteLater();
    }
    if (mpSharedThread)
    {
        if (mpSharedThread->isRunning())
        {
            mpSharedThread->quit();
            mpSharedThread->wait();
        }
        mpSharedThread->deleteLater();
        mpSharedThread = nullptr;
    }
    if (mpSharedNAM)
    {
        mpSharedNAM->deleteLater();
        mpSharedNAM = nullptr;
    }
}

TDownloadManager* TDownloadManager::Instance()
{
    static TDownloadManager downloadMgr;
    return &downloadMgr;
}


void TDownloadManager::AddTask(int task_id, TDownloadInfo downloadInfo)
{
    TDownloadTask * pDownloadTask = new TDownloadTask(this, task_id, THREAD_NUM, downloadInfo);
    mTaskInfoHash.insert(task_id, downloadInfo);
    mDownloadTaskList.append(pDownloadTask);
    StartTask(task_id);
}

void TDownloadManager::StartTask(int task_id)
{
    int index = FindByID(task_id);
    if (index != -1)
    {
        TDownloadTask * pDownloadTask = mDownloadTaskList.at(index);
        if (mnRunningTaskNum < MAX_RUNNING_NUM) 
        {
            if (pDownloadTask->IsPause()) {
                pDownloadTask->RestartTask();
            } else if (!pDownloadTask->IsRuning()) {
                pDownloadTask->StartTask();
            } else {
                qDebug() << __FUNCTION__ << "Warning : the task is running, you can't start it again !" << task_id;
            }
        }
        else
        {
            if (pDownloadTask->IsPause())
            {
                pDownloadTask->SetDownloadState(HTS_PREPARED);
            }            
        }        
    }
    else
    {
        qDebug() << __FUNCTION__ << "Warning : can't find the task by id !" << task_id << mDownloadTaskList.size();
    }   
}

void TDownloadManager::StopTask(int task_id)
{
    int index = FindByID(task_id);
    if (index != -1)
    {
        TDownloadTask * pDownloadTask = mDownloadTaskList.at(index);
        if (pDownloadTask->IsRuning())
        {          
            pDownloadTask->StopTask();      
        }
        else
        {
            qDebug() << __FUNCTION__ << "Warning : the task is not start, you can't stop it !" << task_id;
        }
    }
    else
    {
        qDebug() << __FUNCTION__ << "Warning : can't find the task by id !" << task_id << mDownloadTaskList.size();
    }
}

void TDownloadManager::RemoveTask(int task_id)
{
    int index = FindByID(task_id);
    if (index != -1)
    {
        TDownloadTask * pDownloadTask = mDownloadTaskList.at(index);
        if (pDownloadTask->IsRuning()) 
        {
            pDownloadTask->StopTask();
        }
        mTaskInfoHash.remove(task_id);
        mDownloadTaskList.removeOne(pDownloadTask);
        delete pDownloadTask;
        pDownloadTask = nullptr;
    }
    else
    {
        qDebug() << __FUNCTION__ << "Warning : can't find the task by id !" << task_id << mDownloadTaskList.size();
    }
}

int TDownloadManager::FindByID(int task_id)
{
    for (int i = 0; i < mDownloadTaskList.size(); i++)
    {
        TDownloadTask *pDownloadTask = mDownloadTaskList.at(i);
        if (pDownloadTask->GetTaskID() == task_id)
        {
            return i;
        }
    }
    qDebug() << __FUNCTION__ << "error : can't find the task by id !" << task_id << mDownloadTaskList.size();
    return -1;
}

void TDownloadManager::AutoStartTask()
{
    TDownloadTask * pDownloadTask = nullptr;
    for (int i = 0; i < mDownloadTaskList.size(); i++)  //�Ƚ��ȳ�
    {
        pDownloadTask = mDownloadTaskList.at(i);
        if (pDownloadTask->IsWaiting())
        {
            //pDownloadTask->StartTask();
            StartTask(pDownloadTask->GetTaskID());
            if (mnRunningTaskNum >= MAX_RUNNING_NUM)
            {
                break;
            }           
        }
    }
}

void TDownloadManager::SendMessage(int taskID, int msgType, const QVariant& msgBody, TSVHashMap& extraParams)
{
    if (msgType == HTM_STATE)
    {
        int state = msgBody.toInt();
        if (state == HTS_FINISHED || state == HTS_PAUSED || state == HTS_FAILED)
        {
            if (state == HTS_FINISHED)
            {            
                if (mTaskInfoHash.find(taskID) != mTaskInfoHash.end())
                {
                    TMPRFileInfoItem& infoItem = mTaskInfoHash[taskID].itemInfo;
                    unsigned short version = 0;
                    if (0 == getMPRLanguageVersionW((wchar_t*)infoItem.tFilePath.utf16(), &version)) {
                        infoItem.tVersion = QString("%1").arg(version, 5, 10, QChar('0'));
                    }
                    extraParams.insert(VERSION, infoItem.tVersion);
                    extraParams.insert(FILESIZE, QFileInfo(infoItem.tFilePath).size());
                    RemoveTask(taskID);
                }
                else
                {
                    qDebug() << __FUNCTION__ << "error : can't find the task by taskID, probably the task is deleted !";
                    return;
                }
            }
            mnRunningTaskNum--;
            if (mnRunningTaskNum < 0) {
                mnRunningTaskNum = 0;
            }
            if (!mbRemoveAll)
            {
                AutoStartTask();
            }
        }
        else if (state == HTS_RUNNING)
        {
            mnRunningTaskNum++;
        }
    }
    emit sigDownloadMessage(taskID, msgType, msgBody, extraParams);
}

void TDownloadManager::LoadTaskInfo(QList<TDownloadInfo> taskList)
{
    for (int i = 0; i < taskList.size(); i++)
    {
        TDownloadInfo downLoadInfo = taskList.at(i);
        TDownloadTask * pDownloadTask = new TDownloadTask(this, downLoadInfo.task_id, THREAD_NUM, taskList.at(i));
        pDownloadTask->LoadTaskInfo(downLoadInfo);
        mTaskInfoHash.insert(downLoadInfo.task_id, downLoadInfo);
        mDownloadTaskList.append(pDownloadTask);
    }
}

void TDownloadManager::RemoveAllTask()
{
    qDebug() << __FUNCTION__;
    mbRemoveAll = true;
    TDownloadTask * pDownloadTask;
    for (int i = 0; i < mDownloadTaskList.size(); i++)
    {
        pDownloadTask = mDownloadTaskList.at(i);
        if (pDownloadTask->IsRuning())
        {
            pDownloadTask->StopTask();
        }
        delete pDownloadTask;
        pDownloadTask = nullptr;
    }
    mDownloadTaskList.clear();
    mnRunningTaskNum = 0;
    mTaskInfoHash.clear();
    mbRemoveAll = false;
}

void TDownloadManager::SaveTaskInfo(TDownloadInfo& info)
{
    int index = FindByID(info.task_id);
    if (index == -1) {
        qDebug() << __FUNCTION__ << info.strSavePath;
        return;
    }
    if (TDownloadTask *pDownloadTask = mDownloadTaskList.at(index))
    {
        qDebug() << __FUNCTION__ << info.task_id << mDownloadTaskList.size();
        return pDownloadTask->SaveTaskInfo(info);
    }
}
