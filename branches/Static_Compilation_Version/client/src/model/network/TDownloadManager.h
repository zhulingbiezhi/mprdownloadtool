#ifndef _TDOWNLOAD_MANAGER_H_
#define _TDOWNLOAD_MANAGER_H_

#include "TDownloadTask.h"

typedef QHash<QString, QVariant> TSVHashMap;
class TThreadManager;
class TFileLib;

class TDownloadManager : public QObject
{
    friend class TDownloadTask;
    Q_OBJECT
public:
    TDownloadManager();
    ~TDownloadManager();
    static TDownloadManager* Instance();

public:
    void AddTask(int task_id, TDownloadInfo downloadInfo);
    void StartTask(int task_id);
    void StopTask(int task_id);
    void RemoveTask(int task_id);
    void LoadTaskInfo(QList<TDownloadInfo> taskList);
    void RemoveAllTask();
    void SaveTaskInfo(TDownloadInfo& info);

private:
    void AutoStartTask();
    int  FindByID(int task_id);
    void SendMessage(int taskID, int msgType, const QVariant& msgBody, TSVHashMap& extraParams = TSVHashMap());

signals:
    void sigDownloadMessage(int taskID, int msgType, const QVariant& msgBody, TSVHashMap& extraParams);
    void sigAddTaskToTable(int task_id, TMPRFileInfoItem infoItem);

private:
    QList<TDownloadTask *>          mDownloadTaskList;
    int                             mnRunningTaskNum;
    QMap<int, TDownloadInfo>        mTaskInfoHash;
    QThread*                        mpSharedThread;
    QNetworkAccessManager*          mpSharedNAM;
    bool                            mbRemoveAll;
};
#endif