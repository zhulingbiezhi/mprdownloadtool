#ifndef _TLOCALSOCKET_H
#define _TLOCALSOCKET_H
class TLocalSocket : public QObject
{
    Q_OBJECT
public:
    explicit TLocalSocket(const QString&servername, QObject *parent = 0);
    ~TLocalSocket();
    bool ConnectToServer();

signals:
    void readUpdateServerMess(const QString& mess);

    public slots:
    void OnReadyRead();
    void OnDisplayError(QLocalSocket::LocalSocketError socketError);

private:
    QLocalSocket *mpLocalSocket;
    QString       mServerName;
    quint16       mBlockSize;
};
#endif