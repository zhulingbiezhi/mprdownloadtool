#ifndef _TNETMPRSEARCHER_H_
#define _TNETMPRSEARCHER_H_

#include "TMPRSearhRepliedInfo.h"
#include "TMPRDeviceDesc.h"

class TWaittingDlg;

class TNetMPRSearcher : public QObject
{
    Q_OBJECT

public:
    TNetMPRSearcher(QObject* parent = 0);
    ~TNetMPRSearcher();

    void                                SearchMPRFromNetwork(const QString& deviceId, const QString& keywork, const QString& prefixCode);
    const QList<TMPRSearchRepliedInfo>& GetReachResult() const;
    QString                             CreateQueryString(const TMPRDeviceDesc& devDesc, const TMPRSearchRepliedInfo& info);
    void                                FetchDownloadUrl(const QString& strUrl, const QString& queryString);

private slots:
   void                                 OnSearchReply();
   void                                 OnSearchError(QNetworkReply::NetworkError);
   void                                 OnFetchUrlReply();
   void                                 OnFetchUrlError(QNetworkReply::NetworkError);

signals:                                
   void                                 SearchReplied();
   void                                 FetchDownloadUrlReplied(const QString& uuid, const TNetMPRUrl&);

private:
    QNetworkAccessManager*              mpNetMgr; 
    QString                             mDeviceId;
    QNetworkReply*                      mpCurrentSeachReply;
    QNetworkReply*                      mpFetchUrlReply;
    QList<TMPRSearchRepliedInfo>        mMPRSearchRepliedInfoList;
    TWaittingDlg*                        mpWaittingDlg;
};

#endif