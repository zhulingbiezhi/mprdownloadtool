#ifndef _THTTPTASK_H_
#define _THTTPTASK_H_

#include <QString>
#include <QObject>
#include <QNetworkReply>
#include <QNetworkAccessManager>

class TDataStream;

enum EHttpTaskMsgType {
    HTM_PROGRESS = 1,
    HTM_STATE = 2,
    HTM_RATE = 4,
    HTM_ERROR = 8,
    HTM_TEXT = 16,
    HTM_SIZE = 32,
    HTM_PHASE = 64,
};
enum EHttpTaskState {
    HTS_UNKNOWN = 1,
    HTS_PAUSED = 2,
    HTS_PREPARED = 4,
    HTS_RUNNING = 8,
    HTS_FAILED = 16,
    HTS_CHECK = 32,
    HTS_FINISHED = 64,
    HTS_CONTINUE = 128,

    HTS_UNFINISHED = 0x1F,
    HTS_ALLSTATE = 0xFF,
};

typedef struct _HttpTaskInfo
{

}THttpTaskInfo;



class THttpSegment : public QObject
{
    Q_OBJECT

public:
    THttpSegment(int http_id, QString strUrl, qint64 qStartOffset, qint64 qEndOffset, TDataStream* pDataStream);
    void  PauseSegTask(bool bPause);
    void  Stop();
    Q_INVOKABLE void  Start();

private:
    bool  ReadReplyData();



signals:
    void sigSegmentDataTransferred(int http_id, qint64 len);
    void sigSegmentTaskFinished(int http_id, int code, QString errStr);

private slots:

    void OnSegmentTaskFinished();
    void OnSegmentTaskReadyRead();

private:
    int                     mHttpID;
    int                     mnRetryCounts;
    bool                    mbPaused;
    qint64                  mStartOffset;
    qint64                  mWriteOffset;
    qint64                  mEndOffset;  
    QString                 mstrUrl;
    QByteArray              mBufferdData;
    TDataStream*            mpDataStream;
    EHttpTaskState          mHttpState;
    QNetworkReply*          mReply;
    QNetworkAccessManager*  mNAM;
};
#endif