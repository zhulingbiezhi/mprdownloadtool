﻿#ifndef _TWEBSERVICEINTERFACE_H_
#define _TWEBSERVICEINTERFACE_H_

#include <QObject>

namespace Mpr
{
enum EmnpErrorCode
{
    EmnpECd_160000,     ///< 成功
    EmnpECd_160001,     ///< 参数错误
    EmnpECd_160002,     ///< 设备已挂失
    EmnpECd_160003,     ///< 未识别用户
    EmnpECd_160004,     ///< 获取ticket失败
    EmnpECd_160005,     ///< 请连接设备
    EmnpECd_160006,     ///< 未找到资源
    EmnpECd_160007,     ///< 帐号未激活
    EmnpECd_160008,     ///< 帐号异常
    EmnpECd_160009,     ///< 帐号不存在
    EmnpECd_160010,     ///< 帐号或密码错误
    EmnpECd_160011,     ///< 设备帐号绑定关系不存在
    EmnpECd_160012,     ///< 设备绑定异常
    EmnpECd_160013,     ///< 设备已绑定
    EmnpECd_160014,     ///< 设备未登记
    EmnpECd_160015,     ///< 登录帐号和设备绑定帐号不一致
    EmnpEcd_160027,     ///< 该设备未购买此商品
    EmnpEcd_160028,     ///< 收费内容无购买记录
    EmnpECd_169998,     ///< 设备验证失败
    EmnpECd_169999      ///< 系统异常
};

struct MprProductInformation
{
    enum ProductType
    {
        EBOOk = 1,
        AUDIO = 2,
        VIDEO = 3
    };

    MprProductInformation() :size(0), isMpr(true), isDiy(true), productType(VIDEO){}
    QString   productImgUrl;
    int       productType;
    QString   publisher;
    QString   author;
    QDateTime timeBuying;
    quint64   size;
    QString   goodsId;
    QString   goodsName;
    QString   mprPrefixCode;
    bool      isMpr;
    QString   uuid;
    QString   downloadId;
    QString   language;
    QString   languageId;
    QString   publishing;
    QString   fileFormat;
    bool      isDiy;
    QString   versionCode;
    QString   producer;
    QString   brandId;
    QString   publicationName;
};

struct MprUserInformation
{
    QString uesrName;
    QString passWord;
    bool    isAutoPassWd;
    bool    isAutoLogin;
    bool    lastLogin;
};
}

struct AllStoreUrl
{
    QString classifyId;
    QString classifyName;
    QString url;
    QList<AllStoreUrl> storeUrlList;
};

class TRequestData
{
public:
    virtual QByteArray toJson()const {return QByteArray("{}");}
    virtual ~TRequestData() {}
};

class TResponseData
{
public:
    virtual bool fromJson(const QByteArray&) {return false;}
    virtual ~TResponseData() {}
};

/*!
 * \brief 上传装机量接口的请求
 */
class TUploadInfoRequest : public TRequestData
{
public:
    QByteArray toJson()const;
    QString userName;
    QString deviceId;
    QString softwareType;
    QString terminalType;
    QString hostSystem;
    QString clientVersion;
    QString hostIP;
    QString time;
};

/*!
 * \brief 上传装机量接口的响应
 */
class TUploadInfoResponse : public TResponseData
{
public:
    bool fromJson(const QByteArray &data);
    bool success;
    QString msgCode;
    QString message;
};


/*!
 * \brief 获取运营商地址接口的请求
 */
class TFetchOperatorRequest : public TRequestData
{
public:
    QByteArray toJson()const;
    QString code;
    QString mprCode;
    QString sysId;
    QString ids;
    QString flag;
    QString versionCode;
};

/*!
 * \brief 获取运营商地址接口的响应
 */
class TFetchOperatorResponse : public TResponseData
{
public:
    bool fromJson(const QByteArray &data);
    QString returnMsg;
    QString repcontent;
    QString mprCode;
    bool    isNew;
    QString url;
};

class TFetchChallengePackageRequest : public TRequestData
{
public:
    QByteArray toJson()const;
    QString platformId;
    QString deviceId;
};

class TFetchChallengePackageResponse : public TResponseData
{
public:
    TFetchChallengePackageResponse():success(false){}
    bool fromJson(const QByteArray &data);

    bool success;
    QString msgCode;
    QString challengePackage;
    QString message;
};

class TCheckDeviceLegitimacyRequest : public TRequestData
{
public:
    QByteArray toJson()const;
    QString deviceId;
    QString challengePackage;
    QString responseChallengePackage;
};

class TCheckDeviceLegitimacyResponse : public TResponseData
{
public:
    bool fromJson(const QByteArray &data);

    bool success;
    QString msgCode;
    QString bundUserName;
    QString brandName;
    QString message;
};


class TAuthenticateRequest : public TRequestData
{
public:
    QByteArray toJson()const;
    QString deviceId;
    QString challengePackage;
    QString responseChallengePackage;
};

class TAuthenticateResponse : public TResponseData
{
public:
    bool fromJson(const QByteArray &data);

    bool success;
    QString msgCode;
    QString bundUserName;
    QString message;
};

class TLoginRequest : public TRequestData
{
public:
    QByteArray toJson()const;
    QString deviceId;
    QString brandId;
    QString deviceModel;
    int deviceType;
    QString username;
    QString password;
    QString outtradeno;
};

class TLoginResponse : public TResponseData
{
public:
    bool fromJson(const QByteArray &data);
    bool success;
    QString msgCode;
    QString accountId;
    QString message;
};

class TLoginOutRequest : public TRequestData
{

public:
    QByteArray toJson()const;
    QString userName;
};

class TLoginOutResponse : public TResponseData
{

public:
    bool fromJson(const QByteArray &data);
    bool success;
    QString msgCode;
    QString message;
};

class TProductInfoRequest : public TRequestData
{
public:
    TProductInfoRequest():pageSize(100000),pageNo(1){}
    QByteArray toJson()const;
    QString userName;
    QString deviceId;
    int pageSize;
    int pageNo;
};

class TProductInfoResponse : public TResponseData
{
public:
    bool fromJson(const QByteArray &data);
    QList<Mpr::MprProductInformation> pInfo;
    bool success;
    QString msgCode;
    QString message;
};

class TFetchBrandNameRequest : public TRequestData
{
public:
    QByteArray toJson()const;
    QString brandId;
};

class TFetchBrandNameResponse : public TResponseData
{
public:
    bool fromJson(const QByteArray &data);
    bool success;
    QString msgCode;
    QString brandName;
    QString message;
};

/*!
 * \brief 获取Store类别接口的响应
 */
class TFetchStoreResponse : public TResponseData
{
public:
    bool fromJson(const QByteArray &data);
    bool success;
    QString msgCode;
    QString message;
    QList<AllStoreUrl> store;

private:
    void RecursiveAnalysis(QVariantList list, QList<AllStoreUrl>& store);
};

/*!
 * \brief 获取Ticket接口的请求
 */
class TFetchTicketRequest : public TRequestData
{
public:
    QByteArray toJson()const;
    QString goodsId;
    QString uuid;
    QString accountId;
    QString deviceId;
};

/*!
 * \brief 获取Ticket接口的响应
 */
class TFetchTicketResponse : public TResponseData
{
public:
    bool fromJson(const QByteArray &data);
    bool success;
    QString msgCode;
    Mpr::MprProductInformation proInfo;
    QByteArray ticket;
    QString interfaceUrl;
    QString message;
};


/*!
 * \brief 获取软件最新版本的请求
 */
class TFetchSoftLatestVersionRequest : public TRequestData
{
public:
    QByteArray toJson()const;
    QString curVCode;
    QString softwareType;
    QString terminalType;
};
/*!
 * \brief 获取固件最新版本的请求
 */
class TFetchFirmLatestVersionRequest : public TRequestData
{
public:
    QByteArray toJson()const;
    QString firmwareVersion;
    QString deviceModel;
    QString deviceType;
};
/*!
 * \brief 获取最新版本的响应
 */
class TFetchLatestVersionResponse : public TResponseData
{
public:
    bool fromJson(const QByteArray &data);
    QString rcode;
    QString vCode;
    QString url;
    QString force;
    QString desc;
    QString fileName;
    QByteArray sourceData;
};

/*!
 * \brief uuid获取license
 */
class TFetchLicenseRequest : public TRequestData
{
public:
    QByteArray toJson()const;
    QString uuid;
    QString mediaType;
    QString accountId;
    QString deviceId;
};

class TFetchLicenseResponse : public TResponseData
{
public:
    bool fromJson(const QByteArray &data);
    bool success;
    QString msgCode;
    QByteArray ticket;
    QString interfaceUrl;
    Mpr::MprProductInformation product;
    QString message;
};

class TValidSessionRequest : public TRequestData
{
public:
    QByteArray toJson()const;
    QString userName;
};

class TValidSessionResponse : public TResponseData
{
public:
    bool fromJson(const QByteArray &data);
    bool success;
};

/*!
 * \brief 打开第三方浏览器接口
 */
class TOpenBrowserResponse : public TResponseData
{
public:
    bool fromJson(const QByteArray &data);
    QString directUrl;
};

class QNetworkAccessManager;
class QNetworkReply;
/*!
 * \brief The WebServiceInterface class is in charge of communicating with web
 *
 * The TWebServiceInterface class is a singleton
 */
class TWebServiceInterface : public QObject
{
    Q_OBJECT
public:
	enum ResponseCode
    {
        ResSuccess,
        ResFail,
        ResNetworkError,
        ResCancel
    };

public:
    TWebServiceInterface(QObject* parent = NULL);
    ~TWebServiceInterface();
    static int getErrorCode(const QString &code);
    static QString getErrorStr(int code);
    static QString getErrorStr(const QString &code);

    void setSebAddress(const QString &host, int port);
    void fetchChallengePackage(QObject *requester, const TFetchChallengePackageRequest  &reqData);
    void authenticate(QObject *requester, const TAuthenticateRequest &reqData);
    void login(QObject *requester, const TLoginRequest &reqData);
    void fetchProductInfo(QObject *requester, const TProductInfoRequest &reqData);
    void fetchBrandName(QObject *requester, const TFetchBrandNameRequest &reqData);
    void checkDeviceLegitimacy(QObject *requester, const TCheckDeviceLegitimacyRequest &reqData);
    void loginOut(QObject *requester, const TLoginOutRequest &reqData);
    void fetchStoreUrl(QObject *requester, const TRequestData &reqData);
	void fetchOperator(QObject *requester, const TFetchOperatorRequest &reqData);
    void fetchSofewareLatestVer(QObject *requester, const TFetchSoftLatestVersionRequest &reqData);
    void fetchFirmwareLatestVer(QObject *requester, const TFetchFirmLatestVersionRequest &reqData);
    void uploadInfo(QObject *requester, const TUploadInfoRequest &reqData);
    void fetchLicense(QObject *requester, const TFetchLicenseRequest &reqData);
    void validSession(QObject *requester, const TValidSessionRequest &reqData);
	void DoMetaDataSearchRequest(QObject *requester, const QString& mprCode);
    void OnTimeOut();
    void ServerRequestTimeout(int mtime);
    void SetMainWindow(QWidget *w);

public slots:
    void fetchTicket(QObject *requester, const TFetchTicketRequest &reqData);

signals:
    void fetchChallengePackageDone(QObject *requester, const TFetchChallengePackageResponse& resData, int code);
    void authenticateDone(QObject *requester, const TAuthenticateResponse &resData);
    void loginDone(QObject *requester, const TLoginResponse &resData);
    void fetchProductInfoDone(QObject *requester, const TProductInfoResponse &reqData);
    void fetchBrandNameDone(QObject *requester, const TFetchBrandNameResponse &resData);
    void checkDeviceLegitimacyDone(QObject *requester, const TCheckDeviceLegitimacyResponse &reqData, int code);
    void loginOutDone(QObject *requester, const TLoginOutResponse &reqData);
    void checkNetwork();
    void fetchTicketDone(QObject *requester, const TFetchTicketResponse &resData, TWebServiceInterface::ResponseCode code);
    void fetchStoreUrlDone(QObject *requester, const TFetchStoreResponse &reqData);

    void fetchSofewareLatestVerDone(QObject *requester, const TFetchLatestVersionResponse &resData, TWebServiceInterface::ResponseCode code);
    void fetchFirmwareLatestVerDone(QObject *requester, const TFetchLatestVersionResponse &resData, TWebServiceInterface::ResponseCode code);
    void fetchOperatorDone(QObject *requester, const TFetchOperatorResponse &reqData, const QString &versionCode);
    void uploadInfoFinish(QObject *requester, const TUploadInfoResponse &reqData);
    //void fetchSearchInfoDone(QObject *requester, const ResponseData &reqData);
    void fetchLicenseDone(QObject *requester, const TFetchLicenseResponse &resData, int code);
    void validSessionDone(QObject *requester, const TValidSessionResponse &resData, int code);
	void sigSearchMetaDataUrl(const QString&);

private slots:
    void fetchChallengePackageFinish();
    void authenticateFinish();
    void loginFinish();
    void OnFetchProductInfoDone();
    void fetchBrandNameFinish();
    void OnCheckDeviceLegitimacyDone();
    void loginOutFinish();
    void fetchTicketFinish();
    void OnFetchStoreUrlDone();

    void fetchOperatorFinish();
    void fetchOperatorFinishTwo();
    void fetchVersionFinish();    
    void fetchSofewareLatestVerFinish();
    void fetchFirmwareLatestVerFinish();
    void uploadInfoDone();
    void fetchLicenseFinish();
    void validSessionFinish();
	void OnMetaDataSeratchReply();

public:
    bool call(QObject *requester, const TRequestData &reqData, const QString &urlSuffix, const char *slot);
    ResponseCode callFinish(TResponseData& response, QObject *&requester);
    bool loginCallFinish(TResponseData& response, QObject *&requester);

private:
    QString       sebHost;
    int           sebPort;   
    bool          mbTimeOut;    
	QTimer       *mpServerTimeOut;
    ResponseCode  mResponseResult;
    QString       mVersionCode;
    QNetworkAccessManager         *netManager;
    QMap<QNetworkReply*, QObject*> mapReplyRequester;   
    QMap<QString, QString>          errorCodeStr;
    
    QWidget    *mpMainWindow;

};


#endif
