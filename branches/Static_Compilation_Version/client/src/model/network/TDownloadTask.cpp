#include "TDataStream.h"
#include "TDownloadManager.h"
#include "TDownloadTask.h"
#include "TDownloadTaskProber.h"
const qint64 minTaskSegmentSize = 1024 * 1024 * 5;

TDownloadTask::TDownloadTask(TDownloadManager* pDownloadMgr, int task_id, int threadNum, TDownloadInfo downloadInfo)
: mnThreadNum(threadNum)
, mDownloadInfo(downloadInfo)
, mpDownloadMgr(pDownloadMgr)
{
    mDownloadInfo.state = HTS_PREPARED;
    mDownloadInfo.task_id = task_id;
    mSegmentArea.clear();
    mpDownloadMgr->SendMessage(mDownloadInfo.task_id, HTM_STATE, mDownloadInfo.state);
    mpDataStream = new TDataStream(mDownloadInfo.strSavePath);
    mpTaskProber = new TDownloadTaskProber(pDownloadMgr->mpSharedNAM);
    mpTaskProber->moveToThread(pDownloadMgr->mpSharedThread);
    connect(mpTaskProber, SIGNAL(sigProbeTaskFinished(int, QString)), this, SLOT(OnProbeTaskFinished(int, QString)));
}

TDownloadTask::~TDownloadTask()
{
    for (int i = 0; i < mlstTaskSegmentInfo.size(); i++)
    {
        mlstTaskSegmentInfo[i].pHttpSegment->PauseSegTask(true);
        if (mlstTaskSegmentInfo[i].pThread->isRunning())
        {
            mlstTaskSegmentInfo[i].pThread->quit();
            mlstTaskSegmentInfo[i].pThread->wait();
        }
        mlstTaskSegmentInfo[i].pHttpSegment->Stop();
        mlstTaskSegmentInfo[i].pThread->deleteLater();
        mlstTaskSegmentInfo[i].pHttpSegment->deleteLater();
    }
    mlstTaskSegmentInfo.clear();
    if (mpDataStream)
    {
        delete mpDataStream;
        mpDataStream = nullptr;
    }

    if (mpTaskProber)
    {
        mpTaskProber->deleteLater();
        mpTaskProber = NULL;
    }
}

void TDownloadTask::StartTask()
{
    mpTaskProber->Start(mDownloadInfo.strRemoteUrl, "", "");
}

void TDownloadTask::OnProbeTaskFinished(int code, QString errStr)
{
    if (code != QNetworkReply::NoError)
    {
        int state = (code == QNetworkReply::OperationCanceledError) ? HTS_PAUSED : HTS_FAILED;
        if (mDownloadInfo.state != state) {
            mDownloadInfo.state = state;
            mpDownloadMgr->SendMessage(mDownloadInfo.task_id, HTM_STATE, mDownloadInfo.state);
        }
    }
    else
    {
        mDownloadInfo.totalBytes = mpTaskProber->GetObjTotalSize();
        StartFromBeginning();
    }
}

void TDownloadTask::StartFromBeginning()
{
    if (mnThreadNum == 0 || mDownloadInfo.totalBytes <= 0)
    {
        qDebug() << __FUNCTION__ << "error : the thread number is 0 !" << mnThreadNum << mDownloadInfo.totalBytes;
        return;
    }
    mpDataStream->OpenStream();
    qint64 segTaskSize = mDownloadInfo.totalBytes / mnThreadNum;
    while (segTaskSize < minTaskSegmentSize) {
        if (mnThreadNum > 1) {
            mnThreadNum--;
            segTaskSize = mDownloadInfo.totalBytes / mnThreadNum;
        }
        else {
            break;
        }
    }
    qint64 beginOffset = 0;
    for (int i = 0; i < mnThreadNum; i++) {
        qint64 endOffset = (i < mnThreadNum - 1) ? (beginOffset + segTaskSize - 1) : (mDownloadInfo.totalBytes - 1);
        THttpSegment* segmentTask = new THttpSegment(i, mDownloadInfo.strRemoteUrl, beginOffset, endOffset, mpDataStream);
        connect(segmentTask, SIGNAL(sigSegmentDataTransferred(int, qint64)), this, SLOT(OnSegmentDataRead(int, qint64)));
        connect(segmentTask, SIGNAL(sigSegmentTaskFinished(int, int, QString)), this, SLOT(OnSegmentTaskFinished(int, int, QString)));
        mSegmentArea.insert(i, TSegmentArea(beginOffset, endOffset));
        beginOffset += segTaskSize;

        TSegtaskInfo info;
        info.http_id = i;
        info.pHttpSegment = segmentTask;
        info.pThread = new QThread();
        info.pHttpSegment->moveToThread(info.pThread);
        info.pThread->start();
        QMetaObject::invokeMethod(info.pHttpSegment, "Start");
        mlstTaskSegmentInfo.append(info);
    }
    mDownloadInfo.state = HTS_RUNNING;
    mpDownloadMgr->SendMessage(mDownloadInfo.task_id, HTM_STATE, mDownloadInfo.state);
}


void TDownloadTask::RestartTask()
{
    mpDataStream->OpenStream();
    for (int i = 0; i < mlstTaskSegmentInfo.size(); i++)
    {
        mlstTaskSegmentInfo[i].pHttpSegment->PauseSegTask(false);
        mlstTaskSegmentInfo[i].pThread->start();
        QMetaObject::invokeMethod(mlstTaskSegmentInfo[i].pHttpSegment, "Start");
    }
    mDownloadInfo.state = HTS_RUNNING;
    mpDownloadMgr->SendMessage(mDownloadInfo.task_id, HTM_STATE, mDownloadInfo.state);
}

void TDownloadTask::StopTask()
{
    for (int i = 0; i < mlstTaskSegmentInfo.size(); i++)
    {
        mlstTaskSegmentInfo[i].pHttpSegment->PauseSegTask(true);       
        if (mlstTaskSegmentInfo[i].pThread->isRunning())
        {
            mlstTaskSegmentInfo[i].pThread->quit();
            mlstTaskSegmentInfo[i].pThread->wait();
        }
        mlstTaskSegmentInfo[i].pHttpSegment->Stop();
    }
    mpTaskProber->Stop();
    mpDataStream->CloseStream();
    mDownloadInfo.state = HTS_PAUSED;
    mpDownloadMgr->SendMessage(mDownloadInfo.task_id, HTM_STATE, mDownloadInfo.state);
}

bool TDownloadTask::IsRuning()
{
    return mDownloadInfo.state & HTS_RUNNING;
}

int TDownloadTask::GetTaskID()
{
    return mDownloadInfo.task_id;
}

bool TDownloadTask::IsPause()
{
    return mDownloadInfo.state & HTS_PAUSED;
}

bool TDownloadTask::IsFinished()
{
    return mDownloadInfo.state & HTS_FINISHED;
}

bool TDownloadTask::IsWaiting()
{
    return mDownloadInfo.state & HTS_PREPARED;
}

bool TDownloadTask::IsFailed()
{
    return mDownloadInfo.state & HTS_FAILED;
}

void TDownloadTask::OnSegmentTaskFinished(int http_id, int code, QString errStr)
{
    if (code != QNetworkReply::NoError)
    {
        qDebug() << __FUNCTION__ << "Error : the segment task failed !" << http_id << "---code : " << code << errStr;
        StopTask();
        mDownloadInfo.state = HTS_FAILED;
        mpDownloadMgr->SendMessage(mDownloadInfo.task_id, HTM_STATE, mDownloadInfo.state);
    }
    else
    {
        int nIndex = FindByID(http_id);
        if (nIndex != -1)
        {
            if (mlstTaskSegmentInfo[nIndex].pThread->isRunning())
            {
                mlstTaskSegmentInfo[nIndex].pThread->quit();
                mlstTaskSegmentInfo[nIndex].pThread->wait();
            }
            mlstTaskSegmentInfo[nIndex].pThread->deleteLater();
            mlstTaskSegmentInfo[nIndex].pHttpSegment->deleteLater();
            mlstTaskSegmentInfo.removeAt(nIndex);
            mSegmentArea.remove(http_id);
        }
        else
        {
            qDebug() << __FUNCTION__ << "Error : can't find the segment task by the ID !" << http_id;
        }
        if (mlstTaskSegmentInfo.isEmpty())
        {
            mpDataStream->CloseStream();
            mpDownloadMgr->SendMessage(mDownloadInfo.task_id, HTM_PROGRESS, mDownloadInfo.percent);
            mDownloadInfo.state = HTS_FINISHED;
            mpDownloadMgr->SendMessage(mDownloadInfo.task_id, HTM_STATE, mDownloadInfo.state);
        }
    }
}

void TDownloadTask::OnSegmentDataRead(int http_id, qint64 length)
{
    mSegmentArea[http_id].startOffset += length;
    mDownloadInfo.transferedBytes += length;
    //qDebug() << __FUNCTION__ << http_id << length;
    int msgMask = 0;
    int percent = mDownloadInfo.transferedBytes * 100 / mDownloadInfo.totalBytes;

    if (mDownloadInfo.percent < percent) {
        mDownloadInfo.percent = percent;
        msgMask |= HTM_PROGRESS;
    }
    //qDebug() << __FUNCTION__ << http_id << length << percent << "%";

    if (msgMask & HTM_PROGRESS) {
        //qDebug() << __FUNCTION__ << "emit progress signals";
        mpDownloadMgr->SendMessage(mDownloadInfo.task_id, HTM_PROGRESS, mDownloadInfo.percent);
    }

    if (msgMask & HTM_STATE) {
        mpDownloadMgr->SendMessage(mDownloadInfo.task_id, HTM_STATE, mDownloadInfo.state);
    }
}

int TDownloadTask::FindByID(int http_id)
{
    for (int i = 0; i < mlstTaskSegmentInfo.size(); i++)
    {
        if (mlstTaskSegmentInfo[i].http_id == http_id)
        {
            return i;
        }
    }
    return -1;
}

void TDownloadTask::SaveTaskInfo(TDownloadInfo& info)
{
    QList<TSegmentArea> areaList = mSegmentArea.values();
    info.progressList.clear();
    for each (TSegmentArea area in areaList)
    {
        info.progressList.push_back(QString::number(area.startOffset));
        info.progressList.push_back(QString::number(area.endOffset));
        qDebug() << __FUNCTION__ << area.startOffset << area.endOffset;
    }
    info.transferedBytes = mDownloadInfo.transferedBytes;
    info.totalBytes = mDownloadInfo.totalBytes;
    info.task_id = mDownloadInfo.task_id;
}

void TDownloadTask::LoadTaskInfo(TDownloadInfo info)
{
    mDownloadInfo.state = HTS_PAUSED;
    mpDownloadMgr->SendMessage(mDownloadInfo.task_id, HTM_STATE, mDownloadInfo.state);
    if (mDownloadInfo.totalBytes > 0)
    {
        mDownloadInfo.percent = mDownloadInfo.transferedBytes * 100 / mDownloadInfo.totalBytes;
        mpDownloadMgr->SendMessage(mDownloadInfo.task_id, HTM_PROGRESS, mDownloadInfo.percent);
    }
    QStringList progressList = info.progressList;
    if (progressList.isEmpty() || progressList.size() % 2 != 0) {
        qDebug() << __FUNCTION__ << progressList.size();
        mDownloadInfo.state = HTS_UNKNOWN;
        return;
    }
    int index = 0;
    mSegmentArea.clear();
    mlstTaskSegmentInfo.clear();

    for (int i = 0; i < progressList.size(); i += 2)
    {
        qint64 beginOffset = progressList.at(i).toLongLong();
        qint64 endOffset = progressList.at(i + 1).toLongLong();

        mSegmentArea.insert(index, TSegmentArea(beginOffset, endOffset));
        THttpSegment* segmentTask = new THttpSegment(index, mDownloadInfo.strRemoteUrl, beginOffset, endOffset, mpDataStream);
        connect(segmentTask, SIGNAL(sigSegmentDataTransferred(int, qint64)), this, SLOT(OnSegmentDataRead(int, qint64)));
        connect(segmentTask, SIGNAL(sigSegmentTaskFinished(int, int, QString)), this, SLOT(OnSegmentTaskFinished(int, int, QString)));
        TSegtaskInfo info;
        info.http_id = index;
        info.pHttpSegment = segmentTask;
        info.pThread = new QThread();
        info.pHttpSegment->moveToThread(info.pThread);
        mlstTaskSegmentInfo.append(info);
        index++;
        mnThreadNum = index;
    }

    mDownloadInfo.percent = mDownloadInfo.transferedBytes * 100 / mDownloadInfo.totalBytes;
    mDownloadInfo.state = HTS_PAUSED;
    mpDownloadMgr->SendMessage(mDownloadInfo.task_id, HTM_STATE, mDownloadInfo.state);
    mpDownloadMgr->SendMessage(mDownloadInfo.task_id, HTM_PROGRESS, mDownloadInfo.percent);
    if (index == 0)
    {
        mDownloadInfo.state = HTS_PREPARED;
    }
}
void TDownloadTask::SetDownloadState(EHttpTaskState state)
{
    mDownloadInfo.state = state | HTS_PAUSED;
    mpDownloadMgr->SendMessage(mDownloadInfo.task_id, HTM_STATE, state);
}
