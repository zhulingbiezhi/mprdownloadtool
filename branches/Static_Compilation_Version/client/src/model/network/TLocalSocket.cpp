#include "TLocalSocket.h"

TLocalSocket::TLocalSocket(const QString&servername, QObject *parent)
: QObject(parent)
, mpLocalSocket(NULL)
{
    mServerName = servername;
}

TLocalSocket::~TLocalSocket()
{
    if (mpLocalSocket)
    {
        mpLocalSocket->abort();
        delete mpLocalSocket;
        mpLocalSocket = NULL;
    }
}
bool TLocalSocket::ConnectToServer()
{
    if (mpLocalSocket == NULL)
    {
        mpLocalSocket = new QLocalSocket(this);
    }

    mpLocalSocket->abort();
    mpLocalSocket->setServerName(mServerName);

    for (int i = 0; i < 5; i++)
    {
        if (!mpLocalSocket->open(QIODevice::ReadWrite))
        {
            QThread::msleep(300);
        }
        else
        {
            connect(mpLocalSocket, &QLocalSocket::readyRead, this, &TLocalSocket::OnReadyRead);
            connect(mpLocalSocket, SIGNAL(error(QLocalSocket::LocalSocketError)), this, SLOT(OnDisplayError(QLocalSocket::LocalSocketError)));

            qDebug() << __FUNCTION__ << "connect server success!";
            return true;
        }
    }

    qDebug() << __FUNCTION__ << "connect server failure!";
    return false;
}

void TLocalSocket::OnReadyRead()
{
    QDataStream in(mpLocalSocket);
    in.setVersion(QDataStream::Qt_4_0);

    if (mpLocalSocket->bytesAvailable() < (int)sizeof(quint16))
    {
        return;
    }

    in >> mBlockSize;

    if (in.atEnd())
    {
        return;
    }

    QString nextFortune;
    in >> nextFortune;
    qDebug() << __FUNCTION__ << nextFortune;

    emit readUpdateServerMess(nextFortune);
}

void TLocalSocket::OnDisplayError(QLocalSocket::LocalSocketError socketError)
{
    switch (socketError)
    {
    case QLocalSocket::ConnectionRefusedError:
        qDebug() << __FUNCTION__ << QString("The connection was refused by the peer. Make sure the fortune server is running.");
        break;
    case QLocalSocket::PeerClosedError:
        break;
    default:
        qDebug() << __FUNCTION__ << QString("The following error occurred: %1.").arg(mpLocalSocket->errorString());
        break;
    }
}