#ifndef _TMPRSEARCHREPLEDINFO_H_
#define _TMPRSEARCHREPLEDINFO_H_

class TMPRSearchRepliedInfo
{
public:
    QString ticket;
    QString language;
    QString size;
    QString brandId;
    QString goodsId;
    QString goodsName;
    QString publicationName;
    QString publishing;
    QString languageId;
    QString publisher;
    QString fileFormat;
    QString producer;
    QString downloadId;
    QString versionCode;    //�汾��
    QString author;
    QString mprPrefixCode;
    QString productType;
    QString uuid;
    QString isMPR;
    QString isDiy;
    QString productImgUrl;
    QString timeBuying;

    QString interfaceUrl;
};

class TNetMPRUrl
{
public:
    QString uuid;
    QString mprUrl;
    QString licenseXml;
    QString msg;
};

#endif