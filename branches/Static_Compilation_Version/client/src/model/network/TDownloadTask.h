#ifndef _TDOWNLOADTASK_H_
#define _TDOWNLOADTASK_H_

#include "THttpTask.h"
#include "TFileEnum.h"
#include <QThread>
#include <QMap>

class TDownloadManager;
class TDownloadTaskProber;


typedef struct  _TSegmentArea
{
    qint64 startOffset;
    qint64 endOffset;
    _TSegmentArea()
    {

    }
    _TSegmentArea(const qint64& start, const qint64& end)
    {
        startOffset = start;
        endOffset = end;
    }
}TSegmentArea;

typedef struct _TDownloadInfo
{
    int         task_id;
    int         percent;
    int         state;
    qint64      totalBytes;
    qint64      transferedBytes;
    QString     strRemoteUrl;
    QString     strTaskRef;
    QString     strSavePath;
    QString     strDeviceID;
    QString     strDeviceSN;
    QStringList progressList;
    TMPRFileInfoItem   itemInfo;
    _TDownloadInfo()
    {
        task_id = -1;
        percent = 0;
        totalBytes = 0;
        transferedBytes = 0;
        state = HTS_UNKNOWN;
        strRemoteUrl = "";
        strTaskRef = "";
        strSavePath = "";
        strDeviceID = "";
        strDeviceSN = "";
        progressList.clear();
    }  
}TDownloadInfo;


typedef struct _TSegtaskInfo
{
    int             http_id;
    QThread*        pThread;
    THttpSegment*   pHttpSegment;
    bool operator==(const _TSegtaskInfo& info)
    {
        return (http_id == info.http_id) && (pThread == info.pThread) && (pHttpSegment == info.pHttpSegment);
    }
    _TSegtaskInfo()
    {
        http_id = -1;
        pThread = nullptr;
        pHttpSegment = nullptr;
    }
}TSegtaskInfo;

class TDownloadTask : public QObject 
{
    Q_OBJECT
public:
    TDownloadTask(TDownloadManager* pDownloadMgr, int task_id, int threadNum, TDownloadInfo downloadInfo);
    ~TDownloadTask();

    int GetTaskID();
    void StartTask();
    void StopTask();
    bool IsRuning();
    bool IsPause();
    bool IsWaiting();
    bool IsFinished();
    bool IsFailed();
    void RestartTask();
    void SaveTaskInfo(TDownloadInfo& info);
    void LoadTaskInfo(TDownloadInfo info);
    void SetDownloadState(EHttpTaskState state);

private:
    int              FindByID(int http_id);
    void             StartFromBeginning();

public slots:
    void    OnSegmentTaskFinished(int http_id, int code, QString errStr);
    void    OnSegmentDataRead(int http_id, qint64 length);
    void    OnProbeTaskFinished(int code, QString errStr);
private:   
    int                     mnThreadNum;
    TDownloadInfo           mDownloadInfo;
    TDataStream*            mpDataStream;
    TDownloadManager*       mpDownloadMgr;
    QList<TSegtaskInfo>     mlstTaskSegmentInfo;
    QMap<int, TSegmentArea> mSegmentArea;
    TDownloadTaskProber*    mpTaskProber;
};

#endif