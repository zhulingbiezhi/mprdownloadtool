#include "THttpTask.h"
#include "TDataStream.h"

const qint64 MIN_WRITE_SIZE = 0;
#define  DOWNLOAD_SECTION_SIZE (512*1024)
#define  MAX_RETRY_COUNT 3
const qint64 SEG_SIZE = 1024 * 1024;

THttpSegment::THttpSegment(int http_id, QString strUrl, qint64 qStartPos, qint64 qEndPos, TDataStream* pDataStream)
: mReply(nullptr)
, mNAM(nullptr)
, mBufferdData("")
, mStartOffset(qStartPos)
, mEndOffset(qEndPos)
, mWriteOffset(mStartOffset)
, mHttpID(http_id)
, mstrUrl(strUrl)
, mnRetryCounts(0)
, mpDataStream(pDataStream)
, mbPaused(false)
{
    
}

void THttpSegment::Start()
{
    if (mbPaused)
    {
        return;
    }
    else if (!mNAM)
    {
        mNAM = new QNetworkAccessManager();
    }
    qint64 qReaderSize = DOWNLOAD_SECTION_SIZE;
    qint64 qRemaindSize = mEndOffset - mStartOffset + 1;
    if (qRemaindSize < qReaderSize)
    {
        qReaderSize = qRemaindSize;
    }
    QNetworkRequest request(mstrUrl);
    request.setRawHeader("User-Agent", "IHttpUpDownloader");
    request.setRawHeader("Range", QString("bytes=%1-%2").arg(mStartOffset).arg(mStartOffset + qReaderSize - 1).toUtf8());
    request.setRawHeader("Accept-Encoding", "*");
    Stop();
    //qDebug() << __FUNCTION__ << QString::number(::GetCurrentThreadId(),16) << "start :" << mStartOffset << "end :" << mEndOffset;
    if (request.url().scheme().toLower() == "https")
    {
        QSslConfiguration conf = request.sslConfiguration();
        conf.setPeerVerifyMode(QSslSocket::VerifyNone);
        conf.setProtocol(QSsl::TlsV1SslV3);
        request.setSslConfiguration(conf);
    }
    mReply = mNAM->get(request);
    if (mReply) {
        mReply->ignoreSslErrors();
        mHttpState = HTS_RUNNING;
        connect(mReply, SIGNAL(finished()), this, SLOT(OnSegmentTaskFinished()));
        connect(mReply, SIGNAL(readyRead()), this, SLOT(OnSegmentTaskReadyRead()));
    }
}
void THttpSegment::OnSegmentTaskReadyRead()
{
    QObject* reply = sender();
    if (reply != mReply) {
        reply->deleteLater();
        return;
    }
}

void THttpSegment::OnSegmentTaskFinished()
{
    QObject* reply = sender();
    if (reply != mReply) {
        reply->deleteLater();
        return;
    }

    if (mReply)
    {
        int error = mReply->error();
        QString errorStr = mReply->errorString();
        if (error == QNetworkReply::NoError)
        {
            if (!ReadReplyData())
            {
                mnRetryCounts++;
            }
            else
            {
                mnRetryCounts = 0;
            }
            if (mStartOffset < mEndOffset && mnRetryCounts < MAX_RETRY_COUNT)
            {
                Start();
                return;
            }
            else if (mStartOffset < mEndOffset && mnRetryCounts > MAX_RETRY_COUNT)
            {
                mHttpState = HTS_FAILED;
            }
            else
            {
                if (mBufferdData.size() > 0)
                {
                    qint64 buffedSize = mBufferdData.size();
                    mpDataStream->WriteData(mWriteOffset, mBufferdData);
                    mWriteOffset += buffedSize;
                    emit sigSegmentDataTransferred(mHttpID, buffedSize);
                    mBufferdData.clear();
                }

            }
        }
        else
        {
            if (!mbPaused && mnRetryCounts < MAX_RETRY_COUNT)
            {
                mnRetryCounts++;
                qDebug() << __FUNCTION__ << "retry download :" << mnRetryCounts << mStartOffset << mEndOffset << errorStr;
                QThread::msleep(500 * mnRetryCounts);
                Start();
                return;
            }
        }

        mReply->deleteLater();
        mReply = nullptr;
        emit sigSegmentTaskFinished(mHttpID, error, errorStr);
    }
}

bool THttpSegment::ReadReplyData()
{
    if (mReply && !mbPaused) 
    {
        if (mReply->bytesAvailable())
        {            
            QByteArray readData = mReply->readAll();
            qint64 readSize = readData.size();
            if (readSize > 0)
            {
                mBufferdData.append(readData);              
                if (mBufferdData.size() > MIN_WRITE_SIZE)
                {
                    qint64 buffedSize = mBufferdData.size();
                    mpDataStream->WriteData(mWriteOffset, mBufferdData);
                    mWriteOffset += buffedSize;
                    emit sigSegmentDataTransferred(mHttpID, buffedSize);
                    mBufferdData.clear();
                }
                mStartOffset += readSize;
            }
            else
            {
                return false;
            }
        }
    }
    return true;
}

void THttpSegment::Stop()
{
    if (mReply)
    {
        disconnect(mReply, SIGNAL(finished()), this, SLOT(OnSegmentTaskFinished()));
        disconnect(mReply, SIGNAL(readyRead()), this, SLOT(OnSegmentTaskReadyRead()));
        if (mReply->isRunning()) {
            mReply->abort();
        }
        mReply->deleteLater();
        mReply = nullptr;
    }

}

void THttpSegment::PauseSegTask(bool bPause)
{
    mbPaused = bPause;
}
