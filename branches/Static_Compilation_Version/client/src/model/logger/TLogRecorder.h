#ifndef TLOGRECORDER_H_
#define TLOGRECORDER_H_


class TLogRecorder
{
 private:
    TLogRecorder();
    ~TLogRecorder();

    static void MsgHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg);

public:
    static  TLogRecorder& Instance();
    void    StartRecord();
    void    HandleMsg(QtMsgType type, const QMessageLogContext &context, const QString &msg);

private:
    QFile*          mpFile;
    QTextStream*    mpTextStream;
    QMutex*         mpMutex;
    QtMessageHandler mOriMsgHandler;
};

#endif