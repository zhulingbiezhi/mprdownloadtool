#ifndef __TFILELIBLOCAL_H_
#define __TFILELIBLOCAL_H_

#include "TFileEnum.h"
#include "TFileLibDescXml.h"
#include "TDistLibDescXml.h"


class TFileLib 
{
public: 
    TFileLib(QString strFileLibDir, QObject *parent = NULL);
    ~TFileLib();
    
    //查找格式只支持： MP3 OGG MPRX三种基本格式
    QList<TMPRFileInfoItem>     FetchFilesByBookName(QString strBookName, FileType format);
    QList<TMPRFileInfoItem>     FetchFilesByPrefixCode(QString prefixCode, FileType format);
    QList<TMPRFileInfoItem>     FetchFilesByFileRef(QString strFileRef, FileType format);
    QList<TMPRFileInfoItem>     FetchLocalFilesAll();

    QList<TTaskInfo>            FetchAllTasks(QString deviceID);

    bool                        UpdateFileInfo(TMPRFileInfoItem &);
    bool                        UpdateTaskInfo(TTaskInfo infoItem);
    bool                        UpdateTaskInfo(QList<TTaskInfo> infoList);
    void                        DeleteTaskInfo(TTaskInfo infoItem);
    QString                     GetFileLibDir() const;

private:
    void                        ConstructDir();

    void                        InitData();
    void                        CheckData();

    TFileLibDescXml*            GetMPRHandle(FileType format = FT_MPR);
    

private: //variable
    TFileLibDescXml*            mDescMPRHandle;
    TFileLibDescXml*            mDescMPRXHandle;
    TTaskDescXml*               mDescTaskHandle;

    QString                     mstrMPRDataDir;
    QString                     mstrMPRXDataDir;
    QString                     mstrFileLibDir;
};

#endif //__TFILELIBLOCAL_H_