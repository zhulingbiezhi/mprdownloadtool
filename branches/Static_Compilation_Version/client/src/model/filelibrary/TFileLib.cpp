#include "TFileLib.h"
#include "TGlobleCtl.h"
#include "TCommonHelper.h"


#include <QDir>
#include <QLinkedListIterator>
#include <QLinkedList>
#include <QHash>
#include <QFileInfo>

TFileLib::TFileLib( QString strFileLibDir, QObject *)
: mDescMPRHandle(nullptr)
, mDescMPRXHandle(nullptr)
, mstrFileLibDir(strFileLibDir)
{
    QString strDir = QDir::fromNativeSeparators(mstrFileLibDir);
    QDir dir(strDir);
    if (!dir.exists()) {
        dir.mkpath(strDir);
    }

    ConstructDir();
    InitData();
}

TFileLib::~TFileLib()
{
    if (NULL != mDescMPRHandle){
        delete mDescMPRHandle;
        mDescMPRHandle = NULL;
    }
    if (NULL != mDescMPRXHandle){
        delete mDescMPRXHandle;
        mDescMPRXHandle = NULL;
    }
}
 
void TFileLib::ConstructDir()
{
    //mpr ogg lib
    QString strMPRDataDir = mstrFileLibDir + gMPRDirName;
    QDir mprDir(strMPRDataDir);
    if (!mprDir.exists()){
        mprDir.mkpath(strMPRDataDir);
    }
    mstrMPRDataDir = strMPRDataDir;


    QString strMPRXDataDir =mstrFileLibDir + gMPRXDirName;
    QDir mprxDir(strMPRXDataDir);
    if (!mprxDir.exists()){
        mprxDir.mkpath(strMPRXDataDir);
    }
    mstrMPRXDataDir = strMPRXDataDir;
}

void TFileLib::InitData()
{
    //Paser MPR Description XML , file format is mp3
    mDescMPRHandle = new TFileLibDescXml(FT_MPR, mstrMPRDataDir);
    //Paser MPRX Description XML
    mDescMPRXHandle = new TFileLibDescXml(FT_MPRX, mstrMPRXDataDir);
    //Paser TASK Description XML
    mDescTaskHandle = new TTaskDescXml(mstrMPRDataDir);
}

QList<TMPRFileInfoItem> TFileLib::FetchFilesByBookName( QString strKey, FileType format)
{
    QList<TMPRFileInfoItem> fileLists;

    if (strKey.isEmpty()){
        return fileLists;
    }

    if (NULL == mDescMPRXHandle || NULL == mDescMPRHandle){
        return fileLists;
    }

    if (FT_MPR & format)
        fileLists += mDescMPRHandle->QueryByBookName(strKey);

    if (FT_MPRX & format)
        fileLists += mDescMPRXHandle->QueryByBookName(strKey);

    return fileLists;
}

QList<TMPRFileInfoItem> TFileLib::FetchFilesByPrefixCode( QString strKey, FileType format)
{
    QList<TMPRFileInfoItem> fileLists;

    if (strKey.isEmpty()){
        return fileLists;
    }

    if (NULL == mDescMPRHandle || NULL == mDescMPRXHandle){
        return fileLists;
    }


    if (FT_MPR & format)
        fileLists += mDescMPRHandle->QueryByPrefixCode(strKey);

    if (FT_MPRX & format)
        fileLists += mDescMPRXHandle->QueryByPrefixCode(strKey);

    return fileLists;
}


QList<TMPRFileInfoItem> TFileLib::FetchFilesByFileRef( QString strKey, FileType format )
{
    QList<TMPRFileInfoItem> fileLists;

    if (strKey.isEmpty()){
        return fileLists;
    }

    if (NULL == mDescMPRHandle || NULL == mDescMPRXHandle){
        return fileLists;
    }

    if (FT_MPR & format)
        fileLists += mDescMPRHandle->QueryByFileRef(strKey);

    if (FT_MPRX & format)
        fileLists += mDescMPRXHandle->QueryByFileRef(strKey);

    return fileLists;
}

bool TFileLib::UpdateFileInfo(TMPRFileInfoItem &infoItem)
{
    if (infoItem.tPrefixCode.isEmpty()
        || infoItem.tVersion.isEmpty()
        || infoItem.tBrandID.isEmpty()){
        qDebug() << "prefixCode is null, data is invalid!" << infoItem.tFilePath;
        return false;
    }

    if (NULL == mDescMPRHandle || NULL == mDescMPRXHandle){
        return false;
    }

    QFileInfo fileInfo(infoItem.tFilePath);
    if (!fileInfo.exists()){
        qDebug()<< "the file is not exist, don't add to xml desc"<<infoItem.tFilePath;
        return false;
    }

    QFileInfo licenceInfo(infoItem.tLicencePath);
    if (!licenceInfo.exists()){
        qDebug()<< "the licence file is not exist, don't add to xml desc"<<infoItem.tLicencePath;
        return false;
    }

    if (infoItem.tFileType == FT_MPR){
        mDescMPRHandle->UpdateFileInfo(infoItem);
        mDescMPRHandle->SaveToXMLFile();

    } else if(infoItem.tFileType == FT_MPRX){
        mDescMPRXHandle->UpdateFileInfo(infoItem);
        mDescMPRXHandle->SaveToXMLFile();
    } else {
        qDebug()<< __FILE__ << __LINE__ <<"error";
    }
    return true;
}


bool TFileLib::UpdateTaskInfo(TTaskInfo infoItem)
{
    if (infoItem.strRemoteUrl.isEmpty())
    {
        qDebug() << "task data is invalid!";
        return false;
    }
    if (mDescTaskHandle == NULL)
    {
        return false;
    }
    mDescTaskHandle->UpdateTaskInfo(infoItem);
    mDescTaskHandle->SaveToXMLFile();
    return true;
}

bool TFileLib::UpdateTaskInfo(QList<TTaskInfo> infoList)
{
//     if (infoList.isEmpty())
//     {
//         qDebug() << "tasklist  is empty!";
//         return false;
//     }
    if (mDescTaskHandle == NULL)
    {
        return false;
    }
    mDescTaskHandle->UpdateTaskInfo(infoList);
    mDescTaskHandle->SaveToXMLFile();
    return true;
}

QString TFileLib :: GetFileLibDir() const
{
    return mstrMPRDataDir;
}

QList<TMPRFileInfoItem> TFileLib::FetchLocalFilesAll()
{
    QList<TMPRFileInfoItem> fileLists;
    if (NULL == mDescMPRHandle || NULL == mDescMPRXHandle){
        return fileLists;
    }

    fileLists += mDescMPRHandle->QueryAll();
    fileLists += mDescMPRXHandle->QueryAll();

    return fileLists;
}

QList<TTaskInfo> TFileLib::FetchAllTasks(QString deviceID)
{
    QList<TTaskInfo> taskLists;
    taskLists = mDescTaskHandle->QueryAll(deviceID);
    return taskLists;
}

void TFileLib::DeleteTaskInfo(TTaskInfo infoItem)
{
    mDescTaskHandle->DeleteTaskInfo(infoItem);
    mDescTaskHandle->SaveToXMLFile();
}

