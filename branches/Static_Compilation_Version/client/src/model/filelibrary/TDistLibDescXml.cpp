#include "TDistLibDescXml.h"
#include "TGlobleCtl.h"

#define  XML_TASK_VERSION 2

TTaskDescXml::TTaskDescXml(QString filePath)
: mdomXmlDoc(NULL)
{
    mstrInfoDir = QDir::fromNativeSeparators(filePath);
    InitFile();
    InitData();
}

TTaskDescXml::~TTaskDescXml()
{
    mTaskHash.clear();
    if (NULL != mdomXmlDoc){
        delete mdomXmlDoc;
        mdomXmlDoc = NULL;
    }
}


void TTaskDescXml::InitFile()
{
    mstrDescFilePath = mstrInfoDir + gDescXmlTaskName;
    QFile file(mstrDescFilePath);
    if (file.exists(mstrDescFilePath)){
        return;
    }

    QString strBackupDescFilePath = mstrInfoDir + gDescXmlTaskName + ".bak";
    QFile backupFile(strBackupDescFilePath);
    if (backupFile.exists(strBackupDescFilePath)){
        backupFile.copy(strBackupDescFilePath, mstrDescFilePath);
        return;
    }

    file.open(QIODevice::WriteOnly);
    file.close();

    AddDescHeader();
    ReWriteXmlFile();
}

void TTaskDescXml::InitData()
{
    QFile file(mstrDescFilePath);
    if (!file.open(QIODevice::ReadOnly)){
        qDebug() << "open file error:" << mstrDescFilePath;
        return;
    }

    if (NULL != mdomXmlDoc){
        delete mdomXmlDoc;
        mdomXmlDoc = NULL;
    }

    mdomXmlDoc = new QDomDocument(mstrDescFilePath);
    if (!mdomXmlDoc->setContent(&file)) {
        qDebug() << "desc-xml file is not complete:" << mstrDescFilePath;
        file.close();
        return;
    }

    file.close();
    ParseDescXml();
    QString strBackupDescFilePath = mstrDescFilePath + ".bak";
    QFile::remove(strBackupDescFilePath);
    QFile::copy(mstrDescFilePath, strBackupDescFilePath);

    SaveToXMLFile();
    if (NULL != mdomXmlDoc){
        delete mdomXmlDoc;
        mdomXmlDoc = NULL;
    }
    return;
}

void TTaskDescXml::ClearData()
{
    mTaskHash.clear();
}

void TTaskDescXml::ParseDescXml()
{
    mTaskHash.clear();

    QDomElement docElem = mdomXmlDoc->documentElement();
    QDomNode n = docElem.firstChild();
    while (!n.isNull()) 
    {
        QDomElement e = n.toElement();
        if (!e.isNull() && xmlTasks == e.tagName()){
            ParseDescInfo(e);   //parse files
        }
        else if (!e.isNull() && xmlSoftwareVersion == e.tagName())
        {
            QDomElement ee = e.firstChild().toElement();
            if (ee.isNull() || xmlSoftwareItem != ee.tagName())
            {
                break;
            }
            else
            {
               if (ee.text().toInt() < XML_TASK_VERSION) {
                   break;
               }
            }
        }
        n = n.nextSibling();
    }
}

void TTaskDescXml::ParseDescInfo(QDomElement docElem)
{
    QDomNode n = docElem.firstChild();
    while (!n.isNull()) {
        QDomElement e = n.toElement();
        if (e.isNull() || xmlTaskItem != e.tagName()){
            n = n.nextSibling();
            continue;
        }

        TTaskInfo tmpItem;
        QDomNode t = e.firstChild();
        while (!t.isNull()){
            QDomElement te = t.toElement();
       
            if (xmlTaskState == te.tagName()){
                tmpItem.taskState = (TTaskState)te.text().toInt();

            }
            else if (xmlRemoteUrl == te.tagName()){
                tmpItem.strRemoteUrl = te.text();

            }
            else if (xmlTotalDownloadBytes == te.tagName()){
                tmpItem.totalDownloadBytes = te.text().toLongLong();

            }
            else if (xmlTransferdBytes == te.tagName()){
                tmpItem.transferedBytes = te.text().toLongLong();

            }
            else if (xmlByteCopyTotal == te.tagName()){
                tmpItem.byteCopyTotal = te.text().toLongLong();

            }
            else if (xmlByteCopied == te.tagName()){
                tmpItem.byteCopied = te.text().toLongLong();

            }
            else if (xmlSegOffsetList == te.tagName()){
                tmpItem.progressList.clear();
                tmpItem.progressList = te.text().split('|');

            }
            else if (xmlPrefixCode == te.tagName()){
                tmpItem.mprInfo.tPrefixCode = te.text();

            }
            else if (xmlFileUuid == te.tagName()){
                tmpItem.mprInfo.tFileUuid = te.text();

            }
            else if (xmlBookName == te.tagName()){
                tmpItem.mprInfo.tBookName = te.text();

            }
            else if (xmlPublisher == te.tagName()){
                tmpItem.mprInfo.tPublisher = te.text();

            }
            else if (xmlFilePath == te.tagName()){
                tmpItem.mprInfo.tFilePath = te.text();

            }
            else if (xmlMprVersion == te.tagName()){
                tmpItem.mprInfo.tVersion = te.text();

            }
            else if (xmlLicenceUuid == te.tagName()){
                tmpItem.mprInfo.tLiceceUuid = te.text();

            }
            else if (xmlLicenceBrandID == te.tagName()){
                tmpItem.mprInfo.tBrandID = te.text();

            }
            else if (xmlLicencePath == te.tagName()){
                tmpItem.mprInfo.tLicencePath = te.text();

            }   
            else if (xmlLicenceDeviceID == te.tagName()){
                tmpItem.strDeviceID = te.text();

            }
            else if (xmlLicenceDeviceSN == te.tagName()){
                tmpItem.strDeviceSN = te.text();

            }
            else{

            }
            t = t.nextSibling();
        }

        QFileInfo tmpFileInfo(tmpItem.mprInfo.tFilePath);
        QFileInfo tmpLicenceInfo(tmpItem.mprInfo.tLicencePath);
        if ((tmpFileInfo.exists() && tmpLicenceInfo.exists()) || tmpItem.taskState == HTS_UNKNOWN)
        {
            QString taskRef = tmpItem.mprInfo.tFileUuid + "_" + tmpItem.strDeviceSN;
            mTaskHash.insert(taskRef, tmpItem);          
        }
        n = n.nextSibling();
    }
}

void TTaskDescXml::AddDescHeader()
{
    if (NULL != mdomXmlDoc){
        delete mdomXmlDoc;
        mdomXmlDoc = NULL;
    }
    mdomXmlDoc = new QDomDocument("myTaskDescFile");
    QDomElement domRoot = mdomXmlDoc->createElement(xmlRoot);
    QDomElement domTask = mdomXmlDoc->createElement(xmlTasks);
    QDomElement domVersion = mdomXmlDoc->createElement(xmlSoftwareVersion);

    domRoot.appendChild(domVersion);
    domRoot.appendChild(domTask);
    mdomXmlDoc->appendChild(domRoot);

    QDomNode n = domRoot.firstChild();
    QDomElement e = n.toElement();
    if (!e.isNull() && xmlSoftwareVersion == e.tagName())
    {
        InsertChild(e, xmlSoftwareItem, QString::number(XML_TASK_VERSION));
    }

}

bool TTaskDescXml::AddDescInfo(QDomElement *docElem, TTaskInfo infoItem)
{
    if (NULL == mdomXmlDoc){
        qDebug() << "xml parse error!";
        return false;
    }
    QDomElement tmpItem = mdomXmlDoc->createElement(xmlTaskItem);
    QString taskRef = infoItem.mprInfo.tFileUuid + "_" + infoItem.strDeviceSN;
    InsertChild(tmpItem, xmlTaskRef, taskRef);
    InsertChild(tmpItem, xmlTaskState, QString::number(infoItem.taskState));
    InsertChild(tmpItem, xmlRemoteUrl, infoItem.strRemoteUrl);
    InsertChild(tmpItem, xmlTotalDownloadBytes, QString::number(infoItem.totalDownloadBytes, 10));
    InsertChild(tmpItem, xmlTransferdBytes, QString::number(infoItem.transferedBytes, 10));
    InsertChild(tmpItem, xmlByteCopyTotal, QString::number(infoItem.byteCopyTotal, 10));
    InsertChild(tmpItem, xmlByteCopied, QString::number(infoItem.byteCopied, 10));
    QString segList = infoItem.progressList.join('|');
    InsertChild(tmpItem, xmlSegOffsetList, segList);
    InsertChild(tmpItem, xmlPrefixCode, infoItem.mprInfo.tPrefixCode);
    InsertChild(tmpItem, xmlFileUuid, infoItem.mprInfo.tFileUuid);
    InsertChild(tmpItem, xmlBookName, infoItem.mprInfo.tBookName);
    InsertChild(tmpItem, xmlPublisher, infoItem.mprInfo.tPublisher);
    InsertChild(tmpItem, xmlFilePath, infoItem.mprInfo.tFilePath);
    InsertChild(tmpItem, xmlMprVersion, infoItem.mprInfo.tVersion);
    InsertChild(tmpItem, xmlLicenceUuid, infoItem.mprInfo.tLiceceUuid);
    InsertChild(tmpItem, xmlLicenceBrandID, infoItem.mprInfo.tBrandID);
    InsertChild(tmpItem, xmlLicencePath, infoItem.mprInfo.tLicencePath);
    InsertChild(tmpItem, xmlLicenceDeviceID, infoItem.strDeviceID);
    InsertChild(tmpItem, xmlLicenceDeviceSN, infoItem.strDeviceSN);

    docElem->appendChild(tmpItem);
    return true;
}

void TTaskDescXml::InsertChild(QDomElement& tmpItem, QString xml, QString xmlData)
{
    QDomElement item = mdomXmlDoc->createElement(xml);
    QDomText valueText = mdomXmlDoc->createTextNode(xmlData);
    item.appendChild(valueText);
    tmpItem.appendChild(item);
}

void TTaskDescXml::ReWriteXmlFile()
{
    if (NULL == mdomXmlDoc){
        qDebug() << "domdocument is null.";
        return;
    }

    QFile file(mstrDescFilePath);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Truncate | QIODevice::Text)){
        return;
    }

    QTextStream out(&file);

    out.setCodec("UTF-8");

    mdomXmlDoc->save(out, 4, QDomNode::EncodingFromTextStream);

    file.close();
}

void TTaskDescXml::SaveToXMLFile()
{
    AddDescHeader();
    QHashIterator<QString, TTaskInfo> it(mTaskHash);
    while (it.hasNext()){
        TTaskInfo infoItem = it.next().value();
        QDomElement docElem = mdomXmlDoc->documentElement();
        QDomNode n = docElem.firstChild();
        while (!n.isNull()) {
            QDomElement e = n.toElement();
            if (!e.isNull() && xmlTasks == e.tagName()){
                AddDescInfo(&e, infoItem);
            }
            n = n.nextSibling();
        }
    }

    ReWriteXmlFile();
}

QList<TTaskInfo> TTaskDescXml::QueryAll(QString deviceID)
{
    QList<TTaskInfo> taskList;
    QHashIterator<QString, TTaskInfo> it(mTaskHash);
    while (it.hasNext()){
        TTaskInfo tmpItem = it.next().value();
        if (tmpItem.strDeviceID == deviceID)
        {
            taskList.push_back(tmpItem);
        }       
    }
    return taskList;
}

void TTaskDescXml::UpdateTaskInfo(TTaskInfo infoItem)
{
    QString taskRef = infoItem.mprInfo.tFileUuid + "_" + infoItem.strDeviceSN;
    TTaskInfo hashItem = mTaskHash.value(taskRef);
    if (hashItem.strRemoteUrl.isEmpty())
    {
        mTaskHash.insert(taskRef, infoItem);
    }
    else
    {
        mTaskHash.remove(taskRef);
        mTaskHash.insert(taskRef, infoItem);
    }
}

void TTaskDescXml::UpdateTaskInfo(QList<TTaskInfo> infoList)
{ 
    mTaskHash.clear();
    for (int i = 0; i < infoList.size(); i++)
    {
        TTaskInfo infoItem = infoList.at(i);
        QString taskRef = infoItem.mprInfo.tFileUuid + "_" + infoItem.strDeviceSN;
        mTaskHash.insert(taskRef, infoItem);
    }
}

void TTaskDescXml::DeleteTaskInfo(TTaskInfo infoItem)
{
    QString taskRef = infoItem.mprInfo.tFileUuid + "_" + infoItem.strDeviceSN;
    TTaskInfo hashItem = mTaskHash.value(taskRef);
    if (!hashItem.strRemoteUrl.isEmpty())
    {
        mTaskHash.remove(taskRef);
    }
}
