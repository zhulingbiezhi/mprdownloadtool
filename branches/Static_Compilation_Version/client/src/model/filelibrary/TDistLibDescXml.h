#ifndef __TDISTASKFILEPARSER_H_
#define __TDISTASKFILEPARSER_H_

#include "TTaskManager.h"
#include "TFileEnum.h"
#include "TDownloadTask.h"
#include <QDomDocument>

const QString xmlRoot = "root";
const QString xmlTasks = "tasks";
const QString xmlTaskItem = "taskItem";

const QString xmlSoftwareVersion = "softVersionItem";
const QString xmlSoftwareItem = "softVersion";

const QString xmlTaskRef = "taskRef";
const QString xmlTaskState = "taskState";
const QString xmlRemoteUrl = "remoteUrl";
const QString xmlTotalDownloadBytes = "totalDownloadBytes";
const QString xmlTransferdBytes = "transferdBytes";
const QString xmlByteCopyTotal = "byteCopyTotal";
const QString xmlByteCopied = "byteCopied";
const QString xmlSegOffsetList = "segOffsetList";


//const QString xmlFileName = "fileName";
const QString xmlPrefixCode = "prefixCode";
//const QString xmlVersion = "version";
const QString xmlFileUuid = "fileUuid";
const QString xmlBookName = "bookName";
const QString xmlPublisher = "publisher";
//const QString xmlFileRef = "fileRef";
const QString xmlFilePath = "filePath";
const QString xmlMprVersion = "version";

//const QString xmlLicenceName = "lecenceName";
const QString xmlLicenceUuid = "licenceUuid";
const QString xmlLicenceBrandID = "licenceBrandid";
const QString xmlLicenceDeviceID = "licenceDeviceID";
const QString xmlLicenceDeviceSN = "licenceDeviceSN";
const QString xmlLicencePath = "licencePath";

class TTaskDescXml : public QObject
{
public:
    TTaskDescXml(QString filePath);
    ~TTaskDescXml();
    QList<TTaskInfo> QueryAll(QString deviceID);
    void SaveToXMLFile();
    void UpdateTaskInfo(TTaskInfo infoItem);
    void UpdateTaskInfo(QList<TTaskInfo> infoList);
    void DeleteTaskInfo(TTaskInfo infoItem);
    void ClearData();

private:
    void InitFile();
    void InitData();
    void ParseDescXml();
    void ParseDescInfo(QDomElement);
    void AddDescHeader();
    bool AddDescInfo(QDomElement *, TTaskInfo);
    void InsertChild(QDomElement& tmpItem, QString xml, QString xmlData);
    void ReWriteXmlFile();


private:
    QDomDocument*                   mdomXmlDoc;
    QHash<QString, TTaskInfo>      mTaskHash;       //prefixcode
    QString                         mstrInfoDir;
    QString                         mstrDescFilePath;
};

#endif