#ifndef __TDESCXMLHANDLE_H_
#define __TDESCXMLHANDLE_H_

#include "TFileEnum.h"
#include <QDomElement>

const QString xmlTagRoot = "root";
const QString xmlTagInfos = "infos";
const QString xmlTagInfoItem = "infoItem";

const QString xmlTagFileName = "fileName";
const QString xmlTagPrefixCode = "prefixCode";
const QString xmlTagVersion = "version";
const QString xmlTagFileUuid = "fileUuid";
const QString xmlTagBookName = "bookName";
const QString xmlTagPublisher = "publisher";
const QString xmlTagFileRef = "fileRef";

const QString xmlTagLicenceName = "licenceName";
const QString xmlTagLicenceUuid = "licenceUuid";
const QString xmlTagLicenceBrandID = "licenceBrandid";
//const QString xmlTagLicenceRef = "licenceRef";

class TFileLibDescXml
{
public: 
    TFileLibDescXml(FileType, QString filePath);
    ~TFileLibDescXml();

    bool  SaveToXMLFile();

    QList<TMPRFileInfoItem>   QueryByBookName(QString);
    QList<TMPRFileInfoItem>   QueryByPrefixCode(QString);
    QList<TMPRFileInfoItem>   QueryByFileRef(QString);
    QList<TMPRFileInfoItem>   QueryAll();

    bool UpdateFileInfo(TMPRFileInfoItem&);

private:
    void InitFile();
    void InitData();
    void CheckData();
    void ClearData();

    void QueryFiles(TInfoItem *);

private: //struct
    void InfoItem2XmlItem(TMPRFileInfoItem&, TInfoItem&);
    QString ToAvailableFileName(const QString& fileName);

    void UpdateInfoItem(TInfoItem&);
    //void UpdateLicenceItem(TLicencesItem);
    bool MoveFileData(QString, QString);
    bool DeleteFiles(QString);
    void FileRename( QString& filePath);

private: //xml
    void ParseDescXml();
    void ParseDescInfo(QDomElement);
    //void ParseDescLicense(QDomElement);
    void AddDescHeader();
    bool AddDescInfo(QDomElement *, TInfoItem);
    void MPRInfoItem2LocalInfo(TMPRFileInfoItem& mprItem,TInfoItem& infoItem);
    //bool AddDescLicence(QDomElement *, TLicencesItem);
    void ReWriteXmlFile();

private: //variable
    FileType                        mFileType;
    QString                         mstrInfoDir;
    QString                         mstrDescFilePath;

    QList<TMPRFileInfoItem>         mFetchList;

    QDomDocument*                   mdomXmlDoc;

    QHash<QString, TInfoItem*>      mInfoHash;       //prefixcode
    //QMultiHash<QString, TLicencesItem*>      mhashLicences;    //uuid
};

#endif //__TDESCXMLHANDLE_H_