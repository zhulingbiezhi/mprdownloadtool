#include "TFileLibDescXml.h"
#include "TGlobleCtl.h"

#include <QDir>
#include <QLinkedListIterator>
#include <QLinkedList>
#include <QHash>
#include <QFileInfo>
#include <Windows.h>

TFileLibDescXml::TFileLibDescXml(FileType fileType,  QString filePath )
    : mstrInfoDir("")
    , mstrDescFilePath("")
    , mFileType(fileType)
    , mdomXmlDoc(nullptr)
{
    mstrInfoDir = QDir::fromNativeSeparators(filePath);
    InitFile();
    InitData();
}

TFileLibDescXml::~TFileLibDescXml()
{
    ClearData();

    if (NULL != mdomXmlDoc){
        delete mdomXmlDoc;
        mdomXmlDoc = NULL;
    }
}

void TFileLibDescXml::InitFile()
{
    mstrDescFilePath = mstrInfoDir + gDescXmlFileName;
    QFile file(mstrDescFilePath);  
    if (file.exists(mstrDescFilePath)){
        return;
    }

    QString strBackupDescFilePath = mstrInfoDir + gDescXmlFileName + ".bak";
    QFile backupFile(strBackupDescFilePath);  
    if (backupFile.exists(strBackupDescFilePath)){
        backupFile.copy(strBackupDescFilePath, mstrDescFilePath);
        return;
    }

    file.open(QIODevice::WriteOnly);  
    file.close();

    AddDescHeader();
    ReWriteXmlFile();
}

void TFileLibDescXml::InitData()
{
    QFile file(mstrDescFilePath);
    if (!file.open(QIODevice::ReadOnly)){
        qDebug()<<"open file error:"<<mstrDescFilePath;
        return ;
    }

    if (NULL != mdomXmlDoc){
        delete mdomXmlDoc;
        mdomXmlDoc = NULL;
    }

    mdomXmlDoc = new QDomDocument(mstrDescFilePath);
    if (!mdomXmlDoc->setContent(&file)) {
        qDebug()<<"desc-xml file is not complete:"<<mstrDescFilePath;
        file.close();
        return ;
    }

    file.close();
    ParseDescXml();

    if(NULL != mdomXmlDoc){
        delete mdomXmlDoc;
        mdomXmlDoc = NULL;
    }

    QString strBackupDescFilePath = mstrDescFilePath + ".bak";
    QFile::remove(strBackupDescFilePath);
    QFile::copy(mstrDescFilePath, strBackupDescFilePath);
    return ;
}

void TFileLibDescXml::CheckData()
{
    //todo:Check the file is exist
}

void TFileLibDescXml::ClearData()
{
    QHashIterator<QString, TInfoItem *> it(mInfoHash);
    while(it.hasNext()){
        TInfoItem* tmpItem = it.next().value();
        if (NULL != tmpItem){
            delete tmpItem; 
            tmpItem = NULL;
        }
    }
    mInfoHash.clear();
}

void TFileLibDescXml::ReWriteXmlFile()
{
    if (NULL == mdomXmlDoc){
        qDebug()<<"domdocument is null.";
        return;
    }

    QFile file(mstrDescFilePath);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Truncate |QIODevice::Text)){
        return;  
    }

    QTextStream out(&file);  

    out.setCodec("UTF-8");  

    mdomXmlDoc->save(out,4,QDomNode::EncodingFromTextStream);  

    file.close();
}

void TFileLibDescXml::ParseDescXml()
{
    ClearData();

    QDomElement docElem = mdomXmlDoc->documentElement();
    QDomNode n = docElem.firstChild();
    while (!n.isNull()) {
        QDomElement e = n.toElement(); 
        if (!e.isNull() && xmlTagInfos == e.tagName()){
            ParseDescInfo(e);   //parse files
        }
        n = n.nextSibling();
    }
}

void TFileLibDescXml::ParseDescInfo(QDomElement docElem)
{
    QDomNode n = docElem.firstChild();
    while (!n.isNull()) {
        QDomElement e = n.toElement();
        if (e.isNull() || xmlTagInfoItem != e.tagName()){
            n = n.nextSibling();
            continue;
        }

        TInfoItem* tmpItem = new TInfoItem;
        QDomNode t = e.firstChild();
        while(!t.isNull()){
            QDomElement te = t.toElement();
            if (xmlTagFileName == te.tagName()){
                tmpItem->strFileName = te.text();

            }else if (xmlTagPrefixCode == te.tagName()){
                tmpItem->strPrefixCode = te.text();

            }else if (xmlTagVersion == te.tagName()){
                tmpItem->strVersion = te.text();

            }else if (xmlTagFileUuid == te.tagName()){
                tmpItem->strDownloadUuid = te.text();

            }else if (xmlTagBookName == te.tagName()){
                tmpItem->strBookName = te.text();

            }else if (xmlTagPublisher == te.tagName()){
                tmpItem->strPublisher = te.text();

            }else if (xmlTagFileRef == te.tagName()){
                tmpItem->strFileRef = te.text();

            }else if (xmlTagLicenceName == te.tagName()){
                tmpItem->strLicenesFileName = te.text();
            }
            else if (xmlTagLicenceBrandID == te.tagName()){
                tmpItem->strLicenesBrandID = te.text();
            }
            else{

            }
            t = t.nextSibling();
        }

        QString strFilePath = mstrInfoDir  + "/" + tmpItem->strFileName;
        QString strLicencePath = mstrInfoDir + "/" + tmpItem->strLicenesFileName;
        QFileInfo tmpFileInfo(strFilePath);
        QFileInfo tmpLicenceInfo(strLicencePath);
        if (tmpFileInfo.exists() && tmpLicenceInfo.exists()){
            tmpItem->strFilePath = strFilePath;
            tmpItem->qFileSize = tmpFileInfo.size();
            mInfoHash.insert(tmpItem->strFileRef, tmpItem);         //如果有desc的xml文件有重复的话会导致内存泄漏   
        }else{
            delete tmpItem;
            tmpItem = NULL;
        }      
        n = n.nextSibling();
    }
}

//query
QList<TMPRFileInfoItem> TFileLibDescXml::QueryByBookName( QString strBookName)
{
    mFetchList.clear();

    if (strBookName.isEmpty()){
        return mFetchList;
    }

    QHashIterator<QString, TInfoItem*> it(mInfoHash);
    while(it.hasNext()){
        TInfoItem *tmpItem = it.next().value();
        QString bookName = tmpItem->strBookName;
        if (bookName.contains(strBookName, Qt::CaseInsensitive)){
            QueryFiles(tmpItem);
        }
    }

    return mFetchList;
}

QList<TMPRFileInfoItem> TFileLibDescXml::QueryByPrefixCode(QString strPrefixCode)
{
    mFetchList.clear();
    if (strPrefixCode.isEmpty()){
        return mFetchList;
    }

    QHashIterator<QString, TInfoItem*> it(mInfoHash);
    while(it.hasNext()){
        TInfoItem *tmpItem = it.next().value();
        QString prefixCode = tmpItem->strPrefixCode;
        if (0 == prefixCode.compare(strPrefixCode, Qt::CaseInsensitive)){
            QueryFiles(tmpItem);
        }
    }
    return mFetchList;
}

QList<TMPRFileInfoItem> TFileLibDescXml::QueryByFileRef( QString strFileRef)
{
    mFetchList.clear();
    if (strFileRef.isEmpty()){
        return mFetchList;
    }

    QHashIterator<QString, TInfoItem*> it(mInfoHash);
    while(it.hasNext()){
        TInfoItem *tmpItem = it.next().value();
        QString fileRef = tmpItem->strFileRef;
        if (0 == fileRef.compare(strFileRef, Qt::CaseInsensitive)){
            QueryFiles(tmpItem);
        }
    }
    return mFetchList;
}

QList<TMPRFileInfoItem> TFileLibDescXml::QueryAll()
{
    mFetchList.clear();

    QHashIterator<QString, TInfoItem*> it(mInfoHash);
    while(it.hasNext()){
        TInfoItem *tmpItem = it.next().value();
        QueryFiles(tmpItem);
    }

    return mFetchList;
}

void TFileLibDescXml::QueryFiles(TInfoItem *pInfoItem)
{
    if (NULL == pInfoItem){
        return;
    }
    TMPRFileInfoItem tmpItem;
    tmpItem.tFileLocation = Local;
    tmpItem.tFileType = mFileType;
    tmpItem.tBookName = pInfoItem->strBookName;
    tmpItem.tPublisher = pInfoItem->strPublisher;
    tmpItem.tPrefixCode = pInfoItem->strPrefixCode;
    tmpItem.tVersion = pInfoItem->strVersion;
    tmpItem.tFileUuid = pInfoItem->strDownloadUuid;
    tmpItem.tFilePath = pInfoItem->strFilePath;
    tmpItem.tFileSize = pInfoItem->qFileSize;
    tmpItem.tFileRef = pInfoItem->strFileRef;
    tmpItem.tLiceceUuid = pInfoItem->strDownloadUuid;
    tmpItem.tBrandID = pInfoItem->strLicenesBrandID;
    tmpItem.tLicencePath = pInfoItem->strLicenesFilePath;

    mFetchList.append(tmpItem);
}

//save
bool TFileLibDescXml::SaveToXMLFile()
{
    AddDescHeader();
    QHashIterator<QString, TInfoItem*> it(mInfoHash);
    while (it.hasNext()){
        TInfoItem *fileItem = it.next().value();
        QDomElement docElem = mdomXmlDoc->documentElement();
        QDomNode n = docElem.firstChild();
        while (!n.isNull()) {
            QDomElement e = n.toElement();
            if (!e.isNull() && xmlTagInfos == e.tagName()){
                AddDescInfo(&e, *fileItem);
            }
            n = n.nextSibling();
        }
    }

    ReWriteXmlFile();
    return true;
}

void TFileLibDescXml::AddDescHeader()
{
    if (NULL != mdomXmlDoc){
        delete mdomXmlDoc;
        mdomXmlDoc = NULL;
    }
    mdomXmlDoc = new QDomDocument("myDescFile");
    QDomElement domRoot = mdomXmlDoc->createElement(xmlTagRoot);  
    QDomElement domInfo = mdomXmlDoc->createElement(xmlTagInfos);  
    //QDomElement DomLicens = mdomXmlDoc->createElement(xmlTagLicences);  

    domRoot.appendChild(domInfo);
    //domRoot.appendChild(DomLicens);
    mdomXmlDoc->appendChild(domRoot);
}

bool TFileLibDescXml::AddDescInfo( QDomElement *docElem, TInfoItem infoItem)
{
    if (NULL == mdomXmlDoc){
        qDebug()<<"xml parse error!";
        return false;
    }
    QDomElement tmpItem = mdomXmlDoc->createElement(xmlTagInfoItem);  

    QDomText valueText;
    QDomElement fileNameItem = mdomXmlDoc->createElement(xmlTagFileName);  
    valueText = mdomXmlDoc->createTextNode(infoItem.strFileName);
    fileNameItem.appendChild(valueText);
    tmpItem.appendChild(fileNameItem);

    QDomElement prefixCodeItem = mdomXmlDoc->createElement(xmlTagPrefixCode);  
    valueText = mdomXmlDoc->createTextNode(infoItem.strPrefixCode);
    prefixCodeItem.appendChild(valueText);
    tmpItem.appendChild(prefixCodeItem);

    QDomElement versionItem = mdomXmlDoc->createElement(xmlTagVersion);  
    valueText = mdomXmlDoc->createTextNode(infoItem.strVersion);
    versionItem.appendChild(valueText);
    tmpItem.appendChild(versionItem);

    QDomElement uuidItem = mdomXmlDoc->createElement(xmlTagFileUuid);  
    valueText = mdomXmlDoc->createTextNode(infoItem.strDownloadUuid);
    uuidItem.appendChild(valueText);
    tmpItem.appendChild(uuidItem);

    QDomElement bookNameItem = mdomXmlDoc->createElement(xmlTagBookName);  
    valueText = mdomXmlDoc->createTextNode(infoItem.strBookName);
    bookNameItem.appendChild(valueText);
    tmpItem.appendChild(bookNameItem);

    QDomElement publiserItem = mdomXmlDoc->createElement(xmlTagPublisher);  
    valueText = mdomXmlDoc->createTextNode(infoItem.strPublisher);
    publiserItem.appendChild(valueText);
    tmpItem.appendChild(publiserItem);

    QDomElement RefItem = mdomXmlDoc->createElement(xmlTagFileRef);  
    valueText = mdomXmlDoc->createTextNode(infoItem.strFileRef);
    RefItem.appendChild(valueText);
    tmpItem.appendChild(RefItem);

    //start licence xml save
    QDomElement licenceNameItem = mdomXmlDoc->createElement(xmlTagLicenceName);
    valueText = mdomXmlDoc->createTextNode(infoItem.strLicenesFileName);
    licenceNameItem.appendChild(valueText);
    tmpItem.appendChild(licenceNameItem);

    QDomElement brandIDItem = mdomXmlDoc->createElement(xmlTagLicenceBrandID);
    valueText = mdomXmlDoc->createTextNode(infoItem.strLicenesBrandID);
    brandIDItem.appendChild(valueText);
    tmpItem.appendChild(brandIDItem);

    docElem->appendChild(tmpItem);  
    return true;
}

void TFileLibDescXml::InfoItem2XmlItem(TMPRFileInfoItem& MPRInfoItem, TInfoItem& infoItem)
{
    QFileInfo fileInfo(MPRInfoItem.tFilePath);
    infoItem.strFileName = MPRInfoItem.tBookName + "_" + MPRInfoItem.tPrefixCode.rightJustified(10, '0') + "_" +
        MPRInfoItem.tVersion.rightJustified(5, '0') + "." + fileInfo.suffix().toLower();
    infoItem.strFileName = ToAvailableFileName(infoItem.strFileName);
    infoItem.strPrefixCode = MPRInfoItem.tPrefixCode;
    infoItem.strVersion = MPRInfoItem.tVersion;
    infoItem.strDownloadUuid = MPRInfoItem.tFileUuid;
    infoItem.strBookName = MPRInfoItem.tBookName;
    infoItem.qFileSize = MPRInfoItem.tFileSize;
    infoItem.strPublisher = MPRInfoItem.tPublisher;
    infoItem.strFilePath = MPRInfoItem.tFilePath;
    infoItem.strFileRef = MPRInfoItem.tPrefixCode + MPRInfoItem.tVersion;

    QFileInfo licenceInfo(MPRInfoItem.tLicencePath);
    if (FT_MPR & MPRInfoItem.tFileType) {
        QFileInfo fMprInf(infoItem.strFileName);
        infoItem.strLicenesFileName = fMprInf.completeBaseName() + ".lic";
    }
    else {
        infoItem.strLicenesFileName = licenceInfo.fileName();
    }

    infoItem.strLicenesBrandID = MPRInfoItem.tBrandID;
    infoItem.strLicenesFilePath = MPRInfoItem.tLicencePath;
}

QString TFileLibDescXml::ToAvailableFileName(const QString& fileName)
{
    QString temp = fileName;
    temp = temp.remove('\\').remove('/').remove(':').remove('*').remove('\"').remove('>').remove('|');
    return temp;
}

void TFileLibDescXml::UpdateInfoItem( TInfoItem& iInfoItem)
{
    TInfoItem* ptmpInfoItem = mInfoHash.value(iInfoItem.strFileRef);
    //if (NULL == ptmpInfoItem)
    {
        QString filePath = mstrInfoDir + "/" + iInfoItem.strFileName;
        //FileRename(filePath);
        if (!MoveFileData(iInfoItem.strFilePath, filePath))
        {
            qDebug() << __FUNCTION__ << "error1 : move file failed !";
        }
        iInfoItem.strFilePath = filePath;

        filePath = mstrInfoDir + "/" + iInfoItem.strLicenesFileName;
        //FileRename(filePath);
        if (!MoveFileData(iInfoItem.strLicenesFilePath, filePath))
        {
            qDebug() << __FUNCTION__ << "error1 : move file failed !";
        }
        iInfoItem.strLicenesFilePath = filePath;
        TInfoItem * pInfoItem = new TInfoItem;
        *pInfoItem = iInfoItem;
        if (ptmpInfoItem)
        {
            mInfoHash.remove(ptmpInfoItem->strFileRef);
        }
        mInfoHash.insert(pInfoItem->strFileRef, pInfoItem);
        return;
    }
// 
//     if (0 != ptmpInfoItem->strDownloadUuid.compare(iInfoItem.strDownloadUuid)){
//         DeleteFiles(ptmpInfoItem->strFilePath);
//         QString filePath = mstrInfoDir + "/" + iInfoItem.strFileName;
//         if (!MoveFileData(iInfoItem.strFilePath, filePath))
//         {
//             qDebug() << __FUNCTION__ << "error2 : move file failed !";
//         }
//         iInfoItem.strFilePath = filePath;        
//     }
//     
//     if (ptmpInfoItem->strLicenesBrandID != iInfoItem.strLicenesBrandID)
//     {
//         DeleteFiles(ptmpInfoItem->strLicenesFilePath);
//         QString filePath = mstrInfoDir + "/" + iInfoItem.strLicenesFileName;
//         if (!MoveFileData(iInfoItem.strLicenesFilePath, filePath))
//         {
//             qDebug() << __FUNCTION__ << "error2 : move file failed !";
//         }
//         iInfoItem.strLicenesFilePath = filePath;
//     }

}

void TFileLibDescXml::MPRInfoItem2LocalInfo(TMPRFileInfoItem& mprItem, TInfoItem& infoItem)
{
    QFileInfo fileInfo(mprItem.tFilePath);
    infoItem.strFileName = mprItem.tBookName + "_" + mprItem.tVersion.rightJustified(5, '0') + "_" + mprItem.tPrefixCode.rightJustified(10, '0') + "." + fileInfo.suffix().toLower();
    infoItem.strFileName = ToAvailableFileName(infoItem.strFileName);
    infoItem.strFilePath = mprItem.tFilePath;
    infoItem.strPrefixCode = mprItem.tPrefixCode;
    infoItem.strVersion = mprItem.tVersion;
    infoItem.strDownloadUuid = mprItem.tFileUuid;
    infoItem.strBookName = mprItem.tBookName;
    infoItem.qFileSize = mprItem.tFileSize;
    infoItem.strPublisher = mprItem.tPublisher;
    infoItem.strFileRef = mprItem.tFileRef = mprItem.tPrefixCode + mprItem.tVersion;
    infoItem.strLicenesFileName = mprItem.tBookName + "_" + mprItem.tVersion.rightJustified(5, '0') + "_" + mprItem.tPrefixCode.rightJustified(10, '0') + ".lic";
    infoItem.strLicenesBrandID = mprItem.tBrandID;
    infoItem.strLicenesFilePath = mprItem.tLicencePath;

//     QString strNewFilePath = fileInfo.absoluteDir().absolutePath() + infoItem.strFileName;
//     QString strNewLicencePath = fileInfo.absoluteDir().absolutePath() + infoItem.strLicenesFileName;
//     infoItem.strLicenesFilePath = mprItem.tLicencePath = strNewLicencePath;
//     infoItem.strFilePath = mprItem.tFilePath = strNewFilePath;
// 
//     QFile::rename(mprItem.tFilePath, strNewFilePath);
//     QFile::rename(mprItem.tFilePath, strNewFilePath);
}

bool TFileLibDescXml::UpdateFileInfo(TMPRFileInfoItem &mprItem)
{
    TInfoItem infoItem;
    MPRInfoItem2LocalInfo(mprItem, infoItem);
    UpdateInfoItem(infoItem);
    mprItem.tFilePath = infoItem.strFilePath;
    mprItem.tLicencePath = infoItem.strLicenesFilePath;
    return true;
}

bool TFileLibDescXml::MoveFileData(QString istrOldPath, QString istrNewPath)
{
    QString strOldPath = QDir::fromNativeSeparators(istrOldPath);
    QString strNewPath = QDir::fromNativeSeparators(istrNewPath);
    QFileInfo oldFileInfo(strOldPath);
    QFileInfo newFileInfo(strNewPath);
    QDir strNewDir = newFileInfo.absoluteDir();
    QDir strOldDir = oldFileInfo.absoluteDir();
    if (strNewDir == strOldDir)
    {
        if (newFileInfo.exists()) {
            DeleteFiles(strNewPath);
            QThread::msleep(100);
        }
        if (!QFile::rename(istrOldPath, istrNewPath))
        {
            qDebug() << __FUNCTION__ << "failed in rename path" << istrOldPath << istrNewPath;
        }
        return true;
    }
    if (!strNewDir.exists(strNewDir.path())){
        strNewDir.mkdir(strNewDir.path());
    }

    QFileInfo fileInfo(strOldPath);
    if (!fileInfo.exists()){
        return false;
    }

    return MoveFileEx((LPCWSTR)strOldPath.utf16(), (LPCWSTR)strNewPath.utf16(), MOVEFILE_REPLACE_EXISTING);
}

bool TFileLibDescXml::DeleteFiles(QString istrPath)
{
    QString strPath = QDir::fromNativeSeparators(istrPath);

    QFileInfo fileInfo(strPath);
    if (!fileInfo.exists()){
        return false;
    }
    return DeleteFile((LPCWSTR)strPath.utf16());
}

void TFileLibDescXml::FileRename( QString& filePath)
{  
    int index = 0;
    QFileInfo ffileInfo(filePath);
    QString baseName = ffileInfo.baseName();
    for (int i = 0; i < 100; i++)
    {
        QFileInfo fileInfo(filePath);
        if (fileInfo.exists(filePath))
        {
            QString newName = baseName + "(" + QString::number(++index) + ")." + fileInfo.suffix();
            filePath = fileInfo.absoluteDir().absolutePath() + newName;
        }
        else
        {
            break;
        }
    }
}

