#ifndef __TMPRDEVICECOMM_H__
#define __TMPRDEVICECOMM_H__
#include <Windows.h>

class TMPRDeviceComm
{
public:
    enum COMM_ID{
        INVALID = -1,
        HID     = 0,
        ZIGBEE  = 1,
        USB_MIN = 'A',
        USB_MAX = 'Z'
    };

public:
    TMPRDeviceComm();
    virtual ~TMPRDeviceComm();
    virtual BOOL ReadData(int* inLen, unsigned char* inData, int inMaxLen) = 0;
    virtual BOOL WriteData(int outLen, const unsigned char* outData) = 0;
    virtual BOOL WriteDataAndRead(int outLen, const unsigned char* outData, int* inLen, unsigned char* inData, int inMaxLen);

public:
            char            id;//标示通信连接唯一id
                               //USB连接为 'A'~'Z'
                               //2.4G HID为0
                               //无效id为 -1
    static  volatile BOOL   s_Singled;
    static  TMPRDeviceComm* s_currentComm;
private:
    LPCRITICAL_SECTION  m_lpCriticalSection ;
};

#endif