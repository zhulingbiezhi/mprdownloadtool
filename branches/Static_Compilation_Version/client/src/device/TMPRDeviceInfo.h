#ifndef __TMPRDEVICEINFO_H__
#define __TMPRDEVICEINFO_H__

#include "MPRDeviceEnum.h"
#include <string>
using namespace std;

enum DongleStat{
    NO_DONGLE,
    DONGLE_CONNECT
};

typedef enum{
    SHORT_PRESS = 1,
    LONG_PRESS = 2,
    DOUBLE_PRESS = 3
}KeyPressType;

//MPR设备描述结构
typedef struct {
    MPRDeviceType  dev_type;                    //设备类型
    char           dev_sn[32];                  //设备序列号，生产管理用
    char           dev_name[32];                //设备名称，例如"MPR-1026"
    int            dev_brand_id;                //设备品牌id，对应的字符串描述须向服务器查询
    unsigned char  dev_uuid[40];                //设备UUID，设备的全局唯一id
    char           dev_firmware_version[512];    //设备固件版本字符串
    char           dev_media_path[128];         //设备媒体存储磁盘路径，最后一个非零字符为'/'
    unsigned long  dev_audio_format;            //设备支持的媒体格式，MPRAudioFormat按位或
    unsigned long  dev_encrypt_algorithm;       //设备支持的加密算法类型，EncryptAlgorithm按位或
    PKI_Algorithm  dev_pki_algorithm;           //设备使用的非对称加密算法，RSA或SM2
} MPRDeviceDesc;

class TMPRDeviceInfo
{
public:
    static void     ParserDeviceDsc(const unsigned char * inBuf, MPRDeviceDesc& rDevDesc);
    static bool     HexToAscii(string hexData, char* asciiData);
};
#endif