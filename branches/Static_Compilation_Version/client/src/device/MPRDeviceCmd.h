#ifndef __MPRDEVICECMD_H__
#define __MPRDEVICECMD_H__

static unsigned const char          EVENT_CMD[]                 = {0x04,0x00,0x02,0x00};
static unsigned const char          START_EVENT_CMD[]           = {0x04,0x00,0x01,0x00};
static unsigned const char          STOP_EVENT_CMD[]            = {0x04,0x00,0x01,0x01};
static unsigned const char          DECRYPT_LICENSE_CMD[]       = {0x00,0x00,0x03,0x00};
static unsigned const char          CREATE_LICENSE_CMD[]        = {0x00,0x00,0x03,0x01};
static unsigned const char          CREATE_BRAND_LICENSE_CMD[]  = {0x00,0x00,0x03,0x02};
static unsigned const char          SET_TIME_CMD[]              = {0x00,0x00,0x05,0x00};
static unsigned const char          DEVICED_ESC_CMD[]           = {0x04,0x00,0x06,0x00};
static unsigned const char          GET_SIGNATURE_CMD[]         = {0x00,0x00,0x0A,0x00};
static unsigned const char          GET_PUBKEY_CMD[]            = {0x04,0x00,0x09,0x00};
static unsigned const char          SEND_SAFEKEY_CMD[]          = {0x00,0x00,0x0B,0x00};
static unsigned const char          GET_SIGNPUBKEY_CMD[]        = {0x04,0x00,0x09,0x01};
static unsigned const char          SEND_PAIR_CMD[]             = {0x00,0x00,0x0C,0x00};
#define POWER_KEY_POSITION          0
#define UP_KEY_POSITION             1
#define DOWN_KEY_POSITION           2
#define MPR_CODE_POSITION           3
#define MPR_CODE_LENGTH             7

#define PUBKEY_HEAD_LEN             28
#define SN_LEN                      13
#endif