//#include "../MPRDeviceAPI.h"
#include "TMPRDeviceUSBInfo.h"
#include "TDeviceEject.h"
#include <Windows.h>
#include <tchar.h>
#include <stdio.h>
#include <map>
#include <string>
#include <fstream>
#include <algorithm>
#include <ctype.h>
#include <cstdio>
#include "MPRDeviceCmd.h"
using namespace std;

map<char, string> TMPRDeviceUSBInfo::device_map;
map<char, char> TMPRDeviceUSBInfo::device_name_map;
map<char, bool> TMPRDeviceUSBInfo::spi_device_map;
bool TMPRDeviceUSBInfo::isMPRDevice(char disk)
{
    bool ret = false;
    string pubkey_filename;
    string device_info_filename;
    string device_id_filename;

    pubkey_filename          = disk;
    pubkey_filename         += ":/Info/PubKey.dat";
    device_info_filename     = disk;
    device_info_filename    += ":/Info/MPRDeviceDesc.ini";
    device_id_filename      = disk;
    device_id_filename      += ":/Info/MPRDeviceID.dat";

    ifstream file(device_info_filename.data(), ifstream::in | ifstream::binary);
    if (file.is_open())
    {
        file.seekg(0, file.end);
        int length = static_cast<int>(file.tellg());
        file.seekg(0, file.beg);

        char* device_info = new char[length];
        if (NULL != device_info) {
            file.read(device_info, length);
        }
        string info(device_info);
        delete device_info;
        int pos = info.find("device uid:") + 16;
        string s_uuid = info.substr(pos, 32);
        char  uuid[16] = { 0 };
        HexToAscii(s_uuid, uuid);
        char  pub_key[1024];
        char  device_id[20];
        char  vendor_id[4];
        int   pub_len = 0;
        unsigned char key_ver;
        if (DoParser(pubkey_filename.data(), key_ver, pub_key, &pub_len, device_id, vendor_id))
        {
            if (!memcmp(uuid, device_id + 4, 16))
            {
                spi_device_map[disk] = false;
                ret = true;
            }

        }
        file.close();
    }
    else
    {
        ifstream idfile(device_id_filename.data(), ifstream::in | ifstream::binary);
        if (idfile.is_open())
        {
            idfile.seekg(0, idfile.end);
            int length = static_cast<int>(idfile.tellg());
            idfile.seekg(0, idfile.beg);
            if (length == 20)
            {
                char* szDevice_id = new char[length];
                if (NULL != szDevice_id) {
                    idfile.read(szDevice_id, length);
                }
                char  pub_key[1024];
                char  device_id[20];
                char  vendor_id[4];
                int   pub_len = 0;
                unsigned char key_ver;
                if (DoParser(pubkey_filename.data(), key_ver, pub_key, &pub_len, device_id, vendor_id))
                {
                    if (!memcmp(szDevice_id, device_id, 20))
                    {
                        spi_device_map[disk] = true;
                        ret = true;
                    }             
                }
                file.close();
            }
        }
    }
    return ret;
}

bool TMPRDeviceUSBInfo::DoParser(const string&  key_file_path,
                                    unsigned char& key_ver,
                                    char * pub_key,
                                    int  * pub_len,
                                    char * device_id,
                                    char * vendor_id)
{
    ifstream file(key_file_path.data(), ifstream::in | ifstream::binary);

    if (!file.is_open())
        return false;

    file.seekg (0, file.end);
    int length                  = static_cast<int>(file.tellg());
    file.seekg (0, file.beg);

    char* data                  = new char[length]; 
    file.read(data, length);

    static const int HEAD_SIZE  = 28;
    if (length < HEAD_SIZE)
        return false;
    key_ver = data[0];
    memcpy_s(device_id, 20, data + 4, 20);
    memcpy_s(vendor_id, 4, data + 4, 4);

    unsigned char check_sum     = (unsigned char)(data[3]);
    int key_len                 = (((unsigned char )data[24]) << 24) + (((unsigned char )data[25]) << 16) + (((unsigned char )data[26]) << 8) + (unsigned char )data[27];

    if (key_len != length - HEAD_SIZE)
        return false;

    unsigned char sum = 0;
    for (int i = 28; i < int(key_len + HEAD_SIZE); i++)
        sum += (unsigned char)(data[i]);

    if (sum != check_sum)
        return false;

    memcpy_s(pub_key, key_len, data + 28, key_len);
    *pub_len = key_len;
    delete data;
    return true;
}

bool TMPRDeviceUSBInfo::ReadDeviceSN(char disk, char (&sn)[15])
{
    bool ret = false;
    string device_info_filename;
    string device_info_SNname;
    device_info_filename     = disk;
    device_info_filename    += ":/Info/MPRDeviceDesc.ini";
    device_info_SNname      = disk;
    device_info_SNname      += ":/Info/SN.txt";
    ifstream file(device_info_filename.data(), ifstream::in | ifstream::binary);

    if (file.is_open())
    {
        file.seekg (0, file.end);
        int length           = static_cast<int>(file.tellg());
        file.seekg (0, file.beg);

        char* device_info    = new char[length]; 
        file.read(device_info, length);
        string info(device_info);
        delete device_info;
        int pos        = info.find("device sn:") + 16;
        if(pos >= 0)
        {
            string s_sn    = info.substr(pos, SN_LEN);
            memcpy_s(sn, sizeof(sn), s_sn.data(), SN_LEN);
            ret = true;
        }
        else
        {
            ret = false;
        }
        file.close();
    }
    else
    {
        ifstream snfile(device_info_SNname.data(), ifstream::in | ifstream::binary);
        if (snfile.is_open())
        {
            snfile.seekg(0, snfile.end);
            int length = static_cast<int>(snfile.tellg());
            snfile.seekg(0, snfile.beg);
            if (length <= 15)
            {
                snfile.read(sn, length);
            }
            snfile.close();
            ret = true;
        }
    }
    return ret;
}

bool TMPRDeviceUSBInfo::ReadSPIFirmwareVersionInfo(char disk, char* outData, int& outSize)
{
    bool ret = false;
    string firmware_version_name;
    firmware_version_name = disk;
    firmware_version_name += ":/Info/firmware_version.txt";
    ifstream file(firmware_version_name.data(), ifstream::in | ifstream::binary);

    if (file.is_open())
    {
        file.seekg(0, file.end);
        int length = static_cast<int>(file.tellg());
        file.seekg(0, file.beg);
        if (length < outSize)
        {
            file.read(outData, length);
        }
        file.close();
        ret = true;
    }
    return ret;

}

int TMPRDeviceUSBInfo::GetDownloadList(const char disk, vector<string>& o_lines)
{
    int             rv      = 0 ;
    string          path;
    path                   += disk;
    path                   += ":/";
    path                   += MPRDOWNLOADLIST_FN;
    ifstream        fs(path.c_str(), ios::in) ;       
    string          line ;
    o_lines.clear();

    while ( getline(fs, line) ) 
    {
        if(!IsDownloadLine(line))
        {
            rv = 0;
            o_lines.clear();
            break;
        }

        vector<string>::iterator it = find(o_lines.begin(), o_lines.end(), line);
        if(o_lines.end() == it)
        {
            ++rv ;
            o_lines.push_back(line) ;
        }
    }

    fs.close();
    if(0 == rv)
    {
        remove(path.c_str());
    }
    else
    {
        RefleshDownloadList(disk, o_lines);
    }

    return rv ;
}

int TMPRDeviceUSBInfo::RefleshDownloadList(const char disk, vector<string>& o_lines)
{
    string          path;
    path                   += disk;
    path                   += ":/";
    path                   += MPRDOWNLOADLIST_FN;
    ofstream        fs(path.c_str(), ios::out | ios::trunc);
    for ( unsigned int i = 0; i < o_lines.size(); ++i ) 
    {
        fs.write(o_lines[i].c_str(), o_lines[i].length());
        fs.write("\n", 1);
    }
    fs.close();
    return o_lines.size();
}

bool TMPRDeviceUSBInfo::IsDownloadLine(const string& line)
{
    if(14 != line.size())
    {
        return false;
    }

    for(int i = 0; i < 10; ++i)
    {
        if(!isdigit(line[i]))
        {
            return false;
        }
    }

    if(line[10] != ',')
    {
        return false;
    }

    if(line[11] != ' ')
    {
        return false;
    }

    for(int i = 12; i < 14; ++i)
    {
        if(!isdigit(line[i]))
        {
            return false;
        }
    }
    return true;
}

int TMPRDeviceUSBInfo::EnumConnectedDevice( char dev_sn_list[1024] )
{
    device_map.clear();
    device_name_map.clear();
    spi_device_map.clear();
    DWORD    MaxDriveSet;
    DWORD    drive = 0;
    char    szDrvName[33] = {0};
    char    pDiskName[32] = {0};
    DWORD    count = 0;
    DWORD    MPRDeviceNum = 0;
    UINT     uMode= SetErrorMode(SEM_FAILCRITICALERRORS);
    MaxDriveSet = GetLogicalDrives();

    for (drive = 0; drive < 32; drive++)  
    {
        if ( MaxDriveSet & (1 << drive) )  
        {
            sprintf_s(szDrvName, "%c:\\", 'A'+drive);

            if(GetDriveTypeA(szDrvName)== DRIVE_REMOVABLE && (drive > 1))
            {
                pDiskName[count++] = (char)('A'+drive);
                if(count >= 32)
                {
                    break;
                }
            }
        }
    }

    for (drive = 0; drive < count; drive++) 
    {
        if(!TMPRDeviceUSBInfo::isMPRDevice(pDiskName[drive]))
        {
            continue;
        }

        char sn[15] = {0};
        if(TMPRDeviceUSBInfo::ReadDeviceSN(pDiskName[drive], sn))
        {
            device_map[pDiskName[drive]] = sn;
            device_name_map[pDiskName[drive]] = pDiskName[drive];
            sn[SN_LEN] = '\n';
            strcat_s( dev_sn_list, 1024, sn);
            MPRDeviceNum ++;
        }
    }

    SetErrorMode(uMode);
    return MPRDeviceNum;
}

int TMPRDeviceUSBInfo::RemoveDevice( const char* p_dev_sn )
{
    char disk = -1;
    for(map<char, string>::iterator it = TMPRDeviceUSBInfo::device_map.begin(); it != TMPRDeviceUSBInfo::device_map.end(); ++it)
    {
        if(!memcmp(it->second.c_str(), p_dev_sn, 13))
        {
            disk = it->first;
            if(TDeviceEject().Eject(disk))
            {
                return NO_ERROR;
            }
            else
            {
                return BAD_CALL;
            }
        }
    }
    return DEVICE_NOT_EXIST;
}

char TMPRDeviceUSBInfo::GetMakerUsbKey()
{
    DWORD    MaxDriveSet;
    DWORD    drive = 0;
    char     szDrvName[33] = {0};
    char     pDiskName[32] = {0};
    DWORD    count = 0;
    UINT     uMode= SetErrorMode(SEM_FAILCRITICALERRORS);
    MaxDriveSet = GetLogicalDrives();

    for (drive = 0; drive < 32; drive++)  
    {
        if ( MaxDriveSet & (1 << drive) )  
        {
            sprintf_s(szDrvName, "%c:\\", 'A'+drive);

            UINT type = GetDriveTypeA(szDrvName);
            if((type == DRIVE_CDROM || type == DRIVE_REMOVABLE) && (drive > 1))
            {
                pDiskName[count++] = (char)('A'+drive);
                if(count >= 32)
                {
                    break;
                }
            }
        }
    }

    for (drive = 0; drive < count; drive++) 
    {
        if(TMPRDeviceUSBInfo::isMPRUsbKey(pDiskName[drive]))
        {
            SetErrorMode(uMode);
            return pDiskName[drive];
        }

    }

    SetErrorMode(uMode);
    return -1;
}

bool TMPRDeviceUSBInfo::isMPRUsbKey( char disk )
{
    bool ret = false;
    string device_info_filename;

    device_info_filename     = disk;
    device_info_filename    += ":/Info/MPRDeviceDesc.ini";

    ifstream file(device_info_filename.data(), ifstream::in | ifstream::binary);

    if (file.is_open())
    {
        file.seekg (0, file.end);
        int length           = static_cast<int>(file.tellg());
        file.seekg (0, file.beg);

        char* device_info    = new char[length]; 
        file.read(device_info, length);
        string info(device_info);
        delete device_info;
        if (info.find("MPRDevice_MakerKey") != -1)
        {
            ret = true;
        }
        file.close();
    }

    return ret;
}


