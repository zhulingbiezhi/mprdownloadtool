#include "TSTDPreCompiledHeader.h"
#include "TMPRDeviceHandler.h"
#include "TMPRDeviceUSBComm.h"
#include "MPRDeviceCmd.h"
#include "TMPRDeviceCommData.h"
#include "TMPRDeviceInfo.h"


TMPRDeviceHandler::TMPRDeviceHandler(char diskName)
    : mpMPRDevDes(0)
{
    mpDevComm = new TMPRDeviceUSBComm(diskName);
}

TMPRDeviceHandler::~TMPRDeviceHandler()
{
    if (NULL != mpDevComm) {
        delete mpDevComm;
        mpDevComm = 0;
    }
    if (NULL != mpMPRDevDes) {
       delete mpMPRDevDes;
       mpMPRDevDes = 0;
    }
}

char TMPRDeviceHandler::GetDiskName()
{
    return mpDevComm->id;
}

bool TMPRDeviceHandler::StartCaptureMPRCode()
{
    unsigned char revBuff[20];
    int revLen = 0;

    for(int i = 0; i < 3; i++)
    {
        if(mpDevComm->WriteDataAndRead(sizeof(START_EVENT_CMD), START_EVENT_CMD, &revLen, revBuff, sizeof(revBuff)))
        {
            if(revBuff[4] == 0x00)
            {
                return true;
            }
        }
    }
    return false;
}

QByteArray TMPRDeviceHandler::HandleCodeBytes(const char* data, int /*len*/)
{
    quint64 code = 0;
    memcpy((char*)&code + 1, data, 7);
    code = qFromBigEndian(code);
    code /= 10;

    return QByteArray::number(code);
}

void TMPRDeviceHandler::TryCaptureMPRCode()
{
    unsigned char revBuff[20] = {0};
    int len = 0;

    if(!mpDevComm->WriteDataAndRead(sizeof(EVENT_CMD), EVENT_CMD, &len, revBuff, sizeof(revBuff))) {
        return ;
    }

    TMPRDeviceCommData data;
    if(MPRDeviceProcessCommData(&data, revBuff, sizeof(revBuff)) != DATA_OK) {
        return;
    }

    if (data.len == 10)
    {
        if (!memcmp("\x0\x0\x0\x0\x0\x0\x0", data.data + MPR_CODE_POSITION, MPR_CODE_LENGTH) ){
            return;
        }
        unsigned char mprcode[MPR_CODE_LENGTH];
        memcpy_s(mprcode, MPR_CODE_LENGTH, data.data + MPR_CODE_POSITION, MPR_CODE_LENGTH);
        QByteArray codeData = HandleCodeBytes((const char*)mprcode, 7);
        QString mprCode = QString(codeData).rightJustified(15, '0');
        emit MPRCodeCaptured(mpDevComm->id, mprCode);
    }
}

bool TMPRDeviceHandler::GetDeviceDesc( TMPRDeviceDesc& devDesc )
{
    if (mpMPRDevDes)
    {
        devDesc = *mpMPRDevDes;
        return true;
    }

    unsigned char revBuff[2*1024] = {0};
    int revLen = 0;
    MPRDeviceDesc desc;
    for (int i = 0; i < 3; i++)
    {
        if(mpDevComm->WriteDataAndRead(sizeof(DEVICED_ESC_CMD), DEVICED_ESC_CMD, &revLen, revBuff, sizeof(revBuff)))
        {
            TMPRDeviceInfo::ParserDeviceDsc(revBuff, desc);
            char str_tmp[64];
            sprintf_s(str_tmp, sizeof(str_tmp), "%c:\\%s", mpDevComm->id, desc.dev_media_path);
            strcpy_s(desc.dev_media_path, str_tmp);

            mpMPRDevDes = new TMPRDeviceDesc;

            mpMPRDevDes->mType              = static_cast<TMPRDeviceDesc::DeviceType>(desc.dev_type);
            mpMPRDevDes->mDeviceSN          = desc.dev_sn;
            mpMPRDevDes->mBrandId           = QString::number(desc.dev_brand_id, 16).rightJustified(8, '0').toLatin1();
            mpMPRDevDes->mUuid              = QByteArray((char*)desc.dev_uuid, 16);
            mpMPRDevDes->mDeviceId          = mpMPRDevDes->mBrandId + mpMPRDevDes->mUuid.toHex();
            mpMPRDevDes->mDeviceName        = desc.dev_name;
            mpMPRDevDes->mDeviceMediaPath   = desc.dev_media_path;
            mpMPRDevDes->mSupportMediaFormat= desc.dev_audio_format;
            mpMPRDevDes->mSupportEncryptType= desc.dev_encrypt_algorithm;
            mpMPRDevDes->mFirmwareVersion   = desc.dev_firmware_version;

            devDesc = *mpMPRDevDes;
            return true;
        }
    }
    return false;
}
