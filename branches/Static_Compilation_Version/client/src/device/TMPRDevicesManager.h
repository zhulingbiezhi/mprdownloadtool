#ifndef __TMPRDEVICESMANGER_H__
#define __TMPRDEVICESMANGER_H__

#include "TMPRDeviceInfo.h"
#include "TMPRDeviceDesc.h"
class TMPRDeviceUSBComm;
class TMPRDeviceHandler;

class TMPRDevicesManager: public QObject
{
    Q_OBJECT

public:
    static  TMPRDevicesManager&     Instance();
    void                            ClearData();

    void                            HandleDeviceChanged(MSG* msg);
    void                            EnumConnectedMPRDevice();
    bool                            GetMPRDeviceDesc(char disk, TMPRDeviceDesc& des);
    bool                            GetMPRDeviceCapacityInfo(char disk, qint64& qTotal, qint64& qFree);
    QString                         GetSPIFirmwareVersionInfo(char disk);

private:
    TMPRDevicesManager();
    ~TMPRDevicesManager();

    void                            StartCaptureMPRCode(char disk);
    void                            DetachMPRDevice(char disk);

private slots:
    void                            OnCaptureCodeTimeout();

signals:
    void                            MPRDevicePlugIn(char disk);
    void                            MPRDevicePlugOut(char disk);
    void                            MPRCodeCaptured(char disk, QString mprCode);

private:
    QMap<char, TMPRDeviceHandler*>  mMPRDeviceHandlerMap;
    QTimer*                         mpCaptureCodeTimer;
    QThread*                        mpCodeCaptureThread;
};

#endif


