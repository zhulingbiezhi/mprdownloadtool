#include "TMPRDeviceUSBComm.h"
#include <windows.h>

#include <ntddscsi.h>
#include <stdio.h>
#include <string>
#ifdef SUPPORT_OS_XP
#include <WinIoCtl.h>
#else
#include <devioctl.h>
#include <ntdddisk.h>
#endif // SUPPORT_OS_XP

using std::string ;

#define SPT_SENSE_LENGTH    32
#define SPTWB_DATA_LENGTH   
#define CDB6GENERIC_LENGTH  1

typedef struct T_SCSI_PASS_THROUGH_WITH_BUFFERS {
    SCSI_PASS_THROUGH spt;
    ULONG             Filler; 
    UCHAR             ucSenseBuf[SPT_SENSE_LENGTH];
    UCHAR             ucDataBuf[4096];
} SCSI_PASS_THROUGH_WITH_BUFFERS, *PSCSI_PASS_THROUGH_WITH_BUFFERS;

HANDLE g_scsi_event;

TMPRDeviceUSBComm::TMPRDeviceUSBComm( char diskName )
: m_hDisk(INVALID_HANDLE_VALUE)
{
    id   = -1;
    if(OpenDisk( diskName ))
    {
        id = diskName;
    }
}

TMPRDeviceUSBComm::~TMPRDeviceUSBComm()
{
    CloseDisk() ;
}

BOOL TMPRDeviceUSBComm::OpenDisk(char diskName)
{
    string  path ;
    path.push_back(diskName) ;
    path += ":\\" ;
    if ( !(DRIVE_REMOVABLE == GetDriveTypeA(path.c_str()) || DRIVE_CDROM == GetDriveTypeA(path.c_str()))) 
    {
        return FALSE ;
    }

    TCHAR    szBuf[260];

    swprintf(szBuf, 259, L"\\\\?\\%c:", diskName);
    m_hDisk = CreateFile(szBuf, GENERIC_READ | GENERIC_WRITE,
        FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, NULL, NULL);

    if(INVALID_HANDLE_VALUE == m_hDisk)
    {
        return FALSE;
    }

    return TRUE;

}

void TMPRDeviceUSBComm::CloseDisk()
{
    if(m_hDisk != INVALID_HANDLE_VALUE)
    {
        CloseHandle(m_hDisk);
        m_hDisk = INVALID_HANDLE_VALUE ;
    }
}

BOOL TMPRDeviceUSBComm::WriteData(int outLen, const unsigned char* outData)
{
    if(INVALID_HANDLE_VALUE == m_hDisk || NULL == outData)
    {
        return FALSE;
    }

    SCSI_PASS_THROUGH_WITH_BUFFERS sptwb;
    BOOL status = 0;
    ULONG returned = 0;
    ULONG length = 0;

    ZeroMemory(&sptwb,sizeof(SCSI_PASS_THROUGH_WITH_BUFFERS));

    sptwb.spt.Length = sizeof(SCSI_PASS_THROUGH);
    sptwb.spt.PathId = 0;
    sptwb.spt.TargetId = 1;
    sptwb.spt.Lun = 0;
    sptwb.spt.CdbLength = CDB6GENERIC_LENGTH;
    sptwb.spt.SenseInfoLength = SPT_SENSE_LENGTH;
    sptwb.spt.DataIn = SCSI_IOCTL_DATA_OUT;
    sptwb.spt.DataTransferLength = outLen;
    sptwb.spt.TimeOutValue = 5;
    sptwb.spt.DataBufferOffset = offsetof(SCSI_PASS_THROUGH_WITH_BUFFERS,ucDataBuf);
    sptwb.spt.SenseInfoOffset = offsetof(SCSI_PASS_THROUGH_WITH_BUFFERS,ucSenseBuf);
    sptwb.spt.Cdb[0] = 0xCE;

    for (int i=0; i<outLen; i++)
    {
        sptwb.ucDataBuf[i] = outData[i] ;
    }

    length = offsetof(SCSI_PASS_THROUGH_WITH_BUFFERS,ucDataBuf) + 
        sptwb.spt.DataTransferLength;

    status = DeviceIoControl(m_hDisk,
        IOCTL_SCSI_PASS_THROUGH,
        &sptwb,
        sizeof(SCSI_PASS_THROUGH_WITH_BUFFERS),
        &sptwb,
        length,
        &returned,
        NULL);
    return status;
}

BOOL TMPRDeviceUSBComm::ReadData(int* inLen, unsigned char* inData, int inMaxLen)
{ 
    if(INVALID_HANDLE_VALUE == m_hDisk || NULL == inData)
    {
        return FALSE;
    }

    SCSI_PASS_THROUGH_WITH_BUFFERS sptwb;
    BOOL status = 0;
    ULONG returned = 0;
    ULONG length = 0;

    ZeroMemory(&sptwb,sizeof(SCSI_PASS_THROUGH_WITH_BUFFERS));

    sptwb.spt.Length = sizeof(SCSI_PASS_THROUGH);
    sptwb.spt.PathId = 0;
    sptwb.spt.TargetId = 1;
    sptwb.spt.Lun = 0;
    sptwb.spt.CdbLength = CDB6GENERIC_LENGTH;
    sptwb.spt.SenseInfoLength = SPT_SENSE_LENGTH;
    sptwb.spt.DataIn = SCSI_IOCTL_DATA_IN;
    sptwb.spt.DataTransferLength = inMaxLen;
    sptwb.spt.TimeOutValue = 5;
    sptwb.spt.DataBufferOffset = offsetof(SCSI_PASS_THROUGH_WITH_BUFFERS,ucDataBuf);
    sptwb.spt.SenseInfoOffset = offsetof(SCSI_PASS_THROUGH_WITH_BUFFERS,ucSenseBuf);
    sptwb.spt.Cdb[0] = 0xCF;

    length = offsetof(SCSI_PASS_THROUGH_WITH_BUFFERS,ucDataBuf) + 
        sptwb.spt.DataTransferLength;

    status = DeviceIoControl(m_hDisk,
        IOCTL_SCSI_PASS_THROUGH,
        &sptwb,
        sizeof(SCSI_PASS_THROUGH_WITH_BUFFERS),
        &sptwb,
        length,
        &returned,
        NULL);
    
    memcpy(inData,sptwb.ucDataBuf,inMaxLen);
    if(returned <= (ULONG)inMaxLen)
        *inLen = returned;
    else
        *inLen = inMaxLen;
    return status;
}

