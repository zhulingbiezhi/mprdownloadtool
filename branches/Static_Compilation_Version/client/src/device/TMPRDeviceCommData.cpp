#include "TMPRDeviceCommData.h"

int MPRDeviceProcessCommData(TMPRDeviceCommData* deviceData,const unsigned char *data, int dataMaxLen)
{
    if ( 3 > dataMaxLen)
        return DATA_ERROR;

    int len = ( data[1] << 8 ) + data[0];
    if( len > dataMaxLen || len < 4)
        return DATA_ERROR;

    deviceData->len  = len - 4;
    deviceData->id   = (data[2] << 8) + data[3];
    deviceData->data = data + 4;
    return DATA_OK;
}