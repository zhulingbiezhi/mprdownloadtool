#ifndef __TMPRDEVICEUSBINFO_H__
#define __TMPRDEVICEUSBINFO_H__

#include <map>
#include <string>
#include <vector>
//#include "..\MPRDeviceAPI.h"
#include "TMPRDeviceInfo.h"
using namespace std;

const string MPRDOWNLOADLIST_FN("Info/MPRDownloadList.dat") ;

class TMPRDeviceUSBInfo : public TMPRDeviceInfo
{
public:    
    TMPRDeviceUSBInfo();
    ~TMPRDeviceUSBInfo();

    static int      GetDownloadList(const char disk, vector<string>& o_lines);
    static int      RefleshDownloadList(const char disk, vector<string>& o_lines);
    static int      EnumConnectedDevice(char dev_sn_list[1024]);
    static int      RemoveDevice(const char* p_dev_sn);
    static char     GetMakerUsbKey();
    static bool     ReadSPIFirmwareVersionInfo(char disk, char* outData, int& outSize);
    static bool     ReadDeviceSN(char disk, char(&sn)[15]);

private:
    static bool     DoParser(const string&  key_file_path,
                             unsigned char& key_ver,
                             char * pub_key,
                             int  * pub_len,
                             char * device_id,
                             char * vendor_id);
    static bool     isMPRDevice(char disk);
    static bool     isMPRUsbKey(char disk);
    static bool     IsDownloadLine(const string& line);

public:
    static map<char, string> device_map;
    static map<char, char>   device_name_map;
    static map<char, bool>   spi_device_map;
};

#endif