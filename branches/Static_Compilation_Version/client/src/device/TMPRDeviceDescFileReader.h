#ifndef __TMPRDEVICEDESCFILEREADER_H__
#define __TMPRDEVICEDESCFILEREADER_H__

#include "TMPRDeviceDesc.h"

class TMPRDeviceDescFileReader : public QObject
{
    Q_OBJECT
public:
    TMPRDeviceDescFileReader(const QString& fielPath);
    ~TMPRDeviceDescFileReader();

    TMPRDeviceDesc GetDeviceDesc();
    bool IsValid() const;
private:
    void ParseFile();

private:
    QString mFilePath;
    TMPRDeviceDesc mDesc;
};

#endif


