#ifndef __TDEFAULTMPRDEVICEMANAGER_H__
#define __TDEFAULTMPRDEVICEMANAGER_H__

#include "TMPRDeviceDesc.h"

class TDefaultMPRDeviceManager : public QObject
{
    Q_OBJECT
public:
    static TDefaultMPRDeviceManager&     Instance();

    QString                              GetPath() const;
    void                                 SetPath(const QString& path);

private:
    TDefaultMPRDeviceManager();
    ~TDefaultMPRDeviceManager();

private:
    QString                             mDefuatlPath;
};

#endif