#include "TMPRDeviceComm.h"
#include <WINDOWS.H>

TMPRDeviceComm* TMPRDeviceComm::s_currentComm   = NULL;

volatile BOOL TMPRDeviceComm::s_Singled       = FALSE;

BOOL TMPRDeviceComm::WriteDataAndRead( int outLen, const unsigned char* outData, int* inLen, unsigned char* inData, int inMaxLen )
{
//     if (!s_Singled)
//     {
//         return FALSE;
//     }

    EnterCriticalSection(m_lpCriticalSection);
    if( !WriteData(outLen, outData))
    {
        LeaveCriticalSection(m_lpCriticalSection);
        return FALSE;
    }

    *inLen  = 0;
    memset(inData, 0x00, inMaxLen);
    if( !ReadData(inLen, inData, inMaxLen))
    {
        LeaveCriticalSection(m_lpCriticalSection);
        return FALSE;
    }

    if(outData[2] == inData[2] && outData[3] == inData[3])
    {
        LeaveCriticalSection(m_lpCriticalSection);
        return TRUE;
    }
    LeaveCriticalSection(m_lpCriticalSection);
    return FALSE;
}

TMPRDeviceComm::TMPRDeviceComm()
{
    m_lpCriticalSection = new CRITICAL_SECTION;
    InitializeCriticalSection(m_lpCriticalSection);
}

TMPRDeviceComm::~TMPRDeviceComm()
{
    DeleteCriticalSection(m_lpCriticalSection);
    if (NULL != m_lpCriticalSection){
        delete m_lpCriticalSection;
        m_lpCriticalSection = NULL;
    }
}

