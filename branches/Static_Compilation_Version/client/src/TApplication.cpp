#include "src/TSTDPreCompiledHeader.h"
#include "TApplication.h"

TApplication::TApplication(int &argc, char **argv, const QString& companyname, const QString& appname)
    :QApplication(argc, argv)
    , mbRunning(false)
    , mCompanyName(companyname)
    , mAppName(appname)
{  
    setApplicationName(mAppName);
    QString serverName = applicationName();
    QLocalSocket socket;
    socket.connectToServer(serverName);
    if (socket.waitForConnected(500))
    {
        mbRunning = true;
        return;
    }
    mpLocalServer = new QLocalServer(this);
    connect(mpLocalServer, SIGNAL(newConnection()), this, SLOT(NewLocalConnection()));
    if (!mpLocalServer->listen(serverName)) 
    {
        if (mpLocalServer->serverError() == QAbstractSocket::AddressInUseError
            && QFile::exists(mpLocalServer->serverName()))
        {
                QFile::remove(mpLocalServer->serverName());
                mpLocalServer->listen(serverName);
        }
    }
}

TApplication::~TApplication()
{
}

void TApplication::NewLocalConnection()  
{  
    QLocalSocket *socket = mpLocalServer->nextPendingConnection();  
    if (!socket)
        return;
    emit ActivateMainWindow();
    socket->deleteLater();
}

bool TApplication::IsRunning()
{
    return mbRunning;
}

void TApplication::SetLanguage(ESoftwareLanguage lang)
{
    mlanguage = lang;
}

TApplication::ESoftwareLanguage TApplication::GetLanguage()
{
    return mlanguage;
}

