#include "TSTDPreCompiledHeader.h"
#include "TCommonDialog.h"


TCommonDialog::TCommonDialog(bool bModal, QWidget* parent)
: QDialog(parent)
, m_bLeftMousePressed(false)
{
	setObjectName("TCommonDialog");
	setWindowFlags(Qt::FramelessWindowHint | Qt::WindowMinimizeButtonHint | Qt::WindowMaximizeButtonHint | Qt::Window);
	setModal(bModal);
	setAttribute(Qt::WA_TranslucentBackground);
 	setMouseTracking(true);
}

TCommonDialog::~TCommonDialog()
{
	
}

void TCommonDialog::mousePressEvent(QMouseEvent* pMouseEvent)
{
	if (pMouseEvent->button() == Qt::LeftButton)
	{
		m_DragPosition = pMouseEvent->globalPos() - this->pos();
		pMouseEvent->accept();
		m_bLeftMousePressed = true;
	}
}

void TCommonDialog::mouseReleaseEvent(QMouseEvent * /*mouseEvent*/) {
	m_bLeftMousePressed = false;
}

void TCommonDialog::mouseMoveEvent(QMouseEvent* pMouseEvent)
{
	if (m_bLeftMousePressed)
	{
		this->move(pMouseEvent->globalPos() - m_DragPosition);
		pMouseEvent->accept();
	}
}

void TCommonDialog::paintEvent(QPaintEvent * /*event*/)
{
	QPainterPath path;
	path.setFillRule(Qt::WindingFill);
	path.addRect(SHADOW_WIDTH, SHADOW_WIDTH, this->width() - 2 * SHADOW_WIDTH, this->height() - 2 * SHADOW_WIDTH);

	QPainter painter(this);
	painter.setRenderHint(QPainter::Antialiasing, true);
	painter.fillPath(path, QBrush(Qt::white));

	QColor color(0, 0, 0, 20);
	for (int i = 0; i < SHADOW_WIDTH; i++)
	{
		QPainterPath path;
		path.setFillRule(Qt::WindingFill);
		path.addRect(SHADOW_WIDTH - i, SHADOW_WIDTH - i, this->width() - (SHADOW_WIDTH - i) * 2, this->height() - (SHADOW_WIDTH - i) * 2);
		color.setAlpha(30 - qSqrt(i) * 10);
		painter.setPen(color);
		painter.drawPath(path);
	}
}

void TCommonDialog::resizeEvent(QResizeEvent * /*event*/)
{
	QBitmap objBitmap(size());
	QPainter painter(&objBitmap);
	painter.fillRect(rect(), Qt::white);
	painter.setBrush(QColor(0, 0, 0));
	painter.drawRoundedRect(this->rect(), 10, 10);
	setMask(objBitmap);
}
