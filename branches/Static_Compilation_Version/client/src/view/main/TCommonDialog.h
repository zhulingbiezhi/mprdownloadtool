#ifndef __TCOMMON_DIALOG_H__
#define __TCOMMON_DIALOG_H__

#include <QDialog>

#define SHADOW_WIDTH 4

class QFrame;
class TCommonDialog :public QDialog
{
	Q_OBJECT

public:
	TCommonDialog(bool bModal = true,QWidget* parent = NULL);
	~TCommonDialog();
protected:
	virtual void paintEvent(QPaintEvent *event)override;
	virtual void resizeEvent(QResizeEvent *event)override;
	void mousePressEvent(QMouseEvent* pMouseEvent);
	void mouseReleaseEvent(QMouseEvent *mouseEvent);
	void mouseMoveEvent(QMouseEvent* pMouseEvent);
private:
	QPoint m_DragPosition;
	bool m_bLeftMousePressed;
};

#endif