#include "src/TSTDPreCompiledHeader.h"
#include "TMainWindow.h"
//#include "TUpgradeClientUI.h"
#include "THeadWidget.h"
#include "TLeftTabWidget.h"
#include "TRightDisplayWidget.h"
#include "TMPRDevicesManager.h"
#include "TDeviceDataManager.h"
#include "TSearchHandle.h"
#include "TUpdateVersionManager.h"
#include "TDownloadManager.h"
#include "TMPRDeviceUSBInfo.h"
#include "TAppCommon.h"
#include "TSystemSettingDlg.h"
#include "TWorkStateConfig.h"
#include "TApplication.h"
#include "TTaskManager.h"
#include "TPopupDialog.h"
#include "TWaittingDlg.h"

#ifndef SUPPORT_OS_XP
#include "Dwmapi.h"
#pragma comment (lib,"Dwmapi.lib")
#endif

TMainWindow::TMainWindow(QWidget* parent)
: QFrame(parent)
, mpHeadWidget(NULL)
, mpCustomerTabWidget(NULL)
, mpDisPlayWidget(NULL)
, mpSearchHandle(NULL)
, mpDwmapiLib(NULL)
{
    CreateUI();
    InitConnect();
    InitData();
    TWorkStateConfig::Instance()->BeginGroup("Common");
    QString strAppPath = TWorkStateConfig::Instance()->GetValue("appPath", "").toString();
    TWorkStateConfig::Instance()->EndGroup();
    QString qAppPath = tApp->applicationFilePath();
    qDebug() << __FUNCTION__ << strAppPath << qAppPath;
    if (!strAppPath.isEmpty())
    {
        if (qAppPath != strAppPath)
        {
            if (QFile::exists(strAppPath))
            {
                QFile::remove(strAppPath);
            }
            QFile::copy(qAppPath, strAppPath);
            TWorkStateConfig::Instance()->BeginGroup("Common");
            TWorkStateConfig::Instance()->SetValue("appPath", "");
            TWorkStateConfig::Instance()->EndGroup();
        }
    }


    ////系统升级 upgrade
    //mpUpgradeClientUI = new TUpgradeClientUI(this);
    //mpUpgradeClientUI->setModal(false);
    //connect(mpUpgradeClientUI, &TUpgradeClientUI::SigShowUpgradeUI, this, &TMainWindow::ShowUpgradeUI);
    //connect(mpUpgradeClientUI, &TUpgradeClientUI::SigCloseSelf, this, &TMainWindow::OnExit);
    ////TUpgradeProgram::Instance().UpgradeCheck();
    //setStyleSheet("QWidget{ border: 1px solid red}");
}

TMainWindow::~TMainWindow()
{
    if (mpDwmapiLib)
    {
        mpDwmapiLib->unload();
        delete mpDwmapiLib;
    }
    TTaskManager::Instance()->RemoveAllTask();
}

void TMainWindow::InitData()
{
    TUpdateVersionManager::Instance().CheckClientVersion();
    //系统启动后接受设备的信号
    TMPRDevicesManager::Instance().EnumConnectedMPRDevice();
    TAppCommon::Instance().SetMainWindow(this);

}

void TMainWindow::CreateUI()
{
    setObjectName("TMainWindow");
    setWindowFlags(Qt::FramelessWindowHint | Qt::Window | Qt::WindowMinMaxButtonsHint);
    setFixedSize(1024, 768);
    mpHeadWidget = new THeadWidget(this);
    mpDisPlayWidget = new TRightDisplayWidget(this);
    mpCustomerTabWidget = new TLeftTabWidget(this);

    QVBoxLayout* pMainLayout = new QVBoxLayout();
    pMainLayout->setContentsMargins(0, 0, 0, 0);
    pMainLayout->setSpacing(0);

    QHBoxLayout* pBottoMLayout = new QHBoxLayout();
    pBottoMLayout->setContentsMargins(0, 0, 0, 0);
    pBottoMLayout->setSpacing(0);
    pBottoMLayout->addWidget(mpCustomerTabWidget);
    pBottoMLayout->addWidget(mpDisPlayWidget);

    pMainLayout->addWidget(mpHeadWidget);
    pMainLayout->addLayout(pBottoMLayout);

    setLayout(pMainLayout);

    InitBorderStyle();

}

void TMainWindow::InitBorderStyle()
{
#ifdef Q_OS_WIN 
#ifndef SUPPORT_OS_XP
    if (QSysInfo::windowsVersion()&(QSysInfo::WV_WINDOWS7 | QSysInfo::WV_WINDOWS8 | QSysInfo::WV_WINDOWS8_1 | QSysInfo::WV_WINDOWS10))
    {
        //mpDwmapiLib = new QLibrary("Dwmapi.dll");   //声明所用到的dll文件
        //if (mpDwmapiLib && mpDwmapiLib->load())
        //{
            BOOL bEnable = false;          
            ::DwmIsCompositionEnabled(&bEnable);
            //FunDwmIsCompositionEnabled pDDwmIsCompositionEnabled = (FunDwmIsCompositionEnabled)mpDwmapiLib->resolve("DwmIsCompositionEnabled");
            //if (pDDwmIsCompositionEnabled)
            //{
            //    pDDwmIsCompositionEnabled(&bEnable);
            //}
            if (bEnable)
            {
                setStyleSheet("QFrame#TMainWindow{background:transparent}");
                DWMNCRENDERINGPOLICY ncrp = DWMNCRP_ENABLED;             
                ::DwmSetWindowAttribute((HWND)winId(), DWMWA_NCRENDERING_POLICY, &ncrp, sizeof(ncrp));
                //FunDwmSetWindowAttribute pDwmSetWindowAttribute = (FunDwmSetWindowAttribute)mpDwmapiLib->resolve("DwmSetWindowAttribute");
                //if (pDwmSetWindowAttribute)
                //{
                //    pDwmSetWindowAttribute((HWND)winId(), DWMWA_NCRENDERING_POLICY, &ncrp, sizeof(ncrp));
                //}
                MARGINS margins = { -1 };
                ::DwmExtendFrameIntoClientArea((HWND)winId(), &margins);
                //FunDwmExtendFrameIntoClientArea pDwmExtendFrameIntoClientArea = (FunDwmExtendFrameIntoClientArea)mpDwmapiLib->resolve("DwmExtendFrameIntoClientArea");               
                //if (pDwmExtendFrameIntoClientArea)
                //{
                //    pDwmExtendFrameIntoClientArea((HWND)winId(), &margins);
                //}

            }
        //}
    }

#endif // !SUPPORT_OS_XP
#endif
}

void TMainWindow::OnExit()
{
    qApp->exit(0);
}

bool TMainWindow::nativeEvent(const QByteArray &eventType, void *message, long *result)
{
#ifdef Q_OS_WIN
    MSG* msg = reinterpret_cast<MSG*>(message);
    switch (msg->message)
    {
    case WM_DEVICECHANGE:
    {
        TMPRDevicesManager::Instance().HandleDeviceChanged(msg);
        break;
    }
    case WM_NCHITTEST:
    {
         *result = HTNOWHERE;
         if (IsInHeadBarArea(QCursor::pos())) {
                   *result = HTCAPTION;
                   return true;
            }
            return false;
    }
    default:
        break;
    }
#else 

#endif

    return QFrame::nativeEvent(eventType, message, result);
}

bool TMainWindow::IsInHeadBarArea(QPoint pos)
{
    if (mpHeadWidget)
    {
        return mpHeadWidget->InHeadBarArea(pos);
    }
    else
    {
        return false; 
    }
}

void TMainWindow::InitConnect()
{
    connect(mpHeadWidget, &THeadWidget::minimumSizeWindow, this, &QMainWindow::showMinimized);
    connect(mpHeadWidget, &THeadWidget::closeWindow, this, &QMainWindow::close);
    connect(mpHeadWidget, &THeadWidget::sysSetting, this, &TMainWindow::OnSystemSetting);
    connect(mpHeadWidget, &THeadWidget::versionCheck, this, &TMainWindow::OnVersionCheck);
    connect(mpCustomerTabWidget, &TLeftTabWidget::deviceTabClicked, mpDisPlayWidget, &TRightDisplayWidget::ShowDeviceTab);
    connect(mpCustomerTabWidget, &TLeftTabWidget::taskTabClicked, mpDisPlayWidget, &TRightDisplayWidget::ShowTaskTab);
    connect(&TMPRDevicesManager::Instance(), &TMPRDevicesManager::MPRDevicePlugIn, this, &TMainWindow::OnMPRDevicePlugIn);
    connect(&TMPRDevicesManager::Instance(), &TMPRDevicesManager::MPRDevicePlugOut, this, &TMainWindow::OnMPRDevicePlugOut);
    connect(&TMPRDevicesManager::Instance(), &TMPRDevicesManager::MPRCodeCaptured, this, &TMainWindow::OnMPRCodeCaptured);
    connect(&TUpdateVersionManager::Instance(), &TUpdateVersionManager::existNewVersion, this, &TMainWindow::OnFindNewVersion);
}

void TMainWindow::OnMPRDevicePlugIn(char disk)
{
    if (TDeviceDataManager::Instance().GetMPRDeviceConnected() && disk != TDeviceDataManager::Instance().GetDiskName())
    {        
        //新设备插入处理
    }
    else
    {
        TDeviceDataManager::Instance().SetDiskName(disk);
        bool bSpiDevice = TMPRDeviceUSBInfo::spi_device_map[disk];
        if (bSpiDevice)
        {
            QString strVersionInfo = TMPRDevicesManager::Instance().GetSPIFirmwareVersionInfo(disk);
            TUpdateVersionManager::Instance().CheckSPIFirmwareVersion(strVersionInfo, QString("%1:").arg(disk));
            char sn[15] = { 0 };
            if (TMPRDeviceUSBInfo::ReadDeviceSN(disk, sn))
            {
                mpDisPlayWidget->ShowSPIInfo(QString(sn));
            }
        }
        else
        {
            TMPRDeviceDesc desc;
            if (TMPRDevicesManager::Instance().GetMPRDeviceDesc(disk, desc))
            {
                TUpdateVersionManager::Instance().CheckFirmwareVersion(desc);
            }
            mpDisPlayWidget->UpdateDeviceInfo();
            TTaskManager::Instance()->LoadTaskInfo(desc.mDeviceId);
        }
    }
}

void TMainWindow::OnMPRDevicePlugOut(char disk)
{
    if (TDeviceDataManager::Instance().GetMPRDeviceConnected() && disk == TDeviceDataManager::Instance().GetDiskName())
    {
        TDeviceDataManager::Instance().SetDiskName(' ');        
        TTaskManager::Instance()->RemoveAllTask();
        mpDisPlayWidget->ResetDeviceInfo();
        TUpdateVersionManager::Instance().CloseWaittingDlg();
    }
    else
    {
        //其他处理
    }
}

void TMainWindow::OnMPRCodeCaptured(char disk, QString mprCode)
{
    if (TDeviceDataManager::Instance().GetMPRDeviceConnected() && disk == TDeviceDataManager::Instance().GetDiskName())
    {
        if (!mpSearchHandle) {
            mpSearchHandle = new TSearchHandle(disk);
            connect(mpSearchHandle, &TSearchHandle::FetchDownloadUrlReplied, this, &TMainWindow::OnFetchDownloadUrlReplied);
        }
        //mprCode = "0000022822";
        mpSearchHandle->FetchFiles("", mprCode.left(10));
    }
    else
    {
        //非当前设备处理
    }
}


void TMainWindow::OnFetchDownloadUrlReplied(const TMPRFileInfoItem& itemInfo, const TMPRSearchRepliedInfo& /*replyInfo*/, const QString& mprUrl, const QString& /*savePath*/, const QString& /*licensePath*/)
{
    qDebug() << __FUNCTION__ << "888 : start download url files !" << mprUrl;
    if (Network == itemInfo.tFileLocation)
    {
        TMPRDeviceDesc desc;
        TMPRDevicesManager::Instance().GetMPRDeviceDesc(TDeviceDataManager::Instance().GetDiskName(), desc);
        TTaskInfo info;
        info.strMediaPath = TDeviceDataManager::Instance().GetMediaPath();
        info.strRemoteUrl = mprUrl;
        info.strDeviceID = desc.mDeviceId;
        info.strDeviceSN = desc.mDeviceSN;
        info.mprInfo = itemInfo;
        TTaskManager::Instance()->AddTask(info);
        mpCustomerTabWidget->SetTabSelected(TLeftTabWidget::TT_Task);
    }
    else
    {
        qDebug() << __FUNCTION__ << "error : the task is exist !";
        //mpDisPlayWidget->AddDownloadTask(-1, itemInfo);
    } 
}

void TMainWindow::OnSystemSetting()
{
    TSystemSettingDlg dlg(this);
    dlg.exec();
}

void TMainWindow::OnVersionCheck()
{
    if (TAppCommon::Instance().IsExistNewVersion())
    {
        QByteArray byteData = TAppCommon::Instance().GetSoftPromateData();
        if (byteData.isEmpty())
        {
            TUpdateVersionManager::Instance().PromptInstallSoftware(TUpdateVersionManager::Install);
        }
        else
        {
            TUpdateVersionManager::Instance().PromptSoftwareVersion(byteData);
        }

        //TUpdateVersionManager::Instance().CheckClientVersion();
    }
    else
    {
        TWaittingDlg dlg(this);
        QString versionInfo;
        versionInfo.append(tr("Software version: V"));
        versionInfo.append(TAppCommon::Instance().GetCurrrentVersion());
        versionInfo.append("\n");
//         versionInfo.append(tr("Update date: "));
//         versionInfo.append(TUpdateVersionManager::Instance().GerSoftwareUpdateDate());
        versionInfo.append(tr("Copyright: shen zhen tian lang shi dai "));
        versionInfo.append("\n");
        versionInfo.append(tr("current version is the latest version"));
        versionInfo.append("\n\n");
        int flag = Qt::AlignLeft | Qt::AlignTop;

        dlg.ShowDialog(versionInfo, tr("Version Information"), flag);
    }
}

void TMainWindow::OnFindNewVersion()
{
    mpHeadWidget->SetFindNewVersion();
}



/*
//---------------------------Upgrade versions--------------------------------
void TMainWindow::moveEvent(QMoveEvent *e)
{
    QWidget::moveEvent(e);
    if (NULL != mpUpgradeClientUI){
        MoveToBottomRight(mpUpgradeClientUI);
    }
}

void TMainWindow::ShowUpgradeUI(bool bForce)
{
    if (bForce) {
        this->hide();
        mpUpgradeClientUI->setFixedSize(400, 130);
    }
    mpUpgradeClientUI->show();
    MoveToBottomRight(mpUpgradeClientUI);
}

void TMainWindow::MoveToBottomRight(QDialog* dlg)
{
    if (NULL == dlg) {
        return;
    }

    QPoint topLeft = QPoint();
    if (!mpUpgradeClientUI->IsForce()) {
        topLeft = this->geometry().bottomRight() - QPoint(dlg->width() + 8, dlg->height() + 8);
    }
    else {
        topLeft = mapToGlobal(QApplication::desktop()->availableGeometry().center() -
            mapToGlobal(QPoint(dlg->width() / 2 + 4, dlg->height() / 2 + 4)));
    }
    QRect rc = QRect(topLeft.x(), topLeft.y(), dlg->width(), dlg->height());
    dlg->setGeometry(rc);
}
*/
