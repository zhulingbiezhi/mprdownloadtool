#include "TSystemSettingDlg.h"
#include "TAppCommon.h"
TSystemSettingDlg::TSystemSettingDlg(QWidget* parent, Qt::WindowFlags f /*= 0*/)
: TBaseDialog(parent, f)
{
    CreateUI();
    InitConnect();
    InitData();
}


TSystemSettingDlg::~TSystemSettingDlg()
{

}

void TSystemSettingDlg::CreateUI()
{
    setObjectName("TSystemSettingDlg");
    CreateTitleWidget();
    CreateCenterWidget();
   

    QVBoxLayout* pMainLayout = new QVBoxLayout();
    pMainLayout->addWidget(mpTitleWidget);
    pMainLayout->addWidget(mpCenterWidget);
    pMainLayout->setContentsMargins(0, 0, 0, 0);
    pMainLayout->setSpacing(0);
    SetLayout(pMainLayout);


}

void TSystemSettingDlg::InitData()
{
    QString strPath = TAppCommon::Instance().GetCurrentSetSavePath();
   if (strPath.isEmpty())
   {
       strPath = "C:/MPRDownloadTool";
   }
   mpLineEdit->setText(strPath);
}

void TSystemSettingDlg::InitConnect()
{
    connect(mpBrowseBtn, &QPushButton::clicked, this, &TSystemSettingDlg::OnBrowseBtnClicked);
    connect(mpOkBtn, &QPushButton::clicked, this, &TSystemSettingDlg::OnOkBtnClicked);
    connect(mpCancelBtn, &QPushButton::clicked, this, &TSystemSettingDlg::close);
}

void TSystemSettingDlg::OnBrowseBtnClicked()
{
    QString strdir = QFileDialog::getExistingDirectory(this, tr("select save directory"), TAppCommon::Instance().GetCurrentSetSavePath());
    if (!strdir.isEmpty())
    {
        mpLineEdit->setText(strdir);
    }
}

void TSystemSettingDlg::OnOkBtnClicked()
{
    TAppCommon::Instance().ChangeSavePath(mpLineEdit->text());
    close();
}

void TSystemSettingDlg::CreateTitleWidget()
{
    mpTitleWidget = new QWidget(this);
    mpTitleWidget->setObjectName("TitleWidget");
    mpTitleWidget->setFixedHeight(35);

    QLabel *toptitle = new QLabel(tr("setting"), mpTitleWidget);
    toptitle->setAlignment(Qt::AlignVCenter);
    toptitle->setObjectName("title");

    QPushButton *closeBtn = new QPushButton(mpTitleWidget);
    closeBtn->setFlat(true);
    closeBtn->setFocusPolicy(Qt::NoFocus);
    closeBtn->setObjectName("closeButton");
    closeBtn->setToolTip(tr("Close"));
    connect(closeBtn, &QPushButton::clicked, this, &TSystemSettingDlg::close);

    QVBoxLayout *closelayout = new QVBoxLayout;
    closelayout->addWidget(closeBtn);
    closelayout->setContentsMargins(0, 0, 5, 17);

    QHBoxLayout *toplayout = new QHBoxLayout;
    toplayout->addWidget(toptitle);
    toplayout->addStretch(1);
    toplayout->addLayout(closelayout);
    toplayout->setContentsMargins(10, 0, 0, 0);
    mpTitleWidget->setLayout(toplayout);
}

void TSystemSettingDlg::CreateCenterWidget()
{
    mpCenterWidget = new QWidget(this);
    mpCenterWidget->setObjectName("CenterWidget");
    mpBrowseBtn = new QPushButton(tr("browse"), this);
    mpOkBtn = new QPushButton(tr("save"), this);
    mpCancelBtn = new QPushButton(tr("cancel"), this);

    mpBrowseBtn->setFixedWidth(50);

    mpBrowseBtn->setObjectName("browseBtn");
    mpOkBtn->setObjectName("OkBtn");
    mpCancelBtn->setObjectName("cancelBtn");

    mpLineEdit = new QLineEdit(this);
    mpLineEdit->setReadOnly(true);
    mpLineEdit->setFixedWidth(250);


    QHBoxLayout* pTopLayout = new QHBoxLayout();
    QHBoxLayout* pMidLayout = new QHBoxLayout();
    QHBoxLayout* pBottomLayout = new QHBoxLayout();

    QLabel* pTipsLabel = new QLabel(tr("save path:"));
    pTipsLabel->setFixedWidth(120);
    pTipsLabel->setAlignment(Qt::AlignRight);

    pTopLayout->addStretch();
    pTopLayout->addWidget(pTipsLabel);
    pTopLayout->addWidget(mpLineEdit);
    pTopLayout->addWidget(mpBrowseBtn);
    pTopLayout->addStretch();

    QLabel* pWaringLabel = new QLabel(tr("It does effective when next start software!"));
    pWaringLabel->setAlignment(Qt::AlignLeft);
    pTopLayout->addStretch();
    pMidLayout->addSpacing(120);
    pMidLayout->addWidget(pWaringLabel);
    pTopLayout->addStretch();

    pBottomLayout->addStretch();
    pBottomLayout->addWidget(mpOkBtn);
    pBottomLayout->addSpacing(20);
    pBottomLayout->addWidget(mpCancelBtn);
    pBottomLayout->addStretch();

    QVBoxLayout* pCenterLayout = new QVBoxLayout();
    pCenterLayout->addLayout(pTopLayout);
    pCenterLayout->addLayout(pMidLayout);
    pCenterLayout->addSpacing(40);
    pCenterLayout->addLayout(pBottomLayout);

    pCenterLayout->setContentsMargins(0, 30, 20, 30);
    mpCenterWidget->setLayout(pCenterLayout);

}
