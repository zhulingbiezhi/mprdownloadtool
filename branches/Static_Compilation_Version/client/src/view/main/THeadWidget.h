#ifndef _THEADWIDGET_H
#define _THEADWIDGET_H
class THeadWidget : public QFrame
{
    Q_OBJECT
public:
    THeadWidget(QWidget* parent = NULL);
    ~THeadWidget();

    bool InHeadBarArea(const QPoint& pos);
    void SetFindNewVersion();
private:
    void CreateUI();
    void InitConnect();

signals:
    void closeWindow();
    void minimumSizeWindow();
    void sysSetting();
    void versionCheck();


private:
    QLabel*            mpIconLabel;
    QPushButton*       mpSettingBtn;
    QPushButton*       mpVersionBtn;
    QPushButton*       mpMinBtn;
    QPushButton*       mpCloseBtn;

};

#endif