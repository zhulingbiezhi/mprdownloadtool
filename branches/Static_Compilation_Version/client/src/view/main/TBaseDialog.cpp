#include "TBaseDialog.h"

TBaseDialog::TBaseDialog(QWidget* parent, Qt::WindowFlags)
    : QDialog(parent, Qt::FramelessWindowHint)
    , mbMouseLeftKeyDown(false)
    , mbShake(false)
{
    setObjectName("TBaseDialog");
    setFocus();
    setAttribute(Qt::WA_TranslucentBackground);

    CreateUI();
    installEventFilter(this);
}

TBaseDialog::~TBaseDialog()
{
	
}

void TBaseDialog::CreateUI()
{
    mpMainFrame = new QFrame(this);
    mpMainFrame->setObjectName("dialogMainFrm");

    QVBoxLayout* pVMainLyt = new QVBoxLayout(this);
    pVMainLyt->setSpacing(0);
    pVMainLyt->addWidget(mpMainFrame);
    pVMainLyt->setMargin(SHADOWFRAMEWIDTH);

    // load images
    if (mImage.isNull())
    {
        QString imagePath(":/MPRDownloadTool/dialog_shadowframe.png");
        if (!mImage.load(imagePath))
        {
            return;
        }
    }

    if (mImageShake.isNull())
    {
        QString imagePath(":/MPRDownloadTool/dialog_shadowframe_shake.png");
        if (!mImageShake.load(imagePath))
        {
            return;
        }
    }
}

void TBaseDialog::paintEvent(QPaintEvent *)
{
    QPainter p(this);

    int nLeftMargin = SHADOWFRAMEWIDTH;
    int nRightMargin = SHADOWFRAMEWIDTH;
    int nTopMargin = SHADOWFRAMEWIDTH;
    int nBottomMargin = SHADOWFRAMEWIDTH;

    QRect topLeftRect = QRect(0, 0, nLeftMargin, nTopMargin);
    QRect topCenterRect = QRect(nLeftMargin, 0, rect().width() - nLeftMargin - nRightMargin, nTopMargin);
    QRect topRightRect = QRect(rect().width() - nRightMargin, 0, nRightMargin, nTopMargin);
    QRect leftCenterRect = QRect(0, nTopMargin, nLeftMargin, rect().height() - nTopMargin - nBottomMargin);
    QRect leftBottomRect = QRect(0, rect().height() - nBottomMargin, nLeftMargin, nBottomMargin);
    QRect rightCenterRect = QRect(rect().width() - nRightMargin, nTopMargin, nRightMargin, rect().height() - nTopMargin - nBottomMargin);
    QRect bottomCenterRect = QRect(nLeftMargin, rect().height() - nBottomMargin, rect().width() - nLeftMargin - nRightMargin, nBottomMargin);
    QRect bottomRightRect = QRect(rect().width() - nRightMargin, rect().height() - nBottomMargin, nRightMargin, nBottomMargin);
    QRect centerRect = QRect(nLeftMargin, nTopMargin, rect().width() - nLeftMargin - nRightMargin, rect().height() - nTopMargin - nBottomMargin);

    if (!mbShake)
    {
        QRect imageRect = mImage.rect();

        QRect sTopLeftRect = QRect(0, 0, nLeftMargin, nTopMargin);
        QRect sTopCenterRect = QRect(nLeftMargin, 0, imageRect.width() - nLeftMargin - nRightMargin, nTopMargin);
        QRect sTopRightRect = QRect(imageRect.width() - nRightMargin, 0, nRightMargin, nTopMargin);
        QRect sLeftCenterRect = QRect(0, nTopMargin, nLeftMargin, imageRect.height() - nTopMargin - nBottomMargin);
        QRect sLeftBottomRect = QRect(0, imageRect.height() - nBottomMargin, nLeftMargin, nBottomMargin);
        QRect sRightCenterRect = QRect(imageRect.width() - nRightMargin, nTopMargin, nRightMargin, imageRect.height() - nTopMargin - nBottomMargin);
        QRect sBottomCenterRect = QRect(nLeftMargin, imageRect.height() - nBottomMargin, imageRect.width() - nLeftMargin - nRightMargin, nBottomMargin);
        QRect sBottomRightRect = QRect(imageRect.width() - nRightMargin, imageRect.height() - nBottomMargin, nRightMargin, nBottomMargin);
        QRect sCenterRect = QRect(nLeftMargin, nTopMargin, imageRect.width() - nLeftMargin - nRightMargin, imageRect.height() - nTopMargin - nBottomMargin);

        p.drawImage(topLeftRect, mImage, sTopLeftRect);
        p.drawImage(topCenterRect, mImage, sTopCenterRect);
        p.drawImage(topRightRect, mImage, sTopRightRect);
        p.drawImage(leftCenterRect, mImage, sLeftCenterRect);
        p.drawImage(leftBottomRect, mImage, sLeftBottomRect);
        p.drawImage(rightCenterRect, mImage, sRightCenterRect);
        p.drawImage(bottomCenterRect, mImage, sBottomCenterRect);
        p.drawImage(bottomRightRect, mImage, sBottomRightRect);
    }
    else
    {
        QRect imageRect = mImageShake.rect();

        QRect sTopLeftRect = QRect(0, 0, nLeftMargin, nTopMargin);
        QRect sTopCenterRect = QRect(nLeftMargin, 0, imageRect.width() - nLeftMargin - nRightMargin, nTopMargin);
        QRect sTopRightRect = QRect(imageRect.width() - nRightMargin, 0, nRightMargin, nTopMargin);
        QRect sLeftCenterRect = QRect(0, nTopMargin, nLeftMargin, imageRect.height() - nTopMargin - nBottomMargin);
        QRect sLeftBottomRect = QRect(0, imageRect.height() - nBottomMargin, nLeftMargin, nBottomMargin);
        QRect sRightCenterRect = QRect(imageRect.width() - nRightMargin, nTopMargin, nRightMargin, imageRect.height() - nTopMargin - nBottomMargin);
        QRect sBottomCenterRect = QRect(nLeftMargin, imageRect.height() - nBottomMargin, imageRect.width() - nLeftMargin - nRightMargin, nBottomMargin);
        QRect sBottomRightRect = QRect(imageRect.width() - nRightMargin, imageRect.height() - nBottomMargin, nRightMargin, nBottomMargin);
        QRect sCenterRect = QRect(nLeftMargin, nTopMargin, imageRect.width() - nLeftMargin - nRightMargin, imageRect.height() - nTopMargin - nBottomMargin);

        p.drawImage(topLeftRect, mImageShake, sTopLeftRect);
        p.drawImage(topCenterRect, mImageShake, sTopCenterRect);
        p.drawImage(topRightRect, mImageShake, sTopRightRect);
        p.drawImage(leftCenterRect, mImageShake, sLeftCenterRect);
        p.drawImage(leftBottomRect, mImageShake, sLeftBottomRect);
        p.drawImage(rightCenterRect, mImageShake, sRightCenterRect);
        p.drawImage(bottomCenterRect, mImageShake, sBottomCenterRect);
        p.drawImage(bottomRightRect, mImageShake, sBottomRightRect);
    }
}

void TBaseDialog::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton)
    {
        mbMouseLeftKeyDown = true;
        mCurTopLeftPt = mapToGlobal(rect().topLeft());
        mCurMovPt = event->globalPos();
    }
    
    QDialog::mousePressEvent(event);
}

void TBaseDialog::mouseMoveEvent(QMouseEvent *event)
{
    if (mbMouseLeftKeyDown){
        MoveDialog(event->globalPos());
    }

    QDialog::mouseMoveEvent(event);
}

void TBaseDialog::mouseReleaseEvent(QMouseEvent *event)
{
    if (mbMouseLeftKeyDown)
    {
        mCurTopLeftPt = QPoint();
        mCurMovPt = QPoint();
        mbMouseLeftKeyDown = false;
    }    

    QDialog::mouseReleaseEvent(event);
}

void TBaseDialog::closeEvent(QCloseEvent *)
{
    if (this->parentWidget())
    {
        this->parentWidget()->setFocus();
    }

    close();
}

void TBaseDialog::MoveDialog(QPoint curMousePoint)
{
    QPoint movePos = curMousePoint - mCurMovPt;
    move(mCurTopLeftPt + movePos);
}

void TBaseDialog::SetLayout(QLayout* lyt)
{
    if (NULL != lyt && mpMainFrame)
    {
        mpMainFrame->setLayout(lyt);
    }
}

void TBaseDialog::SetFixedSize(int nWidth, int nHeight)
{
    if (mpMainFrame && nWidth > 0 && nHeight > 0)
    {
        mpMainFrame->setFixedSize(nWidth, nHeight);
    }
}

void TBaseDialog::SetMinimumSize(int nMinWidth, int nMinHeight)
{
    if (mpMainFrame && nMinWidth > 0 && nMinHeight > 0)
    {
        mpMainFrame->setMinimumSize(nMinWidth, nMinHeight);
    }
}

void TBaseDialog::SetMaximumSize(int nMaxWidth, int nMaxHeight)
{
    if (mpMainFrame && nMaxWidth > 0 && nMaxHeight > 0)
    {
        mpMainFrame->setMaximumSize(nMaxWidth, nMaxHeight);
    }
}

void TBaseDialog::SetStyleSheet(const QString& styleSheet)
{
    if (mpMainFrame)
    {
        mpMainFrame->setStyleSheet(styleSheet);
    }
}
