#ifndef _TLEFTTABWIDGET_H
#define _TLEFTTABWIDGET_H
class TLeftTabWidget : public QFrame
{

    Q_OBJECT
public:
    enum TTab_Type
    {
        TT_Device,
        TT_Task
    };
    TLeftTabWidget(QWidget* parent = NULL);
    ~TLeftTabWidget();
    void SetTabSelected(TTab_Type);

private:
    void CreateUI();
    void InitConect();

signals:
    void taskTabClicked();
    void deviceTabClicked();


private:
    QPushButton*           mpDeviceBtn;
    QPushButton*           mpTaskBtn;

};
#endif