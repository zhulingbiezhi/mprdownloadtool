#ifndef _TRIGHTDISPLAYWIDGET_H
#define _TRIGHTDISPLAYWIDGET_H
#include "TFileEnum.h"
class TDeviceTabWidget;
class TTaskTabWidget;
class TRightDisplayWidget : public QFrame
{
    Q_OBJECT
public:
    TRightDisplayWidget(QWidget* parent = NULL);
    ~TRightDisplayWidget();
    void AddDownloadTask(int nTaskId, TMPRFileInfoItem info);
    void ShowSPIInfo(QString strDeviceId);

private:
    void CreateUI();
    void InitConnect();


public slots:
    void ShowTaskTab();
    void ShowDeviceTab();
    void UpdateDeviceInfo();
    void ResetDeviceInfo();

private:
    TDeviceTabWidget*       mpDeviceTabWidget;
    TTaskTabWidget*         mpTaskTabWidget;
    QStackedLayout*         mpMainLayout;

};

#endif