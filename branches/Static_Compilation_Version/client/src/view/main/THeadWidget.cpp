#include "THeadWidget.h"

THeadWidget::THeadWidget(QWidget* parent /*= NULL*/)
: QFrame(parent)
{
    CreateUI();
    InitConnect();
}

THeadWidget::~THeadWidget()
{

}

void THeadWidget::CreateUI()
{
    setFixedHeight(52);
    setObjectName("THeadWidget");
    mpIconLabel = new QLabel(this);
    mpIconLabel->setText(tr("MPR DownloadTool"));
    mpIconLabel->setObjectName("titleLable");
    mpSettingBtn = new QPushButton(this);
    mpSettingBtn->setFixedSize(30, 30);
    mpSettingBtn->setToolTip(tr("Setting"));
    mpSettingBtn->setObjectName("settingBtn");
    mpVersionBtn = new QPushButton(this);
    mpVersionBtn->setFixedSize(21, 21);
    mpVersionBtn->setObjectName("versionBtn");
    mpVersionBtn->setToolTip(tr("Version"));
    //mpVersionBtn->setVisible(false);
    mpMinBtn = new QPushButton(this);
    mpMinBtn->setFixedSize(30, 30);
    mpMinBtn->setObjectName("minMizeBtn");
    mpMinBtn->setToolTip(tr("minimumSize"));
    mpCloseBtn = new QPushButton(this);
    mpCloseBtn->setFixedSize(30, 30);
    mpCloseBtn->setObjectName("closeBtn");
    mpCloseBtn->setToolTip(tr("close"));


    QHBoxLayout* pMainLayout = new QHBoxLayout();
    pMainLayout->setContentsMargins(20, 0, 20, 0);
    pMainLayout->setSpacing(0);
    pMainLayout->addWidget(mpIconLabel);
    pMainLayout->addStretch();
    pMainLayout->addWidget(mpSettingBtn);
    pMainLayout->addSpacing(5);
    pMainLayout->addWidget(mpVersionBtn);
    pMainLayout->addSpacing(20);
    pMainLayout->addWidget(mpMinBtn);
    pMainLayout->addSpacing(5);
    pMainLayout->addWidget(mpCloseBtn);

    setLayout(pMainLayout);

}

void THeadWidget::InitConnect()
{
    connect(mpCloseBtn, &QPushButton::clicked, this, &THeadWidget::closeWindow);
    connect(mpMinBtn, &QPushButton::clicked, this, &THeadWidget::minimumSizeWindow);
    connect(mpSettingBtn, &QPushButton::clicked, this, &THeadWidget::sysSetting);
    connect(mpVersionBtn, &QPushButton::clicked, this, &THeadWidget::versionCheck);
}

bool THeadWidget::InHeadBarArea(const QPoint& pos)
{
    QRect rectBtn = QRect(mpSettingBtn->mapToGlobal(mpSettingBtn->rect().topLeft()), mpCloseBtn->mapToGlobal(mpCloseBtn->rect().bottomRight()));
    if (rectBtn.contains(pos)) {
        return false;
    }
    if (rect().contains(this->mapFromGlobal(pos)))
    {
        return true;
    }
    return false;
}

void THeadWidget::SetFindNewVersion()
{
    mpVersionBtn->setProperty("hasNewVersion", true);
    if (style())
    {
        style()->polish(mpVersionBtn);
    }
}

