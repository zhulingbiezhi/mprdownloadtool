#ifndef _TSPLASHSCREEN_H_
#define _TSPLASHSCREEN_H_

class TSplashScreen: public QSplashScreen
{
        Q_OBJECT

public:
        TSplashScreen(const QPixmap& pixmap, bool isProgress);
        ~TSplashScreen();

        void SetProgress(int value); //外部改变进度

private slots:
        void OnProgressChanged(int);

private:
        QProgressBar*   mProgressBar;
};

#endif // _TSPLASHSCREEN_H_