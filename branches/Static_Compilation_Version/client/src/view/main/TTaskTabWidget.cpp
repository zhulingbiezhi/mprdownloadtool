#include "TTaskTabWidget.h"
#include "TDeviceInfoWidget.h"
#include "TDeviceDataManager.h"
#include "TAppCommon.h"



TTaskTabWidget::TTaskTabWidget(QWidget* parent /*= NULL*/)
: QFrame(parent)
{
    CreateUI();
    InitConnect();
}

TTaskTabWidget::~TTaskTabWidget()
{

}

void TTaskTabWidget::CreateUI()
{
    setObjectName("TTaskTabWidget");
    mpDeviceInfoWidget = new TDeviceInfoWidget(this);

    mpTaskTableWidget = new QTableWidget(this);


    mpTipsLabel = new QLabel(this);
    mpTipsLabel->setFixedHeight(50);
    mpTipsLabel->setAlignment(Qt::AlignHCenter);
    mpTipsLabel->setText(tr("Warm prompt: Using USB data lin to connet device with computer first, and make computer staying connected to the Internet.\n Then your can download mpr files by touch mpr book using touch and talk pen!"));

    mpNoTaskLabel = new QLabel(this);
    mpNoTaskLabel->setAlignment(Qt::AlignCenter);
    mpNoTaskLabel->setObjectName("NoTaskLabel");
    mpNoTaskLabel->setText(tr("There is no download task, please touch reading the MPR book"));

    mpStackedLayout = new QStackedLayout();
    mpStackedLayout->setContentsMargins(0, 0, 0, 0);
    mpStackedLayout->addWidget(mpNoTaskLabel);
    mpStackedLayout->addWidget(mpTaskTableWidget);

    QVBoxLayout* pMainLayout = new QVBoxLayout();
    pMainLayout->setContentsMargins(0, 0, 0, 0);
    pMainLayout->setSpacing(0);

    pMainLayout->addWidget(mpDeviceInfoWidget);
    pMainLayout->addLayout(mpStackedLayout);
    pMainLayout->addSpacing(10);
    pMainLayout->addWidget(mpTipsLabel);

    setLayout(pMainLayout);
    InitTable();
}


void TTaskTabWidget::InitConnect()
{
    connect(TTaskManager::Instance(), &TTaskManager::sigDownloadMessage, this, &TTaskTabWidget::OnHandleDownloadMsg);
    connect(TTaskManager::Instance(), &TTaskManager::sigCopyfileMessage, this, &TTaskTabWidget::OnHandleCopyfileMsg);
    connect(TTaskManager::Instance(), &TTaskManager::sigAddTask, this, &TTaskTabWidget::OnAddTask);
}


void TTaskTabWidget::InitTable()
{
    mpTaskTableWidget->horizontalHeader()->setStretchLastSection(true);
    mpTaskTableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
    mpTaskTableWidget->verticalHeader()->setHidden(true);
    mpTaskTableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
    mpTaskTableWidget->setSelectionMode(QAbstractItemView::SingleSelection);
    mpTaskTableWidget->horizontalHeader()->setHighlightSections(false);
    mpTaskTableWidget->setFocusPolicy(Qt::NoFocus);
    mpTaskTableWidget->setShowGrid(false);

    QStringList headers;
    headers << tr("  ") << tr("Book Name") << tr("Publisher Name") << tr("Format") << tr("Status") << tr("Operate");

    mpTaskTableWidget->setColumnCount(headers.size());
    mpTaskTableWidget->setHorizontalHeaderLabels(headers);
    mpTaskTableWidget->horizontalHeader()->setDefaultAlignment(Qt::AlignHCenter | Qt::AlignVCenter);

    mpTaskTableWidget->setColumnWidth(0, 30);
    mpTaskTableWidget->setColumnWidth(1, 200);
    mpTaskTableWidget->setColumnWidth(2, 150);
    mpTaskTableWidget->setColumnWidth(3, 100);
    mpTaskTableWidget->setColumnWidth(4, 280);
}

void TTaskTabWidget::UpdateDeviceInfo()
{
    mpDeviceInfoWidget->UpdateShowInfo();
}

void TTaskTabWidget::ShowSPIInfo(QString strDeviceSn)
{
    mpDeviceInfoWidget->ShowSPIInfo(strDeviceSn);
}
void TTaskTabWidget::ResetDeviceInfo()
{
    ResetTaskTable();
    mpDeviceInfoWidget->ResetDeviceInfo();
}

void TTaskTabWidget::OnAddTask(int nTaskId, TMPRFileInfoItem mprInfo)
{
    TTableTaskInfo task;
    task.nTaskId = nTaskId;
    task.downloadProgress = 0;
    task.copyfileProgress = 0;
    task.downloadInfo = mprInfo;
    AddRow(task);
    mTaskInfoHash.insert(nTaskId, task);
    mpStackedLayout->setCurrentWidget(mpTaskTableWidget);
}

void TTaskTabWidget::OnHandleDownloadMsg(int taskID, int msgType, const QVariant& msgBody, TSVHashMap& extraParams)
{
    switch (msgType) {
    case HTM_PROGRESS:   //进度消息
        HandleProgressMsg(taskID, msgBody.toInt());
        break;
    case HTM_STATE:     //状态切换消息
        HandleStateMsg(taskID, msgBody.toInt(), extraParams);
        break;
    case HTM_ERROR:     //错误消息
        HandleErrorMsg(taskID, msgBody.toString());
        break;
    case HTM_TEXT:      //文本消息
        break;
    case HTM_SIZE:      //任务大小反馈
        break;
    case HTM_PHASE:     //任务子阶段反馈
        break;
    default:
        break;
    }
}

void TTaskTabWidget::OnHandleCopyfileMsg(int taskID, int msgType, const QVariant& msgBody, TSVHashMap& extraParams)
{
    switch (msgType) {
    case HTM_PROGRESS:   //进度消息
        HandleProgressMsg(taskID, msgBody.toInt());
        break;
    case HTM_STATE:     //状态切换消息
        HandleStateMsg(taskID, msgBody.toInt(), extraParams);
        break;
    case HTM_ERROR:     //错误消息
        //HandleErrorMsg(taskID, msgBody.toString());
        break;
    case HTM_TEXT:      //文本消息
        break;
    case HTM_SIZE:      //任务大小反馈
        break;
    case HTM_PHASE:     //任务子阶段反馈
        break;
    default:
        break;
    }
}

void TTaskTabWidget::AddRow(TTableTaskInfo& taskInfo)
{
    taskInfo.nRow = mpTaskTableWidget->rowCount();
    mpTaskTableWidget->insertRow(mpTaskTableWidget->rowCount());
    //mpBookInfoTable->setCellWidget(mpBookInfoTable->rowCount() - 1, 0, InitIdWidget(infoItem.nWorkFlowID, infoItem.nPreProStatu, infoItem.nWorkStatu));

    QTableWidgetItem *item0 = new QTableWidgetItem(QString("  %1").arg(mpTaskTableWidget->rowCount()));
    item0->setTextAlignment(Qt::AlignLeft | Qt::AlignVCenter);
    mpTaskTableWidget->setItem(mpTaskTableWidget->rowCount() - 1, 0, item0);

    QTableWidgetItem *item1 = new QTableWidgetItem(QString("  %1").arg(taskInfo.downloadInfo.tBookName));
    item1->setTextAlignment(Qt::AlignLeft | Qt::AlignVCenter);
    mpTaskTableWidget->setItem(mpTaskTableWidget->rowCount() - 1, 1, item1);

    QTableWidgetItem *item2 = new QTableWidgetItem(QString("  %1").arg(taskInfo.downloadInfo.tPublisher));
    item1->setTextAlignment(Qt::AlignLeft | Qt::AlignVCenter);
    mpTaskTableWidget->setItem(mpTaskTableWidget->rowCount() - 1, 2, item2);

    QTableWidgetItem *item3 = new QTableWidgetItem(QString("  %1").arg("MPR"));
    item3->setTextAlignment(Qt::AlignLeft | Qt::AlignVCenter);
    mpTaskTableWidget->setItem(mpTaskTableWidget->rowCount() - 1, 3, item3);


    //QDate tempDate = QDate::fromString(infoItem.strCreateTime.left(10), "yyyy-MM-dd");
    //QTime tempTime = QTime::fromString(infoItem.strCreateTime.right(8), "hh:mm:ss");
    //QDateTime tempDataTime(tempDate, tempTime, Qt::UTC);
    mpTaskTableWidget->setCellWidget(mpTaskTableWidget->rowCount() - 1, 4, InitStateCellWidget(taskInfo));
    mpTaskTableWidget->setCellWidget(mpTaskTableWidget->rowCount() - 1, 5, InitOptCellWidget(taskInfo));
}

void TTaskTabWidget::ResetTaskTable()
{
    mpTaskTableWidget->clearContents();
    mpTaskTableWidget->setRowCount(0);
    QHashIterator<int, TTableTaskInfo> it(mTaskInfoHash);
    while (it.hasNext())
    {
        TTableTaskInfo task = it.next().value();
        TTaskManager::Instance()->RemoveTask(task.nTaskId);
    } 
    mTaskInfoHash.clear();
    mpStackedLayout->setCurrentWidget(mpNoTaskLabel);
}

int TTaskTabWidget::GetClickedRow()
{
    int nRow = 0;
    int nTempRow = mpTaskTableWidget->indexAt(mpTaskTableWidget->mapFromGlobal(QCursor::pos())).row();
    if (nTempRow < 0) {
        nRow = mpTaskTableWidget->rowCount() - 1;
    }
    else {
        nRow = nTempRow - 1;
    }
    return nRow;
}

QWidget* TTaskTabWidget::InitStateCellWidget(TTableTaskInfo& taskInfo)
{
    QWidget*  pWidget = new QWidget(this);
    QProgressBar* pProgress = new QProgressBar(this);
    pProgress->setObjectName("TaskProgressBar");
    pProgress->setFixedSize(180, 16);
    pProgress->setMaximum(100);

    QLabel* pStateLabel = new QLabel(this);
    QHBoxLayout* pLayout = new QHBoxLayout(pWidget);
    pLayout->setContentsMargins(0, 0, 0, 0);
    pLayout->addWidget(pProgress);
    pLayout->addWidget(pStateLabel);
    taskInfo.pProgressBar = pProgress;
    taskInfo.pLabel = pStateLabel;
    return pWidget;
}

QWidget* TTaskTabWidget::InitOptCellWidget(TTableTaskInfo& taskInfo)
{
    QWidget*  pWidget = new QWidget(this);
    QPushButton* pPauseBtn = new QPushButton(this);
    QPushButton* pStartBtn = new QPushButton(this);
    QPushButton* pDeleteBtn = new QPushButton(this);

    pPauseBtn->setFixedSize(22, 22);
    pStartBtn->setFixedSize(22, 22);
    pDeleteBtn->setFixedSize(22, 22);

    pStartBtn->setToolTip(tr("start"));
    pPauseBtn->setToolTip(tr("pause"));
    pDeleteBtn->setToolTip(tr("delete"));

    pStartBtn->setObjectName("PauseOpBtn");
    pPauseBtn->setObjectName("StartOpBtn");
    pDeleteBtn->setObjectName("RemoveBtn");


    taskInfo.pStartBtn = pStartBtn;
    taskInfo.pDeleteBtn = pDeleteBtn;
    taskInfo.pPauseBtn = pPauseBtn;

    connect(pPauseBtn, &QPushButton::clicked, this, &TTaskTabWidget::OnPauseTask);
    connect(pDeleteBtn, &QPushButton::clicked, this, &TTaskTabWidget::OnDeleteTask);
    connect(pStartBtn, &QPushButton::clicked, this, &TTaskTabWidget::OnStartTask);

    pStartBtn->setVisible(false);
    QHBoxLayout* pLayout = new QHBoxLayout(pWidget);
    pLayout->setContentsMargins(0, 0, 0, 0);
    pLayout->addStretch();
    pLayout->addWidget(pStartBtn);
    pLayout->addWidget(pPauseBtn);
    pLayout->addSpacing(5);
    pLayout->addWidget(pDeleteBtn);
    pLayout->addStretch();
    return pWidget;
}

void TTaskTabWidget::OnDeleteTask()
{
    int nRow = GetClickedRow();

    if (nRow != -1 && nRow < mpTaskTableWidget->rowCount())
    {
        int task_id = GetTaskIDByRow(nRow);
        TTaskManager::Instance()->RemoveTask(task_id, true);
        mTaskInfoHash.remove(task_id);
    }
    mpTaskTableWidget->removeRow(nRow);

    if (mTaskInfoHash.isEmpty())
    {
        mpStackedLayout->setCurrentWidget(mpNoTaskLabel);
    }
    else
    {
        for (int i = 0; i < mpTaskTableWidget->rowCount(); i++)
        {
            mpTaskTableWidget->item(i, 0)->setText(QString("  %1").arg(i+1));
        }
        QHashIterator<int, TTableTaskInfo> it(mTaskInfoHash);
        while (it.hasNext())
        {
            TTableTaskInfo task = it.next().value();
            if (task.nRow > nRow)
            {
                mTaskInfoHash[task.nTaskId].nRow--;
            }
        }
    }
}

void TTaskTabWidget::OnPauseTask()
{
    int nRow = GetClickedRow();
    if (nRow != -1 && nRow < mpTaskTableWidget->rowCount())
    {
        int task_id = GetTaskIDByRow(nRow);
        TTaskManager::Instance()->StopTask(task_id);
    }
}

void TTaskTabWidget::OnStartTask()
{
    int nRow = GetClickedRow();
    if (nRow != -1 && nRow < mpTaskTableWidget->rowCount())
    {
        int task_id = GetTaskIDByRow(nRow);
        TTaskManager::Instance()->StartTask(task_id);
    }
}

void TTaskTabWidget::UpdatetTaskState(int taskID, TTaskState taskState, TSVHashMap& extraParams)
{
    TTableTaskInfo& taskInfo = mTaskInfoHash[taskID];
    if (taskInfo.pLabel == nullptr || taskInfo.pPauseBtn == nullptr || taskInfo.pStartBtn == nullptr)
    {
        qDebug() << __FUNCTION__ << "error : the task is not create !";
        return;
    }
    taskInfo.pPauseBtn->setEnabled(true);
    switch (taskState)
    {
    case TS_DOWNLOAD_PREPARE:
        taskInfo.pLabel->setText(tr("Prepare download"));
        taskInfo.pPauseBtn->setVisible(true);
        taskInfo.pStartBtn->setVisible(false);
        break;
    case TS_DOWNLOAD:
        taskInfo.pLabel->setText(tr("Downloading"));
        taskInfo.pPauseBtn->setVisible(true);
        taskInfo.pStartBtn->setVisible(false);
        break;
    case TS_DOWNLOAD_PAUSE:
        taskInfo.pLabel->setText(tr("Download pause"));
        taskInfo.pStartBtn->setVisible(true);
        taskInfo.pPauseBtn->setVisible(false);
        break;
    case TS_DOWNLOAD_FINISH:
        taskInfo.pLabel->setText(tr("Download finished"));
        taskInfo.pPauseBtn->setVisible(true);
        taskInfo.pStartBtn->setVisible(false);
        break;
    case TS_DOWNLOAD_ERROR:
        taskInfo.pPauseBtn->setVisible(true);
        taskInfo.pStartBtn->setVisible(false);
        taskInfo.pPauseBtn->setEnabled(false);
        taskInfo.pLabel->setText(tr("Download failed"));
        break;
    case TS_COPYFILE_PREPARE:
        taskInfo.pStartBtn->setVisible(false);
        taskInfo.pPauseBtn->setVisible(true);
        taskInfo.pPauseBtn->setEnabled(false);
        taskInfo.pLabel->setText(tr("Prepare copy"));
        taskInfo.downloadInfo.tVersion = extraParams.value(VERSION).toString();
        taskInfo.downloadInfo.tFileSize = extraParams.value(FILESIZE).toInt();
        HandleProgressMsg(taskID, 0);
        break;
    case TS_COPYFILE:
        taskInfo.pPauseBtn->setVisible(true);
        taskInfo.pStartBtn->setVisible(false);
        taskInfo.pLabel->setText(tr("Copying"));
        break;
    case TS_COPYFILE_PAUSE:
        taskInfo.pStartBtn->setVisible(true);
        taskInfo.pPauseBtn->setVisible(false);
        taskInfo.pLabel->setText(tr("Copy pause"));
        break;
    case TS_COPYFILE_FINISH:
        taskInfo.pPauseBtn->setVisible(true);
        taskInfo.pStartBtn->setVisible(false);
        taskInfo.pPauseBtn->setEnabled(false);
        taskInfo.pLabel->setText(tr("Copy finished"));
        UpdateDeviceInfo();
        taskInfo.downloadInfo.tFileCreateTime = QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss");
        taskInfo.downloadInfo.tFilePath = extraParams.value(MPRPATH).toString();
        taskInfo.downloadInfo.tLicencePath = extraParams.value(LICENSEPATH).toString();
        TDeviceDataManager::Instance().AddDeviceFile(taskInfo.downloadInfo);
        emit capacityChange();
        break;
    case TS_COPYFILE_ERROR:
        taskInfo.pPauseBtn->setVisible(true);
        taskInfo.pStartBtn->setVisible(false);
        taskInfo.pPauseBtn->setEnabled(false);
        taskInfo.pLabel->setText(tr("Copy failed"));
        break;
    }
}

int TTaskTabWidget::GetTaskIDByRow(int nRow)
{
    QHashIterator<int, TTableTaskInfo> it(mTaskInfoHash);
    while (it.hasNext())
    {
        TTableTaskInfo task = it.next().value();
        if (task.nRow == nRow)
        {
            return it.key();
        }
    }
    qDebug() << __FUNCTION__ << "error : can't find the task by row !";
    return -1;
}

void TTaskTabWidget::HandleProgressMsg(int nDownloadTaskId, int nProgerss)
{
    if (nDownloadTaskId != -1)
    {
        if (mTaskInfoHash.find(nDownloadTaskId) != mTaskInfoHash.end())
        {
            mTaskInfoHash[nDownloadTaskId].downloadProgress = nProgerss;
            mTaskInfoHash[nDownloadTaskId].pProgressBar->setValue(nProgerss);
        }
        else
        {
            qDebug() << __FUNCTION__ << "can't find the downloadProgress !" << nDownloadTaskId;
        }
    }
}

void TTaskTabWidget::HandleStateMsg(int nDownloadTaskId, int nState, TSVHashMap& extraParams)
{
    UpdatetTaskState(nDownloadTaskId, (TTaskState)nState, extraParams);
}

void TTaskTabWidget::HandleErrorMsg(int nDownloadTaskId, QString strError)
{
    qDebug() << __FUNCTION__ << nDownloadTaskId << strError;
}

