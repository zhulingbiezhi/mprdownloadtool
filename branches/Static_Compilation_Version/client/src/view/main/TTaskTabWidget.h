#ifndef _TTASKTABWIDGET_H
#define _TTASKTABWIDGET_H
#include "TFileEnum.h"
#include "TCopyFileState.h"
#include "TTaskManager.h"

class TDeviceInfoWidget;

class TTaskTabWidget : public QFrame
{
    Q_OBJECT
public:

    struct TTableTaskInfo
    {
        int                 nRow;
        int                 nTaskId;
        int                 downloadProgress;
        int                 copyfileProgress;
        TMPRFileInfoItem    downloadInfo;
        QProgressBar*       pProgressBar;
        QLabel*             pLabel;
        QPushButton*        pDeleteBtn;
        QPushButton*        pPauseBtn;
        QPushButton*        pStartBtn;
        TTableTaskInfo()
        {
            pDeleteBtn = nullptr;
            pPauseBtn = nullptr;
            pStartBtn = nullptr;
        }
    };

    TTaskTabWidget(QWidget* parent = NULL);
    ~TTaskTabWidget();
    void ResetDeviceInfo();
    void ShowSPIInfo(QString);

private:
    int  GetClickedRow();
    int  GetTaskIDByRow(int nRow);

    void CreateUI();
    void InitTable();
    void InitConnect();
    void ResetTaskTable();
    void AddRow(TTableTaskInfo& taskInfo);
    void UpdatetTaskState(int taskID, TTaskState taskState, TSVHashMap& extraParams);

    void HandleProgressMsg(int nDownloadTaskId, int nProgerss);
    void HandleStateMsg(int nDownloadTaskId, int nState, TSVHashMap& extraParams);
    void HandleErrorMsg(int nDownloadTaskId, QString strError);

    QWidget* InitStateCellWidget(TTableTaskInfo& taskInfo);
    QWidget* InitOptCellWidget(TTableTaskInfo& taskInfo);


public slots:
    void UpdateDeviceInfo();
    void OnAddTask(int nTaskId, TMPRFileInfoItem mprInfo);
    void OnHandleDownloadMsg(int taskID, int msgType, const QVariant& msgBody, TSVHashMap& extraParams);
    void OnHandleCopyfileMsg(int taskID, int msgType, const QVariant& msgBody, TSVHashMap& extraParams);

private slots:
    void OnDeleteTask();
    void OnPauseTask();
    void OnStartTask();

signals:
    void capacityChange();

private:
    TDeviceInfoWidget*    mpDeviceInfoWidget;
    QTableWidget*         mpTaskTableWidget;
    QLabel*               mpTipsLabel;
    QLabel*               mpNoTaskLabel;
    QStackedLayout*       mpStackedLayout;
    //QList<TTableTaskInfo>      mTaskInfoList;
    QHash<int, TTableTaskInfo> mTaskInfoHash;
};
#endif