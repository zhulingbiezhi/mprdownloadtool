#include "TPopupDialog.h"

TPopupDialog::TPopupDialog(int buttons, QString title, const QString& text, QWidget* parent, int type, Qt::WindowFlags f)
    : TBaseDialog(parent, f)
    , mResult(-1)
    , opacityInc(0.05)
    , maxOpacity(1)
    , isDisplay(true)
    , displayTime(0)
{
    this->setObjectName("PopupDialog");

    CreateTitleWidget(title);      
    CreateCenterWidget(type, text, buttons);   

    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->addWidget(mpTitleWidget);
    mainLayout->addWidget(mpCenterWidget);
    mainLayout->setSpacing(0);
    mainLayout->setContentsMargins(0, 0, 0, 0);
    SetLayout(mainLayout);
}

TPopupDialog::~TPopupDialog()
{

}

void TPopupDialog::CreateTitleWidget(QString title)
{
    mpTitleWidget = new QWidget(this);
    mpTitleWidget->setObjectName("TitleWidget");
    mpTitleWidget->setFixedHeight(35);

    QLabel *toptitle = new QLabel(title, mpTitleWidget);
    toptitle->setAlignment(Qt::AlignVCenter);
    toptitle->setObjectName("title");

    QPushButton *closeBtn = new QPushButton(mpTitleWidget);
    closeBtn->setFlat(true);
    closeBtn->setFocusPolicy(Qt::NoFocus);
    closeBtn->setObjectName("closeButton");
    closeBtn->setToolTip(tr("Close"));
    connect(closeBtn, &QPushButton::clicked, this, &TPopupDialog::OnClose);

    QVBoxLayout *closelayout = new QVBoxLayout;
    closelayout->addWidget(closeBtn);
    closelayout->setContentsMargins(0, 0, 5, 17);

    QHBoxLayout *toplayout = new QHBoxLayout;
    toplayout->addWidget(toptitle);
    toplayout->addStretch(1);
    toplayout->addLayout(closelayout);
    toplayout->setContentsMargins(10, 0, 0, 0);
    mpTitleWidget->setLayout(toplayout);
}

void TPopupDialog::CreateCenterWidget(int type, QString mString, int button)
{   
    mpCenterWidget = new QWidget(this);
    mpCenterWidget->setObjectName("CenterWidget");
    mpCenterWidget->setFixedHeight(160);

    mpMainEnterKey = new QShortcut(QKeySequence("Return"), this);
    mpSmallEnterKey = new QShortcut(QKeySequence("Enter"), this);

    QLabel *labelImage = new QLabel(mpCenterWidget);
    labelImage->setFixedSize(46, 46);

    QImage typeImg("");
    if (type == Error)
    {
        typeImg.load(":/MPRDownloadTool/error.png");
    }
    else if (type == Warning)
    {
        typeImg.load(":/MPRDownloadTool/warning.png");
    }
    else
    {
        typeImg.load(":/MPRDownloadTool/prompt.png");
    }
    labelImage->setPixmap(QPixmap::fromImage(typeImg));

    QLabel *labelMessage = new QLabel(mString, mpCenterWidget);
    labelMessage->setObjectName("MessageText");

    QVBoxLayout *msgLayout = new QVBoxLayout;
    msgLayout->addStretch(1);
    msgLayout->addWidget(labelImage, 0, Qt::AlignHCenter);
    msgLayout->addWidget(labelMessage, 0, Qt::AlignHCenter);
    msgLayout->addStretch(1);
    msgLayout->setSpacing(0);
    msgLayout->setContentsMargins(0, 0, 0, 0);
    
    // Buttons Layout
    mpBtnOk = new QPushButton(tr("Ok"), mpCenterWidget);
    mpBtnOk->setObjectName("OkButton");
    mpBtnOk->setFocusPolicy(Qt::NoFocus);
    mpBtnOk->setCheckable(true);
    mpBtnOk->hide();
    connect(mpBtnOk, &QPushButton::clicked, this, &TPopupDialog::OnOKAction);

    mpBtnCancel = new QPushButton(tr("Cancel"), mpCenterWidget);
    mpBtnCancel->setObjectName("CancelButton");
    mpBtnCancel->setFocusPolicy(Qt::NoFocus);
    mpBtnCancel->hide();
    connect(mpBtnCancel, &QPushButton::clicked, this, &TPopupDialog::OnCancelAction);

    mpBtnYes = new QPushButton(tr("Yes"), mpCenterWidget);
    mpBtnYes->setObjectName("YesButton");
    mpBtnYes->setFocusPolicy(Qt::NoFocus);
    mpBtnYes->setCheckable(true);
    mpBtnYes->hide();
    connect(mpBtnYes, &QPushButton::clicked, this, &TPopupDialog::OnYesAction);

    mpBtnNO = new QPushButton(tr("No"), mpCenterWidget);
    mpBtnNO->setObjectName("NoButton");
    mpBtnNO->setFocusPolicy(Qt::NoFocus);
    mpBtnNO->hide();
    connect(mpBtnNO, &QPushButton::clicked, this, &TPopupDialog::OnNoAction);

    QHBoxLayout *btnLayout = new QHBoxLayout;
    btnLayout->addStretch(1);
    btnLayout->setSpacing(0);
    btnLayout->setContentsMargins(20, 0, 0, 0);

    QVBoxLayout *centerLayout = new QVBoxLayout;
    centerLayout->addStretch(1);
    centerLayout->addLayout(msgLayout);
    centerLayout->setSpacing(0);

    if (NULL == button)
    {
        centerLayout->addStretch(1);
        centerLayout->setContentsMargins(20, 20, 20, 20);
        mpCenterWidget->setLayout(centerLayout);
        return;
    }

    centerLayout->addSpacing(30);

    if (button & Yes)
    {
        btnLayout->addWidget(mpBtnYes);
        btnLayout->addSpacing(20);

        mpBtnYes->show();
        mpBtnYes->setCheckable(true);
        mpBtnYes->setChecked(true);
        mpBtnOk->setChecked(false);
        connect(mpMainEnterKey, &QShortcut::activated, this, &TPopupDialog::OnYesAction);
        connect(mpSmallEnterKey, &QShortcut::activated, this, &TPopupDialog::OnYesAction);
    }

    if (button & Ok)
    {
        btnLayout->addWidget(mpBtnOk);
        btnLayout->addSpacing(20);

        mpBtnOk->show();
        mpBtnOk->setCheckable(true);
        mpBtnOk->setChecked(true);
        mpBtnYes->setChecked(false);
        connect(mpMainEnterKey, &QShortcut::activated, this, &TPopupDialog::OnOKAction);
        connect(mpSmallEnterKey, &QShortcut::activated, this, &TPopupDialog::OnOKAction);
    }

    if (button & No)
    {
        btnLayout->addWidget(mpBtnNO);
        btnLayout->addSpacing(20);

        mpBtnNO->show();
    }

    if (button & Cancel)
    {
        btnLayout->addWidget(mpBtnCancel);
        btnLayout->addSpacing(20);

        mpBtnCancel->show();
    }
    btnLayout->addStretch(1);
    centerLayout->addLayout(btnLayout);
    centerLayout->addStretch(1);
    centerLayout->setContentsMargins(20, 20, 15, 20);
    mpCenterWidget->setLayout(centerLayout);
}

int TPopupDialog::GetResult()
{
    return mResult;
}

void TPopupDialog::OnOKAction()
{
    mResult = Ok;
    emit ClickedBnt();
    close();
}

void TPopupDialog::OnCancelAction()
{
    mResult = Cancel;
    emit ClickedBnt();
    close();
}

void TPopupDialog::OnYesAction()
{
    mResult = Yes;
    emit ClickedBnt();
    close();
}

void TPopupDialog::OnNoAction()
{
    mResult = No;
    emit ClickedBnt();
    close();
}

void TPopupDialog::OnClose()
{
    mResult = Close;  
    emit ClickedBnt();
    close();
}

void TPopupDialog::OnTimeOut()
{
    this->close();
    return;

    if (this->isVisible())
    {
        if (isDisplay)
        {
            if (this->windowOpacity() < maxOpacity)
            {
                this->setWindowOpacity(this->windowOpacity() + opacityInc);
            }
            else
            {
                displayTime += 50;
                if (displayTime >= mCloseTime)
                {
                    displayTime = 0;
                    isDisplay = false;
                }
            }
        }
        else
        {
            if (this->windowOpacity() > 0.0)
            {
                this->setWindowOpacity(this->windowOpacity() - opacityInc);
            }
            else
            {
                mResult = Close;
                if (mpCloseTimer->isActive())
                {
                    mpCloseTimer->stop();
                }
                emit ClickedBnt();
                this->close();
            }                       
        }
    }
}
      
void TPopupDialog::AutoCloseDialog(int mtime)
{  
    mCloseTime = mtime * 1000;
    mpCloseTimer = new QTimer(this);
    connect(mpCloseTimer, &QTimer::timeout, this, &TPopupDialog::OnTimeOut);
    //this->setWindowOpacity(0);
    mpCloseTimer->start(mCloseTime);
}

void TPopupDialog::SetButtonText(TMessageBoxButton btnId, QString text)
{
    switch (btnId)
    {
    case Ok:
        mpBtnOk->setText(text);
        break;
    case Cancel:
        mpBtnCancel->setText(text);
        break;
    case Yes:
        mpBtnYes->setText(text);
        break;
    case No:
        mpBtnNO->setText(text);
        break;
    case Close:
        break;
    default:
        break;
    }
}