#include "TBaseMessageBox.h"

TBaseMessageBox::TBaseMessageBox(QWidget* parent, const QString& strMsgText , Btn_Type btnType)
: TCommonDialog(true,parent)
, mBtnType(btnType)
, mpImageLbl(NULL)
, mpTipsMsgLbl(NULL)
, mpOkBtn(NULL)
, mpCancelBtn(NULL)
, mpTipsFrame(NULL)
, mnBtnID(-1)
{
	setObjectName("TBaseMessageBox");
	setMinimumSize(360, 200);
    setMaximumSize(640, 200);
	CreateUI();
	CreateLayOut();
	CreateConnect();
	MoveToParentCenter(parent);
	SetMsgText(strMsgText);
}

TBaseMessageBox::~TBaseMessageBox()
{

}

void TBaseMessageBox::CreateUI()
{
	mpImageLbl = new QLabel(this);
	mpTipsMsgLbl = new QLabel(this);
	mpOkBtn = new QPushButton(this);
	mpCancelBtn = new QPushButton(this);
	
	mpOkBtn->setFocusPolicy(Qt::NoFocus);
	mpCancelBtn->setFocusPolicy(Qt::NoFocus);
	mpCancelBtn->setVisible(false);

	mpImageLbl->setObjectName("ImageLabel");
	mpTipsMsgLbl->setObjectName("TipsMsgLbl");

	SetBtnText(0, tr("OK"));
	SetBtnText(1, tr("Cancel"));

	mpOkBtn->setObjectName("FirstBtn");
	mpCancelBtn->setObjectName("SecondBtn");

	mpImageLbl->setFixedSize(72, 50);
	mpOkBtn->setFixedSize(90,32);
	mpCancelBtn->setFixedSize(90, 32);
}

void TBaseMessageBox::CreateLayOut()
{
	mpTipsFrame = new QFrame(this);
	mpTipsFrame->setObjectName("TipsMsgFrame");
	mpTipsFrame->setFixedHeight(120);

	QHBoxLayout* pTipsHLyt = new QHBoxLayout();
	pTipsHLyt->setSpacing(0);
	pTipsHLyt->setMargin(0);
	pTipsHLyt->setAlignment(Qt::AlignVCenter);
	pTipsHLyt->addSpacing(45);
	pTipsHLyt->addWidget(mpImageLbl);
	pTipsHLyt->addWidget(mpTipsMsgLbl);
	pTipsHLyt->addSpacing(45);
	pTipsHLyt->addStretch();
	mpTipsFrame->setLayout(pTipsHLyt);

	QHBoxLayout* pControlBtnHLyt = new QHBoxLayout();
	pControlBtnHLyt->setSpacing(0);
	pControlBtnHLyt->setMargin(0);
	pControlBtnHLyt->setAlignment(Qt::AlignCenter);
	pControlBtnHLyt->addWidget(mpOkBtn);
	if (Type_TwoBtn == mBtnType){
		mpCancelBtn->setVisible(true);
		pControlBtnHLyt->addSpacing(14);
		pControlBtnHLyt->addWidget(mpCancelBtn);
	}

	QVBoxLayout* pMainVLyt = new QVBoxLayout();
	pMainVLyt->setSpacing(0);
	pMainVLyt->setMargin(SHADOW_WIDTH);

	pMainVLyt->addWidget(mpTipsFrame);
	pMainVLyt->addSpacing(24);
	pMainVLyt->addLayout(pControlBtnHLyt);
	pMainVLyt->addStretch();
	setLayout(pMainVLyt);
}

void TBaseMessageBox::CreateConnect()
{
	connect(mpOkBtn, &QPushButton::clicked, this, &TBaseMessageBox::OnBtnClicked);
	connect(mpCancelBtn, &QPushButton::clicked, this, &TBaseMessageBox::OnBtnClicked);
}

void TBaseMessageBox::SetMsgText(const QString& strMsgText)
{
	if (mpTipsMsgLbl){
		mpTipsMsgLbl->clear();
		mpTipsMsgLbl->setText(strMsgText);
	}
}

void TBaseMessageBox::SetBtnText(int iIndex, const QString& strText)
{
	if (0 == iIndex && mpOkBtn){
		mpOkBtn->setText(strText);
	}

	if (1 == iIndex && mpCancelBtn){
		mpCancelBtn->setText(strText);
	}
}

void TBaseMessageBox::OnBtnClicked()
{
	QObject *pSender = QObject::sender();
	if (pSender == static_cast<QObject*>(mpOkBtn)){
		mnBtnID = 0;
	}

	if (pSender == static_cast<QObject*>(mpCancelBtn)){
		mnBtnID = 1;
	}

	emit btnClickedSignal(mnBtnID);
	close();
}

int TBaseMessageBox::GetClikedBtnId() const
{
	return mnBtnID;
}

void TBaseMessageBox::MoveToParentCenter(QWidget* parent)
{
	if (NULL == parent){
		move((QApplication::desktop()->screenGeometry().width() - width()) / 2, (QApplication::desktop()->screenGeometry().height() - height()) / 2);
	}
	else
	{
		QPoint ptParentLeftTopPos = parent->mapToGlobal(parent->rect().topLeft());
		int xOff = (parent->width() - width()) / 2;
		int yOff = (parent->height() - height()) / 2;

        QRect desktopRect = qApp->desktop()->availableGeometry(this);
        QRect newRect = geometry();
        newRect.moveTo(ptParentLeftTopPos.x() + xOff, ptParentLeftTopPos.y() + yOff);
        if (desktopRect.contains(newRect))
        {
            move(newRect.topLeft());
        }
	}
}
