#include "TLeftTabWidget.h"

TLeftTabWidget::TLeftTabWidget(QWidget* parent /*= NULL*/)
: QFrame(parent)
{
    CreateUI();
    InitConect();
}

TLeftTabWidget::~TLeftTabWidget()
{

}

void TLeftTabWidget::CreateUI()
{
    setObjectName("TNaviWidget");
    setFixedWidth(150);
    
    mpDeviceBtn = new QPushButton(tr("My Device"), this);
    mpDeviceBtn->setFixedHeight(40);
    mpDeviceBtn->setCheckable(true);
    mpTaskBtn = new QPushButton(tr("My Task"), this);
    mpTaskBtn->setFixedHeight(40);
    mpTaskBtn->setCheckable(true);
    mpTaskBtn->setChecked(true);

    QButtonGroup* pBtnGroup = new QButtonGroup(this);
    pBtnGroup->addButton(mpDeviceBtn);
    pBtnGroup->addButton(mpTaskBtn);

    QVBoxLayout* pMainLayout = new QVBoxLayout();
    pMainLayout->setContentsMargins(0, 10, 0, 0);
    pMainLayout->setSpacing(0);
    pMainLayout->addWidget(mpDeviceBtn);
    pMainLayout->addWidget(mpTaskBtn);
    pMainLayout->addStretch();
    setLayout(pMainLayout);
}

void TLeftTabWidget::InitConect()
{
    connect(mpDeviceBtn, &QPushButton::clicked, this, &TLeftTabWidget::deviceTabClicked);
    connect(mpTaskBtn, &QPushButton::clicked, this, &TLeftTabWidget::taskTabClicked);
}

void TLeftTabWidget::SetTabSelected(TTab_Type type)
{
    switch (type)
    {
    case  TT_Device:
        mpDeviceBtn->click();
        break;
    case TT_Task:
        mpTaskBtn->click();
        break;
    default:
        break;
    }
}
