#include "TRightDisplayWidget.h"
#include "TTaskTabWidget.h"
#include "TDeviceTabWidget.h"
TRightDisplayWidget::TRightDisplayWidget(QWidget* parent /*= NULL*/)
: QFrame(parent)
{
    CreateUI();
    InitConnect();
}

TRightDisplayWidget::~TRightDisplayWidget()
{

}

void TRightDisplayWidget::CreateUI()
{
    setObjectName("TDisplayWidget");
    mpDeviceTabWidget = new TDeviceTabWidget(this);
    mpTaskTabWidget = new TTaskTabWidget(this);

    mpMainLayout = new QStackedLayout();
    mpMainLayout->setContentsMargins(0, 0, 0, 0);

    mpMainLayout->addWidget(mpTaskTabWidget);
    mpMainLayout->addWidget(mpDeviceTabWidget);

    setLayout(mpMainLayout);
}

void TRightDisplayWidget::ShowTaskTab()
{
    mpMainLayout->setCurrentWidget(mpTaskTabWidget);
}

void TRightDisplayWidget::ShowDeviceTab()
{
    mpMainLayout->setCurrentWidget(mpDeviceTabWidget);
}

void TRightDisplayWidget::UpdateDeviceInfo()
{
    mpTaskTabWidget->UpdateDeviceInfo();
    mpDeviceTabWidget->UpdateDeviceInfo();
}

void TRightDisplayWidget::ShowSPIInfo(QString strDeviceId)
{
    mpTaskTabWidget->ShowSPIInfo(strDeviceId);
    mpDeviceTabWidget->ShowSPIInfo(strDeviceId);
}


void TRightDisplayWidget::ResetDeviceInfo()
{
    mpTaskTabWidget->ResetDeviceInfo();
    mpDeviceTabWidget->ResetDeviceInfo();
}

void TRightDisplayWidget::AddDownloadTask(int nTaskId, TMPRFileInfoItem info)
{
    mpTaskTabWidget->OnAddTask(nTaskId, info);
}

void TRightDisplayWidget::InitConnect()
{
    connect(mpDeviceTabWidget, &TDeviceTabWidget::capacityChange, mpTaskTabWidget, &TTaskTabWidget::UpdateDeviceInfo);
    connect(mpTaskTabWidget, &TTaskTabWidget::capacityChange, mpDeviceTabWidget, &TDeviceTabWidget::UpdateDeviceInfo);
}

