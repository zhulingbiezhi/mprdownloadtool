#include "TWaittingDlg.h"

TWaittingDlg::TWaittingDlg(QWidget* parent /*= NULL*/, Qt::WindowFlags f /*= 0*/)
: TBaseDialog(parent, f)
{
    setObjectName("TWaittingDlg");
    CreateUI();
    InitTimer();
}

TWaittingDlg::~TWaittingDlg()
{

}

void TWaittingDlg::CreateUI()
{
    setObjectName("TWaittingDlg");
    CreateTitleWidget();
    CreateCenterWidget();
    QVBoxLayout* pMainLayout = new QVBoxLayout();
    pMainLayout->addWidget(mpTitleWidget);
    pMainLayout->addStretch();
    pMainLayout->addWidget(mpCenterWidget);
    pMainLayout->addStretch();
    pMainLayout->setContentsMargins(0, 0, 0, 0);
    pMainLayout->setSpacing(0);
    SetLayout(pMainLayout);
    setMinimumSize(452, 267);
}

void TWaittingDlg::InitTimer()
{
    mpTimer = new QTimer(this);
    mpTimer->setInterval(1000);
    connect(mpTimer, &QTimer::timeout, this, &TWaittingDlg::OnTimerTricked);
}

void TWaittingDlg::CreateCenterWidget()
{
    mpCenterWidget = new QWidget(this);
    mpCenterWidget->setObjectName("CenterWidget");
    mpTipsLabel = new QLabel(this);
    mpTipsLabel->setAlignment(Qt::AlignRight);
    mpTipsLabel->setObjectName("TipsLabel");
    mpWaitingLabel = new QLabel(this);
    mpWaitingLabel->setAlignment(Qt::AlignLeft);
    QHBoxLayout* pLayout = new QHBoxLayout();
    pLayout->setContentsMargins(0, 0, 0, 0);
    pLayout->setSpacing(0);
    pLayout->addWidget(mpTipsLabel);
    pLayout->addWidget(mpWaitingLabel);

    QVBoxLayout* pCenterLayout = new QVBoxLayout(mpCenterWidget);
    pCenterLayout->setContentsMargins(20, 0, 20, 0);
    pCenterLayout->addStretch(); 
    pCenterLayout->addLayout(pLayout);
    pCenterLayout->addStretch();  
}

void TWaittingDlg::OnTimerTricked()
{
     if (mnTrickCount % 3 == 0)
     {
         mnTrickCount = 0;
     }
     mnTrickCount++;
     QString strText = QString("%1").arg(".", mnTrickCount, '.');
     for (int i = mnTrickCount; i < 3; i ++)
     {
         strText += " ";
     }
     mpWaitingLabel->setText(strText);
}

void TWaittingDlg::ShowText(QString str)
{
    mpTipsLabel->setAlignment(Qt::AlignCenter);
    mpCloseBtn->setVisible(false);
    mpTipsLabel->setText(str);
    mnTrickCount = 0;
    mpTimer->start();
    exec();
}

void TWaittingDlg::ShowDialog(QString strText, QString strTitle, int flag)
{
    mpTipsLabel->setAlignment((Qt::AlignmentFlag)flag);
    mpWaitingLabel->setVisible(false);
    mpCloseBtn->setVisible(true);
    mpTipsLabel->setText(strText);
    mpToptitle->setText(strTitle);
    exec();
}

void TWaittingDlg::Close()
{
    if (mpTimer->isActive())
    {
        mpTimer->stop();
    }
    hide();
}

void TWaittingDlg::CreateTitleWidget()
{
    mpTitleWidget = new QWidget(this);
    mpTitleWidget->setObjectName("TitleWidget");
    mpTitleWidget->setFixedHeight(35);

    mpCloseBtn = new QPushButton(mpTitleWidget);
    mpCloseBtn->setFlat(true);
    mpCloseBtn->setFocusPolicy(Qt::NoFocus);
    mpCloseBtn->setObjectName("closeButton");
    mpCloseBtn->setToolTip(tr("Close"));
    mpCloseBtn->setFixedSize(36, 18);
    connect(mpCloseBtn, &QPushButton::clicked, this, &TWaittingDlg::Close);

    QVBoxLayout *closelayout = new QVBoxLayout;
    closelayout->addWidget(mpCloseBtn);
    closelayout->setContentsMargins(0, 0, 5, 17);

    mpToptitle = new QLabel(tr("Info"), mpTitleWidget);
    mpToptitle->setAlignment(Qt::AlignVCenter);
    mpToptitle->setObjectName("title");
   
    QHBoxLayout *toplayout = new QHBoxLayout;
    toplayout->addWidget(mpToptitle);
    toplayout->addStretch();
    toplayout->addLayout(closelayout);
    toplayout->setContentsMargins(10, 0, 0, 0);
    mpTitleWidget->setLayout(toplayout);
}
