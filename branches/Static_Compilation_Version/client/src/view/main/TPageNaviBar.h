#ifndef _TPAGENAVIBAR_INC_
#define _TPAGENAVIBAR_INC_

#include <QWidget>

class QLabel;
class QLineEdit;
class QPushButton;
class TPageNaviBar : public QWidget
{
    Q_OBJECT

public:
    TPageNaviBar(QWidget* pParent = nullptr);
    void setCurPageInfo(int curPage, int totalPageCnt, int totalCnt);

signals:
    void sigJumpToPage(int pageNo);
private slots:
    void onPrevPageBtnClicked();
    void onFirstPageBtnClicked();
    void onPrevBatchBtnClicked();
    void onPageBtnGroup0Clicked();
    void onPageBtnGroup1Clicked();
    void onPageBtnGroup2Clicked();
    void onPageBtnGroup3Clicked();
    void onPageBtnGroup4Clicked();
    void onNextBatchBtnClicked();
    void onLastPageBtnClicked();
    void onNextPageBtnClicked();
    void onPageEditFinished();
private:
    void createwidget();
    void createconnect();
    void createlayout();
    void setVisibleAll(bool state);

private:
    QPushButton*        mPrevPageBtn;
    QPushButton*        mFirstPageBtn;
    QPushButton*        mPrevBatchBtn;
    QPushButton*        mPageBtnGroup[5];
    QPushButton*        mNextBatchBtn;
    QPushButton*        mLastPageBtn;
    QPushButton*        mNextPageBtn;

    QLabel*             mPageInfoLabel1;
    QLineEdit*          mJmpPageEdit;
    QLabel*             mPageInfoLabel2;

    int                 mTotalPageCnt;
    int                 mCurPageIndex;
    int                 mPageGrpStartIdx;

    bool                mbNextBatchToInt;
    bool                mbPrevBatchToInt;
};

#endif // _TPAGENAVIBAR_INC_
