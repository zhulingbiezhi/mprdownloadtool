#ifndef _TMAININTERFACE_H_
#define _TMAININTERFACE_H_

class TMainWindow;
class TDeleteFileThread;

class TMainInterface : public QObject
{
    Q_OBJECT

public:
    TMainInterface(QObject *pParent = NULL);
    ~TMainInterface();

    void Run();

private slots:
    void OnExit();
    void OnMainWindowClose(bool bInitiative);

private:
    TMainWindow*    mpMainWindows;
    bool            mbInitiative;
    TDeleteFileThread* mThread;
};

#endif // _TMAININTERFACE_H_