#include "TDeviceInfoWidget.h"
#include "TMPRDevicesManager.h"
#include "TDeviceDataManager.h"
#define SPI_DEVICE_NAME "MPR-1026SPI"
TDeviceInfoWidget::TDeviceInfoWidget(QWidget* parent /*= NULL*/)
: QFrame(parent)
{
    CreateUI();
}

TDeviceInfoWidget::~TDeviceInfoWidget()
{

}

void TDeviceInfoWidget::CreateUI()
{
    setObjectName("TDeviceInfoWidget");
    setFixedHeight(50);
    QLabel* pTypeLabel = new QLabel(tr("Device Type:"), this);
    mpDeviceTypeLabel = new QLabel(this);
    mpDeviceTypeLabel->setObjectName("DeviceTypeLabel");
    mpDeviceTypeLabel->setFixedWidth(100);
    QLabel* pIdLabel = new QLabel(tr("Device Id:"), this);
    mpDeviceIdLabel = new QLabel(this);
    mpDeviceIdLabel->setObjectName("DeviceIdLabel");
    mpDeviceIdLabel->setFixedWidth(130);

    QLabel* pCapacityLabel = new QLabel(tr("Capacity:"), this);
    mpCapacityProgress = new QProgressBar(this);
    mpCapacityProgress->setObjectName("CapacityProgress");
    mpCapacityProgress->setTextVisible(false);
    mpCapacityProgress->setRange(0, 100);
    mpCapacityProgress->setValue(0);
    mpCapacityProgress->setFixedWidth(300);

    mpCapacityLabel = new QLabel(this);
    mpCapacityLabel->setObjectName("CapacityLabel");
    mpCapacityLabel->setText(QString(tr("Used:%1,Total:%2")).arg("0M").arg("0M"));
    mpCapacityLabel->setAlignment(Qt::AlignHCenter);

    QVBoxLayout* pCapacityLayout = new QVBoxLayout();
    pCapacityLayout->setContentsMargins(0, 4, 0, 0);
    pCapacityLayout->setSpacing(0);
    pCapacityLayout->addWidget(mpCapacityLabel);
    mpCapacityProgress->setLayout(pCapacityLayout);


    QHBoxLayout* pMainLayout = new QHBoxLayout();
    pMainLayout->setContentsMargins(30, 0, 30, 0);
    pMainLayout->setSpacing(0);

    pMainLayout->addWidget(pTypeLabel);
    pMainLayout->addWidget(mpDeviceTypeLabel);
    pMainLayout->addWidget(pIdLabel);
    pMainLayout->addWidget(mpDeviceIdLabel);
    pMainLayout->addSpacing(50);
    pMainLayout->addWidget(pCapacityLabel);
    pMainLayout->addWidget(mpCapacityProgress);
    pMainLayout->addStretch();

    setLayout(pMainLayout);
}

void TDeviceInfoWidget::UpdateShowInfo()
{
    if (TDeviceDataManager::Instance().GetMPRDeviceConnected())
    {
        char disk = TDeviceDataManager::Instance().GetDiskName();
        TMPRDeviceDesc desc;
        if (TMPRDevicesManager::Instance().GetMPRDeviceDesc(disk, desc))
        {
            mpDeviceTypeLabel->setText(desc.mDeviceName);
            mpDeviceIdLabel->setText(desc.mDeviceSN);
            //need to do show capacity
            qint64 qTotalSize, qFreeSize;
            bool bRet = TMPRDevicesManager::Instance().GetMPRDeviceCapacityInfo(disk, qTotalSize, qFreeSize);
            if (bRet)
            {
                UpdateCapacityInfo(qTotalSize, qTotalSize - qFreeSize);
            }
        }
    }
}

void TDeviceInfoWidget::UpdateCapacityInfo(qint64 qTotal, qint64 qUsed)
{
    QString strSpace(tr("Used:%1%2,Total:%3%4"));
    const quint64 cKB = 1024;
    const quint64 cMB = 1024 * 1024;
    const quint64 cGB = 1024ll * 1024 * 1024;

    QList<quint64> valueList;
    valueList << qUsed << qTotal;

    foreach(quint64 value, valueList)
    {
        if (value > cGB)
        {
            if (value & 0x7FFFFFFF)
            {
                strSpace = strSpace.arg(static_cast<double>(value * 10 / cGB) / 10, 0, 'f', 2).arg("GB");
            }
            else
            {
                strSpace = strSpace.arg(value >> 30).arg("GB");
            }
        }
        else if (value > cMB)
        {
            if (value & 0xFFFFF)
            {
                strSpace = strSpace.arg(static_cast<double>(value * 10 / cMB) / 10, 0, 'f', 2).arg("MB");
            }
            else
            {
                strSpace = strSpace.arg(value >> 20).arg("MB");
            }
        }
        else if (value > cKB)
        {
            if (value & 0xFFF)
            {
                strSpace = strSpace.arg(static_cast<double>(value * 10 / cKB) / 10, 0, 'f', 2).arg("KB");
            }
            else
            {
                strSpace = strSpace.arg(value >> 10).arg("KB");
            }
        }
        else
        {
            strSpace = strSpace.arg(value).arg("B");
        }
    }

    mpCapacityLabel->setText(strSpace);

    if (qTotal > cGB)
    {
        qTotal /= 1024;
        qUsed /= 1024;
    }

    if (qUsed == 0 && qTotal == 0)
    {
        mpCapacityProgress->setMinimum(0);
        mpCapacityProgress->setMaximum(100);
        mpCapacityProgress->setValue(0);
    }
    else
    {
        mpCapacityProgress->setMinimum(0);
        mpCapacityProgress->setMaximum(qTotal);
        mpCapacityProgress->setValue(qUsed);
    }
}

void TDeviceInfoWidget::ResetDeviceInfo()
{
    mpCapacityLabel->setText(QString(tr("Used:%1,Total:%2")).arg("0M").arg("0M"));
    mpCapacityProgress->setValue(0);
    mpDeviceTypeLabel->setText("");
    mpDeviceIdLabel->setText("");
}

void TDeviceInfoWidget::ShowSPIInfo(QString strDeviceSn)
{
    char disk = TDeviceDataManager::Instance().GetDiskName();
    mpDeviceTypeLabel->setText(SPI_DEVICE_NAME);
    mpDeviceIdLabel->setText(strDeviceSn);
    //need to do show capacity
    qint64 qTotalSize, qFreeSize;
    bool bRet = TMPRDevicesManager::Instance().GetMPRDeviceCapacityInfo(disk, qTotalSize, qFreeSize);
    if (bRet)
    {
        UpdateCapacityInfo(qTotalSize, qTotalSize - qFreeSize);
    }  
}
