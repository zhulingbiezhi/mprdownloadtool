﻿#ifndef _TUPDATEVERSIONMANAGER_H_
#define _TUPDATEVERSIONMANAGER_H_
#include "TWebServiceInterface.h"
#include "TCopyFileState.h"

class TMPRDeviceDesc;
class TLocalSocket;
class TPopupDialog;
class TWaittingDlg;
class TCopyFileTask;
class TUpdateProcesser;
class TUpdateVersionManager : public QObject
{
    Q_OBJECT
public:
    enum TUpdateType
    {
        TSoftware_Update,
        TFirmware_Update
    };
    enum ReturnCode
    {
        ExistNewVer = 0,
        NoExistNewVer = 1
    };
    enum PromptDlgType
    {
        Download = 0,
        Install = 1
    };
    explicit TUpdateVersionManager(QObject *parent = 0);
    ~TUpdateVersionManager();

    void CheckClientVersion();
    void CheckFirmwareVersion(TMPRDeviceDesc desc);
    void CheckSPIFirmwareVersion(QString strFirmwareInfo, QString strRootDir);
    void CloseWaittingDlg();
    void PromptSoftwareVersion(QByteArray byte);
    QString GerSoftwareUpdateDate();
    static TUpdateVersionManager& Instance();
    void PromptInstallSoftware(PromptDlgType type);

private:
    bool OSIsWinXP();
    void CopyUpdatefileToFirmware();
    void FirmwareDownloadError();
    void SoftwareDownloadError();

    void NewUpdatePackageDownload(TUpdateType type);
    void CreateUpdateProcess(TUpdateType type);
    void CloseUpdateProcesser();
    void UpdateDownloadPrompt(TUpdateType type, const TFetchLatestVersionResponse &resData);

    void SoftwarePromptDlg(PromptDlgType type);
    QString GetLocalFirmwareUpdatePackageName();
    QString GetLocalSoftwareUpdatePackageName();
    QString GetLocalSoftwareVersion();
    QString GetUpdateSoftwarePath();
    QString GetSVN(QString strFirmwareVersion);


public slots:
    void OnReadUpdateServerMessSlot(const QString& text);
    void OnCopyUpdataPackageStateChange(CopyFileState);

private slots:
    void OnFetchFirmwareLatestVerDone(QObject *requester, const TFetchLatestVersionResponse &resData, TWebServiceInterface::ResponseCode code);
    void OnFetchSofewareLatestVerDone(QObject *requester, const TFetchLatestVersionResponse &resData, TWebServiceInterface::ResponseCode code);
signals:
    void existNewVersion();

private:

    QString         mstrUpdateFileName;
    QString         mstrFirmwarePackageDir;
    QString         mstrSoftwarePackageDir;
    QString         mstrDeviceRootDir;
    QString         mstrFirmwareDeviceTempPath;
    QString         mstrFirmwareDevicePath;
    //QString         mstrUpdateExePath;
    TLocalSocket*   mpFirmwareSocket;
    TLocalSocket*   mpSoftwareSocket;
    TWaittingDlg*   mpWaitDlg;
    TUpdateProcesser*           mpUpdateProcesser;
    TWebServiceInterface*       mpServiceInterFace;
    TCopyFileTask*              mpCurrentTask;
    TFetchLatestVersionResponse mFirmwareResData;
    TFetchLatestVersionResponse mSoftwareResData;
};


#endif
