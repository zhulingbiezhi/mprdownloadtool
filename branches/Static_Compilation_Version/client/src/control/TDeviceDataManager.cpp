#include "TDeviceDataManager.h"
#include "TFileScanner.h"

TDeviceDataManager::TDeviceDataManager()
: mbConnected(false)
, mcDiskName(' ')
, mpFileScanThread(NULL)
, mpScanner(NULL)
{
    mpFileScanThread = new QThread();
}

TDeviceDataManager::~TDeviceDataManager()
{
    if (mpFileScanThread)
    {
        if (mpFileScanThread->isRunning())
        {
            mpFileScanThread->quit();
            mpFileScanThread->wait();
        }
        delete mpFileScanThread;
        mpFileScanThread = NULL;
    }

}

TDeviceDataManager& TDeviceDataManager::Instance()
{
    static TDeviceDataManager obj;
    return obj;
}

void TDeviceDataManager::SetDiskName(char disk)
{
    if (disk == ' ')
    {
        mbConnected = false;
        AbortCurrentScanTask();
    }
    else
    {
        mbConnected = true;
        CreateScanTask(disk);
    }
    mcDiskName = disk;
}


bool TDeviceDataManager::QueryFileExist(const QString& strPrefixCode, const QString& strVersion)
{
    for (int i = 0; i < mFileItemList.size(); i++)
    {
        if (mFileItemList.at(i).tPrefixCode == strPrefixCode && mFileItemList.at(i).tVersion == strVersion)
        {
            return true;
        }
    }
    return false;
}

bool TDeviceDataManager::RemoveDeviceFile(const QString& strPrefixCode,const QString& strVersion, bool bDeleteFile)
{
    for (int i = 0; i < mFileItemList.size(); i ++)
    {
        if (mFileItemList.at(i).tPrefixCode == strPrefixCode && mFileItemList.at(i).tVersion == strVersion)
        {
            if (bDeleteFile)
            {
                qDebug() << __FUNCTION__ << "Remove MPR :" << QFile::remove(mFileItemList.at(i).tFilePath);
                qDebug() << __FUNCTION__ << "Remove License :" << QFile::remove(mFileItemList.at(i).tLicencePath);
            }
            mFileItemList.removeAt(i);
            break;
        }
    }
    return false;
}

bool TDeviceDataManager::AddDeviceFile(TMPRFileInfoItem item)
{
    QString strPrefix, strVersion;
    if (item.tPrefixCode.isEmpty() || item.tVersion.isEmpty())
    {
        bool bRet = TFileScanner::GetMPRFileInfo(item.tFilePath, strPrefix, strVersion);
        if (bRet)
        {
            RemoveDeviceFile(strPrefix, strVersion, false);
        }
    }
    else
    {
        RemoveDeviceFile(item.tPrefixCode, item.tVersion, false);
    }
    mFileItemList.push_front(item);
    emit addNewFile();
    return true;
}

void TDeviceDataManager::CreateScanTask(char disk)
{
     mpScanner = new TFileScanner(disk);
     mpScanner->moveToThread(mpFileScanThread);
     if (!mpFileScanThread->isRunning())
     {
         mpFileScanThread->start();
     }
    connect(mpScanner, &TFileScanner::finished, this, &TDeviceDataManager::OnFileScanFinished);
    QMetaObject::invokeMethod(mpScanner, "StartScanning");
}

void TDeviceDataManager::AbortCurrentScanTask()
{
    if (mpScanner)
    {
        //disconnect(mpScanner, &TFileScanner::finished, this, &TDeviceDataManager::OnFileScanFinished);
        mpScanner->Stop();
    }
    mFileItemList.clear();
}

void TDeviceDataManager::OnFileScanFinished(char disk)
{
    TFileScanner* pSender = static_cast<TFileScanner*>(sender());
    if (pSender == mpScanner)
    {
        mFileItemList = mpScanner->GetAllFiles();
        mpScanner = NULL;
        emit sigScanFileFinished(disk);
    }
    delete mpScanner;

}

QList<TMPRFileInfoItem> TDeviceDataManager::GetFileInfoItems(int nPage, int nCountPerPage /*= 10*/)
{
    QList<TMPRFileInfoItem> retList;
    int nSize = mFileItemList.size();
    int nBegin = (nPage - 1) * nCountPerPage;
    if (nSize == 0 || nBegin >= nSize || nPage < 0)
    {
        return retList;
    }
    else
    {
        int nEnd = nSize - nBegin > nCountPerPage ? nBegin + nCountPerPage - 1 : nSize - 1;
        for (int i = nBegin; i <= nEnd; i ++)
        {
            retList.append(mFileItemList.at(i));
        }
        return retList;
    }
}

QString TDeviceDataManager::GetMediaPath()
{
    return QString("%1:/MPR").arg(mcDiskName);
}
