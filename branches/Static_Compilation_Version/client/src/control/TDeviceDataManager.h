#ifndef _TDEVICEDATAMANAGER_H
#define _TDEVICEDATAMANAGER_H
#include "TFileEnum.h"
class TDescriptionOerater;
class TFileScanner;
class TDeviceDataManager : public QObject
{
    Q_OBJECT
public:
    TDeviceDataManager();
    ~TDeviceDataManager();

    static TDeviceDataManager& Instance();

    void   SetDiskName(char);
    bool                        RemoveDeviceFile(const QString& strPrefixCode, const QString& strVersion, bool bDeleteFile = true);
    bool                        AddDeviceFile(TMPRFileInfoItem);
    bool                        QueryFileExist(const QString& strPrefixCode, const QString& strVersion);
    

    const bool& GetMPRDeviceConnected() const { return mbConnected; }
    const char& GetDiskName() const {return mcDiskName; }

    int                         GetFilesCount() const{ return mFileItemList.size(); }
    QList<TMPRFileInfoItem>     GetFileInfoItems(int nPage, int nCountPerPage = 10);

    QString     GetMediaPath();

private slots:
    void OnFileScanFinished(char);

signals:
    void sigScanFileFinished(char);
    void addNewFile();

private:
    void             CreateScanTask(char disk);
    void             AbortCurrentScanTask();

private:
    bool              mbConnected;
    char              mcDiskName;
    QThread          *mpFileScanThread;
    TFileScanner     *mpScanner;
    QList<TMPRFileInfoItem>   mFileItemList;


};
#endif