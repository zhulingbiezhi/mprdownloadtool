#ifndef  _TTASKMANAGER_H_
#define  _TTASKMANAGER_H_

#include "TDownloadManager.h"
#include "TCopyFileManager.h"

#include <QHash>

enum  TTaskState
{
    TS_UNKOWN,
    TS_DOWNLOAD_PREPARE,
    TS_DOWNLOAD,
    TS_DOWNLOAD_PAUSE,
    TS_DOWNLOAD_FINISH,
    TS_DOWNLOAD_ERROR,
    TS_COPYFILE_PREPARE,
    TS_COPYFILE,
    TS_COPYFILE_PAUSE,
    TS_COPYFILE_FINISH,
    TS_COPYFILE_ERROR
};

struct TTaskInfo
{
    int                 task_id;
    qint64              totalDownloadBytes;
    qint64              transferedBytes;
    qint64              byteCopied;
    qint64              byteCopyTotal;
    QString             strMediaPath;
    QString             strRemoteUrl;
    QString             strDeviceID;
    QString             strDeviceSN;
    QStringList         progressList;
    CopyFileState       copyMprState;
    CopyFileState       copyLicState;
    TCopyFileTask*      pCopyMprTask;
    TCopyFileTask*      pCopyLicTask;
    TTaskState          taskState;

    TMPRFileInfoItem    mprInfo;
    TTaskInfo()
    {
        task_id = -1;
        totalDownloadBytes = 0;
        transferedBytes = 0;
        byteCopied = 0;
        byteCopyTotal = 0;
        copyMprState = CSNone;
        copyLicState = CSNone;
        pCopyMprTask = nullptr;
        pCopyLicTask = nullptr;
        taskState = TS_UNKOWN;
    }
};

class TFileLib;

class TTaskManager : public QObject
{
    Q_OBJECT
public:
    TTaskManager();
    ~TTaskManager();

    static TTaskManager * Instance();

public:
    void AddTask(TTaskInfo taskInfo);
    void StopTask(int taskID);
    void StartTask(int taskID);
    void RemoveTask(int taskID, bool bDirectly = false);
    void RemoveAllTask();
    void LoadTaskInfo(QString deviceID);

protected:


private:
    int GetNextValidIndex();
    void ActiveCopyTask(TTaskInfo& taskInfo, bool bLoad = false);
    void HandleDownloadStateMsg(int taskID, int nState, TSVHashMap& extraParams);
    void FillDownloadInfo(TTaskInfo& taskInfo, TDownloadInfo& downloadInfo);
    void SaveTaskInfo();
    bool LoadCopyTask(TTaskInfo& taskInfo);

signals:
    void sigDownloadMessage(int taskID, int msgType, const QVariant& msgBody, TSVHashMap& extraParams = TSVHashMap());
    void sigCopyfileMessage(int taskID, int msgType, const QVariant& msgBody, TSVHashMap& extraParams = TSVHashMap());
    void sigAddTask(int taskID, TMPRFileInfoItem itemInfo);

private slots:
    void onDownloadMessage(int taskID, int msgType, const QVariant& msgBody, TSVHashMap& extraParams);
    void OnCopyMprStateChange(CopyFileState state);
    void OnCopyLicStateChange(CopyFileState state);
    void OnCopyProgressChanged(qint64 qCopyed, qint64 qTotal);

private:
    int                           mnTaskIndex;
    QHash<int, TTaskInfo>         mTaskInfoHash;
    QHash<int, TCopyFileTask*>    mCopyMprTaskHash;
    QHash<int, TCopyFileTask*>    mCopyLicTaskHash;
    TFileLib*                     mpLocalFileLib;
    QHash<int, int>               mCopyThreadHash;
    QHash<int, QString>           mCopyThreadString;
};

#endif