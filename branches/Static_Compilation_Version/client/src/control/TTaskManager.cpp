#include "TTaskManager.h"
#include "TCopyFileManager.h"
#include "TDownloadManager.h"
#include "TCopyFileTask.h"
#include "TAppCommon.h"
#include "TLocalLibHandle.h"
#include "TFileLib.h"
#include "TDeviceDataManager.h"

TTaskManager::TTaskManager()
: mnTaskIndex(0)
{
    mpLocalFileLib = TLocalLibHandle::Instance().GetLocalLib();
    connect(TDownloadManager::Instance(), &TDownloadManager::sigDownloadMessage, this, &TTaskManager::onDownloadMessage);
    mCopyThreadHash[0] = 0;
    mCopyThreadHash[1] = 0;
    mCopyThreadHash[2] = 0;
}

TTaskManager::~TTaskManager()
{
    //RemoveAllTask();
}

TTaskManager * TTaskManager::Instance()
{
    static TTaskManager taskMgr;
    return &taskMgr;
}

void TTaskManager::AddTask(TTaskInfo taskInfo)
{
    TDownloadInfo info;
    FillDownloadInfo(taskInfo, info);
    emit sigAddTask(taskInfo.task_id, taskInfo.mprInfo);
    mTaskInfoHash.insert(taskInfo.task_id, taskInfo);
    SaveTaskInfo();
    TDownloadManager::Instance()->AddTask(taskInfo.task_id, info);
}

void TTaskManager::FillDownloadInfo(TTaskInfo& taskInfo, TDownloadInfo& downloadInfo)
{
    taskInfo.task_id = GetNextValidIndex();
    downloadInfo.task_id = taskInfo.task_id;
    downloadInfo.strTaskRef = taskInfo.mprInfo.tFileUuid + "_" + taskInfo.strDeviceSN;
    downloadInfo.strRemoteUrl = taskInfo.strRemoteUrl;
    downloadInfo.strSavePath = taskInfo.mprInfo.tFilePath;
    downloadInfo.strDeviceID = taskInfo.strDeviceID;
    downloadInfo.strDeviceSN = taskInfo.strDeviceSN;
    downloadInfo.itemInfo = taskInfo.mprInfo;
    downloadInfo.progressList = taskInfo.progressList;
    downloadInfo.totalBytes = taskInfo.totalDownloadBytes;
    downloadInfo.transferedBytes = taskInfo.transferedBytes;
}

void TTaskManager::onDownloadMessage(int taskID, int msgType, const QVariant& msgBody, TSVHashMap& extraParams)
{
    if (mTaskInfoHash[taskID].taskState > TS_DOWNLOAD_ERROR)
    {
        qDebug() << "the task is not downloading !";
        return;
    }
    if (msgType == HTM_STATE)
    {
        HandleDownloadStateMsg(taskID, msgBody.toInt(), extraParams);
    }
    else if (msgType == HTM_PROGRESS)
    {
        emit sigDownloadMessage(taskID, msgType, msgBody);
    }
}

void TTaskManager::ActiveCopyTask(TTaskInfo& taskInfo, bool bLoad)
{
    if (taskInfo.strMediaPath.isEmpty())
    {
        qDebug() << __FUNCTION__ << "error : can't find the device path !";
        return;
    }
    QString strMprFileName = taskInfo.mprInfo.tBookName + "_" + taskInfo.mprInfo.tVersion + "_" + taskInfo.mprInfo.tPrefixCode;
    QString strMprDestPath = taskInfo.strMediaPath + "/" + strMprFileName + ".mpr";
    QString strLicDestPath = taskInfo.strMediaPath + "/" + strMprFileName + ".lic";

    QString srcDir = QFileInfo(taskInfo.mprInfo.tFilePath).absoluteDir().absolutePath();
    QString strSrcFileName = taskInfo.mprInfo.tBookName + "_" + taskInfo.mprInfo.tVersion.rightJustified(5, '0') + "_" + taskInfo.mprInfo.tPrefixCode.rightJustified(10, '0');
    QString strMprSrcPath = srcDir + "/" + strSrcFileName + ".mpr";
    QString strLicSrcPath = srcDir + "/" + strSrcFileName + ".lic";

    QHashIterator<int, int> it(mCopyThreadHash);
    int index = 0;
    int min = mCopyThreadHash[0];
    for (int i = 0; i < 2; i++)
    {
         if (min > mCopyThreadHash[i+1])
         {
             min = mCopyThreadHash[i + 1];
             index = i + 1;
         }        
    }

    taskInfo.pCopyMprTask = TCopyFileManager::Instance()->AddTask(strMprSrcPath, strMprDestPath, QString::number(index));
    taskInfo.pCopyLicTask = TCopyFileManager::Instance()->AddTask(strLicSrcPath, strLicDestPath);

    mCopyThreadHash[index]++;
    mCopyMprTaskHash.insert(taskInfo.task_id, taskInfo.pCopyMprTask);
    mCopyLicTaskHash.insert(taskInfo.task_id, taskInfo.pCopyLicTask);
    taskInfo.taskState = TS_COPYFILE_PREPARE;
    if (mCopyMprTaskHash.keys(taskInfo.pCopyMprTask).size() > 1)
    {
        qDebug() << __FUNCTION__ << "warning : the copy task is exist !";  
        return;
    }
    
    connect(taskInfo.pCopyLicTask, &TCopyFileTask::CopyStateChanged, this, &TTaskManager::OnCopyLicStateChange);
    connect(taskInfo.pCopyMprTask, &TCopyFileTask::CopyStateChanged, this, &TTaskManager::OnCopyMprStateChange);
    connect(taskInfo.pCopyMprTask, &TCopyFileTask::CopyProgressChanged, this, &TTaskManager::OnCopyProgressChanged);
    if (!bLoad)
    {
        TCopyFileManager::Instance()->ActivateTask(taskInfo.pCopyMprTask, false);
        TCopyFileManager::Instance()->ActivateTask(taskInfo.pCopyLicTask, false);
    }
}

void TTaskManager::OnCopyMprStateChange(CopyFileState state)
{
    TCopyFileTask * pSender = static_cast<TCopyFileTask *>(sender());
    QList<int> task_idList = mCopyMprTaskHash.keys(pSender);
    qDebug() << __FUNCTION__ << "the list size : " << task_idList.size();
    for (int i = 0; i < task_idList.size(); i++)
    {
        int task_id = task_idList.at(i);
        if (mTaskInfoHash[task_id].taskState < TS_COPYFILE_PREPARE)
        {
            qDebug() << __FUNCTION__ << "the task is not copying !";
            return;
        }
        switch (state)
        {
        case CSCopying:
            mTaskInfoHash[task_id].taskState = TS_COPYFILE;
            break;
        case CSInterrupted:
            mTaskInfoHash[task_id].taskState = TS_COPYFILE_PAUSE;
            break;
        case CSCopySucceeded:
            mTaskInfoHash[task_id].taskState = TS_COPYFILE_FINISH;
            break;
        case CSOpenFileError:
        case CSCheckDiskSpace:
        case CSNotEnoughDiskSpace:
        case CSDeviceNotExist:
            mTaskInfoHash[task_id].taskState = TS_COPYFILE_ERROR;
            break;
        default:
            mTaskInfoHash[task_id].taskState = TS_COPYFILE_PREPARE;
            break;
        }        
        TSVHashMap extraParams = TSVHashMap();
        if (mTaskInfoHash.find(task_id) != mTaskInfoHash.end() && state == CSCopySucceeded)
        {
            TCopyFileTask* pCopyMprTask = mTaskInfoHash[task_id].pCopyMprTask;
            TCopyFileTask* pCopyLicTask = mTaskInfoHash[task_id].pCopyLicTask;
            if (pCopyMprTask != NULL && pCopyLicTask != NULL)
            {
                extraParams.insert(MPRPATH, pCopyMprTask->GetDstPath());
                extraParams.insert(LICENSEPATH, pCopyLicTask->GetDstPath());
            }
        }
        emit sigCopyfileMessage(task_id, HTM_STATE, mTaskInfoHash[task_id].taskState, extraParams);
        if (state == CSCopySucceeded) 
        {
            RemoveTask(task_id);
        }        
    }
}

void TTaskManager::OnCopyLicStateChange(CopyFileState state)
{
    TCopyFileTask * pSender = static_cast<TCopyFileTask *>(sender());
    int task_id = mCopyLicTaskHash.key(pSender);
    if (mTaskInfoHash[task_id].taskState < TS_COPYFILE_PREPARE)
    {
        qDebug() << __FUNCTION__ << "the task is not copying !";
        return;
    }
    if (state == CSError)
    {
        mTaskInfoHash[task_id].taskState = TS_COPYFILE_ERROR;
        emit sigDownloadMessage(task_id, HTM_STATE, mTaskInfoHash[task_id].taskState);
    }
}

void TTaskManager::OnCopyProgressChanged(qint64 qCopyed, qint64 qTotal)
{
    TCopyFileTask * pSender = static_cast<TCopyFileTask *>(sender());
    QList<int> task_idList = mCopyMprTaskHash.keys(pSender);
    int nPercent = qCopyed * 100 / qTotal;
    for (int i = 0; i < task_idList.size(); i++)
    {
        int task_id = task_idList.at(i);
        mTaskInfoHash[task_id].byteCopied = qCopyed;
        mTaskInfoHash[task_id].byteCopyTotal = qTotal;
        if (mTaskInfoHash[task_id].taskState >= TS_COPYFILE_PREPARE)
        {
            emit sigCopyfileMessage(task_id, HTM_PROGRESS, nPercent);
        }
    }
}

void TTaskManager::HandleDownloadStateMsg(int taskID, int nState, TSVHashMap& extraParams)
{
    if (taskID != -1)
    {
        switch (nState)
        {
        case HTS_PAUSED:
            mTaskInfoHash[taskID].taskState = TS_DOWNLOAD_PAUSE;
            break;
        case HTS_PREPARED:
            mTaskInfoHash[taskID].taskState = TS_DOWNLOAD_PREPARE;
            break;
        case HTS_RUNNING:
            mTaskInfoHash[taskID].taskState = TS_DOWNLOAD;
            break;
        case HTS_FAILED:
            mTaskInfoHash[taskID].taskState = TS_DOWNLOAD_ERROR;
            break;
        case  HTS_FINISHED:
            mTaskInfoHash[taskID].taskState = TS_DOWNLOAD_FINISH;
            mTaskInfoHash[taskID].mprInfo.tVersion = extraParams.value(VERSION).toString();
            mTaskInfoHash[taskID].mprInfo.tFileSize = extraParams.value(FILESIZE).toInt();
            mpLocalFileLib->UpdateFileInfo(mTaskInfoHash[taskID].mprInfo);
            ActiveCopyTask(mTaskInfoHash[taskID]);
            break;
        default:
            mTaskInfoHash[taskID].taskState = TS_UNKOWN;
            break;
        }
        emit sigDownloadMessage(taskID, HTM_STATE, mTaskInfoHash[taskID].taskState, extraParams);
    }
}

void TTaskManager::RemoveTask(int taskID, bool bDirectly)
{
    if (taskID < 0)
    {
        qDebug() << __FUNCTION__ << "error : the task id is not exist !" << taskID;
        return;
    }
    if (mTaskInfoHash[taskID].taskState <= TS_DOWNLOAD_ERROR)
    {
        TDownloadManager::Instance()->RemoveTask(taskID);
    }
    else
    {
        StopTask(taskID);
        if (bDirectly && mTaskInfoHash[taskID].taskState != TS_COPYFILE_FINISH) 
        {
            QString strMprPath = mCopyMprTaskHash[taskID]->GetTempPath();
            QString strLicPath = mCopyLicTaskHash[taskID]->GetDstPath();
            int times = 0;
            while (times < 200)  {
                QCoreApplication::processEvents();
                if (mTaskInfoHash[taskID].taskState == TS_COPYFILE_PAUSE) {
                    break;
                }
                QThread::msleep(10);
                times++;
            }
            qDebug() << __FUNCTION__ << QFile::exists(strMprPath) << QFile::remove(strMprPath);
            qDebug() << __FUNCTION__ << QFile::exists(strLicPath) << QFile::remove(strLicPath);
        }
        TCopyFileManager::Instance()->RemoveTask(mCopyMprTaskHash[taskID]);
        TCopyFileManager::Instance()->RemoveTask(mCopyLicTaskHash[taskID]);
        mCopyMprTaskHash.remove(taskID);
        mCopyLicTaskHash.remove(taskID);
    }
    mTaskInfoHash.remove(taskID);
    if (bDirectly) {
        SaveTaskInfo();
    }
}

int TTaskManager::GetNextValidIndex()
{
    return mnTaskIndex++;
}

void TTaskManager::StopTask(int taskID)
{
    if (mTaskInfoHash[taskID].taskState <= TS_DOWNLOAD_ERROR)
    {
        TDownloadManager::Instance()->StopTask(taskID);
    }
    else
    {
        TCopyFileManager::Instance()->StopTask(mCopyMprTaskHash[taskID]);
        TCopyFileManager::Instance()->StopTask(mCopyLicTaskHash[taskID]);
    }
}

void TTaskManager::StartTask(int taskID)
{
    if (mTaskInfoHash[taskID].taskState <= TS_DOWNLOAD_ERROR)
    {
        TDownloadManager::Instance()->StartTask(taskID);
    }
    else
    {
        mCopyMprTaskHash[taskID]->SetState();
        mCopyLicTaskHash[taskID]->SetState();
        TCopyFileManager::Instance()->ActivateTask(mCopyMprTaskHash[taskID], false);
        TCopyFileManager::Instance()->ActivateTask(mCopyLicTaskHash[taskID], false);
    }
}

void TTaskManager::RemoveAllTask()
{
    qDebug() << __FUNCTION__;
    SaveTaskInfo();
    TCopyFileManager::Instance()->ClearAllTask();
    TDownloadManager::Instance()->RemoveAllTask();
    mTaskInfoHash.clear();
    mCopyMprTaskHash.clear();
    mCopyLicTaskHash.clear();
}

void TTaskManager::SaveTaskInfo()
{
    QList<TTaskInfo> infoList;
    QHashIterator<int, TTaskInfo> it(mTaskInfoHash);
    
    while (it.hasNext())
    {
        TTaskInfo taskInfo = it.next().value();
        qDebug() << __FUNCTION__ << "#### start save task : " << taskInfo.task_id << taskInfo.taskState << taskInfo.mprInfo.tFilePath << taskInfo.mprInfo.tBookName;
        
        if (taskInfo.taskState == TS_COPYFILE_FINISH) {
            continue;
        }
        TDownloadInfo info;
        if (taskInfo.taskState <= TS_DOWNLOAD_ERROR)
        {
            info.task_id = taskInfo.task_id;
            TDownloadManager::Instance()->SaveTaskInfo(info);
        }
        taskInfo.progressList = info.progressList;
        taskInfo.transferedBytes = info.transferedBytes;
        taskInfo.totalDownloadBytes = info.totalBytes;
        infoList.append(taskInfo);
    }
    mpLocalFileLib->UpdateTaskInfo(infoList);
}

void TTaskManager::LoadTaskInfo(QString deviceID)
{
    mTaskInfoHash.clear();
    mCopyMprTaskHash.clear();
    mCopyLicTaskHash.clear();

    QList<TTaskInfo> taskList = mpLocalFileLib->FetchAllTasks(deviceID);

    QList<TDownloadInfo> downLoadList;
    for (int i = 0; i < taskList.size(); i++)
    {
        TTaskInfo taskInfo = taskList.at(i);
        taskInfo.strMediaPath = TDeviceDataManager::Instance().GetMediaPath();
        if (taskInfo.taskState <= TS_DOWNLOAD_ERROR)
        {
            TDownloadInfo downLoadInfo;
            FillDownloadInfo(taskInfo, downLoadInfo);            
            mTaskInfoHash.insert(taskInfo.task_id, taskInfo);
            downLoadList.append(downLoadInfo);
            emit sigAddTask(taskInfo.task_id, taskInfo.mprInfo);
        }
        else
        {
            taskInfo.taskState = TS_COPYFILE_PREPARE;
            taskInfo.task_id = GetNextValidIndex();
            mTaskInfoHash.insert(taskInfo.task_id, taskInfo);
            emit sigAddTask(taskInfo.task_id, taskInfo.mprInfo);
            if (!LoadCopyTask(taskInfo))
            {
                taskInfo.totalDownloadBytes = 0;
                taskInfo.transferedBytes = 0;
                taskInfo.progressList.clear();
                taskInfo.taskState = TS_UNKOWN;

                TDownloadInfo downLoadInfo;
                FillDownloadInfo(taskInfo, downLoadInfo);
                downLoadList.append(downLoadInfo);
            }    
            mTaskInfoHash[taskInfo.task_id] = taskInfo;
        }
    
    }
    TDownloadManager::Instance()->LoadTaskInfo(downLoadList);
}

bool TTaskManager::LoadCopyTask(TTaskInfo& taskInfo)
{
    bool bSrcFlag = true;
    bool bDestFlag = true;
    ActiveCopyTask(taskInfo, true);
    QString strDestMprPath = taskInfo.pCopyMprTask->GetTempPath();
    QString strDestLicPath = taskInfo.pCopyLicTask->GetDstPath();
    QString strSrcMprPath = taskInfo.pCopyMprTask->GetSrcPath();
    QString strSrcLicPath = taskInfo.pCopyLicTask->GetSrcPath();
    if (!QFileInfo(strDestMprPath).exists()/* || !QFileInfo(strDestLicPath).exists()*/)
    {
        qDebug() << __FUNCTION__ << "the dest file is not exit !" << strDestMprPath << strDestLicPath;
        bDestFlag= false;
    }
    
    if (!QFileInfo(strSrcMprPath).exists() || !QFileInfo(strSrcLicPath).exists())
    {
        qDebug() << __FUNCTION__ << "the src file is not exit !" << strSrcMprPath << strSrcLicPath;
        bSrcFlag = false;
    }
    
    if (bSrcFlag && !bDestFlag)
    {        
        taskInfo.pCopyMprTask->ResetData(0, taskInfo.byteCopyTotal);
        return true;
    }
    else if (!bSrcFlag && !bDestFlag)
    {
        RemoveTask(taskInfo.task_id);
        return false;
    }
    else
    {
        taskInfo.pCopyMprTask->ResetData(taskInfo.byteCopied, taskInfo.byteCopyTotal);
        return true;
    }
    
}
