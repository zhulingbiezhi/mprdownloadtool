#ifndef _TCOPYFILEMANAGER_H_
#define _TCOPYFILEMANAGER_H_

#include "TCopyFileState.h"

/*
拷贝类管理
说明：  
拷贝管理类，支持多个环境拷贝，新增任务时候，根据目的dstfilePath进行分类，详细说明见接口
任务线程：根据创建投入的参数sn进行创建，如果sn线程已经存在，则不会进行再创建
*/

class TCopyFileTask;
class TCopyFileManager : public QObject
{
    Q_OBJECT
private:
    TCopyFileManager();
    ~TCopyFileManager();

public:
     static TCopyFileManager*       Instance();

     //清除所有的任务，删除所有的任务线程，无论是否多个地方引用，慎用
     void                           ClearAllTask();

     //新增Task, 如果已经存在，则返回已存在的Task 
     TCopyFileTask*                 AddTask(const QString& srcFilePath, const QString dstFilePath, const QString& sn = QString());

     //删除Task，当只有一个引用时将删除任务，否则返回
     void                           RemoveTask(TCopyFileTask* task);
     
     //激活任务，
     void                           ActivateTask(TCopyFileTask* task, bool bConectSignal = true);

     //停止任务，运行则停止
     void                           StopTask(TCopyFileTask* task);

private slots:
    //线程状态变更，注意： 线程结束后（成功、终端、错误等）会自动销毁
     void                            OnCopyStateChanged(CopyFileState);

private:
    QThread*                        mpDefaultCopyThread;        //默认拷贝县城

    QMap<QString, QThread*>         mpDeviceSnThreadMap;        //sn对应的线程管理map

    QMap<QString, TCopyFileTask*>   mTaskUrlMap;                //线程存在记录
    QMap<TCopyFileTask*, int>       mTaskRefCount;              //线程引用计数器，初始值为1， 新增是存在则自增1

};

#endif


/*
使用记录： 文件分发工具
*/