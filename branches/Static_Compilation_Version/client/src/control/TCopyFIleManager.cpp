#include "TSTDPreCompiledHeader.h"
#include "TCopyFileManager.h"
#include "TCopyFileTask.h"

TCopyFileManager::TCopyFileManager()
{
    qRegisterMetaType<CopyFileState>();

    mpDefaultCopyThread = new QThread(this);
    mTaskRefCount.clear();
    mTaskUrlMap.clear();
    mpDeviceSnThreadMap.clear();
}

TCopyFileManager::~TCopyFileManager()
{
    qDebug() << __FUNCTION__;
}

TCopyFileManager* TCopyFileManager::Instance()
{
    static TCopyFileManager ins;
    return &ins;
}

TCopyFileTask* TCopyFileManager::AddTask( const QString& srcFilePath, const QString dstFilePath, const QString& sn)
{
    QThread* thread = 0;
    if (sn.isEmpty()) {
        thread = mpDefaultCopyThread;
    } else {
        auto it = mpDeviceSnThreadMap.find(sn);
        if (it != mpDeviceSnThreadMap.end())  {
            thread = it.value();
        } else {
            thread = new QThread(this);
            mpDeviceSnThreadMap[sn] = thread;
        }
    }

    auto it = mTaskUrlMap.find(dstFilePath);
    if (it != mTaskUrlMap.end())
    {
        mTaskRefCount[it.value()]++;
        qDebug() << __FUNCTION__ << "warning : the copy task is exit !";
        return it.value();
    }

    TCopyFileTask* task = new TCopyFileTask(srcFilePath, dstFilePath);

    mTaskRefCount.insert(task, 1);
    mTaskUrlMap[dstFilePath] = task;
    task->moveToThread(thread);

    return task;
}

void TCopyFileManager::ActivateTask(TCopyFileTask* task, bool bConectSignal)
{
    if (task == NULL) {
        return;
    }
    auto it = mTaskRefCount.find(task);
    if (it == mTaskRefCount.end()){
        return;
    }

//     if (1 < mTaskRefCount.value(task)) {
//         return;
//     }

    QThread* thread = task->thread();
    if (!thread->isRunning()) {
        thread->start();
    }
    if (bConectSignal)
    {
        connect(task, SIGNAL(CopyStateChanged(CopyFileState)), this, SLOT(OnCopyStateChanged(CopyFileState)));
    }

    QMetaObject::invokeMethod(task, "Start", Qt::QueuedConnection);
}

void TCopyFileManager::RemoveTask(TCopyFileTask* task)
{
//     if (mTaskRefCount.size() % 2 != 0)
//     {
//         return;
//     }
    if (task == NULL) {
        return;
    }
    auto it = mTaskRefCount.find(task);
    if (it == mTaskRefCount.end()){
        return;
    }

    mTaskRefCount[task]--;
    if (0 == mTaskRefCount[task])
    {
        task->Stop();
        mTaskRefCount.remove(task);
        mTaskUrlMap.remove(task->GetDstPath());
        task->deleteLater();
        task = NULL;
    }
}

void TCopyFileManager::StopTask( TCopyFileTask* task )
{
    auto it = mTaskRefCount.find(task);
    if (it == mTaskRefCount.end()){
        return;
    }

    task->Stop();
}


void TCopyFileManager::ClearAllTask()
{
    QMapIterator<TCopyFileTask*, int> it(mTaskRefCount);
    while(it.hasNext()){
        it.next();
        mTaskRefCount.insert(it.key(), 1);
        RemoveTask(it.key());
    }

    mTaskUrlMap.clear();
    mTaskRefCount.clear();

    if (mpDefaultCopyThread->isRunning())
    {
        mpDefaultCopyThread->quit();
        mpDefaultCopyThread->wait();
    }

    for (auto it = mpDeviceSnThreadMap.begin(); it != mpDeviceSnThreadMap.end(); it++)
    {
        QThread* thread = it.value();
        if (thread->isRunning())
        {
            thread->quit();
            thread->wait();
        }
    }
    mpDeviceSnThreadMap.clear();
}

void TCopyFileManager::OnCopyStateChanged( CopyFileState state)
{
    return;
    if (CSInit != state 
        && CSWaitForCopy != state 
        && CSCopying != state 
        && CSInterrupted != state)
    {
        TCopyFileTask* task = static_cast<TCopyFileTask*>(sender());

        auto it = mTaskRefCount.find(task);
        if (it == mTaskRefCount.end()){
            return;
        }

        task->Stop();
        mTaskRefCount.remove(task);
        mTaskUrlMap.remove(task->GetDstPath());
        task->deleteLater();
        task = NULL;
    }
}