#ifndef __TMPRFILEINFOITEM_H_
#define __TMPRFILEINFOITEM_H_

#include "MPRDeviceEnum.h"

enum FileLocation{
    Local,
    Network
};

enum FileType{
    FT_None = 1,
    FT_MP3  = 2,
    FT_OGG  = 4,
    FT_MPRX = 8,
    FT_MPR  = FT_MP3|FT_OGG,
    FT_ALL  = FT_MPR|FT_MPRX
};

struct TInfoItem{
    QString     strFileName;
    QString     strPrefixCode;
    QString     strVersion;
    QString     strDownloadUuid;
    QString     strBookName;
    QString     strPublisher;
    QString     strFilePath;
    quint64     qFileSize;                  //文件大小
    QString     strFileRef;
    QString     strLicenesFileName;
    //QString     strLicenesUuid;
    QString     strLicenesBrandID;
    QString     strLicenesFilePath;
    //QString     strLicenesFileRef;
};
/*
注：
下载过程中， 如果tFilePath为空，则下载数据文件，如果不为空，则不下载，tLicencePath同理

*/
struct TMPRFileInfoItem
{
    FileLocation    tFileLocation;              //位置：net 或者是local 
    FileType        tFileType;                  //文件类型： mpr 、 mprx
    QString         tBookName;                  //书名
    quint64         tFileSize;                  //文件大小
    QString         tPublisher;                 //出版社
    QString         tPrefixCode;                //前置码
    QString         tVersion;                   //mpr file languageid
    QString         tFileUuid;                 //search reply uuid , new mprfile is different with locallib
    QString         tFilePath;    
    QString         tLiceceUuid;
    QString         tBrandID;
    QString         tLicencePath;
    QString         tFileRef;
    QString         tFileCreateTime;
    TMPRFileInfoItem(){
        tFileLocation = Local;
        tFileType = FT_MPR;
        tFileSize = 0;
    };
};
Q_DECLARE_METATYPE(TMPRFileInfoItem)

enum TableType{
    E_TTNone = 0,
    E_TTSearch  ,
    E_TTState   ,
    E_TTLocal   ,
    E_TTDevice  ,
    E_TTTask
};

//同步功能用来显示
struct TTableItemInfo{
    int                 tRowID;                        //显示时的行ID
    TableType           tTableType;
    QString             tDstFileLib;                   //同步模块使用字段
    TMPRFileInfoItem    tFileInfoItem;
    MPRAudioFormat      tAudioFormat;
    TTableItemInfo(){
        tRowID = -1;
        tAudioFormat = WAV;
        tTableType = E_TTNone;
    };
};

class TXmlDisTaskFileItem
{
public:
    FileType    format;
    QString     fileRef;
};

class TXmlDisTaskItem
{
public:
    QString                     mTaskName;
    FileType                    mTaskType;
    QList<TXmlDisTaskFileItem>  mTaskFileItemList;

    TXmlDisTaskItem(){
        mTaskName.clear();
        mTaskType = FT_OGG;
        mTaskFileItemList.clear();
    };
};
#endif //__TMPRFILEINFOITEM_H_