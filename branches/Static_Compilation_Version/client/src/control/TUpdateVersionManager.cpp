﻿#include "TUpdateVersionManager.h"
#include "TMPRDeviceDesc.h"
#include "TLocalSocket.h"
#include "TAppDataDirManager.h"
#include "TAppCommon.h"
#include "TCopyFileTask.h"
#include "TCopyFileManager.h"
#include "TPopupDialog.h"
#include "TWaittingDlg.h"
#include "update.h"
#include "TAppCommon.h"
#include "TApplication.h"
#include "TWorkStateConfig.h"

#ifdef Q_OS_WIN
#include <Windows.h>
#endif


#if _MSC_VER >= 1600
#pragma execution_character_set("utf-8")
#endif
#ifdef UNICODE
#define QStringToTCHAR(x)     (const wchar_t*)x.utf16()
#else
#define QStringToTCHAR(x)     x.local8Bit().constData()
#endif

#define SPI_DEVICE_NAME "MPR-1026SPI"
#define SPI_DEVICE_TYPE "1"

TUpdateVersionManager::TUpdateVersionManager(QObject *parent) 
    : QObject(parent)
    , mpFirmwareSocket(NULL)
    , mpServiceInterFace(NULL)
    , mpSoftwareSocket(NULL)
    , mstrDeviceRootDir("")
    , mstrFirmwarePackageDir("")
    , mstrSoftwarePackageDir("")
    , mstrUpdateFileName("")
    , mstrFirmwareDevicePath("")
    , mstrFirmwareDeviceTempPath("")
    //, mstrUpdateExePath("")
    , mpWaitDlg(NULL)
    , mpUpdateProcesser(NULL)
    , mpCurrentTask(NULL)
{
    mpServiceInterFace = new TWebServiceInterface();
    connect(mpServiceInterFace, &TWebServiceInterface::fetchFirmwareLatestVerDone, this, &TUpdateVersionManager::OnFetchFirmwareLatestVerDone);
    connect(mpServiceInterFace, &TWebServiceInterface::fetchSofewareLatestVerDone, this, &TUpdateVersionManager::OnFetchSofewareLatestVerDone);
//    QDir dir;
//    dir.setCurrent(QApplication::applicationDirPath());
//#ifdef QT_NO_DEBUG
//    mstrUpdateExePath = dir.absoluteFilePath("update.exe");
//#else
//    mstrUpdateExePath = dir.absoluteFilePath("updated.exe");
//#endif

}

TUpdateVersionManager::~TUpdateVersionManager()
{
    if (mpServiceInterFace)
    {
        delete mpServiceInterFace;
        mpServiceInterFace = NULL;
    }
}

void TUpdateVersionManager::CheckClientVersion()
{
    if (mpSoftwareSocket == NULL)
    {
        mpSoftwareSocket = new TLocalSocket("SoftwareUpdateServer", this);
        connect(mpSoftwareSocket, &TLocalSocket::readUpdateServerMess, this, &TUpdateVersionManager::OnReadUpdateServerMessSlot);
    }
    if (!mpSoftwareSocket->ConnectToServer())
    {
        TFetchSoftLatestVersionRequest request;
        request.curVCode = TAppCommon::Instance().GetCurrrentVersion();
        request.terminalType = "1";
        request.softwareType = "9";
        mpServiceInterFace->fetchSofewareLatestVer(this, request);
    }

}



void TUpdateVersionManager::CheckFirmwareVersion(TMPRDeviceDesc desc)
{
    if (desc.mDeviceSN.isEmpty())
    {
        return;
    }

    if (mpFirmwareSocket == NULL)
    {
        mpFirmwareSocket = new TLocalSocket("FirmwareUpdateServer", this);
        connect(mpFirmwareSocket, &TLocalSocket::readUpdateServerMess, this, &TUpdateVersionManager::OnReadUpdateServerMessSlot);
    }

    if (!mpFirmwareSocket->ConnectToServer())
    {
        //检测版本
        TFetchFirmLatestVersionRequest request;
        request.firmwareVersion = GetSVN(desc.mFirmwareVersion);
        request.deviceModel = desc.mDeviceName;
        request.deviceType = QString::number(desc.mType, 10);
        mstrDeviceRootDir = desc.mDeviceMediaPath.left(desc.mDeviceMediaPath.lastIndexOf("\\"));
        mpServiceInterFace->fetchFirmwareLatestVer(this, request);
    }
    
}

void TUpdateVersionManager::CheckSPIFirmwareVersion(QString strFirmwareInfo, QString strRootDir)
{
    if (strFirmwareInfo.isEmpty())
    {
        return;
    }

    if (mpFirmwareSocket == NULL)
    {
        mpFirmwareSocket = new TLocalSocket("FirmwareUpdateServer", this);
        connect(mpFirmwareSocket, &TLocalSocket::readUpdateServerMess, this, &TUpdateVersionManager::OnReadUpdateServerMessSlot);
    }

    if (!mpFirmwareSocket->ConnectToServer())
    {
        //检测版本
        TFetchFirmLatestVersionRequest request;
        request.firmwareVersion = strFirmwareInfo;
        request.deviceModel = SPI_DEVICE_NAME;
        request.deviceType = SPI_DEVICE_TYPE;
        mstrDeviceRootDir = strRootDir;
        mpServiceInterFace->fetchFirmwareLatestVer(this, request);
    }

}

void TUpdateVersionManager::OnReadUpdateServerMessSlot(const QString& text)
{
    TLocalSocket* pSender = static_cast<TLocalSocket*>(sender());
    qDebug() << __FUNCTION__ << text;
    if (pSender == mpSoftwareSocket)
    {
        if (text == "software success")
        {
            CloseWaittingDlg();
            PromptInstallSoftware(Install);
        }
        else if (text == "software error")
        {
            CloseWaittingDlg();
            SoftwareDownloadError();
        }
    }
    if (pSender == mpFirmwareSocket)
    {
        if (text == "firmware success")
        {
            //复制文件到硬件中
            CopyUpdatefileToFirmware();
            CloseUpdateProcesser();
        }
        else if (text == "firmware error")
        {
            //出错处理
            CloseWaittingDlg();
            CloseUpdateProcesser();
            FirmwareDownloadError();
        }
    }
}

void TUpdateVersionManager::CopyUpdatefileToFirmware()
{
    QString strDeviceUpdateDir = mstrDeviceRootDir + "\\update";
    QDir dir(strDeviceUpdateDir);
    if (!dir.exists())
    {
        QDir::current().mkpath(strDeviceUpdateDir);
    }
    else
    {
        dir.cd(strDeviceUpdateDir);
    }

    QString sLocalFirmwareUpdatePackageName = GetLocalFirmwareUpdatePackageName();
    if (sLocalFirmwareUpdatePackageName == NULL)
    {
        CloseWaittingDlg();
        qDebug() << __FUNCTION__ << "LocalFirmwareUpdatePackageName is empty";
        return;
    }

    mstrFirmwareDevicePath = strDeviceUpdateDir + "\\" + mstrUpdateFileName;
    mstrFirmwareDeviceTempPath = strDeviceUpdateDir + "\\" + sLocalFirmwareUpdatePackageName + ".tmp";
    QString strFwareUpdateFileLocalpath = mstrFirmwarePackageDir + "/" + sLocalFirmwareUpdatePackageName;

    //复制文件
    //if (mpCopyFileThread == NULL)
    //{
    //    mpCopyFileThread = new TCopyFileThread();
    //    connect(mpCopyFileThread, &TCopyFileThread::copyStarted, this, &TUpdateVersionManager::copyUpdatePackageStarted);
    //    connect(mpCopyFileThread, &TCopyFileThread::copyEnded, this, &TUpdateVersionManager::OnCopyUpdataPackageEnded);
    //}

    //if (!mpCopyFileThread->copyFile(FwareUpdateFileLocalpath, mFirmwareDeviceTempPath))
    //{
    //    qDebug() << __FUNCTION__ << "copy file error" << FwareUpdateFileLocalpath << mFirmwareDeviceTempPath;
    //    return;
    //}

    mpCurrentTask = TCopyFileManager::Instance()->AddTask(strFwareUpdateFileLocalpath, mstrFirmwareDeviceTempPath);
    //connect(pTask, &TCopyFileTask::CopyProgressChanged, this, &TUpdateVersionManager::copyUpdatePackageStarted);
    connect(mpCurrentTask, &TCopyFileTask::CopyStateChanged, this, &TUpdateVersionManager::OnCopyUpdataPackageStateChange);
    TCopyFileManager::Instance()->ActivateTask(mpCurrentTask, false);

}

void TUpdateVersionManager::OnCopyUpdataPackageStateChange(CopyFileState state)
{
    if (CSInit != state
        && CSWaitForCopy != state
        && CSCopying != state)
    {
        CloseWaittingDlg();
        TCopyFileManager::Instance()->RemoveTask(static_cast<TCopyFileTask*>(sender()));
    }
    if (static_cast<TCopyFileTask*>(sender()) != mpCurrentTask)
    {
        return;
    }
    if (state & CSError)
    {
        QFile::remove(mstrFirmwareDeviceTempPath);
        TPopupDialog msg(TPopupDialog::Ok | TPopupDialog::Cancel, tr("Firmware upgrade failure"), tr("Upgrade package copy failed"), TAppCommon::Instance().GetMainWindow(), TPopupDialog::Error);
        msg.SetButtonText(TPopupDialog::Ok, tr("Retry"));
        msg.exec();
        if (msg.GetResult() == TPopupDialog::Ok)
        {
            NewUpdatePackageDownload(TFirmware_Update);
        }
    }
    else if (state == CSCopySucceeded)
    {
        if (QFile::exists(mstrFirmwareDevicePath))
        {
            QFile::remove(mstrFirmwareDevicePath);
        }
        if (!QFile::rename(mstrFirmwareDeviceTempPath, mstrFirmwareDevicePath))
        {
            qDebug() << __FUNCTION__ << "rename file error";
        }
        else
        {
            TPopupDialog msg(TPopupDialog::Ok, tr("Prompt"), tr("Firmware update success,please pull out the device and wait to reboot"), TAppCommon::Instance().GetMainWindow());
            msg.exec();
        }
    }


}

void TUpdateVersionManager::SoftwareDownloadError()
{
    TPopupDialog msg(TPopupDialog::Ok | TPopupDialog::Cancel, tr("Software upgrade failure"), tr("Upgrade package download failed"), TAppCommon::Instance().GetMainWindow(), TPopupDialog::Error);
    msg.SetButtonText(TPopupDialog::Ok, tr("Retry"));
    msg.exec();
    if (msg.GetResult() == TPopupDialog::Ok)
    {
        SoftwarePromptDlg(Download);
    }
}

void TUpdateVersionManager::FirmwareDownloadError()
{
    TPopupDialog msg(TPopupDialog::Ok | TPopupDialog::Cancel, tr("Firmware upgrade failure"), tr("Upgrade package download failed"), TAppCommon::Instance().GetMainWindow(), TPopupDialog::Error);
    msg.SetButtonText(TPopupDialog::Ok, tr("Retry"));
    msg.exec();
    if (msg.GetResult() == TPopupDialog::Ok)
    {
        NewUpdatePackageDownload(TFirmware_Update);
    }
}

void TUpdateVersionManager::NewUpdatePackageDownload(TUpdateType type)
{
    CreateUpdateProcess(type);
    if (type == TSoftware_Update)
    {
        mpSoftwareSocket->ConnectToServer();
    }
    else if (type == TFirmware_Update)
    {
        mpFirmwareSocket->ConnectToServer();
    }
    if (!mpWaitDlg)
    {
        mpWaitDlg = new TWaittingDlg(TAppCommon::Instance().GetMainWindow());
    }
    if (type == TFirmware_Update)
    {
        mpWaitDlg->ShowText(tr("file is downloading, do not pull out the device"));
    }
    else
    {
        mpWaitDlg->ShowText(tr("file is downloading, please do not close the program !"));
    }   
}

QString TUpdateVersionManager::GetLocalFirmwareUpdatePackageName()
{
    QSettings configIniRead(mstrFirmwarePackageDir + "/firmwareupdate.ini", QSettings::IniFormat);
    QString strName = configIniRead.value("firmware/name").toString();
    return strName;
}

QString TUpdateVersionManager::GetLocalSoftwareUpdatePackageName()
{
    QSettings configIniRead(mstrSoftwarePackageDir + "/softwareupdate.ini", QSettings::IniFormat);
    QString strName = configIniRead.value("software/name").toString();
    return strName;
}


QString TUpdateVersionManager::GetUpdateSoftwarePath()
{
    QSettings configIniRead(mstrSoftwarePackageDir + "/softwareupdate.ini", QSettings::IniFormat);
    QString strPath = configIniRead.value("software/appPath").toString();
    return strPath;
}

QString TUpdateVersionManager::GetLocalSoftwareVersion()
{
    QSettings configIniRead(mstrSoftwarePackageDir + "/softwareupdate.ini", QSettings::IniFormat);
    QString strVersion = configIniRead.value("software/vcode").toString();
    return strVersion;
}

void TUpdateVersionManager::CreateUpdateProcess(TUpdateType type)
{
    if (!mpUpdateProcesser)
    {
        mpUpdateProcesser = new TUpdateProcesser();
    }
    if (type == TFirmware_Update)
    {
        mpUpdateProcesser->sInstallPackageSaveDir = mstrFirmwarePackageDir;
        mpUpdateProcesser->sDownloadUrl = mFirmwareResData.url;
        mpUpdateProcesser->sVcode = mFirmwareResData.vCode;
        mpUpdateProcesser->sDesc = mFirmwareResData.desc;
        mpUpdateProcesser->sForceInstall = mFirmwareResData.force;
        mpUpdateProcesser->sUpdateType = "firmware";
    }
    else if (type == TSoftware_Update)
    {
        mpUpdateProcesser->sInstallPackageSaveDir = mstrSoftwarePackageDir;
        mpUpdateProcesser->sDownloadUrl = mSoftwareResData.url;
        mpUpdateProcesser->sVcode = mSoftwareResData.vCode;
        mpUpdateProcesser->sDesc = mSoftwareResData.desc;
        mpUpdateProcesser->sForceInstall = mSoftwareResData.force;
        mpUpdateProcesser->sUpdateType = "software";
    }
    mpUpdateProcesser->startUpdate();

}


void TUpdateVersionManager::OnFetchSofewareLatestVerDone(QObject *requester, const TFetchLatestVersionResponse &resData, TWebServiceInterface::ResponseCode code)
{
    Q_UNUSED(requester);
    Q_UNUSED(code);
    qDebug() << __FUNCTION__ << resData.rcode << resData.vCode << resData.url;
    if (resData.rcode.isEmpty())
    {
        qDebug() << __FUNCTION__ << resData.desc;
        return;
    }
    if (resData.rcode.toInt() == ExistNewVer)
    {
        QDir dir;
        dir.setCurrent(TAppDataDirManager::Instance().GetAppDataDir());
        if (!dir.cd("softwareUpdate"))
        {
            if (dir.mkdir("softwareUpdate"))
            {
                mstrSoftwarePackageDir = dir.currentPath() + QDir::separator() + "softwareUpdate";
                UpdateDownloadPrompt(TSoftware_Update, resData);
            }
            else
            {
                qDebug() << __FUNCTION__ << "mkdir SoftUpdatepackage fail.";
            }
        }
        else
        {
            mstrSoftwarePackageDir = dir.absolutePath();
            QString sLocalSoftwareName = GetLocalSoftwareUpdatePackageName();
            QString sLocalSoftwareVer = GetLocalSoftwareVersion();
            qDebug() << __FUNCTION__ << sLocalSoftwareVer << resData.vCode << TAppCommon::Instance().GetCurrrentVersion();
            if (sLocalSoftwareVer == resData.vCode && TAppCommon::Instance().GetCurrrentVersion() != resData.vCode)
            {
                emit existNewVersion();
                TAppCommon::Instance().SetExistVersion(QByteArray());
                PromptInstallSoftware(Install);
            }
            else
            {
                dir.remove(sLocalSoftwareName);
                dir.remove("softwareupdate.ini");
                UpdateDownloadPrompt(TSoftware_Update, resData);
            }
        }
    }
    else
    {

    }
}


void TUpdateVersionManager::OnFetchFirmwareLatestVerDone(QObject *requester, const TFetchLatestVersionResponse &resData, TWebServiceInterface::ResponseCode code)
{
    Q_UNUSED(requester);
    Q_UNUSED(code);
    qDebug() << __FUNCTION__ << resData.rcode << resData.vCode << resData.url;

    if (resData.rcode.isEmpty())
    {
        return;
    }

    if (resData.rcode.toInt() == ExistNewVer)
    {
        QDir dir;
        mstrUpdateFileName = resData.fileName;
        dir.setCurrent(TAppDataDirManager::Instance().GetAppDataDir());
        if (!dir.cd("firmwareUpdate"))
        {
            if (dir.mkdir("firmwareUpdate"))
            {
                mstrFirmwarePackageDir = dir.absolutePath() + QDir::separator() + "firmwareUpdate";
                UpdateDownloadPrompt(TFirmware_Update, resData);
            }
            else
            {
                qDebug() << __FUNCTION__ << "mkdir firmwareUpdate fail.";
            }
        }
        else
        {
            mstrFirmwarePackageDir = dir.absolutePath();
            QString sLocalFirmwareUpdatePackageName = GetLocalFirmwareUpdatePackageName();
            dir.remove(sLocalFirmwareUpdatePackageName);
            dir.remove("firmwareupdate.ini");
            UpdateDownloadPrompt(TFirmware_Update, resData);
        }
    }
}

void TUpdateVersionManager::UpdateDownloadPrompt(TUpdateType type, const TFetchLatestVersionResponse &resData)
{
    if (type == TSoftware_Update)
    {
        emit existNewVersion();
        TAppCommon::Instance().SetExistVersion(resData.sourceData);
        mSoftwareResData = resData;
        TPopupDialog msg(TPopupDialog::Yes | TPopupDialog::No, tr("Software upgrade tips"), tr("Software version exist new version, whether to download upgrade?"), TAppCommon::Instance().GetMainWindow(), TPopupDialog::Warning);
        msg.exec();
        if (msg.GetResult() == TPopupDialog::Yes)
        {
            SoftwarePromptDlg(Download);
            CloseWaittingDlg();
        }
    }
    else if (type == TFirmware_Update)
    {
        mFirmwareResData = resData;
        if (mFirmwareResData.force == "0")
        {
            TPopupDialog msg(TPopupDialog::Yes | TPopupDialog::No, tr("Firmware upgrade tips"), tr("Reading pen firmware exist new version, whether to download upgrade?"), TAppCommon::Instance().GetMainWindow(), TPopupDialog::Warning);
            msg.exec();
            if (msg.GetResult() == TPopupDialog::Yes)
            {
                NewUpdatePackageDownload(TFirmware_Update);
            }
        }
        else
        {
            TPopupDialog msg(TPopupDialog::Yes, tr("Firmware upgrade tips"), tr("current firmwarevision is incompaticatable, you need to download upgrade!"), TAppCommon::Instance().GetMainWindow(), TPopupDialog::Warning);
            msg.exec();
            if (msg.GetResult() == TPopupDialog::Yes)
            {
                NewUpdatePackageDownload(TFirmware_Update);
            }
        }

    }
}

TUpdateVersionManager& TUpdateVersionManager::Instance()
{
    static TUpdateVersionManager obj;
    return obj;
}

void TUpdateVersionManager::CloseWaittingDlg()
{
    if (mpWaitDlg && mpWaitDlg->isVisible())
    {
        mpWaitDlg->Close();
    }
}

void TUpdateVersionManager::CloseUpdateProcesser()
{
    if (mpUpdateProcesser)
    {
        delete mpUpdateProcesser;
        mpUpdateProcesser = NULL;
    }
}

void TUpdateVersionManager::PromptInstallSoftware(PromptDlgType type)
{
    TPopupDialog msg(TPopupDialog::Ok | TPopupDialog::Cancel, tr("Software upgrade tips"), tr("Update download finished, restart or not?"), TAppCommon::Instance().GetMainWindow(), TPopupDialog::Warning);
    msg.SetButtonText(TPopupDialog::Ok, tr("restart"));
    msg.exec();
    if (msg.GetResult() == TPopupDialog::Ok)
    {
        if (type == Install)
        {
            QSettings configIniRead(mstrFirmwarePackageDir + "/firmwareupdate.ini", QSettings::IniFormat);
            configIniRead.setValue("software/updateTime", QDate::currentDate().toString("yyyy-MM-dd"));
        }
        SoftwarePromptDlg(type);
    }
}
QString TUpdateVersionManager::GetSVN(QString strFirmwareVersion)
{
    QStringList  infoList = strFirmwareVersion.split(";");
    for each (QString info in infoList)
    {
        if (info.contains("SVN", Qt::CaseInsensitive))
        {
            QStringList svnList = info.split(":");
            if (svnList.size() == 2)
            {
                return svnList.at(1);
            }
        }
    }
    return "";
}

void TUpdateVersionManager::SoftwarePromptDlg(PromptDlgType type)
{
    if (type == Download)
    {
        NewUpdatePackageDownload(TSoftware_Update);
    }
    else if (type == Install)
    {
        QString installpath;
        QSettings configIniRead(mstrSoftwarePackageDir + "/softwareupdate.ini", QSettings::IniFormat);

        QString installexename = configIniRead.value("software/name").toString();
        installpath = mstrSoftwarePackageDir + "/" + installexename;

        TWorkStateConfig::Instance()->BeginGroup("Common");
        TWorkStateConfig::Instance()->SetValue("appPath", tApp->applicationFilePath());
        TWorkStateConfig::Instance()->EndGroup();

#ifdef Q_OS_WIN
        if (OSIsWinXP())
        {
            qDebug() << __FUNCTION__ << "WINDOWS XP";
            QProcess::startDetached(installpath, QStringList()); 
        }
        else
        {
            qDebug() << __FUNCTION__ << "NO WINDOWS XP";
            ShellExecute(NULL, QString("runas").toStdWString().c_str(), installpath.toStdWString().c_str(), QString().toStdWString().c_str()
                , QString().toStdWString().c_str(), SW_SHOW);
        }
#elif
         
#endif
        qApp->quit();
    }
}



bool TUpdateVersionManager::OSIsWinXP()
{
    qDebug() << __FUNCTION__;
    OSVERSIONINFO osver;
    osver.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
    GetVersionEx(&osver);
    if (osver.dwPlatformId == 2)
    {
        if (osver.dwMajorVersion == 5 && osver.dwMinorVersion == 1)
        {
            return true;
        }
    }

    return false;
}

void TUpdateVersionManager::PromptSoftwareVersion(QByteArray byte)
{
    if (mpSoftwareSocket != NULL)
    {
        delete mpSoftwareSocket;
        mpSoftwareSocket = nullptr;
        mpSoftwareSocket = new TLocalSocket("SoftwareUpdateServer", this);
        connect(mpSoftwareSocket, &TLocalSocket::readUpdateServerMess, this, &TUpdateVersionManager::OnReadUpdateServerMessSlot);
    }
    QDir dir;
    QString sLocalSoftwareName = GetLocalSoftwareUpdatePackageName();
    dir.setCurrent(TAppDataDirManager::Instance().GetAppDataDir());      
    dir.cd("softwareUpdate");
    qDebug() << __FUNCTION__ << dir.remove(sLocalSoftwareName);
    qDebug() << __FUNCTION__ << dir.remove("softwareupdate.ini");
    TFetchLatestVersionResponse resData;
    resData.fromJson(byte);
    UpdateDownloadPrompt(TSoftware_Update, resData);
}

QString TUpdateVersionManager::GerSoftwareUpdateDate()
{
    QSettings configIniRead(mstrSoftwarePackageDir + "/softwareupdate.ini", QSettings::IniFormat);
    QString strCurDate = QDate::currentDate().toString("yyyy-MM-dd");
    QString strUpdateTime = configIniRead.value("software/updateTime", strCurDate).toString();
    if (strCurDate == strUpdateTime)
    {
        configIniRead.setValue("software/updateTime", strCurDate);
    }
    return strUpdateTime;
}

