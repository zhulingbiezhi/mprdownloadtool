@echo off
::警告 请不要任意改动和编辑代码，如有配置问题，请进入到脚本_project_setting.bat中进行设定

set local=%~dp0
call "%local%../_project_setting.bat"

echo.[Delete old files] ......
    cd  /d "%ProClientDir%"
	echo.del src\version.h
    del src\version.h
	
    cd  /d "%ProPackageDir%"
	echo.del package_version.iss
    del package_version.iss
echo.

echo.[Make new files] ......
    cd /d "%ProTrunkDir%"
    SubWCRev . "version.template"  "version.h"
	echo copy version.h "%ProPackageDir%\package_version.iss" /B /Y
		copy version.h "%ProPackageDir%\package_version.iss" /B /Y
	echo copy version.h "%ProClientDir%\src\version.h" /B /Y
		copy version.h "%ProClientDir%\src\version.h" /B /Y
echo.

echo.[Del version.h] ......
    cd /d "%ProTrunkDir%"
    echo.del version.h
        del version.h
echo.

if "%Package%" EQU "true" (exit /B 1)

if "%Generate%" EQU "true" (exit  /B 1)

pause