<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>QObject</name>
    <message>
        <location filename="src/model/dumpcore/TDumpCore.cpp" line="51"/>
        <source>Program an exception occurs, exception information has been saved to the desktop, FileName:</source>
        <translation>软件异常，异常信息已保存于桌面！</translation>
    </message>
    <message>
        <location filename="src/model/dumpcore/TDumpCore.cpp" line="52"/>
        <source>Progress Error</source>
        <translation>软件异常</translation>
    </message>
    <message>
        <location filename="src/device/TDeviceEject.cpp" line="157"/>
        <source>Media in Drive %c has been ejected safely.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/device/TDeviceEject.cpp" line="163"/>
        <source>Media in Drive %c can be safely removed.
</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TBaseMessageBox</name>
    <message>
        <location filename="src/view/main/TBaseMessageBox.cpp" line="42"/>
        <source>OK</source>
        <translation>确认</translation>
    </message>
    <message>
        <location filename="src/view/main/TBaseMessageBox.cpp" line="43"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>TDeviceInfoWidget</name>
    <message>
        <location filename="src/view/main/TDeviceInfoWidget.cpp" line="20"/>
        <source>Device Type:</source>
        <translation>识读器型号：</translation>
    </message>
    <message>
        <location filename="src/view/main/TDeviceInfoWidget.cpp" line="24"/>
        <source>Device Id:</source>
        <translation>品牌ID：</translation>
    </message>
    <message>
        <location filename="src/view/main/TDeviceInfoWidget.cpp" line="29"/>
        <source>Capacity:</source>
        <translation>容量：</translation>
    </message>
    <message>
        <location filename="src/view/main/TDeviceInfoWidget.cpp" line="39"/>
        <location filename="src/view/main/TDeviceInfoWidget.cpp" line="161"/>
        <source>Used:%1,Total:%2</source>
        <translation>已用%1，总共%2</translation>
    </message>
    <message>
        <location filename="src/view/main/TDeviceInfoWidget.cpp" line="88"/>
        <source>Used:%1%2,Total:%3%4</source>
        <translation>已用%1%2，总共%3%4</translation>
    </message>
</context>
<context>
    <name>TDeviceTabWidget</name>
    <message>
        <location filename="src/view/main/TDeviceTabWidget.cpp" line="73"/>
        <source>  </source>
        <translation> </translation>
    </message>
    <message>
        <location filename="src/view/main/TDeviceTabWidget.cpp" line="73"/>
        <source>Book Name</source>
        <translation>书名</translation>
    </message>
    <message>
        <location filename="src/view/main/TDeviceTabWidget.cpp" line="73"/>
        <source>Publisher Name</source>
        <translation>出版社</translation>
    </message>
    <message>
        <location filename="src/view/main/TDeviceTabWidget.cpp" line="73"/>
        <source>Format</source>
        <translation>文件类型</translation>
    </message>
    <message>
        <location filename="src/view/main/TDeviceTabWidget.cpp" line="73"/>
        <source>Download Time</source>
        <translation>下载时间</translation>
    </message>
    <message>
        <location filename="src/view/main/TDeviceTabWidget.cpp" line="73"/>
        <source>File Size</source>
        <translation>大小</translation>
    </message>
    <message>
        <location filename="src/view/main/TDeviceTabWidget.cpp" line="73"/>
        <source>Operate</source>
        <translation>操作</translation>
    </message>
    <message>
        <location filename="src/view/main/TDeviceTabWidget.cpp" line="187"/>
        <source>delete mpr files</source>
        <translation>删除文件</translation>
    </message>
    <message>
        <location filename="src/view/main/TDeviceTabWidget.cpp" line="187"/>
        <source>Are you sure to delete this MPF file?</source>
        <translation>您是否确认删除MPR文件？</translation>
    </message>
</context>
<context>
    <name>THeadWidget</name>
    <message>
        <location filename="src/view/main/THeadWidget.cpp" line="24"/>
        <source>Setting</source>
        <translation>设置</translation>
    </message>
    <message>
        <source>Version Info</source>
        <translation type="vanished">版本</translation>
    </message>
    <message>
        <location filename="src/view/main/THeadWidget.cpp" line="20"/>
        <source>MPR DownloadTool</source>
        <translation>MPR文件下载小工具</translation>
    </message>
    <message>
        <location filename="src/view/main/THeadWidget.cpp" line="29"/>
        <source>Version</source>
        <translation>版本</translation>
    </message>
    <message>
        <location filename="src/view/main/THeadWidget.cpp" line="34"/>
        <source>minimumSize</source>
        <translation>最小化</translation>
    </message>
    <message>
        <location filename="src/view/main/THeadWidget.cpp" line="38"/>
        <source>close</source>
        <translation>关闭</translation>
    </message>
</context>
<context>
    <name>TLeftTabWidget</name>
    <message>
        <location filename="src/view/main/TLeftTabWidget.cpp" line="20"/>
        <source>My Device</source>
        <translation>我的设备</translation>
    </message>
    <message>
        <location filename="src/view/main/TLeftTabWidget.cpp" line="23"/>
        <source>My Task</source>
        <translation>任务列表</translation>
    </message>
</context>
<context>
    <name>TMainWindow</name>
    <message>
        <source>Software upgrade tips</source>
        <translation type="vanished">软件升级提示</translation>
    </message>
    <message>
        <source>Current version : %1</source>
        <translation type="vanished">当前版本：%1</translation>
    </message>
    <message>
        <location filename="src/view/main/TMainWindow.cpp" line="332"/>
        <source>Software version: V</source>
        <translation>软件版本： V</translation>
    </message>
    <message>
        <source>
Update date: 
</source>
        <translation type="vanished">
更新日期：
</translation>
    </message>
    <message>
        <location filename="src/view/main/TMainWindow.cpp" line="337"/>
        <source>Copyright: shen zhen tian lang shi dai </source>
        <oldsource>Copyright: shen zhen tian lang shi dai 
</oldsource>
        <translation>版本信息：深圳市天朗时代科技有限公司 版权所有</translation>
    </message>
    <message>
        <source>
Update date: </source>
        <translation type="vanished">
更新日期：</translation>
    </message>
    <message>
        <source>
Copyright: shen zhen tian lang shi dai 
</source>
        <translation type="vanished">
版权信息：深圳市天朗时代科技有限公司 版权所有
</translation>
    </message>
    <message>
        <location filename="src/view/main/TMainWindow.cpp" line="339"/>
        <source>current version is the latest version</source>
        <translation>当前版本是最新版</translation>
    </message>
    <message>
        <location filename="src/view/main/TMainWindow.cpp" line="343"/>
        <source>Version Information</source>
        <translation>版本信息</translation>
    </message>
</context>
<context>
    <name>TNetMPRSearcher</name>
    <message>
        <location filename="src/model/network/TNetMPRSearcher.cpp" line="125"/>
        <source>no file is exit !</source>
        <translation>无对应文件</translation>
    </message>
    <message>
        <location filename="src/model/network/TNetMPRSearcher.cpp" line="125"/>
        <source>Tips</source>
        <translation>提示</translation>
    </message>
    <message>
        <location filename="src/model/network/TNetMPRSearcher.cpp" line="193"/>
        <location filename="src/model/network/TNetMPRSearcher.cpp" line="200"/>
        <source>download data error!</source>
        <translation>下载数据错误！</translation>
    </message>
</context>
<context>
    <name>TPageNaviBar</name>
    <message>
        <location filename="src/view/main/TPageNaviBar.cpp" line="28"/>
        <source>Previous Page</source>
        <translation>前一页</translation>
    </message>
    <message>
        <location filename="src/view/main/TPageNaviBar.cpp" line="74"/>
        <source>Next Page</source>
        <translation>下一页</translation>
    </message>
    <message>
        <location filename="src/view/main/TPageNaviBar.cpp" line="94"/>
        <source>Page</source>
        <translation>页</translation>
    </message>
    <message>
        <location filename="src/view/main/TPageNaviBar.cpp" line="334"/>
        <source>Total %1 pages / Total %2 records, To</source>
        <translation>共%1页/共%2条记录,跳转</translation>
    </message>
</context>
<context>
    <name>TPopupDialog</name>
    <message>
        <location filename="src/view/main/TPopupDialog.cpp" line="43"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="src/view/main/TPopupDialog.cpp" line="97"/>
        <source>Ok</source>
        <translation>确认</translation>
    </message>
    <message>
        <location filename="src/view/main/TPopupDialog.cpp" line="104"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="src/view/main/TPopupDialog.cpp" line="110"/>
        <source>Yes</source>
        <translation>是</translation>
    </message>
    <message>
        <location filename="src/view/main/TPopupDialog.cpp" line="117"/>
        <source>No</source>
        <translation>否</translation>
    </message>
</context>
<context>
    <name>TSystemSettingDlg</name>
    <message>
        <location filename="src/view/main/TSystemSettingDlg.cpp" line="53"/>
        <source>select save directory</source>
        <oldsource>select import directory</oldsource>
        <translation>缓存路径</translation>
    </message>
    <message>
        <location filename="src/view/main/TSystemSettingDlg.cpp" line="72"/>
        <source>setting</source>
        <translation>设置</translation>
    </message>
    <message>
        <location filename="src/view/main/TSystemSettingDlg.cpp" line="80"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="src/view/main/TSystemSettingDlg.cpp" line="99"/>
        <source>browse</source>
        <translation>浏览</translation>
    </message>
    <message>
        <location filename="src/view/main/TSystemSettingDlg.cpp" line="100"/>
        <source>save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="src/view/main/TSystemSettingDlg.cpp" line="101"/>
        <source>cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="src/view/main/TSystemSettingDlg.cpp" line="118"/>
        <source>save path:</source>
        <translation>本地存储路径：</translation>
    </message>
    <message>
        <location filename="src/view/main/TSystemSettingDlg.cpp" line="128"/>
        <source>It does effective when next start software!</source>
        <translation>软件重启后生效！</translation>
    </message>
</context>
<context>
    <name>TTaskTabWidget</name>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="31"/>
        <source>Warm prompt: Using USB data lin to connet device with computer first, and make computer staying connected to the Internet.
 Then your can download mpr files by touch mpr book using touch and talk pen!</source>
        <translation>温馨提示：请通过USB数据线将识读器连接到电脑，保持电脑接入互联网，使用识读器点击图书中发声的区域开始声音文件下载</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="36"/>
        <source>There is no download task, please touch reading the MPR book</source>
        <translation>暂无下载任务，请点读MPR出版物下载</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="77"/>
        <source>  </source>
        <translation> </translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="77"/>
        <source>Book Name</source>
        <translation>书名</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="77"/>
        <source>Publisher Name</source>
        <translation>出版社</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="77"/>
        <source>Format</source>
        <translation>格式</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="77"/>
        <source>Status</source>
        <translation>状态</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="77"/>
        <source>Operate</source>
        <translation>操作</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="249"/>
        <source>start</source>
        <translation>开始</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="250"/>
        <source>pause</source>
        <translation>暂停</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="251"/>
        <source>delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="344"/>
        <source>Prepare download</source>
        <translation>准备下载</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="349"/>
        <source>Downloading</source>
        <translation>下载中</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="354"/>
        <source>Download pause</source>
        <translation>下载暂停中</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="359"/>
        <source>Download finished</source>
        <translation>下载完成</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="367"/>
        <source>Download failed</source>
        <oldsource>Download Error</oldsource>
        <translation type="unfinished">下载失败</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="373"/>
        <source>Prepare copy</source>
        <translation>准备拷贝</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="386"/>
        <source>Copy pause</source>
        <translation>拷贝暂停中</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="404"/>
        <source>Copy failed</source>
        <oldsource>Copy error</oldsource>
        <translation type="unfinished">拷贝失败</translation>
    </message>
    <message>
        <source>Prepare</source>
        <translation type="vanished">准备下载</translation>
    </message>
    <message>
        <source>Download</source>
        <translation type="vanished">下载中</translation>
    </message>
    <message>
        <source>Pause</source>
        <translation type="vanished">暂停中</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="381"/>
        <source>Copying</source>
        <oldsource>Copyfile</oldsource>
        <translation>拷贝中</translation>
    </message>
    <message>
        <source>Finished</source>
        <translation type="vanished">已完成</translation>
    </message>
    <message>
        <source>Not Enough Space</source>
        <translation type="vanished">空间不足</translation>
    </message>
    <message>
        <location filename="src/view/main/TTaskTabWidget.cpp" line="392"/>
        <source>Copy finished</source>
        <oldsource>Copy File Failed</oldsource>
        <translation>拷贝完成</translation>
    </message>
    <message>
        <source>Download File Failed</source>
        <translation type="vanished">下载失败</translation>
    </message>
    <message>
        <source>file download error</source>
        <translation type="vanished">下载失败</translation>
    </message>
</context>
<context>
    <name>TUpdateVersionManager</name>
    <message>
        <location filename="src/control/TUpdateVersionManager.cpp" line="259"/>
        <source>Prompt</source>
        <translation>升级</translation>
    </message>
    <message>
        <location filename="src/control/TUpdateVersionManager.cpp" line="259"/>
        <source>Firmware update success,please pull out the device and wait to reboot</source>
        <translation>硬件升级成功，请拔出设备，等待设备重启</translation>
    </message>
    <message>
        <location filename="src/control/TUpdateVersionManager.cpp" line="239"/>
        <location filename="src/control/TUpdateVersionManager.cpp" line="280"/>
        <source>Firmware upgrade failure</source>
        <translation>硬件升级失败</translation>
    </message>
    <message>
        <location filename="src/control/TUpdateVersionManager.cpp" line="269"/>
        <location filename="src/control/TUpdateVersionManager.cpp" line="280"/>
        <source>Upgrade package download failed</source>
        <translation>硬件升级包下载失败</translation>
    </message>
    <message>
        <location filename="src/control/TUpdateVersionManager.cpp" line="240"/>
        <location filename="src/control/TUpdateVersionManager.cpp" line="270"/>
        <location filename="src/control/TUpdateVersionManager.cpp" line="281"/>
        <source>Retry</source>
        <translation>重试</translation>
    </message>
    <message>
        <location filename="src/control/TUpdateVersionManager.cpp" line="239"/>
        <source>Upgrade package copy failed</source>
        <translation>硬件升级包拷贝失败</translation>
    </message>
    <message>
        <location filename="src/control/TUpdateVersionManager.cpp" line="269"/>
        <source>Software upgrade failure</source>
        <translation>软件升级失败</translation>
    </message>
    <message>
        <location filename="src/control/TUpdateVersionManager.cpp" line="306"/>
        <source>file is downloading, do not pull out the device</source>
        <translation>正在下载新版固件，请勿断开连接</translation>
    </message>
    <message>
        <location filename="src/control/TUpdateVersionManager.cpp" line="310"/>
        <source>file is downloading, please do not close the program !</source>
        <translation>正在下载文件，请勿关闭程序 ！</translation>
    </message>
    <message>
        <location filename="src/control/TUpdateVersionManager.cpp" line="471"/>
        <location filename="src/control/TUpdateVersionManager.cpp" line="529"/>
        <source>Software upgrade tips</source>
        <translation>软件升级提示</translation>
    </message>
    <message>
        <location filename="src/control/TUpdateVersionManager.cpp" line="471"/>
        <source>Software version exist new version, whether to download upgrade?</source>
        <translation>软件存在新版本，是否需要下载更新 ？</translation>
    </message>
    <message>
        <location filename="src/control/TUpdateVersionManager.cpp" line="484"/>
        <location filename="src/control/TUpdateVersionManager.cpp" line="493"/>
        <source>Firmware upgrade tips</source>
        <translation>硬件升级</translation>
    </message>
    <message>
        <location filename="src/control/TUpdateVersionManager.cpp" line="484"/>
        <source>Reading pen firmware exist new version, whether to download upgrade?</source>
        <translation>设备存在硬件升级版本，是否升级？</translation>
    </message>
    <message>
        <location filename="src/control/TUpdateVersionManager.cpp" line="493"/>
        <source>current firmwarevision is incompaticatable, you need to download upgrade!</source>
        <translation>当前硬件版本不兼容，请升级！</translation>
    </message>
    <message>
        <location filename="src/control/TUpdateVersionManager.cpp" line="529"/>
        <source>Update download finished, restart or not?</source>
        <translation>更新文件下载完成，是否进行重启</translation>
    </message>
    <message>
        <location filename="src/control/TUpdateVersionManager.cpp" line="530"/>
        <source>restart</source>
        <translation>重启</translation>
    </message>
    <message>
        <source>Update download finished, install or not?</source>
        <translation type="vanished">更新文件下载完成，是否继续安装 ？</translation>
    </message>
    <message>
        <source>Install</source>
        <translation type="vanished">安装</translation>
    </message>
</context>
<context>
    <name>TWaittingDlg</name>
    <message>
        <location filename="src/view/main/TWaittingDlg.cpp" line="115"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="src/view/main/TWaittingDlg.cpp" line="123"/>
        <source>Info</source>
        <translation>提示</translation>
    </message>
</context>
<context>
    <name>TWebServiceInterface</name>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="670"/>
        <source>Success</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="673"/>
        <source>Parameter error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="676"/>
        <source>Equipment loss</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="679"/>
        <source>Not identify user</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="682"/>
        <source>Failed to get ticket</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="685"/>
        <source>Please connect equipment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="688"/>
        <source>Resource was not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="691"/>
        <source>Account inactive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="694"/>
        <source>Account exception</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="697"/>
        <source>Account does not exist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="700"/>
        <source>Account or password error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="703"/>
        <source>Equipment account binding relationship does not exist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="706"/>
        <source>Equipment abnormal binding</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="709"/>
        <source>Equipment has bound</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="712"/>
        <source>Equipment wasn&apos;t register</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="715"/>
        <source>Inconsistent binding account login account and equipment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="718"/>
        <source>The device did not buy the goods</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="721"/>
        <source>No purchase record fee content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="724"/>
        <source>Equipment validation failure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="727"/>
        <source>A system exception</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="1245"/>
        <source>Failure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/model/network/TWebServiceInterface.cpp" line="1245"/>
        <source>The search timeout, please try again</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>serverSocket</name>
    <message>
        <location filename="src/update/serversocket.cpp" line="48"/>
        <source>software success</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/update/serversocket.cpp" line="49"/>
        <source>software error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/update/serversocket.cpp" line="50"/>
        <source>firmware success</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/update/serversocket.cpp" line="51"/>
        <source>firmware error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/update/serversocket.cpp" line="52"/>
        <source>connect success</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
