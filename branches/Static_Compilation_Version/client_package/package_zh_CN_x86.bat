﻿@echo. off

call ..\_project_setting.bat

set PackageDir=%cd%
set ProgramName=%ProName%
set ExeFileName=%ProName%.exe
set PackageOutDir=@output

echo.[Package for win32 release (ZH_CN)] ......
cd /d %PackageDir%
echo.Package dir: %PackageDir%
echo.

echo.[Check File] ......
IF NOT EXIST "%ReleaseDir_X86%\%ExeFileName%" (
	echo.Error: "%ReleaseDir_X86%\%ExeFileName%" is not exist.
	exit 1
)

echo.[Clear old data] ......
del /f /a /q %PackageOutDir%\*.*
rmdir /s/q "%PackageSrcDir%"

echo.

echo.[Copy main_files] ......
copy /y "%ReleaseDir_X86%\%ExeFileName%" "%PackageOutDir%"
copy /y "%ReleaseDir_X86%\%ProgramName%.pdb" "%PackageOutDir%"
echo.


echo.
echo. 

echo. #############################################################################
echo.
echo.     生成中文版安装包成功!!!!!!
echo.
echo.     安装包路径：package\%PackageOutDir%\%InstallPkgName%
echo.
echo. #############################################################################
echo.
